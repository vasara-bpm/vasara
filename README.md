Vasara BPM Technology Preview
=============================

Vasara BPM is an open source technology mash up with some middleware and custom
front end for managing and executing BPMN driven business processes and case management.


Sneak preview
-------------

```
vagrant up --provider=[libvirt|virtualbox]
```


Exposable services
-------------------

Successfully launched vagrant box will result with the following services being available to be exposed:

* React-Admin at http://localhost:3000/ (with credentials camunda-admin:admin)
* Keycloack at  http://localhost:8000/ (with credentials admin:admin)
* Camunda at  http://localhost:8080/ (with credentials camunda-admin:admin)
* Mailhog at http://localhost:8025/
* Hasura at http://localhost:8090/ (with secret admin)


Troubleshooting
---------------

Please, contact us by [filing an issue](https://gitlab.com/vasara-bpm/vasara/-/issues).
