NIX_BUILD ?= nix build --no-link

BOX_NAME ?= jyukopla/vasara-bpm
BOX_VERSION ?= 0.9.8

.PHONY: format
format:
	find . -name "*.nix" -print0|xargs -0 nix fmt

all:
	$(NIX_BUILD)

.PHONY: publish
publish: publish-virtualbox publish-libvirt

.PHONY: publish-libvirt
publish-libvirt:
	vagrant cloud publish -rf $(BOX_NAME) $(BOX_VERSION) libvirt $$($(NIX_BUILD) --json .#vagrantLibvirt|jq -r .[0].outputs.out)

.PHONY: publish-virtualbox
publish-virtualbox:
	vagrant cloud publish -rf $(BOX_NAME) $(BOX_VERSION) virtualbox $$($(NIX_BUILD) --json .#vagrantVirtualbox|jq -r .[0].outputs.out)
