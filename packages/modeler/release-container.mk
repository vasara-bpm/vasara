.PHONY: image
image: $(SOURCES)
	$$(nix build $(NIX_OPTIONS) --json ../vasara-app#modeler-image|jq -r .[0].outputs.out)|podman load
