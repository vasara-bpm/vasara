{
  description = "Vasara GitLab Runner";

  nixConfig = {
    extra-trusted-public-keys =
      "vasara-bpm.cachix.org-1:T18iQZQvYDy/6VdGmttnkkq7rYi3JP0S1RjjdnXNu/4=";
    extra-substituters = "https://vasara-bpm.cachix.org";
  };

  inputs = {

    # Generic
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/release-23.05";
    nixpkgs-runner.url =
      "github:NixOS/nixpkgs/72c1d14d767dabc402f50bb38f283ee14bc5f449";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/master";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    # Vasara
    vasara-bpm = {
      url = "gitlab:vasara-bpm/pkgs";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };

  };

  outputs = { self, nixpkgs, nixpkgs-unstable, flake-utils, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ ];
        };
        pkgs-unstable = import nixpkgs-unstable { inherit system; };
        pkgs-runner = import inputs.nixpkgs-runner { inherit system; };

        python = pkgs.python38;
        pythonPackages = pkgs.python38Packages;

        gitlab = ps:
          with ps;
          buildPythonPackage rec {
            pname = "python-gitlab";
            version = "4.1.1";
            src = fetchPypi {
              inherit pname version;
              sha256 = "sha256-5JEgg9XUCnm1eHWgMwg+LIYONLzEZE72TKN3p1XFsMk=";
            };
            format = "pyproject";
            nativeBuildInputs =
              [ pythonPackages.setuptools pythonPackages.requests_toolbelt ];
            propagatedBuildInputs =
              [ pythonPackages.requests pythonPackages.requests_toolbelt ];
          };

        jsondiff = ps:
          with ps;
          buildPythonPackage rec {
            pname = "jsondiff";
            version = "1.3.0";
            src = fetchPypi {
              inherit pname version;
              sha256 = "0wcxb4z1kw3cxs25hyijbbfhspbwm2262di9n0nv0cd0113vy8ji";
            };
          };

        xmldiff = ps:
          with ps;
          buildPythonPackage rec {
            pname = "xmldiff";
            version = "2.4";
            src = fetchPypi {
              inherit pname version;
              sha256 = "sha256-Bb6iDOHyyWeGg7zODDupmB+H2StwnRkOAYvL8Efsz2M=";
            };
            propagatedBuildInputs = [
              pythonPackages.lxml
              pythonPackages.setuptools
              pythonPackages.six
            ];
          };

        keycloak = ps:
          with ps;
          buildPythonPackage rec {
            pname = "python-keycloak";
            version = "0.20.0";
            src = pythonPackages.fetchPypi {
              inherit pname version;
              sha256 = "10z5asky68gg6p5x9n7xfixxnvq273m4jab2g7b0vzncspkqa7xp";
            };
            doCheck = false;
            propagatedBuildInputs =
              [ pythonPackages.python-jose pythonPackages.requests ];
          };

        pythonWithPackages = python.withPackages (ps:
          with ps; [
            requests
            click
            ldap
            (jsondiff ps)
            (keycloak ps)
            (xmldiff ps)
            (gitlab ps)
          ]);

        call-name = "vasara-gitlab-runner";

      in {

        # Default executable
        apps.default = {
          type = "app";
          program = "${self.packages.${system}.default}/bin/gitlab-runner";
        };

        # Default package
        packages.default = pkgs-runner.gitlab-runner;

        # Container image
        packages.image = pkgs.dockerTools.streamLayeredImage {
          name = call-name;
          tag = "latest";
          created = "now";
          contents = [
            (pkgs.buildEnv {
              name = "image-contents";
              paths = [
                pkgs.bashInteractive
                pkgs.coreutils
                pkgs.csvkit
                pkgs.curl
                pkgs.dockerTools.fakeNss
                pkgs.dockerTools.usrBinEnv
                pkgs.findutils
                pkgs.diffutils
                pkgs.gawk
                pkgs.git
                pkgs-runner.gitlab-runner
                pkgs.gnugrep
                pkgs.gnumake
                pkgs.gnused
                pkgs.hostname
                pkgs.jq
                pkgs.openssh
                pkgs.postgresql_15
                pkgs.tini
                pythonWithPackages
                pkgs.nushell
                inputs.vasara-bpm.packages.x86_64-linux.hasura-cli-full
              ];
              pathsToLink = [ "/etc" "/sbin" "/bin" ];
            })
          ];
          extraCommands = ''
            mkdir -p usr/bin && ln -s /sbin/env usr/bin/env
            mkdir -p tmp && chmod a+rxwt tmp
          '';
          config = {
            Entrypoint = [
              "${pkgs.tini}/bin/tini"
              "--"
              "${pkgs-runner.gitlab-runner}/bin/gitlab-runner"
            ];
            Env = [ "TMPDIR=/tmp" "HOME=/tmp" ];
            Labels = { };
            User = "nobody";
          };
        };

        # Nomad artifact
        packages.artifact = let
          env = pkgs.buildEnv {
            name = "${call-name}-env";
            paths = [
              pkgs.bashInteractive
              pkgs.coreutils
              pkgs.csvkit
              pkgs.curl
              pkgs.findutils
              pkgs.diffutils
              pkgs.gawk
              pkgs.git
              pkgs.openssh
              pkgs.gnused
              pkgs-runner.gitlab-runner
              pkgs.gnugrep
              pkgs.gnumake
              pkgs.hostname
              pkgs.jq
              pkgs.postgresql_15
              pkgs.tini
              pythonWithPackages
              pkgs.nushell
              inputs.vasara-bpm.packages.${system}.hasura-cli-full
            ];
          };
          closure = (pkgs.writeReferencesToFile env);
        in pkgs.runCommand call-name { buildInputs = [ pkgs.makeWrapper ]; } ''
          # aliases
          mkdir -p usr/local/bin
          for filename in ${env}/bin/??*; do
            cat > usr/local/bin/$(basename $filename) << EOF
          #!/usr/local/bin/sh
          set -e
          exec $(basename $filename) "\$@"
          EOF
          done
          rm -f usr/local/bin/sh
          chmod a+x usr/local/bin/*

          # shell
          makeWrapper ${pkgs.bashInteractive}/bin/sh usr/local/bin/sh \
            --set SHELL /usr/local/bin/sh \
            --prefix PATH : ${pkgs.coreutils}/bin \
            --prefix PATH : ${pkgs.csvkit}/bin \
            --prefix PATH : ${pkgs.curl}/bin \
            --prefix PATH : ${pkgs.findutils}/bin \
            --prefix PATH : ${pkgs.diffutils}/bin \
            --prefix PATH : ${pkgs.gawk}/bin \
            --prefix PATH : ${pkgs-runner.gitlab-runner}/bin \
            --prefix PATH : ${pkgs.git}/bin \
            --prefix PATH : ${pkgs.openssh}/bin \
            --prefix PATH : ${pkgs.gnused}/bin \
            --prefix PATH : ${pkgs.gnumake}/bin \
            --prefix PATH : ${pkgs.gnugrep}/bin \
            --prefix PATH : ${
              inputs.vasara-bpm.packages.${system}.hasura-cli-full
            }/bin \
            --prefix PATH : ${pkgs.hostname}/bin \
            --prefix PATH : ${pkgs.jq}/bin \
            --prefix PATH : ${pkgs.postgresql_15}/bin \
            --prefix PATH : ${pythonWithPackages}/bin \
            --prefix PATH : ${pkgs.nushell}/bin \
            --prefix PATH : ${pkgs.tini}/bin \
            --set HASURA_CLI_DOT_HASURA ${
              inputs.vasara-bpm.packages.${system}.hasura-cli-dot-hasura
            }

          # artifact
          tar cvzhP \
            --hard-dereference \
            --exclude="${env}" \
            --exclude="*-python3.8-*" \
            --exclude="*ncurses*/ncurses*/ncurses*" \
            --files-from=${closure} ${
              inputs.vasara-bpm.packages.${system}.hasura-cli-dot-hasura
            } \
            usr > $out || true
        '';

        # Development shell
        devShells.default = pkgs.mkShell {
          buildInputs = [
            pkgs.bashInteractive
            pkgs.coreutils
            #             pkgs.csvkit
            pkgs.curl
            pkgs.findutils
            pkgs.diffutils
            pkgs.gawk
            pkgs.git
            pkgs.openssh
            pkgs.gnused
            pkgs-runner.gitlab-runner
            pkgs.gnugrep
            pkgs.gnumake
            pkgs.hostname
            pkgs.jq
            pkgs.postgresql_15
            pkgs.tini
            pythonWithPackages
            pkgs.nushell
            inputs.vasara-bpm.packages.${system}.hasura-cli-full
          ];
        };

        devShells.with-podman =
          inputs.vasara-bpm.devShells.${system}.podman.overrideAttrs (old: {
            buildInputs = old.buildInputs
              ++ self.devShells.${system}.default.buildInputs ++ [ ];
          });

        formatter = pkgs.nixfmt;
      });
}
