"""FastAPI router."""
from aiohttp import ClientTimeout
from app.config import settings
from app.utils import verify_response_status
from concurrent.futures import ThreadPoolExecutor
from fastapi.exceptions import HTTPException
from fastapi.param_functions import Depends
from fastapi.responses import RedirectResponse  # type: ignore
from fastapi.routing import APIRouter
from fastapi.security.http import HTTPAuthorizationCredentials
from fastapi.security.http import HTTPBearer
from functools import partial
from oauthlib.oauth2 import WebApplicationClient
from oauthlib.oauth2.rfc6749.errors import OAuth2Error
from pydantic import BaseModel
from requests_oauthlib import OAuth2Session
from starlette import status
from starlette.requests import Request
from starlette.responses import Response
from typing import Any
from typing import Callable
from typing import Dict
from typing import List
from typing import Optional
import aiocache  # type: ignore
import aiohttp
import asyncio
import base64
import json
import jwt
import logging
import os
import time


router = APIRouter()
logger = logging.getLogger(__name__)

DEFAULT_WORKER = ThreadPoolExecutor()

COOKIE_PATH = "/"
COOKIE_NAME_ACCESS_TOKEN = "access_token"
COOKIE_NAME_ACCESS_TOKEN_EXPIRES_AT = "access_token_expires_at"
COOKIE_NAME_REFRESH_TOKEN = "refresh_token"
COOKIE_NAME_REFRESH_TOKEN_EXPIRES_AT = "refresh_token_expires_at"
COOKIE_NAME_ID_TOKEN = "id_token"

COOKIE_NAME_STATE = "state"
COOKIE_NAME_VERIFIER = "verifier"
COOKIE_NAME_NONCE = "nonce"


# Note: testing oauthlib against http development IdP env OAUTHLIB_INSECURE_TRANSPORT


async def sync(func: Callable[[Any], Any], *args: Any, **kwargs: Any) -> Any:
    """Run sync function in thread pool."""
    return await asyncio.get_event_loop().run_in_executor(
        DEFAULT_WORKER, partial(func, *args, **kwargs)
    )


class OIDCProviderConfiguration(BaseModel):
    """OIDC provider configuration."""

    issuer: str
    authorization_endpoint: str
    token_endpoint: str
    userinfo_endpoint: str
    jwks_uri: str
    response_types_supported: List[str]
    subject_types_supported: List[str]
    id_token_signing_alg_values_supported: List[str]
    scopes_supported: List[str]
    token_endpoint_auth_methods_supported: List[str]
    claims_supported: List[str]
    code_challenge_methods_supported: List[str]


@aiocache.cached()  # type: ignore
async def get_provider_configuration() -> OIDCProviderConfiguration:
    """Get OIDC provider configuration."""
    headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
    }
    async with aiohttp.ClientSession(
        headers=headers,
        trust_env=True,
        timeout=ClientTimeout(total=settings.CAMUNDA_TIMEOUT),
    ) as http:
        url = f"{settings.OIDC_PROVIDER_URI}/.well-known/openid-configuration"
        get = await http.get(url)
        await verify_response_status(get, status=(200,), error_status=404)
        return OIDCProviderConfiguration(**await get.json())


@aiocache.cached(ttl=60 * 60 * 24)  # type: ignore
async def get_keys() -> Dict[str, jwt.algorithms.RSAAlgorithm]:
    """Get JWSK keys."""
    provider = await get_provider_configuration()
    headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
    }
    async with aiohttp.ClientSession(
        headers=headers,
        trust_env=True,
        timeout=ClientTimeout(total=settings.CAMUNDA_TIMEOUT),
    ) as http:
        url = provider.jwks_uri
        get = await http.get(url)
        await verify_response_status(get, status=(200,), error_status=404)
        keys = await get.json()
        return {
            key["kid"]: jwt.algorithms.RSAAlgorithm.from_jwk(  # type: ignore
                json.dumps(key)
            )
            for key in keys["keys"]
            if key["kty"] == "RSA"
        }


async def challenge(request: Request) -> None:
    """OIDC challenge."""
    provider = await get_provider_configuration()
    oidc_client = WebApplicationClient(
        client_id=settings.OIDC_CLIENT_ID,
        client_secret=settings.OIDC_CLIENT_SECRET,
    )
    oidc_session = OAuth2Session(
        client=oidc_client,
        scope=settings.OIDC_SCOPE,
        redirect_uri=settings.OIDC_REDIRECT_URI,
    )

    code_verifier = oidc_client.create_code_verifier(length=96)
    assert set(["S256", "plain"]).intersection(
        set(provider.code_challenge_methods_supported)
    )
    if "S256" in provider.code_challenge_methods_supported:
        code_challenge_method = "S256"
    elif "plain" in provider.code_challenge_methods_supported:
        code_challenge_method = "plain"
    code_challenge = oidc_client.create_code_challenge(
        code_verifier=code_verifier, code_challenge_method=code_challenge_method
    )

    state = request.url
    nonce = base64.urlsafe_b64encode(os.urandom(32)).rstrip(b"=").decode()
    authorization_url = ""  # type: str
    authorization_url, _ = oidc_session.authorization_url(  # type: ignore
        provider.authorization_endpoint,
        code_challenge=code_challenge,
        code_challenge_method=code_challenge_method,
        nonce=nonce,
        state=state,
    )

    secure = request.headers.get("X-Forwarded-Proto") == "https"
    response = Response()
    response.set_cookie(
        key=COOKIE_NAME_ID_TOKEN,
        value="",
        path=COOKIE_PATH,
        samesite="Strict",
        httponly=True,
        secure=secure,
        max_age=0,
    )
    response.set_cookie(
        key=COOKIE_NAME_ACCESS_TOKEN,
        value="",
        path=COOKIE_PATH,
        samesite="Strict",
        httponly=True,
        secure=secure,
        max_age=0,
    )
    response.set_cookie(
        key=COOKIE_NAME_ACCESS_TOKEN_EXPIRES_AT,
        value="",
        path=COOKIE_PATH,
        samesite="Strict",
        httponly=True,
        secure=secure,
        max_age=0,
    )
    response.set_cookie(
        key=COOKIE_NAME_REFRESH_TOKEN,
        value="",
        path=COOKIE_PATH,
        samesite="Strict",
        httponly=True,
        secure=secure,
        max_age=0,
    )
    response.set_cookie(
        key=COOKIE_NAME_REFRESH_TOKEN_EXPIRES_AT,
        value="",
        path=COOKIE_PATH,
        samesite="Strict",
        httponly=True,
        secure=secure,
        max_age=0,
    )
    response.set_cookie(
        key=COOKIE_NAME_STATE,
        value=f"{state}",
        path=COOKIE_PATH,
        samesite="Strict",
        httponly=True,
        secure=secure,
        max_age=60 * 10,
    )
    response.set_cookie(
        key=COOKIE_NAME_NONCE,
        value=nonce,
        path=COOKIE_PATH,
        samesite="Strict",
        httponly=True,
        secure=secure,
        max_age=60 * 10,
    )
    response.set_cookie(
        key=COOKIE_NAME_VERIFIER,
        value=code_verifier,
        path=COOKIE_PATH,
        samesite="Strict",
        httponly=True,
        secure=secure,
        max_age=60 * 10,
    )
    response.headers["Location"] = authorization_url
    raise HTTPException(
        status_code=status.HTTP_303_SEE_OTHER,
        headers=response.headers,  # type: ignore
    )


class AccessToken(HTTPBearer):
    """Access token bearer authentication scheme."""

    async def __call__(self, request: Request) -> HTTPAuthorizationCredentials:
        """Get access token."""
        request.headers.get("X-Forwarded-Proto") == "https"
        access_token: Optional[str] = request.cookies.get(COOKIE_NAME_ACCESS_TOKEN)

        if not access_token:
            # TODO: check expiry and do refresh
            await challenge(request)

        return HTTPAuthorizationCredentials(scheme="bearer", credentials=access_token)


class IdToken(HTTPBearer):
    """ID token bearer authentication scheme."""

    async def __call__(self, request: Request) -> HTTPAuthorizationCredentials:
        """Get ID token."""
        id_token: Optional[str] = request.cookies.get("id_token")
        if not id_token:
            await challenge(request)
        else:
            try:
                keys = await get_keys()
                header = jwt.get_unverified_header(id_token)  # type: ignore
                public_key = keys.get(header.get("kid"))
                jwt.decode(
                    id_token,
                    public_key,
                    algorithms=header["alg"],
                    audience=settings.OIDC_CLIENT_ID,
                )
            except Exception as e:
                logger.debug(str(e), exc_info=True)
                await challenge(request)

        return HTTPAuthorizationCredentials(scheme="bearer", credentials=id_token)


@router.get(
    "/login/oidc_redirect_uri",
    include_in_schema=False,
)
async def oidc_redirect_uri(
    request: Request,
    response: Response,
) -> None:
    """OIDC redirect URI."""
    state = request.cookies.get("state")
    if state != request.query_params.get("state"):
        raise HTTPException(status_code=403, detail="Invalid state")

    nonce = request.cookies.get("nonce")
    if not nonce:
        raise HTTPException(status_code=403, detail="No nonce")

    code_verifier = request.cookies.get("verifier")
    if not code_verifier:
        raise HTTPException(status_code=403, detail="No code verifier")

    code = request.query_params.get("code")
    if not code:
        raise HTTPException(status_code=403, detail="No code")

    oidc_client = WebApplicationClient(
        client_id=settings.OIDC_CLIENT_ID,
        client_secret=settings.OIDC_CLIENT_SECRET,
    )
    oidc_session = OAuth2Session(
        client=oidc_client,
        scope=settings.OIDC_SCOPE,
        redirect_uri=settings.OIDC_REDIRECT_URI,
    )

    provider = await get_provider_configuration()
    try:
        token = await sync(
            oidc_session.fetch_token,
            token_url=provider.token_endpoint,
            client_id=settings.OIDC_CLIENT_ID,
            client_secret=settings.OIDC_CLIENT_SECRET,
            code=code,
            code_verifier=code_verifier,
        )
    except OAuth2Error as e:
        raise HTTPException(status_code=403, detail=str(e))

    id_token = token.get("id_token")

    id_token = oidc_session.token.get("id_token")
    if not id_token:
        raise HTTPException(status_code=403, detail="No id token")

    keys = await get_keys()
    header = jwt.get_unverified_header(id_token)  # type: ignore
    if "kid" not in header or "alg" not in header:
        raise HTTPException(status_code=403, detail="No kid or alg")

    public_key = keys.get(header.get("kid"))
    if not public_key:
        raise HTTPException(status_code=403, detail="No public key")
    try:
        payload = jwt.decode(
            id_token,
            public_key,
            algorithms=header["alg"],
            audience=settings.OIDC_CLIENT_ID,
        )
    except jwt.PyJWTError as e:
        raise HTTPException(status_code=403, detail=str(e))

    if "nonce" not in payload or payload.get("nonce") != nonce:
        raise HTTPException(status_code=403, detail="Invalid nonce")

    secure = request.headers.get("X-Forwarded-Proto") == "https"
    response.set_cookie(
        key=COOKIE_NAME_STATE,
        value="",
        path=COOKIE_PATH,
        samesite="Strict",
        httponly=True,
        secure=secure,
        max_age=0,
    )
    response.set_cookie(
        key=COOKIE_NAME_NONCE,
        value="",
        path=COOKIE_PATH,
        samesite="Strict",
        httponly=True,
        secure=secure,
        max_age=0,
    )
    response.set_cookie(
        key=COOKIE_NAME_VERIFIER,
        value="",
        path=COOKIE_PATH,
        samesite="Strict",
        httponly=True,
        secure=secure,
        max_age=0,
    )
    response.set_cookie(
        key=COOKIE_NAME_ID_TOKEN,
        value=id_token,
        path=COOKIE_PATH,
        httponly=True,
        samesite="Strict",
        secure=secure,
        max_age=settings.OIDC_EXPIRY_SECONDS,
    )

    access_token = token.get("access_token")
    access_token_expires_in = token.get("expires_in")
    refresh_token = token.get("refresh_token")
    refresh_token_expires_in = token.get("refresh_expires_in")

    if access_token_expires_in:
        response.set_cookie(
            key=COOKIE_NAME_ACCESS_TOKEN_EXPIRES_AT,
            value=str(int(time.time()) + access_token_expires_in),
            path=COOKIE_PATH,
            secure=secure,
            max_age=access_token_expires_in,
        )
        if access_token:
            response.set_cookie(
                key=COOKIE_NAME_ACCESS_TOKEN,
                value=access_token,
                path=COOKIE_PATH,
                httponly=False,
                samesite="Strict",
                secure=secure,
                max_age=access_token_expires_in,
            )
    elif access_token:
        response.set_cookie(
            key=COOKIE_NAME_ACCESS_TOKEN,
            value=access_token,
            path=COOKIE_PATH,
            httponly=False,
            samesite="Strict",
            secure=secure,
        )

    if refresh_token_expires_in:
        response.set_cookie(
            key=COOKIE_NAME_REFRESH_TOKEN_EXPIRES_AT,
            value=str(int(time.time()) + refresh_token_expires_in),
            path=COOKIE_PATH,
            httponly=False,
            samesite="Strict",
            secure=secure,
            max_age=refresh_token_expires_in,
        )
        if refresh_token:
            response.set_cookie(
                key=COOKIE_NAME_REFRESH_TOKEN,
                value=refresh_token,
                path=COOKIE_PATH,
                httponly=True,
                samesite="Strict",
                secure=secure,
                max_age=access_token_expires_in,
            )
    elif refresh_token:
        response.set_cookie(
            key=COOKIE_NAME_REFRESH_TOKEN,
            value=refresh_token,
            path=COOKIE_PATH,
            httponly=True,
            samesite="Strict",
            secure=secure,
        )

    response.headers["Location"] = state

    raise HTTPException(
        status_code=status.HTTP_303_SEE_OTHER,
        headers=response.headers,  # type: ignore
    )


async def get_authenticated_principal(token: HTTPAuthorizationCredentials) -> str:
    """Get authenticated principal."""
    keys = await get_keys()
    header = jwt.get_unverified_header(token.credentials)  # type: ignore
    public_key = keys.get(header.get("kid"))
    payload = jwt.decode(
        token.credentials,
        public_key,
        algorithms=header["alg"],
        audience=settings.OIDC_CLIENT_ID,
    )
    username = payload.get(settings.OIDC_USERNAME_CLAIM)
    if not username:
        raise HTTPException(status_code=403, detail="No username")
    return (
        f"{username}"
        if "@" in f"{username}"
        else f"{username}@{settings.DEFAULT_PRINCIPAL_NAME_DOMAIN}"
    )


@router.get("/login", description="Login challenge", tags=["Login"])
async def login(
    credentials: HTTPAuthorizationCredentials = Depends(IdToken()),
) -> RedirectResponse:
    """Login."""
    return RedirectResponse("/")
