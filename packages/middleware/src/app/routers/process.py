"""FastAPI router."""
from app.config import emcee
from app.config import settings
from app.services.camunda import camunda_session
from app.services.camunda import get_external_tasks
from app.services.camunda import get_historic_process_instance_variables
from app.services.camunda import get_historic_user_task_variables
from app.services.camunda import get_process_activity_instances
from app.services.camunda import get_process_instance
from app.services.camunda import get_process_instance_variables
from app.services.camunda import get_process_instances_by_key
from app.services.camunda import get_user_task_by_key
from app.services.camunda import get_user_task_form_variables
from app.services.camunda import get_user_task_variables
from app.services.camunda import get_user_tasks_by_key
from app.types.camunda import CompleteTaskDto
from app.types.camunda import HistoricProcessInstanceDto
from app.types.camunda import HistoricTaskInstanceDto
from app.types.camunda import ProcessDefinitionDiagramDto
from app.types.camunda import ProcessInstanceWithVariablesDto
from app.types.camunda import StartProcessInstanceFormDto
from app.types.camunda import State
from app.types.camunda import ValueType
from app.types.camunda import VariableValueDto
from app.types.process import ActivityInstance
from app.types.process import CompleteUserTask
from app.types.process import ProcessDefinition
from app.types.process import ProcessInstance
from app.types.process import StartProcessInstance
from app.types.process import UserTask
from app.types.process import Variable
from app.utils import asdict
from app.utils import asjson
from app.utils import verify_response_status
from fastapi.exceptions import HTTPException
from fastapi.routing import APIRouter
from starlette.requests import Request
from starlette.responses import Response
from typing import Any
from typing import List
from typing import Optional
from typing import Union
from uuid import uuid4
import asyncio
import itertools
import time


router = APIRouter()


def get_type(value: Any) -> ValueType:
    """Infer Camunda variable type from variable."""
    if isinstance(value, str):
        return ValueType.String
    if isinstance(value, int):
        return ValueType.Integer
    if isinstance(value, float):
        return ValueType.Double
    if isinstance(value, bool):
        return ValueType.Boolean
    return ValueType.String


@router.post(
    "/process",
    status_code=201,
    response_model=ProcessInstance,
    summary="Start new process",
    tags=["Main"],
)
async def post_process(
    data: StartProcessInstance,
    request: Request,
    response: Response,
    wait: Optional[Union[int, float]] = 0,
) -> ProcessInstance:
    """Start new process."""
    async with camunda_session() as http:
        # Create
        key = data.processDefinitionKey
        url = (
            f"{settings.CAMUNDA_REST_ENDPOINT}/process-definition/key/{key}/submit-form"
        )
        payload = StartProcessInstanceFormDto(
            businessKey=str(uuid4()),
            variables={
                name: VariableValueDto(value=value, type=get_type(value))
                for name, value in (data.variables or {}).items()
            },
        )
        post = await http.post(url, data=asjson(payload))
        await verify_response_status(post, status=(200,))

        # Get
        initial = ProcessInstanceWithVariablesDto(**await post.json())
        assert initial.id, "Process instance is missing 'id'."
        assert initial.businessKey, "Process instance is missing 'businessKey'."
        result = await get_process(
            instance_id=initial.id, business_key=initial.businessKey, wait=wait
        )
        assert isinstance(result, ProcessInstance)

        # Redirect
        response.headers["Location"] = request.url_for(
            name=get_process.__name__,
            instance_id=result.id,
            business_key=result.businessKey,
        )

        # Return
        return result


async def wait_for_external_tasks(
    instances: List[HistoricProcessInstanceDto], wait_for: Union[int, float]
) -> Union[int, float]:
    """Wait for external tasks."""
    # Get external tasks
    get_external_tasks_tasks = [
        asyncio.create_task(get_external_tasks(instance.id))
        for instance in instances
        if instance.id
    ]
    await asyncio.wait(get_external_tasks_tasks)
    external_tasks = list(
        itertools.chain.from_iterable(
            [task.result() for task in get_external_tasks_tasks]
        )
    )
    start = time.time()
    keys = [
        f"{task.processInstanceId}:{task.topicName}"
        for task in external_tasks
        if task.topicName
    ]
    tasks = [asyncio.create_task(emcee.wait(key, timeout=wait_for)) for key in keys]
    if tasks:
        await asyncio.wait(tasks)
        wait_left = wait_for - (time.time() - start)
    else:
        wait_left = 0

    # If an externalTask was completed, retry for additional wait
    if wait_left > 0:
        results = [task.result() for task in tasks if task.done()]
        if True in results:
            return wait_left

    return -1


@router.get(
    "/process/{instance_id}/{business_key}",
    response_model=ProcessInstance,
    summary="Get process status",
    tags=["Main"],
)
async def get_process(
    instance_id: str, business_key: str, wait: Optional[Union[int, float]] = 0
) -> ProcessInstance:
    """Get process status."""
    # Get instance
    main_instance = None
    all_instances = await get_process_instances_by_key(business_key)
    for instance in all_instances:
        if instance.id == instance_id:
            main_instance = instance
            break
    if not main_instance:
        raise HTTPException(
            status_code=404,
            detail=f'Process instance with id "{instance_id}" and '
            f'business key "{business_key}" was not found.',
        )

    # Optionally wait for incomplete external tasks
    if wait:
        wait = await wait_for_external_tasks(all_instances, wait) - 0.1
        if wait > -1:
            result = await get_process(instance_id, business_key, wait=wait)
            assert isinstance(result, ProcessInstance)
            return result

    # Get user tasks and variables
    get_user_tasks_task, get_variables_task, get_activities_task = (
        asyncio.create_task(get_user_tasks_by_key(business_key)),
        asyncio.create_task(
            get_process_instance_variables(instance_id)
            if main_instance.state == State.ACTIVE
            else get_historic_process_instance_variables(instance_id)
        ),
        asyncio.create_task(
            get_process_activity_instances(instance_id, main_instance.state)
        ),
    )
    await asyncio.wait((get_user_tasks_task, get_variables_task, get_activities_task))
    user_tasks, variables, activities = (
        get_user_tasks_task.result(),
        get_variables_task.result(),
        get_activities_task.result(),
    )

    # Merge tasks with process
    data = asdict(main_instance)
    data["tasks"] = user_tasks
    data["activities"] = [
        ActivityInstance(
            id=activity.id,
            name=activity.activityName,
            type=activity.activityType,
            incident=bool(activity.incidents),
        )
        for activity in activities
    ]
    data["variables"] = {name: value.value for name, value in (variables or {}).items()}
    result = ProcessInstance(**data)
    assert isinstance(result, ProcessInstance)

    # Return
    return result


@router.get(
    "/process/{instance_id}/{business_key}/tasks/{task_definition_key}",
    response_model=UserTask,
    summary="Get user task details",
    tags=["Main"],
)
async def get_process_user_task(
    instance_id: str, business_key: str, task_definition_key: str
) -> UserTask:
    """Get user task details."""
    # Get instance to check businessKey
    instance = await get_process_instance(instance_id)
    if instance.businessKey != business_key:
        raise HTTPException(
            status_code=404,
            detail=f'Process instance with id "{instance_id}" and '
            f'business key "{business_key}" was not found.',
        )

    # Get
    task = await get_user_task_by_key(task_definition_key, business_key)

    # Simplify
    result = UserTask(**asdict(task))

    assert task.id, "User task is missing 'id'."
    assert task.processInstanceId, "User task is missing 'processInstanceId'."
    if isinstance(task, HistoricTaskInstanceDto):
        variables = await get_historic_user_task_variables(task.processInstanceId)
        if result.variables is None:
            result.variables = {}
        result.variables.update(
            {name: value.value for name, value in variables.items()}
        )
        result.completed = True
    else:
        variables = await get_user_task_variables(task.id)
        if result.variables is None:
            result.variables = {}
        result.variables.update(
            {name: value.value for name, value in variables.items()}
        )
        form = await get_user_task_form_variables(task.id)
        if result.form is None:
            result.form = {}
        result.form = {
            name: Variable(**asdict(value))
            for name, value in form.items()
            if name not in result.variables
        }
        result.variables.update({name: value.value for name, value in form.items()})

    # Return
    return result


@router.post(
    "/process/{instance_id}/{business_key}/tasks/{task_definition_key}",
    status_code=302,
    response_model=ProcessInstance,
    summary="Complete open user task",
    tags=["Main"],
)
async def post_user_task(
    instance_id: str,
    business_key: str,
    task_definition_key: str,
    request: Request,
    response: Response,
    data: Optional[CompleteUserTask] = None,
) -> None:
    """Complete open user task."""
    # Get instance to check businessKey
    instance = await get_process_instance(instance_id)
    if instance.businessKey != business_key:
        raise HTTPException(
            status_code=404,
            detail=f'Process instance with id "{instance_id}" and '
            f'business key "{business_key}" was not found.',
        )

    # Get
    task = await get_user_task_by_key(task_definition_key, instance.businessKey)
    assert task.id
    task_definition_key = task.id

    # Complete
    payload = CompleteTaskDto(
        variables={
            name: VariableValueDto(value=value)
            for name, value in data.variables.items()
        }
        if data
        else {}
    )
    url = f"{settings.CAMUNDA_REST_ENDPOINT}/task/{task_definition_key}/submit-form"
    async with camunda_session() as http:
        post = await http.post(url, data=asjson(payload))
        await verify_response_status(post, status=(204,), error_status=400)

    # Return
    response.headers["Location"] = (
        request.url_for(
            name=get_process.__name__,
            instance_id=instance_id,
            business_key=business_key,
        )
        + "?wait=1"
    )


@router.get(
    "/process/{instance_id}/{business_key}/definition",
    response_model=ProcessDefinition,
    summary="Get process definition",
    tags=["Main"],
)
async def get_definition(instance_id: str, business_key: str) -> ProcessDefinition:
    """Get process definition."""
    # Get instance to check businessKey
    instance = await get_process_instance(instance_id)
    if instance.businessKey != business_key:
        raise HTTPException(
            status_code=404,
            detail=f'Process instance with id "{instance_id}" and '
            f'business key "{business_key}" was not found.',
        )
    assert instance.processDefinitionId, "Process instance is missing 'definitionId'"

    async with camunda_session() as http:
        url = f"{settings.CAMUNDA_REST_ENDPOINT}/process-definition/{instance.processDefinitionId}/xml"
        get = await http.get(url)
        await verify_response_status(get, status=(200,), error_status=404)
        result = ProcessDefinitionDiagramDto(**await get.json())
        return ProcessDefinition(**asdict(result))
