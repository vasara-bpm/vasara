"""FastAPI router."""
from app.types.hasura import HasuraActionRequest
from fastapi.routing import APIRouter
from io import BytesIO
from openpyxl import load_workbook  # type: ignore
from openpyxl import Workbook
from pydantic import BaseModel
from pydantic import Field
from typing import Any
from typing import Dict
from typing import List
import base64


router = APIRouter()


class SheetsJson(BaseModel):
    """Sheets in Json."""

    sheets: Dict[str, List[List[Any]]] = Field(
        title="JSON object with 2 dimensional arrays"
    )

    class Config:
        """Schema configuration."""

        schema_extra = {
            "example": {
                "sheets": {
                    "demo": [
                        ["City", "Country", "Population"],
                        ["Tokyo", "Japan", 37400068],
                        ["Delhi", "India", 29399141],
                        ["Shanghai", "China", 26317104],
                        ["São Paulo", "Brazil", 21846507],
                    ]
                }
            }
        }


class SheetsXlsx(BaseModel):
    """Sheets in Xlsx."""

    base64: str = Field(title="Base64 encoded xlsx file.")


@router.post(
    "/xlsx/export",
    response_model=SheetsXlsx,
    summary="Export xlsx",
    tags=["xlsx"],
)
async def jsonToXlsx(
    data: HasuraActionRequest[SheetsJson],
) -> SheetsXlsx:
    """Export action."""
    wb = Workbook()
    first_sheet = True
    for title, rows in data.input.sheets.items():
        if first_sheet:
            ws = wb.active
            ws.title = title
            first_sheet = False
        else:
            ws = wb.create_sheet(title=title)
        for row in rows:
            ws.append(row)
    output = BytesIO()
    wb.save(output)
    return SheetsXlsx(base64=base64.b64encode(output.getvalue()))


@router.post(
    "/xlsx/import",
    response_model=SheetsJson,
    summary="Import LSX",
    tags=["xlsx"],
)
async def xlsxToJson(
    data: HasuraActionRequest[SheetsXlsx],
) -> SheetsJson:
    """Export action."""
    wb = load_workbook(BytesIO(base64.b64decode(data.input.base64)))
    sheets = {}
    for sheet in wb:
        rows = []
        for row in sheet.iter_rows(values_only=True):
            # Because openpyxl transforms boolean values into functions,
            # we need to transform them back into boolean values on import.
            rows.append(
                tuple(
                    [
                        True
                        if value in ["=TRUE()", "TRUE"]
                        else False
                        if value in ["=FALSE()", "FALSE"]
                        else value
                        for value in row
                    ]
                )
            )
        sheets[sheet.title] = rows
    return SheetsJson(sheets=sheets)
