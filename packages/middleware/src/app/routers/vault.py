"""FastAPI router."""
from aiohttp import ClientTimeout
from app.config import logger
from app.config import settings
from app.services.camunda import get_historic_user_tasks_by_keys
from app.services.camunda import get_process_instances_by_key
from app.services.hasura import query_user_task_form_sources
from app.services.vault import decrypt_text
from app.services.vault import encrypt_text
from app.types.hasura import HasuraAuthenticatedActionRequest
from app.utils import asjson
from app.utils import verify_response_status
from contextlib import asynccontextmanager
from fastapi.routing import APIRouter
from multidict import CIMultiDict
from pydantic import BaseModel
from pydantic import Field
from typing import AsyncGenerator
from typing import Dict
from typing import List
from typing import Optional
from typing import Tuple
from uuid import uuid4
import aiohttp
import json


router = APIRouter()


class VaultTransitEncrypt(BaseModel):
    """Vault transit encrypt payload."""

    businessKey: str = Field(title="Case business key")
    sources: List[str] = Field(title="Variable source bindings")
    plaintext: str = Field(title="Text value to encrypt")


class VaultTransitEncryptRequest(VaultTransitEncrypt):
    """Vault transit encrypt payload wit configuration."""

    class Config:
        """Pydantic configuration."""

        schema_extra = {
            "example": {
                "businessKey": str(uuid4()),
                "sources": ["context.variable"],
                "pii": True,
                "confidential": True,
                "plaintext": "Hello World",
            },
        }

    pii: bool = Field(title="Personal identity information")
    confidential: bool = Field(title="Confidential information")


class VaultTransitEncryptRequestBatch(BaseModel):
    """Vault transit encrypt batch."""

    batch: List[VaultTransitEncryptRequest] = Field(title="Vault transit encrypt batch")


class VaultTransitCiphertext(BaseModel):
    """Vault encryption response."""

    ciphertext: str = Field(title="Encrypted text prefixed with key")


class VaultTransitDecryptRequestBatch(BaseModel):
    """Vault transit decrypt batch."""

    batch: List[VaultTransitCiphertext] = Field(title="Vault transit decrypt batch")


class VaultTransitPlaintext(BaseModel):
    """Vault encryption response."""

    plaintext: Optional[str] = Field(title="Decrypted plain text", default=None)


def choose_transit_path_key(data: VaultTransitEncryptRequest) -> Tuple[str, str]:
    """Choose transit path and key to use."""
    if data.pii and data.confidential:
        return (
            settings.VAULT_TRANSIT_PII_RESTRICTED_PATH,
            settings.VAULT_TRANSIT_PII_RESTRICTED_KEY,
        )
    if data.pii and not data.confidential:
        return (
            settings.VAULT_TRANSIT_PII_SENSITIVE_PATH,
            settings.VAULT_TRANSIT_PII_SENSITIVE_KEY,
        )
    if data.confidential:
        return (
            settings.VAULT_TRANSIT_DATA_RESTRICTED_PATH,
            settings.VAULT_TRANSIT_DATA_RESTRICTED_KEY,
        )
    return settings.VAULT_TRANSIT_DEFAULT_PATH, settings.VAULT_TRANSIT_DEFAULT_KEY


async def encrypt_operation(
    data: VaultTransitEncryptRequest, extra_headers: Optional[CIMultiDict[str]] = None
) -> VaultTransitCiphertext:
    """Encrypt operation."""
    path, key = choose_transit_path_key(data)
    ciphertext = await encrypt_text(
        plaintext=data.plaintext,
        transit_path=path,
        transit_key=key,
        extra_headers=extra_headers,
    )
    assert ciphertext.startswith("vault:")
    ciphertext_path_key = f"vault:{path}:{key}:{ciphertext[6:]}"
    wrapped = VaultTransitEncrypt(
        businessKey=data.businessKey,
        sources=data.sources,
        plaintext=ciphertext_path_key,
    )
    wrapped_ciphertext = await encrypt_text(
        plaintext=asjson(wrapped),
        transit_path=settings.VAULT_TRANSIT_DEFAULT_PATH,
        transit_key=settings.VAULT_TRANSIT_DEFAULT_KEY,
        extra_headers=extra_headers,
    )
    assert wrapped_ciphertext.startswith("vault:")
    wrapped_ciphertext_path_key = (
        f"vault:{settings.VAULT_TRANSIT_DEFAULT_PATH}:"
        f"{settings.VAULT_TRANSIT_DEFAULT_KEY}"
        f":{wrapped_ciphertext[6:]}"
    )
    return VaultTransitCiphertext(ciphertext=wrapped_ciphertext_path_key)


@router.post(
    "/vault/encrypt",
    response_model=List[VaultTransitCiphertext],
    summary="Encrypt",
    tags=["Vault"],
)
async def encrypt(
    data: HasuraAuthenticatedActionRequest[VaultTransitEncryptRequestBatch],
) -> List[VaultTransitCiphertext]:
    """Encrypt action."""
    return [
        await encrypt_operation(
            operation,
            extra_headers=CIMultiDict(
                [
                    ("Jyu-Principal", data.session_variables.x_hasura_user_id),
                    ("Jyu-Metadata", operation.businessKey),
                ]
                + [("Jyu-Metadata", source) for source in operation.sources]
            ),
        )
        for operation in data.input.batch
    ]


# This custom endpoint is required, because the official REST API does not yet
# support querying for for multiple resource Ids
# https://docs.camunda.org/manual/latest/reference/rest/authorization/get-query/
CAMUNDA_AUTHORIZATIONS_QUERY = """\
query(
  $userIds: [String!]!,
  $resourceIds: [String!]!
){
  camunda_Authorizations(userIdIn: $userIds, resourceIdIn: $resourceIds) {
    id
    type
    permissions
    userId
    groupId
    resourceType
    resourceId
    removalTime
    rootProcessInstanceId
  }
}
"""


class CamundaAuthorizationsQueryVariables(BaseModel):
    """Camunda authorizations query variables."""

    userIds: List[str] = Field("User ids to query authorizations for")
    resourceIds: List[str] = Field("Resource ids to query authorization for")


class CamundaAuthorization(BaseModel):
    """Camunda authorization."""

    id: str = Field(title="The id of the authorization.")
    type: str = Field(title="The type of the authorization.")
    permissions: List[str] = Field(title="Permissions assigned by this authentication.")
    userId: Optional[str] = Field(
        title="The user this authorization has been created for."
    )
    groupId: Optional[str] = Field(
        title="The group this authorization has been created for."
    )
    resourceType: str = Field(title="A string representing the resource type.")
    resourceId: str = Field(title="The resource Id.")
    removalTime: Optional[str] = Field(title="The date an authorization is cleaned up.")
    rootProcessInstanceId: Optional[str] = Field(title="The root process instance.")


class CamundaAuthorizationsQuery(BaseModel):
    """Camunda authorizations query."""

    query: str = CAMUNDA_AUTHORIZATIONS_QUERY
    variables: CamundaAuthorizationsQueryVariables


class CamundaAuthorizationsQueryData(BaseModel):
    """User task form sources response data."""

    camunda_Authorizations: List[CamundaAuthorization]


class CamundaAuthorizationsQueryResponse(BaseModel):
    """User task form sources response."""

    data: CamundaAuthorizationsQueryData


@asynccontextmanager
async def camunda_graphql_session() -> AsyncGenerator[aiohttp.ClientSession, None]:
    """Get aiohttp session with Camunda headers."""
    headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "Authorization": settings.CAMUNDA_GRAPHQL_AUTHORIZATION,
    }
    async with aiohttp.ClientSession(
        headers=headers,
        trust_env=True,
        timeout=ClientTimeout(total=settings.CAMUNDA_TIMEOUT),
    ) as session:
        yield session


async def query_camunda_authorizations(
    userId: str, resourceIds: List[str]
) -> List[CamundaAuthorization]:
    """Query matching authorizations for given user id."""
    async with camunda_graphql_session() as http:
        url = settings.CAMUNDA_GRAPHQL_ENDPOINT
        query = CamundaAuthorizationsQuery(
            variables=CamundaAuthorizationsQueryVariables(
                userIds=[userId], resourceIds=resourceIds
            )
        )
        post = await http.post(url, data=asjson(query))
        await verify_response_status(post, (200,))
        response = CamundaAuthorizationsQueryResponse(**await post.json())
        return response.data.camunda_Authorizations


async def decrypt_operation(  # noqa: R0914 too many local variables
    data: VaultTransitCiphertext,
    user_id: str,
    principals: List[str],
    extra_headers: Optional[CIMultiDict[str]] = None,
) -> VaultTransitPlaintext:
    """Decrypt operation."""
    extra_headers = extra_headers or CIMultiDict()
    assert data.ciphertext.startswith("vault:")
    wrapped_plaintext = await decrypt_text(
        ciphertext=data.ciphertext, extra_headers=extra_headers
    )
    wrapped = VaultTransitEncrypt(**json.loads(wrapped_plaintext))
    extra_headers.update(
        CIMultiDict(
            [("Jyu-Metadata", wrapped.businessKey)]
            + [("Jyu-Metadata", source) for source in wrapped.sources]  # noqa: E1133
        )
    )
    instances = await get_process_instances_by_key(wrapped.businessKey)
    definitions: Dict[str, int] = {
        instance.processDefinitionKey: instance.processDefinitionVersion
        for instance in instances
        if instance.processDefinitionKey and instance.processDefinitionVersion
    }
    sources, vasara_authorizations = await query_user_task_form_sources(
        principals=principals,
        process_definition_keys=list(definitions),
        sources=wrapped.sources,
        max_version=max(*list(definitions.values()), 0),
    )
    task_definition_keys = set()
    for source in sources:
        if (
            source.user_task_id not in task_definition_keys
            and source.process_definition_version
            <= (definitions.get(source.process_definition_key) or 0)
        ):
            task_definition_keys.add(source.user_task_id)
    tasks = await get_historic_user_tasks_by_keys(
        list(task_definition_keys), wrapped.businessKey
    )
    task_ids = list([task.id for task in tasks if task.id])
    task_authorizations = await query_camunda_authorizations(
        userId=user_id, resourceIds=task_ids
    )
    if any(
        [
            "READ" in authorization.permissions or "UPDATE" in authorization.permissions
            for authorization in task_authorizations
        ]
    ):
        # Can decrypt, if has READ or UPDATE permission on a user task,
        # which has the field with encrypted value (by source) on it
        plaintext = await decrypt_text(
            ciphertext=wrapped.plaintext, extra_headers=extra_headers
        )
        return VaultTransitPlaintext(plaintext=plaintext)

    process_authorizations = await query_camunda_authorizations(
        userId=user_id,
        resourceIds=[instance.id for instance in instances if instance.id],
    )
    if all(
        [
            any(["READ" in auth.permissions for auth in process_authorizations]),
            any([auth.can_select for auth in vasara_authorizations]),
        ]
    ):
        # Can decrypt, if has READ permission on a process with the matching
        # business key and Vasara ACL permission to read matching source
        plaintext = await decrypt_text(
            ciphertext=wrapped.plaintext, extra_headers=extra_headers
        )
        return VaultTransitPlaintext(plaintext=plaintext)

    logger.info(
        'Denied "%s" to decrypt "%s" for business key "%s" task ids "%s".',
        user_id,
        ",".join(wrapped.sources),
        wrapped.businessKey,
        ",".join(task_ids),
    )
    return VaultTransitPlaintext(plaintext=None)


@router.post(
    "/vault/decrypt",
    response_model=List[VaultTransitPlaintext],
    summary="Decrypt",
    tags=["Vault"],
)
async def decrypt(
    data: HasuraAuthenticatedActionRequest[VaultTransitDecryptRequestBatch],
) -> List[VaultTransitPlaintext]:
    """Decrypt action."""

    # Principals is set by graphql-oidc-auth -service as a Postgres array
    principals = (
        (data.session_variables.x_hasura_principals or "").strip("{}").split(",")
    )

    return [
        await decrypt_operation(
            operation,
            user_id=data.session_variables.x_hasura_user_id,
            principals=principals,
            extra_headers=CIMultiDict(
                [("Jyu-Principal", data.session_variables.x_hasura_user_id)]
            ),
        )
        for operation in data.input.batch
    ]
