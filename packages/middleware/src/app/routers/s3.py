"""FastAPI router."""
from app.config import settings
from app.routers.login import get_authenticated_principal
from app.routers.login import IdToken
from app.routers.vault import query_camunda_authorizations
from app.services.camunda import get_process_instances_by_key
from concurrent.futures import ThreadPoolExecutor
from fastapi.exceptions import HTTPException
from fastapi.param_functions import Depends
from fastapi.responses import RedirectResponse  # type: ignore
from fastapi.routing import APIRouter
from fastapi.security.http import HTTPAuthorizationCredentials
from functools import partial
from typing import Any
from typing import Callable
from urllib.parse import urlparse
import asyncio
import datetime
import logging
import minio  # type: ignore


router = APIRouter()
logger = logging.getLogger(__name__)

DEFAULT_WORKER = ThreadPoolExecutor()


# Note: testing oauthlib against http development IdP env OAUTHLIB_INSECURE_TRANSPORT


async def sync(func: Callable[[Any], Any], *args: Any, **kwargs: Any) -> Any:
    """Run sync function in thread pool."""
    return await asyncio.get_event_loop().run_in_executor(
        DEFAULT_WORKER, partial(func, *args, **kwargs)
    )


@router.get(
    "/attachment/{name:path}",
    summary="Get attachment",
    tags=["data"],
)
async def attachment(
    name: str, credentials: HTTPAuthorizationCredentials = Depends(IdToken())
) -> RedirectResponse:
    """Get attachment from S3."""
    principal_name = await get_authenticated_principal(credentials)
    url = urlparse(settings.S3_ENDPOINT_URL)
    s3 = minio.Minio(
        endpoint=f"{url.hostname}:{url.port}",
        access_key=settings.S3_ACCESS_KEY,
        secret_key=settings.S3_SECRET_KEY,
        secure=url.scheme == "https",
    )

    try:
        stat = await sync(s3.stat_object, settings.S3_BUCKET_NAME, name)
    except minio.error.S3Error as e:
        if e.code == "NoSuchKey":
            raise HTTPException(status_code=404)
        raise
    if "X-Amz-Meta-Business-Key" in stat.metadata:
        business_key = stat.metadata["X-Amz-Meta-Business-Key"]
        instances = await get_process_instances_by_key(business_key)
        process_authorizations = (
            await query_camunda_authorizations(
                userId=principal_name,
                resourceIds=[instance.id for instance in instances if instance.id],
            )
            if instances
            else []
        )
        if not any(["READ" in auth.permissions for auth in process_authorizations]):
            raise HTTPException(status_code=403, detail="Not authorized")
        presigned_url = await sync(
            s3.presigned_get_object,
            settings.S3_BUCKET_NAME,
            name,
            expires=datetime.timedelta(seconds=settings.S3_EXPIRY_SECONDS),
        )
        return RedirectResponse(presigned_url)
    else:
        raise HTTPException(status_code=404)
