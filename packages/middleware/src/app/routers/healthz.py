"""FastAPI router."""
from app.types.heartbeat import Heartbeat
from datetime import datetime
from fastapi.routing import APIRouter


router = APIRouter()


@router.get(
    "/healthz", response_model=Heartbeat, summary="Service health status", tags=["Meta"]
)
async def healthz() -> Heartbeat:
    """Service health status."""
    timestamp = datetime.utcnow().isoformat()
    return Heartbeat(timestamp=timestamp)


#   now = datetime.utcnow()

#   if (now - timedelta(seconds=45)).isoformat() < heartbeat.state.timestamp:
#       return Heartbeat(timestamp=heartbeat.state.timestamp)

#   age = (now - datetime.fromisoformat(heartbeat.state.timestamp)).total_seconds()
#   raise HTTPException(status_code=500, detail=f"No heartbeat for {age} seconds")
