"""Service configuration."""
from enum import Enum
from pydantic import BaseSettings


class Settings(BaseSettings):
    """Settings."""

    GUEST_PROCESS_LIMIT_PER_SECOND: float = 0.5  # per second
    GUEST_PROCESS_LIMIT_MAX_QUEUE: int = 10


class Topic(str, Enum):
    """External task topics."""

    CAMUNDA_GRANT = "vasara.grant"
