"""Service external tasks."""
from app.camunda.config import Topic
from app.config import settings
from app.services.camunda import camunda_session
from app.services.camunda import task_variable_list
from app.services.camunda import task_variable_bool
from app.services.hasura import query_camunda_principals
from app.types.camunda import CompleteExternalTaskDto
from app.types.camunda import ExternalTaskFailureDto
from app.types.camunda import LockedExternalTaskDto
from app.types.worker import ExternalTaskComplete
from app.types.worker import ExternalTaskFailure
from app.utils import asdict
from app.utils import asjson
from app.utils import verify_response_status
from enum import Enum
from pydantic import BaseModel
from pydantic import Field
from typing import Dict
from typing import List
from typing import Optional
from typing import Union
import logging


logger = logging.getLogger(__name__)


class AuthorizationDto(BaseModel):
    """Camunda authorization result."""

    id: str
    type: int = Field(default=1)
    permissions: List[str]
    userId: Optional[str] = Field(default=None)
    groupId: Optional[str] = Field(default=None)
    resourceType: int = Field(default=8)
    resourceId: str


class CreateAuthorizationDto(BaseModel):
    """Camunda authorization update."""

    type: int = Field(default=1)
    permissions: List[str]
    userId: Optional[str] = Field(default=None)
    groupId: Optional[str] = Field(default=None)
    resourceType: int = Field(default=8)
    resourceId: str


class UpdateAuthorizationDto(BaseModel):
    """Camunda authorization update."""

    permissions: List[str]
    userId: Optional[str] = Field(default=None)
    groupId: Optional[str] = Field(default=None)
    resourceType: int = Field(default=8)
    resourceId: str


class ProcessResourceType(int, Enum):
    """Camunda resource type."""

    PROCESS_INSTANCE = 8
    HISTORIC_PROCESS_INSTANCE = 20


async def ensure_user_authorization(
    permission: str,
    process_id: str,
    user_ids: List[str],
    resource_type: ProcessResourceType,
) -> None:
    """Ensure read permissions for given user ids for certain process."""
    url = f"{settings.CAMUNDA_REST_ENDPOINT}/authorization"
    async with camunda_session() as http:
        response = await http.get(
            f"{url}",
            params={
                "type": 1,  # Grant
                "resourceType": resource_type,
                "resourceId": process_id,
                "userIdIn": ",".join(user_ids),
            },
        )
        await verify_response_status(response, status=(200,), error_status=404)
        old = [AuthorizationDto(**item) for item in await response.json()]

    updates: Dict[str, UpdateAuthorizationDto] = {}
    for authorization in old:
        if permission not in authorization.permissions:
            authorization.permissions.append(permission)
            updates[authorization.id] = UpdateAuthorizationDto(**asdict(authorization))
        if authorization.userId in user_ids:
            user_ids.remove(authorization.userId)

    creates: List[CreateAuthorizationDto] = []
    for user_id in user_ids:
        creates.append(
            CreateAuthorizationDto(
                type=1,  # Grant
                permissions=[permission],
                userId=user_id,
                groupId=None,
                resourceType=resource_type,
                resourceId=process_id,
            )
        )

    async with camunda_session() as http:
        for id_, update in updates.items():
            url = f"{settings.CAMUNDA_REST_ENDPOINT}/authorization/{id_}"
            response = await http.put(url, data=asjson(update))
            if response.status != 204:
                if response.content_type == "application/json":
                    error = await response.json()
                else:
                    error = await response.text()
                logger.warning("Failed to update %s as %s with %s", url, update, error)
        for create in creates:
            url = f"{settings.CAMUNDA_REST_ENDPOINT}/authorization/create"
            response = await http.post(url, data=asjson(create))
            if response.status != 200:
                if response.content_type == "application/json":
                    error = await response.json()
                else:
                    error = await response.text()
                logger.warning("Failed to create %s as %s with %s", url, create, error)


async def ensure_group_authorization(
    permission: str,
    process_id: str,
    group_ids: List[str],
    resource_type: ProcessResourceType,
) -> None:
    """Ensure read permissions for given group ids for certain process."""
    url = f"{settings.CAMUNDA_REST_ENDPOINT}/authorization"
    async with camunda_session() as http:
        response = await http.get(
            f"{url}",
            params={
                "type": 1,  # Grant
                "resourceType": resource_type,
                "resourceId": process_id,
                "groupIdIn": ",".join(group_ids),
            },
        )
        await verify_response_status(response, status=(200,), error_status=404)
        old = [AuthorizationDto(**item) for item in await response.json()]

    updates: Dict[str, UpdateAuthorizationDto] = {}
    for authorization in old:
        if permission not in authorization.permissions:
            authorization.permissions.append(permission)
            updates[authorization.id] = UpdateAuthorizationDto(**asdict(authorization))
        if authorization.groupId in group_ids:
            group_ids.remove(authorization.groupId)

    creates: List[CreateAuthorizationDto] = []
    for group_id in group_ids:
        creates.append(
            CreateAuthorizationDto(
                type=1,  # Grant
                permissions=[permission],
                userId=None,
                groupId=group_id,
                resourceType=resource_type,
                resourceId=process_id,
            )
        )

    async with camunda_session() as http:
        for id_, update in updates.items():
            url = f"{settings.CAMUNDA_REST_ENDPOINT}/authorization/{id_}"
            response = await http.put(url, data=asjson(update))
            if response.status != 204:
                if response.content_type == "application/json":
                    error = await response.json()
                else:
                    error = await response.text()
                logger.warning("Failed to update %s as %s with %s", url, update, error)
        for create in creates:
            url = f"{settings.CAMUNDA_REST_ENDPOINT}/authorization/create"
            response = await http.post(url, data=asjson(create))
            if response.status != 200:
                if response.content_type == "application/json":
                    error = await response.json()
                else:
                    error = await response.text()
                logger.warning("Failed to create %s as %s with %s", url, create, error)


async def grant_authorization(
    task: LockedExternalTaskDto,
) -> Union[ExternalTaskComplete, ExternalTaskFailure]:  # noqa: R9014
    """Complete task."""
    assert task.processInstanceId

    try:
        grant = [
            x
            for x in task_variable_list(task, "permissions")
            if x in ["READ", "CREATE", "UPDATE", "UPDATE_VARIABLE"]
        ]
    except KeyError:
        grant = ["READ"]
    # CREATE is used by /graphql update_camunda_ProcessInstance_correlate

    try:
        strict = task_variable_bool(task, "strict")
    except KeyError:
        strict = True

    try:
        principals = task_variable_list(task, "principals")
    except KeyError:
        principals = []

    try:
        users = task_variable_list(task, "users")
    except KeyError:
        users = []
    users.extend(principals)
    users = [x for x in users if (x or f"{x}").strip()]

    try:
        groups = task_variable_list(task, "groups")
    except KeyError:
        groups = []
    groups.extend(principals)
    groups = [x for x in groups if (x or f"{x}").strip()]

    _, result = await query_camunda_principals(user_ids=users, group_ids=groups)

    if not result.users and not principals and not strict:
        result.users = list(set(users))

    if not result.groups and not principals and not strict:
        result.groups = list(set(groups))

    if not result.users and not result.groups:
        return ExternalTaskFailure(
            task=task,
            response=ExternalTaskFailureDto(
                workerId=task.workerId,
                errorMessage="No principals",
                errorDetails=f"No users found for {users} or groups found for {groups}",
            ),
        )

    if result.users:
        for permission in grant:
            await ensure_user_authorization(
                permission,
                task.processInstanceId,
                [user.id for user in result.users],
                ProcessResourceType.PROCESS_INSTANCE,
            )
    if result.users and "READ" in grant:
        await ensure_user_authorization(
            "READ",
            task.processInstanceId,
            [user.id for user in result.users],
            ProcessResourceType.HISTORIC_PROCESS_INSTANCE,
        )

    if result.groups:
        for permission in grant:
            await ensure_group_authorization(
                permission,
                task.processInstanceId,
                [group.id for group in result.groups],
                ProcessResourceType.PROCESS_INSTANCE,
            )
    if result.groups and "READ" in grant:
        await ensure_group_authorization(
            "READ",
            task.processInstanceId,
            [group.id for group in result.groups],
            ProcessResourceType.HISTORIC_PROCESS_INSTANCE,
        )

    return ExternalTaskComplete(
        task=task, response=CompleteExternalTaskDto(workerId=task.workerId)
    )


TASKS = {Topic.CAMUNDA_GRANT: grant_authorization}
