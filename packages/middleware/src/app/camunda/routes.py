"""FastAPI router."""
from app.config import settings
from app.services.camunda import camunda_session
from app.types.camunda import AuthorizationDto
from app.types.camunda import StartProcessInstanceFormDto
from app.types.camunda import ValueType
from app.types.camunda import VariableValueDto
from app.types.hasura import HasuraAnonymousActionRequest
from app.utils import asjson
from app.utils import verify_response_status
from fastapi.routing import APIRouter
from pydantic import BaseModel
from typing import Any
from typing import List
from typing import Optional
import asyncio
import datetime
import email_validator
import uuid


router = APIRouter()


class CamundaGuestProcessDefinitionRequest(BaseModel):
    """Guest-startable process definition details query."""


class CamundaGuestProcessDefinition(BaseModel):
    """Guest-startable process definition details."""

    id: str
    key: str
    name: str
    description: Optional[str]
    startableInTasklist: bool


class InsertCamundaGuestProcessInstanceRequest(BaseModel):
    """Start guest-startable process instance."""

    processDefinitionKey: str
    firstName: str
    lastName: str
    email: str


class CamundaGuestProcessInstance(BaseModel):
    """Guest-stared process instance."""

    id: str


@router.post(
    "/camunda/guest/process-definition",
    response_model=List[CamundaGuestProcessDefinition],
    summary="Transient",
    tags=["Camunda"],
)
async def camunda_guest_process_definition(
    data: HasuraAnonymousActionRequest[CamundaGuestProcessDefinitionRequest],
) -> List[CamundaGuestProcessDefinition]:
    """Get guest-startable process definitions details."""
    assert data
    async with camunda_session() as http:
        url = f"{settings.CAMUNDA_REST_ENDPOINT}/authorization"
        auth_query_params = {"type": 0, "resourceType": 6}  # process definition
        get = await http.get(url, params=auth_query_params)
        await verify_response_status(get, (200,))
        authorizations = [AuthorizationDto(**item) for item in await get.json()]

        definition_ids = [
            authorization.resourceId
            for authorization in authorizations
            if authorization.resourceId
            and set(authorization.permissions or []).issuperset(
                {"READ", "CREATE_INSTANCE"}
            )
        ]
        if not definition_ids:
            return []

        url = f"{settings.CAMUNDA_REST_ENDPOINT}/process-definition"
        definition_query_params = {"keysIn": ",".join(definition_ids), "latest": "true"}
        get = await http.get(url, params=definition_query_params)
        await verify_response_status(get, (200,))

        definitions = [
            CamundaGuestProcessDefinition(**item) for item in await get.json()
        ]

    return definitions


LAST_CALL = datetime.datetime.utcnow()
CALL_QUEUE: asyncio.Queue[Any] = asyncio.Queue()


@router.post(
    "/camunda/guest/process-instance",
    response_model=CamundaGuestProcessInstance,
    summary="Transient",
    tags=["Camunda"],
)
async def insert_camunda_guest_process_instance(
    data: HasuraAnonymousActionRequest[InsertCamundaGuestProcessInstanceRequest],
) -> CamundaGuestProcessInstance:
    """Start guest-startable process instance."""

    def sanitize(name: str) -> str:
        """Sanitize string."""
        return "".join([s for s in name if s.isalnum() or s in ["., "]])

    # Rate limit
    assert (
        CALL_QUEUE.qsize() < settings.GUEST_PROCESS_LIMIT_MAX_QUEUE
    ), "Peak traffic. Please, wait a while and try again."
    global LAST_CALL  # noqa: W0603
    delta = datetime.datetime.utcnow() - LAST_CALL
    if delta.seconds < (1 / settings.GUEST_PROCESS_LIMIT_PER_SECOND):
        await CALL_QUEUE.put(None)
        await asyncio.sleep(
            (1 / settings.GUEST_PROCESS_LIMIT_PER_SECOND) - delta.seconds
        )
        await CALL_QUEUE.get()
    LAST_CALL = datetime.datetime.utcnow()

    first_name = sanitize(data.input.firstName)
    last_name = sanitize(data.input.lastName)
    validated_email = email_validator.validate_email(data.input.email)
    email = validated_email.ascii_email or validated_email.email

    async with camunda_session() as http:
        key = data.input.processDefinitionKey
        url = (
            f"{settings.CAMUNDA_REST_ENDPOINT}/process-definition/key/{key}/submit-form"
        )

        business_key = str(uuid.uuid4())
        payload = StartProcessInstanceFormDto(
            variables={
                "firstName": VariableValueDto(value=first_name, type=ValueType.String),
                "lastName": VariableValueDto(value=last_name, type=ValueType.String),
                "email": VariableValueDto(value=email, type=ValueType.String),
            },
            businessKey=business_key,
        )
        post = await http.post(url, data=asjson(payload))
        await verify_response_status(post, (200,))

        process_instance = CamundaGuestProcessInstance(**await post.json())
    return process_instance
