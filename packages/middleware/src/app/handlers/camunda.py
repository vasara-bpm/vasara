"""Camunda ExternalTask handler for Camunda tasks."""
from aiohttp import ClientResponse
from app.config import settings
from app.services.camunda import camunda_session
from app.types.camunda import CompleteExternalTaskDto
from app.types.camunda import LockedExternalTaskDto
from app.types.worker import ExternalTaskComplete
from app.utils import verify_response_status
from asyncio import ALL_COMPLETED
from asyncio import Task
from typing import List
import asyncio
import json


async def verify_delete_tasks(tasks: List[Task[ClientResponse]]) -> None:
    """Ensure list of delete tasks being successfully awaited."""
    if len(tasks) > 0:
        done, pending = await asyncio.wait(tasks, return_when=ALL_COMPLETED)
        assert len(pending) == 0
        for future in done:
            await verify_response_status(future.result(), status=(204, 404))


async def delete_variables(task: LockedExternalTaskDto) -> ExternalTaskComplete:
    """Delete current and historic process variables."""
    instance_id = task.processInstanceId

    if (
        task.variables
        and task.variables["exclude"]
        and task.variables["exclude"].value
        and task.variables["exclude"].valueInfo
        and task.variables["exclude"].valueInfo.get("serializationDataFormat")
        == "application/json"
    ):
        exclude = set(json.loads(task.variables["exclude"].value or "[]"))
    else:
        exclude = set()

    async with camunda_session() as http:
        url = (
            f"{settings.CAMUNDA_REST_ENDPOINT}/process-instance/{instance_id}/variables"
        )
        get = await verify_response_status(await http.get(url), status=(200,))
        variables = await get.json()

        tasks = []
        for name in variables:
            if name not in exclude:
                tasks.append(asyncio.create_task(http.delete(f"{url}/{name}")))
        await verify_delete_tasks(tasks)

        url = f"{settings.CAMUNDA_REST_ENDPOINT}/history/variable-instance"
        params = {"processInstanceId": instance_id}
        get = await http.get(url, params=params)
        await verify_response_status(get, status=(200,))
        variables = await get.json()

        tasks = []
        for variable in variables:
            if variable["name"] not in exclude:
                tasks.append(
                    asyncio.create_task(http.delete(f"{url}/{variable['id']}"))
                )
        await verify_delete_tasks(tasks)

    return ExternalTaskComplete(
        task=task,
        response=CompleteExternalTaskDto(workerId=task.workerId),
    )
