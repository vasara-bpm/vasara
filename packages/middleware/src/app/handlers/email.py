"""Email task handler."""
from app.config import settings
from app.services.camunda import camunda_session
from app.services.camunda import task_variable_list
from app.services.camunda import task_variable_str
from app.services.hasura import query_camunda_principals
from app.services.substitute import build_substitution_query
from app.services.substitute import query_substitution_values
from app.services.substitute import safe_substitute
from app.types.camunda import CompleteExternalTaskDto
from app.types.camunda import ExternalTaskBpmnError
from app.types.camunda import ExternalTaskFailureDto
from app.types.camunda import LockedExternalTaskDto
from app.types.camunda import ValueType
from app.types.hasura import CamundaPrincipalsQueryData
from app.types.worker import ExternalTaskComplete
from app.types.worker import ExternalTaskFailure
from app.utils import asdict
from app.utils import asjson
from app.utils import calculate_timeout
from app.utils import verify_response_status
from concurrent.futures import ThreadPoolExecutor
from email.charset import Charset
from email.charset import QP
from email.encoders import encode_base64
from email.generator import Generator
from email.header import Header
from email.message import Message
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from enum import Enum
from html.parser import HTMLParser
from io import StringIO
from mimetypes import guess_type
from pydantic import BaseModel
from pydantic import Field
from typing import Any
from typing import Dict
from typing import List
from typing import Optional
from typing import Sequence
from typing import Tuple
from typing import Union
import asyncio
import binascii
import email_validator
import json
import logging
import smtplib


CHARSET = Charset("utf-8")
CHARSET.header_encoding = QP
CHARSET.body_encoding = QP


logger = logging.getLogger(__name__)

RETRIES_MAX = 5
RETRY_TIMEOUT = 60000 * 5
RETRY_TIMEOUT_MAX = 60000 * 60 * 24

SMTP_WORKER = ThreadPoolExecutor(max_workers=1)


class MarkupCleaner(HTMLParser):
    """HTML markup cleaner."""

    def __init__(self) -> None:
        """Initialize parser and buffer."""
        super().__init__(convert_charrefs=True)
        self.reset()
        self.text = StringIO()

    def handle_data(self, data: str) -> None:
        """Feed cleaned data into buffer."""
        self.text.write(data)

    def get_data(self) -> str:
        """Return cleaned data from buffer."""
        return self.text.getvalue()

    def error(self, message: str) -> None:
        """Log error in parsing markup."""
        logger.warning(message)


def strip_tags(s: str) -> str:
    """Strip HTML tags from string."""
    cleaner = MarkupCleaner()
    cleaner.feed(s)
    return cleaner.get_data()


class AuthorizationDto(BaseModel):
    """Camunda authorization result."""

    id: str
    type: int = Field(default=1)
    permissions: List[str]
    userId: Optional[str] = Field(default=None)
    groupId: Optional[str] = Field(default=None)
    resourceType: int = Field(default=8)
    resourceId: str


class CreateAuthorizationDto(BaseModel):
    """Camunda authorization update."""

    type: int = Field(default=1)
    permissions: List[str]
    userId: Optional[str] = Field(default=None)
    groupId: Optional[str] = Field(default=None)
    resourceType: int = Field(default=8)
    resourceId: str


class UpdateAuthorizationDto(BaseModel):
    """Camunda authorization update."""

    permissions: List[str]
    userId: Optional[str] = Field(default=None)
    groupId: Optional[str] = Field(default=None)
    resourceType: int = Field(default=8)
    resourceId: str


class ProcessResourceType(int, Enum):
    """Camunda resource type."""

    PROCESS_INSTANCE = 8
    HISTORIC_PROCESS_INSTANCE = 20


async def ensure_read_authorization(
    process_id: str, group_ids: List[str], resource_type: ProcessResourceType
) -> None:
    """Ensure read permissions for given group ids for certain process."""
    url = f"{settings.CAMUNDA_REST_ENDPOINT}/authorization"
    async with camunda_session() as http:
        response = await http.get(
            f"{url}",
            params={
                "type": 1,  # Grant
                "resourceType": resource_type,
                "resourceId": process_id,
                "groupIdIn": ",".join(group_ids),
            },
        )
        await verify_response_status(response, status=(200,), error_status=404)
        old = [AuthorizationDto(**item) for item in await response.json()]

    updates: Dict[str, UpdateAuthorizationDto] = {}
    for authorization in old:
        if "READ" not in authorization.permissions:
            authorization.permissions.append("READ")
            updates[authorization.id] = UpdateAuthorizationDto(**asdict(authorization))
        if authorization.groupId in group_ids:
            group_ids.remove(authorization.groupId)

    creates: List[CreateAuthorizationDto] = []
    for group_id in group_ids:
        creates.append(
            CreateAuthorizationDto(
                type=1,  # Grant
                permissions=["READ"],
                userId=None,
                groupId=group_id,
                resourceType=resource_type,
                resourceId=process_id,
            )
        )

    async with camunda_session() as http:
        for id_, update in updates.items():
            url = f"{settings.CAMUNDA_REST_ENDPOINT}/authorization/{id_}"
            response = await http.put(url, data=asjson(update))
            if response.status != 204:
                if response.content_type == "application/json":
                    error = await response.json()
                else:
                    error = await response.text()
                logger.warning("Failed to update %s as %s with %s", url, update, error)
        for create in creates:
            url = f"{settings.CAMUNDA_REST_ENDPOINT}/authorization/create"
            response = await http.post(url, data=asjson(create))
            if response.status != 200:
                if response.content_type == "application/json":
                    error = await response.json()
                else:
                    error = await response.text()
                logger.warning("Failed to create %s as %s with %s", url, create, error)


def split_email_address(email: str) -> Tuple[str, str]:
    """Split email into descriptive part and address."""
    if "<" not in email:
        descriptive = address = email_validator.validate_email(email).email.strip()
        return descriptive, address
    descriptive, address = email.split("<", 1)
    address = email_validator.validate_email(address.split(">")[0]).email.strip()
    return descriptive.strip().strip('"'), address


class Attachment(BaseModel):
    """Attachment as a bytestring with metadata."""

    data: bytes
    filename: str
    mimetype: str


def create_email(  # noqa: R9014
    sender: str,
    reply_to: Optional[str],
    recipients: List[str],
    bcc_recipients: List[str],
    subject: str,
    body: str,
    attachments: List[Attachment],
) -> MIMEMultipart:
    """Create encoded email."""
    sender_parts = split_email_address(sender)
    recipients_parts = map(split_email_address, recipients)
    bcc_recipients_parts = map(split_email_address, bcc_recipients)

    msg = MIMEMultipart()
    msg["Subject"] = strip_tags(subject)
    msg["From"] = f"{Header(sender_parts[0], CHARSET).encode()} <{sender_parts[1]}>"
    if reply_to:
        descriptive, address = split_email_address(reply_to)
        msg["Reply-To"] = (
            f"{Header(descriptive, CHARSET).encode()} <{address}>"
            if descriptive != address
            else address
        )
    msg["To"] = ",".join(
        [
            f"{Header(descriptive, CHARSET).encode()} <{address}>"
            if descriptive != address
            else address
            for descriptive, address in recipients_parts
        ]
    )
    if bcc_recipients:
        msg["Bcc"] = ",".join(
            [
                f"{Header(descriptive, CHARSET).encode()} <{address}>"
                if descriptive != address
                else address
                for descriptive, address in bcc_recipients_parts
            ]
        )

    main = MIMEMultipart("alternative")
    plain = strip_tags(body).replace("\n", "\r\n")
    html = plain.replace("\r\n", "<br/>")

    # noinspection PyTypeChecker
    main.attach(MIMEText(plain, "plain", CHARSET))  # type: ignore
    main.attach(MIMEText(html, "html", "utf-8"))
    msg.attach(main)

    # attachments
    for attachment in attachments:
        mime = (
            attachment.mimetype.split("/")
            if attachment.mimetype.count("/") == 1
            else "application/octet-stream".split("/")
        )
        part = MIMEBase(*mime, name=f"{Header(attachment.filename, CHARSET).encode()}")
        part.set_payload(attachment.data)
        encode_base64(part)
        part.add_header(
            "Content-Disposition",
            "attachment",
            filename=f"{Header(attachment.filename, CHARSET).encode()}",
        )
        msg.attach(part)

    return msg


def create_debug_email(  # noqa: R9014
    sender: str,
    reply_to: Optional[str],
    recipients: List[str],
    bcc_recipients: List[str],
    subject: str,
    body: str,
    attachments: List[str],
) -> str:
    message = create_email(
        sender,
        reply_to,
        recipients,
        bcc_recipients,
        subject,
        "\n".join([body, ""] + attachments),
        [],
    )
    str_io = StringIO()
    g = Generator(str_io, False)
    g.flatten(message)
    return str_io.getvalue()


async def get_process_attachments(
    task: LockedExternalTaskDto, paths: List[str]
) -> List[Attachment]:
    """Get process file variables as attachments."""
    attachments = []
    for name in paths:
        variable = (task.variables or {}).get(name)
        if not variable or variable.type != ValueType.File:
            continue
        value_info = variable.valueInfo or {}
        async with camunda_session() as http:
            url = f"{settings.CAMUNDA_REST_ENDPOINT}/execution/{task.executionId}/localVariables/{name}/data"
            get = await http.get(url, headers={"Accept": "application/octet-stream"})
            await verify_response_status(get, status=(200,))
            data = await get.read()
        filename = value_info.get("filename") or "attachment"
        mimetype = (
            value_info.get("mimeType")
            or value_info.get("mimetype")
            or guess_type(filename)[0]
            or "application/octet-stream"
        )
        attachments.append(Attachment(data=data, filename=filename, mimetype=mimetype))
    return attachments


async def get_entity_attachments(
    business_key: str, paths: List[str]
) -> List[Attachment]:
    """Get attachments."""
    entity_keys = []

    for path in paths:
        if path.count(".") == 1:
            entity_keys.append(path)
            entity_keys.append(path.split(".")[0] + ".metadata")
    if not paths:
        return []

    query, _ = build_substitution_query(entity_keys)
    result = await query_substitution_values(query, business_key=business_key)

    attachments = []
    for key, value_ in result.items():
        if key not in entity_keys:
            continue
        if value_.startswith("\\x"):
            binary = binascii.unhexlify(value_[2:])
            entity, field = key.split(".", 1)
            if f"{entity}.metadata.{field}.name" in result:
                filename = result[f"{entity}.metadata.{field}.name"]
            else:
                filename = "attachment"
            if f"{entity}.metadata.{field}.type" in result:
                mimetype = result[f"{entity}.metadata.{field}.type"]
            else:
                mimetype = "application/octet-stream"
            attachments.append(
                Attachment(
                    data=binary,
                    filename=filename,
                    mimetype=mimetype,
                )
            )

    return attachments


def send_message(
    message: MIMEMultipart,
) -> Any:
    """Send email asynchronously in single thread thread pool executor."""

    if settings.SMTP_KEEP_BCC:
        s: smtplib.SMTP = SMTPKeepBCC(settings.SMTP_HOST, settings.SMTP_PORT)
    else:
        s = smtplib.SMTP(settings.SMTP_HOST, settings.SMTP_PORT)

    if settings.SMTP_TLS:
        s.ehlo()
        s.starttls()
        # TODO: s.login(...) if necessary

    return s.send_message(message)


async def mail_send(
    task: LockedExternalTaskDto,
    loop: Optional[asyncio.AbstractEventLoop] = None,
    worker: Optional[ThreadPoolExecutor] = SMTP_WORKER,
) -> Union[ExternalTaskComplete, ExternalTaskFailure]:  # noqa: R9014
    """Complete task."""
    assert task.businessKey
    assert task.processInstanceId

    errors: Dict[str, str] = {}

    try:
        to = [addr for addr in task_variable_list(task, "to") if addr]
    except KeyError:
        to = []
    to_recipients, result, errors_ = await get_email_addresses(to)
    errors.update(errors_)

    try:
        bcc = [addr for addr in task_variable_list(task, "bcc") if addr]
    except KeyError:
        bcc = []
    bcc_recipients, _, errors_ = await get_email_addresses(bcc)
    errors.update(errors_)

    try:
        reply_to = task_variable_str(task, "from")
    except KeyError:
        reply_to = None
    reply_to_recipients: List[str] = []
    if reply_to:
        reply_to_recipients, _, errors_ = await get_email_addresses([reply_to])
        errors.update(errors_)

    subject = task_variable_str(task, "subject")
    text = task_variable_str(task, "text")

    try:
        attachments = task_variable_list(task, "attachments")
    except KeyError:
        attachments = []
    try:
        substitutes = task_variable_list(task, "substitutes")
    except KeyError:
        substitutes = []
    templates = await safe_substitute(task, [subject, text], substitutes)
    assert len(templates) >= 2

    logger.debug("Reply-To: %s => %s", reply_to, reply_to_recipients)
    logger.debug("Subject: %s", subject)
    logger.debug("To: %s => %s", to, to_recipients)
    logger.debug("Bcc: %s => %s", bcc, bcc_recipients)
    message = create_email(
        settings.SMTP_SENDER,
        reply_to_recipients[0] if reply_to_recipients else None,
        to_recipients,
        bcc_recipients,
        templates[0],
        templates[1],
        (
            await get_entity_attachments(task.businessKey, attachments)
            + await get_process_attachments(task, attachments)
        ),
    )

    if "%{baseUrl}" in text and result.groups:
        await ensure_read_authorization(
            task.processInstanceId,
            [group.id for group in result.groups],
            ProcessResourceType.PROCESS_INSTANCE,
        )
        await ensure_read_authorization(
            task.processInstanceId,
            [group.id for group in result.groups],
            ProcessResourceType.HISTORIC_PROCESS_INSTANCE,
        )

    assert len(to_recipients + bcc_recipients) > 0, "\n".join(
        [f"No recipients: {to_recipients + bcc_recipients}", f"Errors: {errors}"]
    )
    try:
        if loop is None:
            loop = asyncio.get_event_loop()
        logger.debug(await loop.run_in_executor(worker, send_message, message))
    except smtplib.SMTPException as e:
        return ExternalTaskFailure(
            task=task,
            response=ExternalTaskFailureDto(
                workerId=task.workerId,
                errorMessage=str(e),
                errorDetails=create_debug_email(
                    settings.SMTP_SENDER,
                    reply_to_recipients[0] if reply_to_recipients else None,
                    to_recipients,
                    bcc_recipients,
                    templates[0],
                    templates[1],
                    attachments,
                ),
                retries=task.retries - 1 if task.retries is not None else RETRIES_MAX,
                retryTimeout=calculate_timeout(
                    RETRY_TIMEOUT,
                    RETRY_TIMEOUT_MAX,
                    task.retries - 1 if task.retries is not None else RETRIES_MAX,
                    RETRIES_MAX,
                ),
            ),
        )

    return ExternalTaskComplete(
        task=task, response=CompleteExternalTaskDto(workerId=task.workerId)
    )


async def get_email_addresses(
    candidates: List[str],
) -> Tuple[List[str], CamundaPrincipalsQueryData, Dict[str, str]]:
    """Resolve and validate email addresses."""
    # Short circuit
    if not candidates:
        return [], CamundaPrincipalsQueryData(users=[], groups=[]), {}

    # Resolve user ids and group ids to email addresses
    users, result = await query_camunda_principals(
        user_ids=candidates, group_ids=[name for name in candidates if "@" not in name]
    )
    logger.debug(
        "Resolved users: %s => %s", candidates, json.dumps(asdict(result), indent=2)
    )

    seen: List[str] = []
    recipients: List[str] = []
    errors: Dict[str, str] = {}

    # Resolve valid email addresses from users and add to recipients
    for user in users:
        try:
            email_validator.validate_email(user.email)
            recipients.append(f'"{user.firstName} {user.lastName}" <{user.email}>')
            seen.append(user.email)
        except email_validator.EmailNotValidError as e:
            errors[user.email] = str(e)

    # Remove all user ids and group ids from candidates and add rest to recipients
    ids = [user.id for user in result.users] + [group.id for group in result.groups]
    for addr in [addr for addr in candidates if addr not in ids]:
        try:
            descriptive, address = split_email_address(addr)
            if address not in seen:
                email_validator.validate_email(address)
                if descriptive and descriptive != address:
                    recipients.append(f'"{descriptive}" <{address}>')
                else:
                    recipients.append(address)
        except email_validator.EmailNotValidError as e:
            errors[addr] = str(e)

    return list(sorted(set(recipients))), result, errors


async def mail_validate(task: LockedExternalTaskDto) -> ExternalTaskComplete:
    """Complete task."""
    errors: Dict[str, str] = {}

    try:
        to = [addr for addr in task_variable_list(task, "to") if addr]
    except KeyError:
        to = []
    _, _, errors_ = await get_email_addresses(to)
    errors.update(errors_)

    try:
        bcc = [addr for addr in task_variable_list(task, "bcc") if addr]
    except KeyError:
        bcc = []
    _, _, errors_ = await get_email_addresses(bcc)
    errors.update(errors_)

    try:
        reply_to = task_variable_str(task, "from")
    except KeyError:
        reply_to = None
    if reply_to:
        _, _, errors_ = await get_email_addresses([reply_to])
        errors.update(errors_)

    return ExternalTaskComplete(
        task=task,
        response=CompleteExternalTaskDto(workerId=task.workerId)
        if not errors
        else ExternalTaskBpmnError(
            workerId=task.workerId,
            errorCode="email_validator.EmailNotValidError",
            errorMessage=json.dumps(errors),
        ),
    )


class SMTPKeepBCC(smtplib.SMTP):
    """Custom BCC support for smtplib.SMTP."""

    def send_message(
        self,
        msg: Message,
        from_addr: Optional[str] = None,
        to_addrs: Union[str, Sequence[str], None] = None,
        mail_options: Any = (),
        rcpt_options: Any = (),
    ) -> Any:
        """Convert message to a bytestring and passes it to sendmail.

        The arguments are as for sendmail, except that msg is an
        email.message.Message object.  If from_addr is None or to_addrs is
        None, these arguments are taken from the headers of the Message as
        described in RFC 2822 (a ValueError is raised if there is more than
        one set of 'Resent-' headers).  Regardless of the values of from_addr and
        to_addr, any Bcc field (or Resent-Bcc field, when the Message is a
        resent) of the Message object won't be transmitted.  The Message
        object is then serialized using email.generator.BytesGenerator and
        sendmail is called to transmit the message.  If the sender or any of
        the recipient addresses contain non-ASCII and the server advertises the
        SMTPUTF8 capability, the policy is cloned with utf8 set to True for the
        serialization, and SMTPUTF8 and BODY=8BITMIME are asserted on the send.
        If the server does not support SMTPUTF8, an SMTPNotSupported error is
        raised.  Otherwise the generator is called without modifying the
        policy.

        """
        # 'Resent-Date' is a mandatory field if the Message is resent (RFC 2822
        # Section 3.6.6). In such a case, we use the 'Resent-*' fields.  However,
        # if there is more than one 'Resent-' block there's no way to
        # unambiguously determine which one is the most recent in all cases,
        # so rather than guess we raise a ValueError in that case.
        #
        # TODO implement heuristics to guess the correct Resent-* block with an
        # option allowing the user to enable the heuristics.  (It should be
        # possible to guess correctly almost all of the time.)
        import copy
        import email
        import io

        self.ehlo_or_helo_if_needed()
        resent = msg.get_all("Resent-Date")
        if resent is None:
            header_prefix = ""
        elif len(resent) == 1:
            header_prefix = "Resent-"
        else:
            raise ValueError("message has more than one 'Resent-' header block")
        if from_addr is None:
            # Prefer the sender field per RFC 2822:3.6.2.
            from_addr = (
                msg[header_prefix + "Sender"]
                if (header_prefix + "Sender") in msg
                else msg[header_prefix + "From"]
            )
            from_addr = email.utils.getaddresses([from_addr])[0][1]
        if to_addrs is None:
            addr_fields = [
                f
                for f in (
                    msg[header_prefix + "To"],
                    msg[header_prefix + "Bcc"],
                    msg[header_prefix + "Cc"],
                )
                if f is not None
            ]
            to_addrs = [a[1] for a in email.utils.getaddresses(addr_fields)]
        # Make a local copy so we can delete the bcc headers.
        msg_copy = copy.copy(msg)
        # del msg_copy['Bcc']
        # del msg_copy['Resent-Bcc']
        international = False
        try:
            "".join([from_addr, *to_addrs]).encode("ascii")
        except UnicodeEncodeError:
            if not self.has_extn("smtputf8"):
                raise smtplib.SMTPNotSupportedError(
                    "One or more source or delivery addresses require"
                    " internationalized email support, but the server"
                    " does not advertise the required SMTPUTF8 capability"
                )
            international = True
        with io.BytesIO() as bytesmsg:
            if international:
                g = email.generator.BytesGenerator(
                    bytesmsg, policy=msg.policy.clone(utf8=True)
                )
                mail_options = (*mail_options, "SMTPUTF8", "BODY=8BITMIME")
            else:
                g = email.generator.BytesGenerator(bytesmsg)
            g.flatten(msg_copy, linesep="\r\n")
            flatmsg = bytesmsg.getvalue()
        # logger.debug([from_addr, to_addrs, flatmsg, mail_options, rcpt_options])
        return self.sendmail(from_addr, to_addrs, flatmsg, mail_options, rcpt_options)
