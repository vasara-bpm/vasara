"""Service types."""
from pydantic import BaseModel
from typing import List


VASARA_ENTITY_QUERY = """\
query(
  $businessKey: String!,
){
  vasara_entity(where: {
    business_key: {_eq: $businessKey }
  }) {
    id
    business_key
  }
}
"""


class VasaraEntityQueryVariables(BaseModel):
    """Vasara entity query by business key."""

    businessKey: str


class VasaraEntityQuery(BaseModel):
    """Vasara entity query by business key."""

    query: str = VASARA_ENTITY_QUERY
    variables: VasaraEntityQueryVariables


class VasaraEntity(BaseModel):
    """Vasara entity identification details."""

    id: str
    business_key: str


class VasaraEntityQueryData(BaseModel):
    """Vasara entity query data."""

    vasara_entity: List[VasaraEntity]


class VasaraEntityQueryResponse(BaseModel):
    """Vasara entity query response."""

    data: VasaraEntityQueryData


VASARA_ENTITY_DELETE = """\
mutation(
  $id: uuid!,
){
  delete_vasara_entity_by_pk(id: $id) {
    id
    business_key
  }
}
"""


class VasaraEntityDeleteVariables(BaseModel):
    """Vasara entity delete by primary key."""

    id: str


class VasaraEntityDelete(BaseModel):
    """Vasara entity delete by primary key."""

    query: str = VASARA_ENTITY_DELETE
    variables: VasaraEntityDeleteVariables


class VasaraEntityDeleteData(BaseModel):
    """Vasara entity mutation data."""

    delete_vasara_entity_by_pk: VasaraEntity


class VasaraEntityDeleteResponse(BaseModel):
    """Vasara entity delete response."""

    data: VasaraEntityDeleteData
