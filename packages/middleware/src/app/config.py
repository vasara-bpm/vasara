"""Settings and logging for the application."""
from app.camunda.config import Settings as CamundaSettings
from app.entity.config import Settings as EntitySettings
from app.purge.config import Settings as PurgeSettings
from app.transient.config import Settings as TransientUserSettings
from asyncio import Task
from typing import Dict
from typing import Optional
from typing import Union
import asyncio
import logging


# https://pydantic-docs.helpmanual.io/usage/settings/
class Settings(  # noqa: R0901
    CamundaSettings, EntitySettings, PurgeSettings, TransientUserSettings
):
    """App configuration settings."""

    class Config:
        env_file = ".env"

    APP_NAME: str = "vasara-middleware"
    LOG_LEVEL: str = "DEBUG"

    CAMUNDA_REST_ENDPOINT: str
    CAMUNDA_REST_AUTHORIZATION: str
    CAMUNDA_GRAPHQL_ENDPOINT: str
    CAMUNDA_GRAPHQL_AUTHORIZATION: str

    CAMUNDA_TIMEOUT: int = 20
    CAMUNDA_POLL_TTL: int = 10
    CAMUNDA_LOCK_TTL: int = 30

    AIOHTTP_TIMEOUT: int = 10

    VAULT_TOKEN: str
    VAULT_ADDR: str
    VAULT_TIMEOUT: int = 10

    VAULT_TRANSIT_DEFAULT_PATH: str = "transit/default"
    VAULT_TRANSIT_DEFAULT_KEY: str = "test1"
    VAULT_TRANSIT_PII_SENSITIVE_PATH: str = "transit/default"
    VAULT_TRANSIT_PII_SENSITIVE_KEY: str = "test1"
    VAULT_TRANSIT_PII_RESTRICTED_PATH: str = "transit/default"
    VAULT_TRANSIT_PII_RESTRICTED_KEY: str = "test1"
    VAULT_TRANSIT_DATA_RESTRICTED_PATH: str = "transit/default"
    VAULT_TRANSIT_DATA_RESTRICTED_KEY: str = "test1"

    VASARA_GRAPHQL_ENDPOINT: str
    VASARA_GRAPHQL_ADMIN_SECRET: str
    VASARA_GRAPHQL_TIMEOUT: int = 60

    SMTP_HOST: str = "localhost"
    SMTP_PORT: int = 1025
    SMTP_TLS: bool = False
    SMTP_SENDER: str = "JYU Vasara <nobody@jyu.fi>"
    SMTP_KEEP_BCC: bool = False

    SUBSTITUTION_BASE_URL: str = "http://localhost:3000"

    EMCEE_TIMEOUT: int = 10

    OIDC_PROVIDER_URI: str
    OIDC_CLIENT_ID: str
    OIDC_CLIENT_SECRET: str
    OIDC_REDIRECT_URI: str
    OIDC_USERNAME_CLAIM: str
    OIDC_SCOPE: str = "openid profile email groups roles"
    OIDC_EXPIRY_SECONDS: int = 60 * 60 * 12

    DEFAULT_PRINCIPAL_NAME_DOMAIN: str = "jyu.fi"

    S3_BUCKET_NAME: str
    S3_ENDPOINT_URL: str
    S3_ACCESS_KEY: str
    S3_SECRET_KEY: str
    S3_EXPIRY_SECONDS: int = 60


settings = Settings()


class MasterOfCeremonies:
    """Master of ceremonies to manage all app events."""

    events: Dict[str, asyncio.Event]
    attendees: Dict[str, int]

    def __init__(self) -> None:
        """Init."""
        self.events = {}
        self.attendees = {}
        self.history: Dict[str, bool] = {}

    async def wait(
        self, key: str, timeout: Union[int, float] = settings.EMCEE_TIMEOUT
    ) -> bool:
        """Wait for event to happen."""
        task: Optional[Task[bool]] = None
        if key in self.history:
            return True
        if key not in self.events:
            self.events[key] = asyncio.Event()
        try:
            self.attendees.setdefault(key, 0)
            self.attendees[key] += 1
            task = asyncio.create_task(self.events[key].wait())
            done, pending = await asyncio.wait({task}, timeout=timeout)
            assert not pending, f"Task {task} unexpectedly not done."
        except AssertionError:
            done = set()
        finally:
            if task and not task.done():
                task.cancel()
            del task
            self.attendees[key] -= 1
            if self.attendees[key] < 1:
                del self.attendees[key]
                del self.events[key]
        return bool(done)

    def set(self, key: str) -> None:
        """Notify all event attendees."""
        if key in self.events:
            self.events[key].set()
        self.history[key] = True
        asyncio.get_event_loop().call_later(
            settings.EMCEE_TIMEOUT, self.history.pop, key, None
        )


emcee = MasterOfCeremonies()


formatter = logging.Formatter(
    "%(asctime)s | %(levelname)s | %(name)s:%(lineno)d | %(message)s",
    "%d-%m-%Y %H:%M:%S",
)

stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
stream_handler.setLevel(settings.LOG_LEVEL)

logger = logging.getLogger("app")
logger.addHandler(stream_handler)
logger.setLevel(settings.LOG_LEVEL)
logger.propagate = False

__all__ = ["emcee", "logger", "settings"]
