"""OIDC type definitions."""
from dataclasses import field
from datetime import datetime
from datetime import timedelta
from pydantic.dataclasses import dataclass


@dataclass
class OIDCAuthorizationGrantRequest:
    """OIDC authorization code grant request."""

    grant_type: str
    scope: str


@dataclass
class OIDCAuthorizationCodeResponse:
    """OIDC authorization code response with access token."""

    access_token: str
    expires_in: int
    scope: str
    token_type: str
    created: datetime = field(default_factory=datetime.utcnow)

    @property
    def valid(self) -> bool:
        """Is access token still valid."""
        now = datetime.utcnow()
        expiration = self.created + timedelta(seconds=self.expires_in - 300)
        return now < expiration
