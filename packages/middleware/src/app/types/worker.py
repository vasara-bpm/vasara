"""External task worker type definitions."""
from app.camunda.config import Topic as CamundaTopic
from app.entity.config import Topic as EntityTopic
from app.purge.config import Topic as PurgeTopic
from app.transient.config import Topic as TransientUserTopic
from app.types.camunda import CompleteExternalTaskDto
from app.types.camunda import ExternalTaskBpmnError
from app.types.camunda import ExternalTaskFailureDto
from app.types.camunda import LockedExternalTaskDto
from enum import Enum
from pydantic import BaseModel
from typing import Any
from typing import Awaitable
from typing import Callable
from typing import Union


class NoOp(BaseModel):
    """Do nothing."""


class ExternalTaskComplete(BaseModel):
    """Completed external task and its response."""

    def __init__(self, **data: Any) -> None:
        """Init."""
        super().__init__(**data)
        if any(
            [
                isinstance(data.get("response"), NoOp),
                isinstance(data.get("response"), CompleteExternalTaskDto),
                isinstance(data.get("response"), ExternalTaskBpmnError),
            ]
        ):
            # https://github.com/samuelcolvin/pydantic/issues/1423
            self.response = data["response"]

    task: LockedExternalTaskDto
    response: Union[CompleteExternalTaskDto, ExternalTaskBpmnError]


class ExternalTaskFailure(BaseModel):
    """Failed external task and its response."""

    task: LockedExternalTaskDto
    response: ExternalTaskFailureDto


ExternalTaskHandler = Callable[
    [LockedExternalTaskDto], Awaitable[Union[ExternalTaskComplete, ExternalTaskFailure]]
]


class DefaultTopic(str, Enum):
    """External task topics."""

    APP_HEARTBEAT = "vasara.middleware.heartbeat"

    CAMUNDA_HISTORY_VARIABLES_DELETE = "camunda.history.variables.delete"
    CAMUNDA_MAIL_SEND = "mail-send"  # for respect to mail-send-connector

    VASARA_MAIL_SEND = "vasara.mail-send"
    VASARA_MAIL_VALIDATE = "vasara.mail-validate"
    VASARA_SUBSTITUTE = "vasara.substitute"
    VASARA_PURGE_RUNTIME = "vasara.purge.runtime"


# Union of all supported topics (enum)
TOPIC = Union[CamundaTopic, DefaultTopic, EntityTopic, PurgeTopic, TransientUserTopic]
