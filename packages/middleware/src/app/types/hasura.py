"""Hasura action types."""
from pydantic import BaseModel
from pydantic import Field
from pydantic.generics import GenericModel
from typing import Any
from typing import Dict
from typing import Generic
from typing import List
from typing import Optional
from typing import TypeVar
from typing import Union


Payload = TypeVar("Payload")


class HasuraErrorExtensions(BaseModel):
    """Hasura error extensions."""

    path: Optional[str] = ""
    code: Optional[str] = ""


class HasuraError(BaseModel):
    """Hasura error message."""

    extensions: Optional[HasuraErrorExtensions] = None
    message: Optional[str] = ""


class HasuraErrorResponse(BaseModel):
    """Hasura error response."""

    errors: List[HasuraError]


class HasuraAction(BaseModel):
    """Hasura action metadata."""

    name: str = Field(title="Action name")


class HasuraAuthenticatedSessionVariables(BaseModel):
    """Hasura default session variables."""

    x_hasura_user_id: str = Field(
        title="Hasura authenticated user id", alias="x-hasura-user-id"
    )
    x_hasura_role: str = Field(
        title="Hasura authenticated user role", alias="x-hasura-role"
    )
    x_hasura_principals: Optional[str] = Field(  # is {postgres,array}
        title="Hasura authenticated user principals", alias="x-hasura-principals"
    )


class HasuraActionRequest(GenericModel, Generic[Payload]):
    """Hasura action request."""

    input: Payload
    action: HasuraAction


class HasuraAuthenticatedActionRequest(GenericModel, Generic[Payload]):
    """Hasura action request."""

    session_variables: HasuraAuthenticatedSessionVariables
    input: Payload
    action: HasuraAction


class HasuraAnonymousSessionVariables(BaseModel):
    """Hasura default session variables."""

    x_hasura_role: str = Field(
        title="Hasura authenticated user role", alias="x-hasura-role"
    )


class HasuraAnonymousActionRequest(GenericModel, Generic[Payload]):
    """Hasura action request."""

    session_variables: HasuraAnonymousSessionVariables
    input: Payload
    action: HasuraAction


USER_TASK_FORM_SOURCE_QUERY = """\
query(
  $principals: [String!]!,
  $processDefinitionKeys: [String!]!,
  $sources: [String!]!,
  $maxProcessDefinitionVersion: Int!
){
  vasara_user_task_form_source(where: {
    process_definition_key: {_in: $processDefinitionKeys},
    process_definition_version: {_lte: $maxProcessDefinitionVersion},
    source: {_in: $sources}
  },
  order_by: {
    process_definition_version: desc
  }) {
    process_definition_key
    process_definition_version
    process_definition_version_tag
    user_task_id
    source
  }
  vasara_authorization(where: {
    principal: {_in: $principals},
    resource: {_in: $sources}
  }) {
    principal
    can_insert
    can_select
    can_update
    can_delete
    resource
    specifier
  }
}
"""


class UserTaskFormSourcesQueryVariables(BaseModel):
    """User task form sources query variables."""

    principals: List[str] = Field(title="Authenticated user principals")
    processDefinitionKeys: List[str] = Field(title="Allowed process definition keys")
    sources: List[str] = Field(title="Allowed user task form source bindings")
    maxProcessDefinitionVersion: int = Field(title="Maximum process definition version")


class UserTaskFormSource(BaseModel):
    """User task form source."""

    process_definition_key: str = Field(title="Process definition key")
    process_definition_version: int = Field(title="Process definition version")
    process_definition_version_tag: Optional[str] = Field(
        title="Process definition version tag"
    )
    user_task_id: str = Field(title="User task definition key")
    source: str = Field(title="User task form field source binding")


class Authorization(BaseModel):
    """Vasara ACL row."""

    principal: str = Field(title="Principal")
    can_insert: bool = Field(title="Can insert value")
    can_select: bool = Field(title="Can select value")
    can_update: bool = Field(title="Can update value")
    can_delete: bool = Field(title="Can delete value")
    resource: str = Field(title="Affected resource")
    specifier: Optional[str] = Field(title="Resource specifier")


class UserTaskFormSourcesQuery(BaseModel):
    """User task form sources query."""

    query: str = USER_TASK_FORM_SOURCE_QUERY
    variables: UserTaskFormSourcesQueryVariables


class UserTaskFormSourcesQueryData(BaseModel):
    """User task form sources response data."""

    vasara_user_task_form_source: List[UserTaskFormSource]
    vasara_authorization: List[Authorization]


class UserTaskFormSourcesQueryResponse(BaseModel):
    """User task form sources response."""

    data: UserTaskFormSourcesQueryData


CAMUNDA_PRINCIPALS_QUERY = """\
query($userIds: [String!]!, $groupIds: [String!]!) {
  users:camunda_Users(ids: $userIds) {
    id
    email
    firstName
    lastName
  }
  groups: camunda_Groups(ids: $groupIds) {
    id
    name
    members {
      id
      email
      firstName
      lastName
    }
  }
}"""


class CamundaPrincipalsQueryVariables(BaseModel):
    """Camunda principals query variables."""

    userIds: List[str] = Field(title="User ids to query")
    groupIds: List[str] = Field(title="Group ids to query")


class CamundaUser(BaseModel):
    """Camunda user."""

    id: str = Field(title="User id")
    email: str = Field(title="Email address")
    firstName: str = Field(title="First name")
    lastName: str = Field(title="Last name")


class CamundaGroup(BaseModel):
    """Camunda group."""

    id: str = Field(title="Group id")
    name: str = Field(title="Group name")
    members: List[CamundaUser] = Field(title="Group members")


class CamundaPrincipalsQuery(BaseModel):
    """Camunda principals query."""

    query: str = CAMUNDA_PRINCIPALS_QUERY
    variables: CamundaPrincipalsQueryVariables


class CamundaPrincipalsQueryData(BaseModel):
    """Camunda principals response data."""

    groups: List[CamundaGroup]
    users: List[CamundaUser]


class CamundaPrincipalsQueryResponse(BaseModel):
    """Camunda principals response."""

    data: CamundaPrincipalsQueryData


class SubstitutionValuesQueryVariables(BaseModel):
    """Substitution values query variables."""

    businessKey: str = Field(title="Substitution entities business key")


class SubstitutionValuesQuery(BaseModel):
    """Substitution values query."""

    query: str = Field(title="Substitution values query")
    variables: Union[SubstitutionValuesQueryVariables, Dict[str, str]]


class SubstitutionValuesQueryResponse(BaseModel):
    """Substitution values response."""

    data: Dict[str, Any]
