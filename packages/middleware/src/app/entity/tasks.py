"""Service external tasks."""
from app.config import settings
from app.entity.config import Topic
from app.entity.utils import mutate_entity_variables
from app.entity.utils import query_raw_values
from app.services.camunda import camunda_session
from app.services.substitute import build_substitution_query
from app.services.substitute import execute_query
from app.services.substitute import ID_QUERY_ALLOW_PREFIX
from app.types.camunda import CompleteExternalTaskDto
from app.types.camunda import LockedExternalTaskDto
from app.types.camunda import ValueType
from app.types.camunda import VariableValueDto
from app.types.worker import ExternalTaskComplete
from app.utils import verify_response_status
from mimetypes import guess_type
from typing import Any
from typing import Dict
import base64
import binascii
import datetime
import json
import re


def dict_to_dotted_values(
    variables: Dict[str, Any], prefix: str = ""
) -> Dict[str, Any]:
    """Flatten variables dictionary."""
    result = {}
    for name, value in variables.items():
        result[f"{prefix}{name}"] = value
        if isinstance(value, dict):
            result.update(dict_to_dotted_values(value, prefix=f"{prefix}{name}."))
            if "id" in value:
                result.update(
                    dict_to_dotted_values(
                        value, prefix=f'{prefix}{name}[{value["id"]}].'
                    )
                )
        elif isinstance(value, list) and len(value) > 0:
            if isinstance(value[0], dict):
                result.update(
                    dict_to_dotted_values(value[0], prefix=f"{prefix}{name}.")
                )
                for idx in range(len(value)):
                    result[f"{prefix}{name}[{idx}]"] = value[idx]
                    result.update(
                        dict_to_dotted_values(
                            value[idx], prefix=f"{prefix}{name}[{idx}]."
                        )
                    )
                    if "id" in value[idx]:
                        result[f'{prefix}{name}[{value[idx]["id"]}]'] = value[idx]
                        result.update(
                            dict_to_dotted_values(
                                value[idx],
                                prefix=f'{prefix}{name}[{value[idx]["id"]}].',
                            )
                        )
            else:
                for idx in range(len(value)):
                    result[f"{prefix}{name}[{idx}]"] = value[idx]
    return result


async def load_from_entity_to_process(
    task: LockedExternalTaskDto,
) -> ExternalTaskComplete:
    """Complete task."""
    assert task.businessKey

    mapping = {}
    variables = {}

    for name, value in (task.variables or {}).items():
        name = name.strip() if name else ""
        if name and (
            value.type == ValueType.String
            and value.value
            and len([v for v in value.value.split(".") if v.strip()]) >= 2
        ):
            mapping[value.value] = name
            if not any(
                [value.value.startswith(prefix) for prefix in ID_QUERY_ALLOW_PREFIX]
            ):
                mapping[value.value.split(".")[0] + ".metadata"] = ""
            variables[name] = VariableValueDto(value=None, type=ValueType.Null)
    assert mapping, "No variables configured"

    query, variables_ = build_substitution_query(list(mapping.keys()))
    if "businessKey" in variables_:
        variables_["businessKey"] = task.businessKey
    response = await execute_query(query, variables_)
    result = dict_to_dotted_values(response.data)

    for key, name in mapping.items():
        if re.sub(r"['\"]", "", key) not in result or not name:
            continue
        value_ = result[re.sub(r"['\"]", "", key)]
        if isinstance(value_, dict) or isinstance(value_, list):
            variables[name] = VariableValueDto(
                value=json.dumps(value_),
                type=ValueType.Json,
            )
        elif value_ is None:
            variables[name] = VariableValueDto(value=None, type=ValueType.Null)
        elif isinstance(value_, bool):
            variables[name] = VariableValueDto(value=value_, type=ValueType.Boolean)
        elif isinstance(value_, int):
            variables[name] = VariableValueDto(value=value_, type=ValueType.Integer)
        elif isinstance(value_, float):
            variables[name] = VariableValueDto(value=value_, type=ValueType.Double)
        elif isinstance(value_, str) and value_.startswith("\\x"):
            encoded_value = binascii.unhexlify(value_[2:])
            value_info = {"encoding": "utf-8"}
            value_type = ValueType.Bytes
            entity, field = key.split(".", 1)
            if f"{entity}.metadata.{field}.name" in result:
                value_info["filename"] = result[f"{entity}.metadata.{field}.name"]
                value_type = ValueType.File
            if f"{entity}.metadata.{field}.type" in result:
                value_info["mimeType"] = result[f"{entity}.metadata.{field}.type"]
                value_info["mimetype"] = result[f"{entity}.metadata.{field}.type"]
                value_type = ValueType.File
            variables[name] = VariableValueDto(
                value=base64.b64encode(encoded_value),
                type=value_type,
                valueInfo=value_info,
            )
        elif isinstance(value_, str):
            try:
                # Fix value to be ISO 8601 compliant in Python datetime way
                value__ = re.sub(r"(\.\d{0,3})\d*", "\\1", value_)
                dt = datetime.datetime.fromisoformat(value__[:23] + value__[-6:])
                variables[name] = VariableValueDto(
                    value=dt.strftime("%Y-%m-%dT%H:%M:%S.%f")[:23]
                    + (dt.strftime("%z") or "+0000"),
                    type=ValueType.Date,
                )
            except ValueError:
                variables[name] = VariableValueDto(value=value_, type=ValueType.String)

    return ExternalTaskComplete(
        task=task,
        response=CompleteExternalTaskDto(workerId=task.workerId, variables=variables),
    )


async def save_from_process_to_entity(  # noqa: R0914
    task: LockedExternalTaskDto,
) -> ExternalTaskComplete:
    """Complete task."""
    assert task.businessKey

    variables_per_entity: Dict[str, Any] = {}
    metadata_fields = {}
    for var_name, var_value in (task.variables or {}).items():
        name_parts = [v for v in var_name.split(".") if v.strip()]
        if len(name_parts) != 2:
            continue
        entity_name = name_parts[0]
        variable_name = name_parts[1]

        metadata_fields[f"{entity_name}.metadata"] = False
        if entity_name not in variables_per_entity:
            variables_per_entity[entity_name] = {}
        variables_per_entity[entity_name][variable_name] = var_value

    assert variables_per_entity, "No variables configured"

    # process can optionally define an author_id variable that is set
    # to the editor_id and author_id fields of entities (author_id only if inserting)
    author_id = (
        task.variables["author_id"].value
        if task.variables is not None and "author_id" in task.variables
        else None
    )

    # get old metadata so we don't blindly overwrite it when saving a file
    # and to check if row already exists so we can decide whether to insert or update
    old_metadata_query, _ = build_substitution_query(list(metadata_fields.keys()))
    old_metadata = await query_raw_values(old_metadata_query, task.businessKey)

    gql_inputs = []
    for ent in variables_per_entity:
        if ent in old_metadata:
            gql_inputs.append(f"${ent}: {ent}_set_input!")
        else:
            gql_inputs.append(f"${ent}: {ent}_insert_input!")

    gql_mutations = []
    gql_variables: Dict[str, Any] = {}

    for entity, variables in variables_per_entity.items():
        gql_variables[entity] = {}
        if entity in old_metadata:
            gql_mutations.append(
                f"""update_{entity} (where: {{ business_key: {{ _eq: "{task.businessKey}" }}}}, _set: ${entity}) {{
                    returning {{ id business_key }}
                }}"""
            )
            if author_id:
                gql_variables[entity]["editor_id"] = author_id
        else:
            gql_mutations.append(
                f"insert_{entity}_one (object: ${entity}) {{ id business_key }}"
            )
            gql_variables[entity]["business_key"] = task.businessKey
            if author_id:
                gql_variables[entity]["editor_id"] = author_id
                gql_variables[entity]["author_id"] = author_id

        for var_name, var_value in variables.items():
            if var_value.type == ValueType.Json:
                gql_variables[entity][var_name] = json.loads(f"{var_value.value}")
            elif var_value.type != ValueType.File:
                gql_variables[entity][var_name] = var_value.value
            else:
                async with camunda_session() as http:
                    var_full_name = f"{entity}.{var_name}"
                    url = f"{settings.CAMUNDA_REST_ENDPOINT}/execution/{task.executionId}/localVariables/{var_full_name}/data"
                    get = await http.get(
                        url, headers={"Accept": "application/octet-stream"}
                    )
                    await verify_response_status(get, status=(200,))
                    data = await get.read()
                    gql_variables[entity][var_name] = "\\x" + binascii.hexlify(
                        data
                    ).decode("utf-8")
                if "metadata" in gql_variables[entity]:
                    metadata = gql_variables[entity]["metadata"] or {}
                else:
                    metadata = (old_metadata.get(entity) or {}).get("metadata") or {}
                # ensure metadata exists and has correct type
                if not isinstance(metadata.get(var_name), dict):
                    metadata[var_name] = {}
                metadata[var_name]["size"] = len(data)
                if var_value.valueInfo:
                    filename = var_value.valueInfo.get("filename")
                    if filename:
                        metadata[var_name]["name"] = filename
                    mime_type = var_value.valueInfo.get(
                        "mimeType"
                    ) or var_value.valueInfo.get("mimetype")
                    if not mime_type:
                        mime_type = guess_type(filename)[0] or "text/plain"
                    metadata[var_name]["type"] = mime_type
                gql_variables[entity]["metadata"] = metadata

            # if a Null variable was previously a file, i.e. has associated info
            # in the metadata field, remove that metadata
            if (
                var_value.type == ValueType.Null
                and entity in old_metadata
                and "metadata" in old_metadata[entity]
                and var_name in old_metadata[entity]["metadata"]
            ):
                if "metadata" in gql_variables[entity]:
                    pruned_metadata = {
                        k: v
                        for k, v in gql_variables[entity]["metadata"].items()
                        if k != var_name
                    }
                else:
                    pruned_metadata = {
                        k: v
                        for k, v in old_metadata[entity]["metadata"].items()
                        if k != var_name
                    }
                gql_variables[entity]["metadata"] = pruned_metadata

    gql_inputs_ = ", ".join(gql_inputs)
    gql_mutations_ = "\n".join(gql_mutations)
    mutate_query = f"""mutation ({gql_inputs_}) {{
        {gql_mutations_}
    }}"""
    await mutate_entity_variables(mutate_query, gql_variables)

    return ExternalTaskComplete(
        task=task,
        response=CompleteExternalTaskDto(workerId=task.workerId),
    )


TASKS = {
    Topic.VASARA_ENTITY_LOAD: load_from_entity_to_process,
    Topic.VASARA_ENTITY_SAVE: save_from_process_to_entity,
}
