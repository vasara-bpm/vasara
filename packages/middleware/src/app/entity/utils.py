"""Entity related utilities."""
from app.config import logger
from app.config import settings
from app.entity.types import EntityMutationQuery
from app.services.hasura import hasura_session
from app.types.hasura import SubstitutionValuesQuery
from app.types.hasura import SubstitutionValuesQueryResponse
from app.types.hasura import SubstitutionValuesQueryVariables
from app.utils import asdict
from app.utils import asjson
from typing import Any
from typing import Dict


async def query_raw_values(query: str, business_key: str) -> Dict[str, Any]:
    """Query entity substitution values."""
    async with hasura_session() as http:
        url = settings.VASARA_GRAPHQL_ENDPOINT
        query_ = SubstitutionValuesQuery(
            query=query,
            variables=SubstitutionValuesQueryVariables(businessKey=business_key),
        )
        post = await http.post(url, data=asjson(query_))
        if post.status not in (200,):
            logger.info(await post.json())
            return {}
        data = await post.json()
        logger.debug(data)
        response = SubstitutionValuesQueryResponse(**data)
        return {
            key: response.data[key][0]
            for key in response.data.keys()
            if response.data[key]
        }


async def mutate_entity_variables(mutation: str, variables: Dict[str, Any]) -> None:
    """Query entity substitution values."""
    async with hasura_session() as http:
        url = settings.VASARA_GRAPHQL_ENDPOINT
        query_ = EntityMutationQuery(query=mutation, variables=variables)
        logger.debug(asdict(query_))
        post = await http.post(url, data=asjson(query_))
        assert (
            post.status == 200
        ), f"Unexpected status {post.status} for mutation {mutation}"
        data = await post.json()
        logger.debug(data)
        assert "errors" not in data, f"Error {data['errors']} for mutation {mutation}"
