"""Vault Transit REST API helpers."""
from aiohttp import ClientTimeout
from app.config import settings
from app.utils import asjson
from app.utils import verify_response_status
from contextlib import asynccontextmanager
from multidict import CIMultiDict
from pydantic.dataclasses import dataclass
from typing import Any
from typing import AsyncGenerator
from typing import Optional
import aiohttp
import base64
import re


@asynccontextmanager
async def vault_session(
    extra_headers: Optional[CIMultiDict[str]] = None,
) -> AsyncGenerator[aiohttp.ClientSession, None]:
    """Get aiohttp session with Vault headers."""
    headers = CIMultiDict(
        {
            "Content-Type": "application/json",
            "Accept": "application/json",
            "X-Vault-Request": "true",
            "X-Vault-Token": settings.VAULT_TOKEN,
        }
    )
    if extra_headers:
        headers.update(extra_headers)
    async with aiohttp.ClientSession(
        headers=headers,
        trust_env=True,
        timeout=ClientTimeout(total=settings.VAULT_TIMEOUT),
    ) as session:
        yield session


@dataclass
class TransitEncryptRequest:
    """Vault transit encryption request."""

    plaintext: bytes


@dataclass
class TransitEncryptResponseData:
    """Vault transit encryption data."""

    ciphertext: str
    key_version: Optional[int]


@dataclass
class TransitEncryptResponse:
    """Vault transit encryption response."""

    request_id: str
    lease_id: str
    renewable: bool
    lease_duration: int
    data: TransitEncryptResponseData
    wrap_info: Optional[Any]
    warnings: Optional[Any]
    auth: Optional[Any]


async def encrypt_text(
    plaintext: str,
    transit_path: str = settings.VAULT_TRANSIT_DEFAULT_PATH,
    transit_key: str = settings.VAULT_TRANSIT_DEFAULT_KEY,
    extra_headers: Optional[CIMultiDict[str]] = None,
) -> str:
    """Return encrypted ciphertext for plain text."""
    return await encrypt_base64(
        base64.b64encode(plaintext.encode("utf-8")),
        transit_path,
        transit_key,
        extra_headers,
    )


async def encrypt_base64(
    plaintext: bytes,
    transit_path: str = settings.VAULT_TRANSIT_DEFAULT_PATH,
    transit_key: str = settings.VAULT_TRANSIT_DEFAULT_KEY,
    extra_headers: Optional[CIMultiDict[str]] = None,
) -> str:
    """Return encrypted ciphertext string for base64 encoded binary."""
    async with vault_session(extra_headers) as http:
        url = f"{settings.VAULT_ADDR}/v1/{transit_path}" f"/encrypt/{transit_key}"
        data = TransitEncryptRequest(plaintext=plaintext)  # type: ignore
        put = await http.put(url, data=asjson(data))
        await verify_response_status(put, status=(200,))
        transit = TransitEncryptResponse(**await put.json())
        return transit.data.ciphertext


@dataclass
class TransitDecryptRequest:
    """Vault transit decryption request."""

    ciphertext: bytes


@dataclass
class TransitDecryptResponseData:
    """Vault transit decrypted data."""

    plaintext: str


@dataclass
class TransitDecryptResponse:
    """Vault transit decryption response."""

    request_id: str
    lease_id: str
    renewable: bool
    lease_duration: int
    data: TransitDecryptResponseData
    wrap_info: Optional[Any]
    warnings: Optional[Any]
    auth: Optional[Any]


async def decrypt_base64(
    ciphertext: bytes,
    transit_path: str = settings.VAULT_TRANSIT_DEFAULT_PATH,
    transit_key: str = settings.VAULT_TRANSIT_DEFAULT_KEY,
    extra_headers: Optional[CIMultiDict[str]] = None,
) -> bytes:
    """Decrypt vault ciphertext into base64 encoded binary."""
    async with vault_session(extra_headers) as http:
        url = f"{settings.VAULT_ADDR}/v1/{transit_path}" f"/decrypt/{transit_key}"
        data = TransitDecryptRequest(ciphertext=ciphertext)  # type: ignore
        put = await http.put(url, data=asjson(data))
        await verify_response_status(put, status=(200,))
        transit = TransitDecryptResponse(**await put.json())
        return transit.data.plaintext.encode("utf-8")


VAULT_PATH_KEY_VERSION_CIPHERTEXT = re.compile(r"^vault:([^:]+):([^:]+):v[0-9]+:.*")


async def decrypt_text(
    ciphertext: str,
    transit_path: str = settings.VAULT_TRANSIT_DEFAULT_PATH,
    transit_key: str = settings.VAULT_TRANSIT_DEFAULT_KEY,
    extra_headers: Optional[CIMultiDict[str]] = None,
) -> str:
    """Decrypt vault ciphertext into decoded text."""

    # Support custom ciphertext format with path and key included in ciphertext
    for path, key in VAULT_PATH_KEY_VERSION_CIPHERTEXT.findall(ciphertext) or []:
        transit_path = path
        transit_key = key
        prefix_length = len(f"vault:{path}:{key}:")
        ciphertext = "vault:" + ciphertext[prefix_length:]
        break

    plaintext = await decrypt_base64(
        ciphertext.encode("utf-8"), transit_path, transit_key, extra_headers
    )
    return base64.b64decode(plaintext).decode("utf-8")
