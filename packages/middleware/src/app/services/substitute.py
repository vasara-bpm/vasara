"""Data based string substitutions."""
from app.config import settings
from app.services.hasura import hasura_session
from app.types.camunda import LockedExternalTaskDto
from app.types.camunda import ValueType
from app.types.camunda import VariableValueDto
from app.types.hasura import SubstitutionValuesQuery
from app.types.hasura import SubstitutionValuesQueryResponse
from app.types.hasura import SubstitutionValuesQueryVariables
from app.utils import asdict
from app.utils import asjson
from string import Template
from typing import Any
from typing import Dict
from typing import List
from typing import Set
from typing import Tuple
import json
import logging
import re


logger = logging.getLogger(__name__)

RE_DATE = re.compile(r"^(\d{4})-(\d{2})-(\d{2})")

RE_KEY = re.compile(r"^[a-zA-Z0-9'_\-\[\]\"\.]+$")

ID_QUERY_ALLOW_PREFIX = [
    "vocabulary_",
    "datahub_",
]


class DottedTemplate(Template):
    """Template with dotted identifiers."""

    delimiter = "%"
    idpattern = r"(?a:[_a-z][_a-z0-9\.\[\]]*)"


def dict_to_substitutions(
    variables: Dict[str, Any], prefix: str = ""
) -> Dict[str, str]:
    """Flatten variables dictionary."""
    result = {}
    for name, value in variables.items():
        if isinstance(value, str):
            result[f"{prefix}{name}"] = (
                value.split("T")[0] if RE_DATE.match(value) else value
            )
        elif isinstance(value, bool):
            result[f"{prefix}{name}"] = "Kyllä / Yes" if value else "Ei / No"
        elif isinstance(value, int):
            result[f"{prefix}{name}"] = str(value)
        elif isinstance(value, float):
            result[f"{prefix}{name}"] = str(value)
        elif value is None:
            result[f"{prefix}{name}"] = ""
        elif isinstance(value, dict):
            if name != "_":
                result.update(dict_to_substitutions(value, prefix=f"{prefix}{name}."))
                if "id" in value:
                    result.update(
                        dict_to_substitutions(
                            value, prefix=f'{prefix}{name}[{value["id"]}].'
                        )
                    )
            results = []
            for key in (
                [k for k in value["@order"] if k in value]
                if "@order" in value
                else sorted(value)
            ):
                if prefix.endswith(": "):
                    result_ = f"{prefix}{key}: " + f"\n{prefix}{key}: ".join(
                        dict_to_substitutions(
                            {"_": value[key]}, prefix=f"{key}: "
                        ).values()
                    )
                elif isinstance(value[key], list):
                    result_ = "\n".join(
                        [
                            "\n".join(
                                dict_to_substitutions(
                                    {"_": value_}, prefix=f"{key}: "
                                ).values()
                            )
                            for value_ in value[key]
                            if isinstance(value_, dict)
                        ]
                    )
                    result_ += "\n".join(
                        [
                            f"{key}: "
                            + f"\n{key}: ".join(
                                dict_to_substitutions({"_": value_}).values()
                            )
                            for value_ in value[key]
                            if not isinstance(value_, dict)
                        ]
                    )
                elif isinstance(value[key], dict):
                    result_ = "\n".join(
                        dict_to_substitutions(
                            {"_": value[key]}, prefix=f"{key}: "
                        ).values()
                    )
                else:
                    result_ = f"{key}: " + f"\n{key}: ".join(
                        dict_to_substitutions(
                            {"_": value[key]}, prefix=f"{key}: "
                        ).values()
                    )
                results.append(result_)
            result[f"{prefix}{name}"] = "\n".join(results)
        elif isinstance(value, list) and len(value) > 0:
            if isinstance(value[0], dict):
                if name != "_":
                    result[f"{prefix}{name}"] = "\n".join(
                        [
                            "\n".join(dict_to_substitutions({"_": value_}).values())
                            for value_ in value
                        ]
                    )
                    result.update(
                        dict_to_substitutions(value[0], prefix=f"{prefix}{name}.")
                    )
                # We assmue that every item is list is a dictionary
                for idx in range(len(value)):
                    result[f"{prefix}{name}[{idx}]"] = "\n".join(
                        dict_to_substitutions({"_": value[idx]}).values()
                    )
                    if name != "_":
                        result.update(
                            dict_to_substitutions(
                                value[idx], prefix=f"{prefix}{name}[{idx}]."
                            )
                        )
                        if "id" in value[idx]:
                            result[f'{prefix}{name}[{value[idx]["id"]}]'] = result[
                                f"{prefix}{name}[{idx}]"
                            ]
                            result.update(
                                dict_to_substitutions(
                                    value[idx],
                                    prefix=f'{prefix}{name}[{value[idx]["id"]}].',
                                )
                            )
            else:
                result__ = dict_to_substitutions(
                    {str(idx): value[idx] for idx in range(len(value))}
                )
                result[f"{prefix}{name}"] = "\n".join(result__.values())
                if name != "_":
                    for idx in range(len(value)):
                        result[f"{prefix}{name}[{idx}]"] = f"{value[idx]}"
    return result


def variables_to_substitutions(
    variables: Dict[str, VariableValueDto], prefix: str = ""
) -> Dict[str, str]:
    """Flatten variables dictionary."""
    result: Dict[str, str] = {
        "baseUrl": settings.SUBSTITUTION_BASE_URL,
    }
    for name, variable in variables.items():
        if variable.type == ValueType.String:
            value = variable.value or ""
            if RE_DATE.match(value):
                result[f"{prefix}{name}"] = value.split("T")[0]
            else:
                result[f"{prefix}{name}"] = value
        elif variable.type == ValueType.Boolean:
            result[f"{prefix}{name}"] = "Kyllä / Yes" if variable.value else "Ei / No"
        elif variable.type == ValueType.Integer:
            result[f"{prefix}{name}"] = str(variable.value)
        elif variable.type == ValueType.Double:
            result[f"{prefix}{name}"] = str(variable.value)
        elif variable.type == ValueType.Null:
            result[f"{prefix}{name}"] = ""
        elif (
            variable.type == ValueType.Object
            and isinstance(variable.value, str)
            and variable.type == "Object"
            and variable.valueInfo is not None
            and variable.valueInfo["serializationDataFormat"] == "application/json"
        ):
            value = json.loads(variable.value)
            if isinstance(value, dict):
                result.update(dict_to_substitutions(value, prefix=f"{prefix}{name}."))
            elif isinstance(value, list) and len(value) > 0:
                if isinstance(value[0], dict):
                    result.update(
                        dict_to_substitutions(value[0], prefix=f"{prefix}{name}.")
                    )
                    for idx in range(len(value)):
                        result[f"{prefix}{name}[{idx}]"] = value[idx]
                        result.update(
                            dict_to_substitutions(
                                value[idx], prefix=f"{prefix}{name}[{idx}]."
                            )
                        )
                else:
                    result_ = dict_to_substitutions(
                        {str(idx): value[idx] for idx in range(len(value))}
                    )
                    result[f"{prefix}{name}"] = "\n".join(result_.values())
                    for idx in range(len(value)):
                        result[f"{prefix}{name}[{idx}]"] = f"{value[idx]}"
    return result


async def safe_substitute(
    task: LockedExternalTaskDto,
    templates: List[str],
    substitutes: List[str],
) -> List[str]:
    """Substitute template."""
    assert task.businessKey
    # First pass
    templates = [
        DottedTemplate(template).safe_substitute(
            **variables_to_substitutions(task.variables or {})
        )
        for template in templates
    ]
    # Second pass
    keys: Set[str] = set()
    for template in templates:
        t = DottedTemplate(template)
        keys = keys | {
            re.sub(r"\[.*?\]", "", match[2])
            for match in t.pattern.findall(t.template)
            if "." in match[2]
        }

    # Substitutes should be selected without indexing or filtering brackets parts
    keys = {key for key in substitutes if re.sub(r"\[.*?\]", "", key) in keys}
    if keys:
        query, variables = build_substitution_query(list(keys))
        if "businessKey" in variables:
            variables["businessKey"] = task.businessKey
        values = {key: "" for key in keys}
        values.update(await query_substitution_values(query, variables))
        templates = [
            DottedTemplate(template).safe_substitute(**values) for template in templates
        ]
    return templates


def build_substitution_query(keys: List[str]) -> Tuple[str, Dict[str, str]]:
    """Build GraphQL query for substitution values to be fetched."""
    current: List[str] = []
    previous: List[str] = []
    unique_keys: List[str] = []
    arguments: Dict[str, str] = {}
    variables: Dict[str, str] = {}
    for key in reversed(sorted(keys)):
        assert RE_KEY.match(key), f"Invalid substitute: {key}"
        if not unique_keys:
            unique_keys.append(key)
        elif not unique_keys[-1].startswith(key):
            unique_keys.append(key)
        elif not unique_keys[-1][len(key) :].startswith("."):
            unique_keys.append(key)
    query = ""
    for key in sorted(unique_keys):
        parts = key.split(".")
        for part in parts:
            match = re.search(r"\[(.*?)\]", part)
            argument = "" if not match or match.group(1).isdigit() else match.group(1)
            if argument:
                # Support quoted arguments
                argument_ = re.sub(r"^['\"]*|['\"]*$", "", argument)
                part = part.replace(f"[{argument}]", f"[{argument_}]")
                argument = argument_
            current.append(part)
            path = part.split("[")[0]  # Remove indexing or filter
            # Skip if previous is a prefix of current
            if previous[0 : len(current)] == current:
                continue
            # Pop until previous is a prefix of current
            while len(previous) >= len(current):
                previous.pop()
                if len(previous) >= len(current):
                    padded = f"{'  ' * len(previous)}}}"
                    query += f"{padded}\n"
                elif len(parts) == 1:
                    query += "  }\n"
            # Start new query when previous is empty
            if len(current) == 1 and not previous:
                if argument and any(
                    [key.startswith(prefix) for prefix in ID_QUERY_ALLOW_PREFIX]
                ):
                    if part not in arguments:
                        arguments[part] = f"{path}_{len(arguments):04}"
                    variables[arguments[part]] = argument
                    # _by_pk cannot be supported, because views don't have primary keys
                    query += f"  {arguments[part]}: {path}(where: {{ id: {{ _eq: ${arguments[part]} }} }}) {{\n"
                    query += "    id\n"
                else:
                    arguments[part] = "businessKey"
                    variables["businessKey"] = ""
                    query += f"  {path}(where: {{ business_key: {{ _eq: $businessKey }} }}) {{\n"
            elif len(current) < len(parts):
                query += f"{'  ' * len(current)}{path} {{\n"
            else:
                query += f"{'  ' * (len(current))}{path}\n"
        previous = current
        current = []
    # Close query
    if len(previous) == 0:
        query += "}\n"
    elif len(previous) == 1:
        query += "  }\n"
    while len(previous):
        previous.pop()
        padded = f"{'  ' * len(previous)}}}"
        query += f"{padded}\n"
    return (
        "query ("
        + ", ".join([f"${arg}: String" for arg in sorted(set(arguments.values()))])
        + ") {\n"
        + query
    ), variables


async def execute_query(
    query: str,
    variables: Dict[str, str] = {},
    business_key: str = "",
) -> SubstitutionValuesQueryResponse:
    """Query entity values."""
    async with hasura_session() as http:
        url = settings.VASARA_GRAPHQL_ENDPOINT
        query_ = SubstitutionValuesQuery(
            query=query,
            variables=SubstitutionValuesQueryVariables(businessKey=business_key)
            if business_key
            else variables,
        )
        logger.debug(asdict(query_))
        post = await http.post(url, data=asjson(query_))
        if post.status not in (200,):
            logger.info(await post.json())
            return SubstitutionValuesQueryResponse(data={})
        data = await post.json()
        logger.debug(data)
        # Collect all values with the same key
        final: Dict[str, Any] = {}
        for key, value in SubstitutionValuesQueryResponse(**data).data.items():
            final_key = re.sub(r"_0\d{3}$", "", key)
            if isinstance(value, list):
                final.setdefault(final_key, []).extend(value)
            else:
                final.setdefault(final_key, []).append(value)
        return SubstitutionValuesQueryResponse(data=final)


async def query_substitution_values(
    query: str, variables: Dict[str, str] = {}, business_key: str = ""
) -> Dict[str, str]:
    """Query entity string substitution values."""
    response = await execute_query(query, variables, business_key)
    return dict_to_substitutions(response.data)
