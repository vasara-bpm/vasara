"""Vault Transit REST API helpers."""
from aiohttp import ClientTimeout
from app.config import logger
from app.config import settings
from app.types.hasura import Authorization
from app.types.hasura import CamundaPrincipalsQuery
from app.types.hasura import CamundaPrincipalsQueryData
from app.types.hasura import CamundaPrincipalsQueryResponse
from app.types.hasura import CamundaPrincipalsQueryVariables
from app.types.hasura import CamundaUser
from app.types.hasura import HasuraErrorResponse
from app.types.hasura import UserTaskFormSource
from app.types.hasura import UserTaskFormSourcesQuery
from app.types.hasura import UserTaskFormSourcesQueryResponse
from app.types.hasura import UserTaskFormSourcesQueryVariables
from app.utils import asjson
from app.utils import verify_response_status
from contextlib import asynccontextmanager
from pydantic import ValidationError
from typing import AsyncGenerator
from typing import List
from typing import Tuple
import aiohttp


@asynccontextmanager
async def hasura_session() -> AsyncGenerator[aiohttp.ClientSession, None]:
    """Get aiohttp session with Hasura headers."""
    headers = {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "X-Hasura-Admin-Secret": settings.VASARA_GRAPHQL_ADMIN_SECRET,
    }
    async with aiohttp.ClientSession(
        headers=headers,
        trust_env=True,
        timeout=ClientTimeout(total=settings.VASARA_GRAPHQL_TIMEOUT),
    ) as session:
        yield session


async def query_user_task_form_sources(
    principals: List[str],
    process_definition_keys: List[str],
    sources: List[str],
    max_version: int,
) -> Tuple[List[UserTaskFormSource], List[Authorization]]:
    """Query matching user task form sources for their task definition keys."""
    async with hasura_session() as http:
        url = settings.VASARA_GRAPHQL_ENDPOINT
        query = UserTaskFormSourcesQuery(
            variables=UserTaskFormSourcesQueryVariables(
                principals=principals,
                processDefinitionKeys=process_definition_keys,
                sources=sources,
                maxProcessDefinitionVersion=max_version,
            )
        )
        post = await http.post(url, data=asjson(query))
        await verify_response_status(post, (200,))
        try:
            response = UserTaskFormSourcesQueryResponse(**await post.json())
        except ValidationError as e:
            raise Exception(HasuraErrorResponse(**await post.json()).errors) from e
        return (
            response.data.vasara_user_task_form_source,
            response.data.vasara_authorization,
        )


async def query_camunda_principals(
    user_ids: List[str],
    group_ids: List[str],
) -> Tuple[List[CamundaUser], CamundaPrincipalsQueryData]:
    """Query camunda principals."""
    async with hasura_session() as http:
        url = settings.VASARA_GRAPHQL_ENDPOINT
        query = CamundaPrincipalsQuery(
            variables=CamundaPrincipalsQueryVariables(
                userIds=user_ids,
                groupIds=group_ids,
            )
        )
        logger.debug(asjson(query))
        post = await http.post(url, data=asjson(query))
        await verify_response_status(post, (200,))
        logger.debug(await post.json())
        try:
            response = CamundaPrincipalsQueryResponse(**await post.json())
        except ValidationError as e:
            raise Exception(HasuraErrorResponse(**await post.json()).errors) from e
    users = {}
    for user in response.data.users:
        users[user.id] = user
    for group in response.data.groups:
        for user in group.members:
            users[user.id] = user
    return list(users.values()), response.data
