"""Service configuration."""
from enum import Enum
from pathlib import Path
from pydantic import BaseSettings
from pydantic import validator
from typing import Type
import jwt


def _maybe_file(value: str) -> str:
    """Read file if it exists."""
    return Path(value).read_text() if Path(value).exists() else value.strip()


class Settings(BaseSettings):
    """Settings."""

    VASARA_TRANSIENT_USER_PRIVATE_KEY: str
    VASARA_TRANSIENT_USER_PRIVATE_KEY_ALG: str = "ES256"

    @validator("VASARA_TRANSIENT_USER_PRIVATE_KEY")
    @classmethod
    def _validate_private_key(cls: Type["Settings"], value: str) -> str:
        # TODO: Support ec-ed25519
        key = _maybe_file(value)
        jwt.encode({}, key, algorithm="ES256")  # assert key is valid
        return key


class Topic(str, Enum):
    """External task topics."""

    VASARA_TRANSIENT_USER_CREATE = "VasaraTransientUserLifecycle.create"
    VASARA_TRANSIENT_USER_DELETE = "VasaraTransientUserLifecycle.delete"
