"""Service API App."""
from app.camunda.routes import router as camunda_router
from app.camunda.tasks import TASKS as camunda_tasks
from app.entity.tasks import TASKS as entity_tasks
from app.handlers import camunda
from app.handlers import email
from app.handlers import heartbeat
from app.handlers import substitute
from app.purge.tasks import TASKS as purge_tasks
from app.routers.healthz import router as healthz_router
from app.routers.login import router as login_router
from app.routers.s3 import router as s3_router
from app.routers.vault import router as vault_router
from app.routers.xlsx import router as xlsx_router
from app.transient.tasks import TASKS as transient_user_tasks
from app.types.worker import DefaultTopic
from app.types.worker import ExternalTaskHandler
from app.types.worker import TOPIC
from app.worker import external_task_worker
from fastapi.applications import FastAPI
from starlette.requests import Request
from starlette.responses import Response
from typing import Awaitable
from typing import Callable
from typing import Dict
import asyncio
import logging


logger = logging.getLogger(__name__)

app = FastAPI(
    title="Vasara Middleware API",
    description="Vasara Hasura Actions.",
    version="1.0.0",
)


@app.on_event("startup")
async def startup_event() -> None:
    """Start external task worker on FastAPI startup."""
    tasks: Dict[TOPIC, ExternalTaskHandler] = {}
    tasks.update(
        {
            DefaultTopic.CAMUNDA_HISTORY_VARIABLES_DELETE: camunda.delete_variables,
            DefaultTopic.APP_HEARTBEAT: heartbeat.handler,
            DefaultTopic.CAMUNDA_MAIL_SEND: email.mail_send,
            DefaultTopic.VASARA_MAIL_SEND: email.mail_send,
            DefaultTopic.VASARA_MAIL_VALIDATE: email.mail_validate,
            DefaultTopic.VASARA_SUBSTITUTE: substitute.substitute,
        }
    )
    tasks.update(purge_tasks)  # type: ignore
    tasks.update(transient_user_tasks)  # type: ignore
    tasks.update(camunda_tasks)  # type: ignore
    tasks.update(entity_tasks)  # type: ignore
    asyncio.ensure_future(external_task_worker(tasks))
    logger.info("Event loop: %s", asyncio.get_event_loop())


app.include_router(camunda_router)
app.include_router(healthz_router)
app.include_router(login_router)
app.include_router(s3_router)
app.include_router(vault_router)
app.include_router(xlsx_router)


@app.middleware("http")
async def cache_headers(
    request: Request, call_next: Callable[[Request], Awaitable[Response]]
) -> Response:
    """Set cache headers."""
    response = await call_next(request)
    response.headers["Cache-Control"] = "no-store, max-age=0"
    return response


def main() -> None:
    """Run Vasara Middleware."""
    import sys
    import uvicorn

    sys.argv.insert(1, "app.main:app")
    sys.argv.insert(2, "--proxy-headers")
    uvicorn.main()
