# -*- coding: utf-8 -*-
"""Test entity tasks."""
from app.entity.tasks import dict_to_dotted_values
from app.entity.tasks import load_from_entity_to_process
from app.types.camunda import LockedExternalTaskDto
from app.types.camunda import ValueType
from app.types.camunda import VariableValueDto
from app.types.hasura import SubstitutionValuesQueryResponse
from app.types.worker import ExternalTaskComplete
from pytest_mock import MockerFixture
import json
import pytest


FIXTURE_RESPONSE_FILE_VARIABLE = {
    "entity": [
        {
            "file_variable": "\\x00",
            "metadata": {
                "file_variable": {
                    "name": "file.txt",
                    "type": "text/plain",
                }
            },
        }
    ]
}

FIXTURE_RESPONSE_MISC_VARIABLES = {
    "entity": [
        {
            "metadata": {},
            "int_variable": 100,
            "double_variable": 100.50,
            "string_variable": "Hello World",
            "true_variable": True,
            "false_variable": False,
            "null_variable": None,
            "date_variable": "2024-02-07T13:51:44.818238+00:00",
            "related": [{"variable": "Connected"}],
            "record_variable": {"key": "value"},
            "table_variable": [{"key": "value"}],
        }
    ]
}

FIXTURE_RESPONSE_VOCABULARY = {
    "vocabulary_country": [
        {
            "name": "Finland",
            "id": "fi",
            "code": "FI",
            "cities": ["Helsinki", "Tampere"],
        }
    ],
}


def test_dict_to_dotted_values_file_variable() -> None:
    dotted = dict_to_dotted_values(FIXTURE_RESPONSE_FILE_VARIABLE)
    assert dotted == {
        "entity": [
            {
                "file_variable": "\\x00",
                "metadata": {
                    "file_variable": {"name": "file.txt", "type": "text/plain"}
                },
            }
        ],
        "entity.file_variable": "\\x00",
        "entity.metadata": {
            "file_variable": {"name": "file.txt", "type": "text/plain"}
        },
        "entity.metadata.file_variable": {"name": "file.txt", "type": "text/plain"},
        "entity.metadata.file_variable.name": "file.txt",
        "entity.metadata.file_variable.type": "text/plain",
        "entity[0]": {
            "file_variable": "\\x00",
            "metadata": {"file_variable": {"name": "file.txt", "type": "text/plain"}},
        },
        "entity[0].file_variable": "\\x00",
        "entity[0].metadata": {
            "file_variable": {"name": "file.txt", "type": "text/plain"}
        },
        "entity[0].metadata.file_variable": {"name": "file.txt", "type": "text/plain"},
        "entity[0].metadata.file_variable.name": "file.txt",
        "entity[0].metadata.file_variable.type": "text/plain",
    }


@pytest.mark.asyncio
async def test_load_entity_file_variable(mocker: MockerFixture) -> None:
    mock_query = mocker.patch(
        "app.entity.tasks.execute_query",
        return_value=SubstitutionValuesQueryResponse(
            data=FIXTURE_RESPONSE_FILE_VARIABLE
        ),
    )

    task = LockedExternalTaskDto(
        id="task-id",
        businessKey="entity-id",
        variables={
            "file": VariableValueDto(
                value="entity.file_variable", type=ValueType.String
            ),
        },
    )

    result = await load_from_entity_to_process(task)

    args, _ = mock_query.call_args
    assert args[1] == {"businessKey": task.businessKey}
    assert (
        args[0]
        == """\
query ($businessKey: String) {
  entity(where: { business_key: { _eq: $businessKey } }) {
    file_variable
    metadata
  }
}
"""
    )

    assert isinstance(result, ExternalTaskComplete)
    assert isinstance(result.response.variables, dict)
    assert "file" in result.response.variables
    assert result.response.variables["file"].type == ValueType.File
    assert isinstance(result.response.variables["file"].valueInfo, dict)
    assert result.response.variables["file"].valueInfo["filename"] == "file.txt"
    assert result.response.variables["file"].valueInfo["encoding"] == "utf-8"
    assert result.response.variables["file"].valueInfo["mimetype"] == "text/plain"
    assert result.response.variables["file"].valueInfo["mimeType"] == "text/plain"
    assert result.response.variables["file"].value == b"AA=="


def test_dict_to_dotted_values_misc_variables() -> None:
    dotted = dict_to_dotted_values(FIXTURE_RESPONSE_MISC_VARIABLES)
    assert dotted == {
        "entity": [
            {
                "date_variable": "2024-02-07T13:51:44.818238+00:00",
                "double_variable": 100.5,
                "false_variable": False,
                "int_variable": 100,
                "metadata": {},
                "null_variable": None,
                "record_variable": {"key": "value"},
                "related": [{"variable": "Connected"}],
                "string_variable": "Hello World",
                "table_variable": [{"key": "value"}],
                "true_variable": True,
            }
        ],
        "entity.date_variable": "2024-02-07T13:51:44.818238+00:00",
        "entity.double_variable": 100.5,
        "entity.false_variable": False,
        "entity.int_variable": 100,
        "entity.metadata": {},
        "entity.null_variable": None,
        "entity.record_variable": {"key": "value"},
        "entity.record_variable.key": "value",
        "entity.related": [{"variable": "Connected"}],
        "entity.related[0]": {"variable": "Connected"},
        "entity.related.variable": "Connected",
        "entity.related[0].variable": "Connected",
        "entity.string_variable": "Hello World",
        "entity.table_variable": [{"key": "value"}],
        "entity.table_variable.key": "value",
        "entity.table_variable[0]": {"key": "value"},
        "entity.table_variable[0].key": "value",
        "entity.true_variable": True,
        "entity[0]": {
            "date_variable": "2024-02-07T13:51:44.818238+00:00",
            "double_variable": 100.5,
            "false_variable": False,
            "int_variable": 100,
            "metadata": {},
            "null_variable": None,
            "record_variable": {"key": "value"},
            "related": [{"variable": "Connected"}],
            "string_variable": "Hello World",
            "table_variable": [{"key": "value"}],
            "true_variable": True,
        },
        "entity[0].date_variable": "2024-02-07T13:51:44.818238+00:00",
        "entity[0].double_variable": 100.5,
        "entity[0].false_variable": False,
        "entity[0].int_variable": 100,
        "entity[0].metadata": {},
        "entity[0].null_variable": None,
        "entity[0].record_variable": {"key": "value"},
        "entity[0].record_variable.key": "value",
        "entity[0].related": [{"variable": "Connected"}],
        "entity[0].related.variable": "Connected",
        "entity[0].related[0]": {"variable": "Connected"},
        "entity[0].related[0].variable": "Connected",
        "entity[0].string_variable": "Hello World",
        "entity[0].table_variable": [{"key": "value"}],
        "entity[0].table_variable.key": "value",
        "entity[0].table_variable[0]": {"key": "value"},
        "entity[0].table_variable[0].key": "value",
        "entity[0].true_variable": True,
    }


@pytest.mark.asyncio
async def test_load_entity_variables(mocker: MockerFixture) -> None:
    mock_query = mocker.patch(
        "app.entity.tasks.execute_query",
        return_value=SubstitutionValuesQueryResponse(
            data=FIXTURE_RESPONSE_MISC_VARIABLES
        ),
    )

    task = LockedExternalTaskDto(
        id="task-id",
        businessKey="entity-id",
        variables={
            "int_variable": VariableValueDto(
                value="entity.int_variable", type=ValueType.String
            ),
            "double_variable": VariableValueDto(
                value="entity.double_variable", type=ValueType.String
            ),
            "string_variable": VariableValueDto(
                value="entity.string_variable", type=ValueType.String
            ),
            "true_variable": VariableValueDto(
                value="entity.true_variable", type=ValueType.String
            ),
            "false_variable": VariableValueDto(
                value="entity.false_variable", type=ValueType.String
            ),
            "null_variable": VariableValueDto(
                value="entity.null_variable", type=ValueType.String
            ),
            "date_variable": VariableValueDto(
                value="entity.date_variable", type=ValueType.String
            ),
            "related_variable": VariableValueDto(
                value="entity.related.variable", type=ValueType.String
            ),
            "record_variable": VariableValueDto(
                value="entity.record_variable", type=ValueType.String
            ),
            "table_variable": VariableValueDto(
                value="entity.table_variable", type=ValueType.String
            ),
        },
    )

    result = await load_from_entity_to_process(task)

    args, _ = mock_query.call_args
    assert args[1] == {"businessKey": task.businessKey}
    assert (
        args[0]
        == """\
query ($businessKey: String) {
  entity(where: { business_key: { _eq: $businessKey } }) {
    date_variable
    double_variable
    false_variable
    int_variable
    metadata
    null_variable
    record_variable
    related {
      variable
    }
    string_variable
    table_variable
    true_variable
  }
}
"""
    )

    assert isinstance(result, ExternalTaskComplete)
    assert isinstance(result.response.variables, dict)
    assert "int_variable" in result.response.variables
    assert result.response.variables["int_variable"].type == ValueType.Integer
    assert result.response.variables["int_variable"].value == 100
    assert "double_variable" in result.response.variables
    assert result.response.variables["double_variable"].type == ValueType.Double
    assert result.response.variables["double_variable"].value == 100.50
    assert "string_variable" in result.response.variables
    assert result.response.variables["string_variable"].type == ValueType.String
    assert result.response.variables["string_variable"].value == "Hello World"
    assert "true_variable" in result.response.variables
    assert result.response.variables["true_variable"].type == ValueType.Boolean
    assert result.response.variables["true_variable"].value is True
    assert "false_variable" in result.response.variables
    assert result.response.variables["false_variable"].type == ValueType.Boolean
    assert result.response.variables["false_variable"].value is False
    assert "null_variable" in result.response.variables
    assert result.response.variables["null_variable"].type == ValueType.Null
    assert result.response.variables["null_variable"].value is None
    assert "related_variable" in result.response.variables
    assert result.response.variables["related_variable"].type == ValueType.String
    assert result.response.variables["related_variable"].value == "Connected"
    assert "record_variable" in result.response.variables
    assert result.response.variables["record_variable"].type == ValueType.Json
    assert result.response.variables["record_variable"].value == json.dumps(
        {"key": "value"}
    )
    assert "table_variable" in result.response.variables
    assert result.response.variables["table_variable"].type == ValueType.Json
    assert result.response.variables["table_variable"].value == json.dumps(
        [{"key": "value"}]
    )
    assert "date_variable" in result.response.variables
    assert result.response.variables["date_variable"].type == ValueType.Date
    assert (
        result.response.variables["date_variable"].value
        == "2024-02-07T13:51:44.818+0000"
    )


def test_dict_to_dotted_values_vocabulary() -> None:
    dotted = dict_to_dotted_values(FIXTURE_RESPONSE_VOCABULARY)
    assert dotted == {
        "vocabulary_country": [
            {
                "cities": ["Helsinki", "Tampere"],
                "code": "FI",
                "id": "fi",
                "name": "Finland",
            }
        ],
        "vocabulary_country[fi]": {
            "cities": ["Helsinki", "Tampere"],
            "code": "FI",
            "id": "fi",
            "name": "Finland",
        },
        "vocabulary_country[0]": {
            "cities": ["Helsinki", "Tampere"],
            "code": "FI",
            "id": "fi",
            "name": "Finland",
        },
        "vocabulary_country.cities": ["Helsinki", "Tampere"],
        "vocabulary_country.cities[0]": "Helsinki",
        "vocabulary_country.cities[1]": "Tampere",
        "vocabulary_country.code": "FI",
        "vocabulary_country.id": "fi",
        "vocabulary_country.name": "Finland",
        "vocabulary_country[fi].cities": ["Helsinki", "Tampere"],
        "vocabulary_country[fi].cities[0]": "Helsinki",
        "vocabulary_country[fi].cities[1]": "Tampere",
        "vocabulary_country[fi].code": "FI",
        "vocabulary_country[fi].id": "fi",
        "vocabulary_country[fi].name": "Finland",
        "vocabulary_country[0].cities": ["Helsinki", "Tampere"],
        "vocabulary_country[0].cities[0]": "Helsinki",
        "vocabulary_country[0].cities[1]": "Tampere",
        "vocabulary_country[0].code": "FI",
        "vocabulary_country[0].id": "fi",
        "vocabulary_country[0].name": "Finland",
    }


@pytest.mark.asyncio
async def test_load_with_vocabulary(mocker: MockerFixture) -> None:
    mock_query = mocker.patch(
        "app.entity.tasks.execute_query",
        return_value=SubstitutionValuesQueryResponse(data=FIXTURE_RESPONSE_VOCABULARY),
    )

    task = LockedExternalTaskDto(
        id="task-id",
        businessKey="entity-id",
        processInstanceId="process-id",
        variables={
            "country": VariableValueDto(
                value="vocabulary_country[fi].name",
                type=ValueType.String,
            ),
            "cities": VariableValueDto(
                value='vocabulary_country["fi"].cities',
                type=ValueType.String,
            ),
        },
    )

    result = await load_from_entity_to_process(task)
    args, _ = mock_query.call_args

    assert args[1] == {"vocabulary_country_0000": "fi"}
    assert (
        args[0]
        == """\
query ($vocabulary_country_0000: String) {
  vocabulary_country_0000: vocabulary_country(where: { id: { _eq: $vocabulary_country_0000 } }) {
    id
    cities
    name
  }
}
"""
    )

    assert isinstance(result, ExternalTaskComplete)
    assert result.response.variables
    assert "cities" in result.response.variables
    assert result.response.variables["cities"].type == ValueType.Json
    assert result.response.variables["cities"].value == '["Helsinki", "Tampere"]'
    assert "country" in result.response.variables
    assert result.response.variables["country"].type == ValueType.String
    assert result.response.variables["country"].value == "Finland"
