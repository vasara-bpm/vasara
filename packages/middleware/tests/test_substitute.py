from app.handlers.substitute import substitute
from app.services.substitute import build_substitution_query
from app.services.substitute import dict_to_substitutions
from app.types.camunda import LockedExternalTaskDto
from app.types.camunda import ValueType
from app.types.camunda import VariableValueDto
from app.types.hasura import SubstitutionValuesQueryResponse
from app.types.worker import ExternalTaskComplete
from pytest_mock import MockerFixture
from typing import List
import pytest


FIXTURE_RESPONSE_MISC_VARIABLES = {
    "entity": [
        {
            "int_variable": 100,
            "double_variable": 100.50,
            "string_variable": "Hello World",
            "true_variable": True,
            "false_variable": False,
            "null_variable": None,
            "date_variable": "2024-02-07T13:51:44.818238+00:00",
            "related": [{"variable": "Connected"}],
            "record_variable": {"key": "value"},
            "table_variable": [{"a": "1", "b": "2"}, {"a": "3", "b": "4"}],
        }
    ]
}


def test_dict_to_dotted_values_misc_variables() -> None:
    dotted = dict_to_substitutions(FIXTURE_RESPONSE_MISC_VARIABLES)
    assert dotted == {
        "entity": "date_variable: 2024-02-07\n"
        "double_variable: 100.5\n"
        "false_variable: Ei / No\n"
        "int_variable: 100\n"
        "null_variable: \n"
        "record_variable: key: value\n"
        "related: variable: Connected\n"
        "string_variable: Hello World\n"
        "table_variable: a: 1\n"
        "table_variable: b: 2\n"
        "table_variable: a: 3\n"
        "table_variable: b: 4\n"
        "true_variable: Kyllä / Yes",
        "entity.date_variable": "2024-02-07",
        "entity.double_variable": "100.5",
        "entity.false_variable": "Ei / No",
        "entity.int_variable": "100",
        "entity.null_variable": "",
        "entity.record_variable": "key: value",
        "entity.record_variable.key": "value",
        "entity.related.variable": "Connected",
        "entity.related": "variable: Connected",
        "entity.related[0]": "variable: Connected",
        "entity.related[0].variable": "Connected",
        "entity.string_variable": "Hello World",
        "entity.table_variable": "a: 1\nb: 2\na: 3\nb: 4",
        "entity.table_variable.a": "1",
        "entity.table_variable.b": "2",
        "entity.table_variable[0]": "a: 1\nb: 2",
        "entity.table_variable[0].a": "1",
        "entity.table_variable[0].b": "2",
        "entity.table_variable[1]": "a: 3\nb: 4",
        "entity.table_variable[1].a": "3",
        "entity.table_variable[1].b": "4",
        "entity.true_variable": "Kyllä / Yes",
        "entity[0]": "date_variable: 2024-02-07\n"
        "double_variable: 100.5\n"
        "false_variable: Ei / No\n"
        "int_variable: 100\n"
        "null_variable: \n"
        "record_variable: key: value\n"
        "related: variable: Connected\n"
        "string_variable: Hello World\n"
        "table_variable: a: 1\n"
        "table_variable: b: 2\n"
        "table_variable: a: 3\n"
        "table_variable: b: 4\n"
        "true_variable: Kyllä / Yes",
        "entity[0].date_variable": "2024-02-07",
        "entity[0].double_variable": "100.5",
        "entity[0].false_variable": "Ei / No",
        "entity[0].int_variable": "100",
        "entity[0].null_variable": "",
        "entity[0].record_variable": "key: value",
        "entity[0].record_variable.key": "value",
        "entity[0].related.variable": "Connected",
        "entity[0].related": "variable: Connected",
        "entity[0].related[0]": "variable: Connected",
        "entity[0].related[0].variable": "Connected",
        "entity[0].string_variable": "Hello World",
        "entity[0].table_variable": "a: 1\nb: 2\na: 3\nb: 4",
        "entity[0].table_variable.a": "1",
        "entity[0].table_variable.b": "2",
        "entity[0].table_variable[0]": "a: 1\nb: 2",
        "entity[0].table_variable[0].a": "1",
        "entity[0].table_variable[0].b": "2",
        "entity[0].table_variable[1]": "a: 3\nb: 4",
        "entity[0].table_variable[1].a": "3",
        "entity[0].table_variable[1].b": "4",
        "entity[0].true_variable": "Kyllä / Yes",
    }


def test_dict_to_dotted_values_vocabulary() -> None:
    dotted = dict_to_substitutions(
        {
            "vocabulary_country": [
                {
                    "name": "Finland",
                    "id": "fi",
                    "code": "FI",
                    "cities": ["Helsinki", "Tampere"],
                }
            ]
        }
    )
    assert dotted == {
        "vocabulary_country": "cities: Helsinki\ncities: Tampere\ncode: FI\nid: fi\nname: Finland",
        "vocabulary_country.cities": "Helsinki\nTampere",
        "vocabulary_country.cities[0]": "Helsinki",
        "vocabulary_country.cities[1]": "Tampere",
        "vocabulary_country.code": "FI",
        "vocabulary_country.id": "fi",
        "vocabulary_country.name": "Finland",
        "vocabulary_country[0]": "cities: Helsinki\ncities: Tampere\ncode: FI\nid: fi\nname: Finland",
        "vocabulary_country[0].cities": "Helsinki\nTampere",
        "vocabulary_country[0].cities[0]": "Helsinki",
        "vocabulary_country[0].cities[1]": "Tampere",
        "vocabulary_country[0].code": "FI",
        "vocabulary_country[0].id": "fi",
        "vocabulary_country[0].name": "Finland",
        "vocabulary_country[fi]": "cities: Helsinki\ncities: Tampere\ncode: FI\nid: fi\nname: Finland",
        "vocabulary_country[fi].cities": "Helsinki\nTampere",
        "vocabulary_country[fi].cities[0]": "Helsinki",
        "vocabulary_country[fi].cities[1]": "Tampere",
        "vocabulary_country[fi].code": "FI",
        "vocabulary_country[fi].id": "fi",
        "vocabulary_country[fi].name": "Finland",
    }


def test_build_substitution_query_empty() -> None:
    keys: List[str] = []
    expected_query = """\
query () {
}
"""
    assert build_substitution_query(keys)[0] == expected_query


def test_build_substitution_query_no_keys() -> None:
    keys = ["foo"]
    expected_query = """\
query ($businessKey: String) {
  foo(where: { business_key: { _eq: $businessKey } }) {
  }
}
"""
    assert build_substitution_query(keys)[0] == expected_query


def test_build_substitution_query_single_key() -> None:
    keys = ["foo.bar"]
    expected_query = """\
query ($businessKey: String) {
  foo(where: { business_key: { _eq: $businessKey } }) {
    bar
  }
}
"""
    assert build_substitution_query(keys)[0] == expected_query


def test_build_substitution_query_single_index_key() -> None:
    keys = ["foo[0].bar"]
    expected_query = """\
query ($businessKey: String) {
  foo(where: { business_key: { _eq: $businessKey } }) {
    bar
  }
}
"""
    assert build_substitution_query(keys)[0] == expected_query


def test_build_substitution_query_deep_key() -> None:
    keys = ["foo.bar.baz.qux"]
    expected_query = """\
query ($businessKey: String) {
  foo(where: { business_key: { _eq: $businessKey } }) {
    bar {
      baz {
        qux
      }
    }
  }
}
"""
    assert build_substitution_query(keys)[0] == expected_query


def test_build_substitution_query_multiple_empty() -> None:
    keys = ["foo", "bar", "baz"]
    expected_query = """\
query ($businessKey: String) {
  bar(where: { business_key: { _eq: $businessKey } }) {
  }
  baz(where: { business_key: { _eq: $businessKey } }) {
  }
  foo(where: { business_key: { _eq: $businessKey } }) {
  }
}
"""
    assert build_substitution_query(keys)[0] == expected_query


def test_build_substitution_query_flat() -> None:
    keys = ["foo.bar", "foo.baz", "foo.qux"]
    expected_query = """\
query ($businessKey: String) {
  foo(where: { business_key: { _eq: $businessKey } }) {
    bar
    baz
    qux
  }
}
"""
    assert build_substitution_query(keys)[0] == expected_query


def test_build_substitution_query_multiple_flat() -> None:
    keys = [
        "foo.bar",
        "foo.baz",
        "foo.qux",
        "bar.foo",
        "bar.bar",
        "bar.baz",
        "baz.foo",
        "baz.bar",
        "baz.baz",
    ]
    expected_query = """\
query ($businessKey: String) {
  bar(where: { business_key: { _eq: $businessKey } }) {
    bar
    baz
    foo
  }
  baz(where: { business_key: { _eq: $businessKey } }) {
    bar
    baz
    foo
  }
  foo(where: { business_key: { _eq: $businessKey } }) {
    bar
    baz
    qux
  }
}
"""
    assert build_substitution_query(keys)[0] == expected_query


def _test_build_substitution_query_single_key() -> None:
    keys = ["foo.bar"]
    expected_query = """\
query ($businessKey: String) {
  foo(where: { business_key: { _eq: $businessKey } }) {
    bar
  }
}
"""
    assert build_substitution_query(keys)[0] == expected_query


def test_build_substitution_query_mixed() -> None:
    keys = [
        "foo",
        "foo.bar",
        "foo.bar.baz",
        "foo.bar.baz.qux",
        "foo.bar.baz.quux",
        "bar.baz.qux",
    ]
    expected_query = """\
query ($businessKey: String) {
  bar(where: { business_key: { _eq: $businessKey } }) {
    baz {
      qux
    }
  }
  foo(where: { business_key: { _eq: $businessKey } }) {
    bar {
      baz {
        quux
        qux
      }
    }
  }
}
"""
    assert build_substitution_query(keys)[0] == expected_query


def test_build_substitution_query_overlapping() -> None:
    keys = [
        "foo.bar.foo",
        "foo.bar.foo_bar",
    ]
    expected_query = """\
query ($businessKey: String) {
  foo(where: { business_key: { _eq: $businessKey } }) {
    bar {
      foo
      foo_bar
    }
  }
}
"""
    assert build_substitution_query(keys)[0] == expected_query


def test_build_vocabulary_substitution_single() -> None:
    keys = ["vocabulary_foo[bar].bar"]
    expected_query = """\
query ($vocabulary_foo_0000: String) {
  vocabulary_foo_0000: vocabulary_foo(where: { id: { _eq: $vocabulary_foo_0000 } }) {
    id
    bar
  }
}
"""
    assert build_substitution_query(keys)[0] == expected_query


def test_build_mixed_substitution_single() -> None:
    keys = ["foo.bar", "vocabulary_foo[bar].bar"]
    expected_query = """\
query ($businessKey: String, $vocabulary_foo_0001: String) {
  foo(where: { business_key: { _eq: $businessKey } }) {
    bar
  }
  vocabulary_foo_0001: vocabulary_foo(where: { id: { _eq: $vocabulary_foo_0001 } }) {
    id
    bar
  }
}
"""
    assert build_substitution_query(keys)[0] == expected_query


def test_dict_to_substitutions() -> None:
    substitutions = {
        "entity": {
            "name": "John",
            "content": {"@order": ["age", "gender"], "gender": "O", "age": 30},
            "displayGender": {"label": "Other"},
        }
    }
    expected_output = {
        "entity": "content: age: 30\ncontent: gender: O\ndisplayGender: label: Other\nname: John",
        "entity.content": "age: 30\ngender: O",
        "entity.content.@order": "age\ngender",
        "entity.content.@order[0]": "age",
        "entity.content.@order[1]": "gender",
        "entity.content.age": "30",
        "entity.content.gender": "O",
        "entity.displayGender": "label: Other",
        "entity.displayGender.label": "Other",
        "entity.name": "John",
    }
    assert dict_to_substitutions(substitutions) == expected_output


@pytest.mark.asyncio
async def test_substitute_with_entity(mocker: MockerFixture) -> None:
    mock_query = mocker.patch(
        "app.services.substitute.execute_query",
        return_value=SubstitutionValuesQueryResponse(
            data={"entity": [{"bar": "World"}]}
        ),
    )

    task = LockedExternalTaskDto(
        id="task-id",
        businessKey="entity-id",
        processInstanceId="process-id",
        variables={
            "foo": VariableValueDto(value="Hello %{entity.bar}", type=ValueType.String),
            "substitutes": VariableValueDto(
                value='["entity.bar"]',
                type=ValueType.Object,
                valueInfo={"serializationDataFormat": "application/json"},
            ),
        },
    )

    result = await substitute(task)

    args, _ = mock_query.call_args

    assert args[1] == {"businessKey": task.businessKey}
    assert (
        args[0]
        == """\
query ($businessKey: String) {
  entity(where: { business_key: { _eq: $businessKey } }) {
    bar
  }
}
"""
    )

    assert isinstance(result, ExternalTaskComplete)
    assert result.response.variables
    assert "foo" in result.response.variables
    assert result.response.variables["foo"].type == ValueType.String
    assert result.response.variables["foo"].value == "Hello World"


@pytest.mark.asyncio
async def test_substitute_with_vocabulary(mocker: MockerFixture) -> None:
    mock_query = mocker.patch(
        "app.services.substitute.execute_query",
        return_value=SubstitutionValuesQueryResponse(
            data={
                "entity": [{"bar": "World"}],
                "vocabulary_country": [{"id": "fi", "name": "Finland"}],
            }
        ),
    )

    task = LockedExternalTaskDto(
        id="task-id",
        businessKey="entity-id",
        processInstanceId="process-id",
        variables={
            "foo": VariableValueDto(
                value="Hello %{entity.bar} from %{vocabulary_country.name}",
                type=ValueType.String,
            ),
            "substitutes": VariableValueDto(
                value='["entity.bar", "vocabulary_country[\\"fi\\"].name"]',
                type=ValueType.Object,
                valueInfo={"serializationDataFormat": "application/json"},
            ),
        },
    )

    result = await substitute(task)

    args, _ = mock_query.call_args

    assert args[1] == {"businessKey": task.businessKey, "vocabulary_country_0001": "fi"}
    assert (
        args[0]
        == """\
query ($businessKey: String, $vocabulary_country_0001: String) {
  entity(where: { business_key: { _eq: $businessKey } }) {
    bar
  }
  vocabulary_country_0001: vocabulary_country(where: { id: { _eq: $vocabulary_country_0001 } }) {
    id
    name
  }
}
"""
    )

    assert isinstance(result, ExternalTaskComplete)
    assert result.response.variables
    assert "foo" in result.response.variables
    assert result.response.variables["foo"].type == ValueType.String
    assert result.response.variables["foo"].value == "Hello World from Finland"
