# -*- coding: utf-8 -*-
"""Test that project is importable."""


def test_import() -> None:
    import app.main  # noqa
