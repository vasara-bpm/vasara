{
  description = "Vasura Hasura Authentication Service";

  # Cachix
  nixConfig = {
    extra-trusted-public-keys =
      "vasara-bpm.cachix.org-1:T18iQZQvYDy/6VdGmttnkkq7rYi3JP0S1RjjdnXNu/4=";
    extra-substituters = "https://vasara-bpm.cachix.org";
  };

  inputs = {

    # Generic
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/release-23.11";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    # Vasara
    vasara-bpm = {
      url = "gitlab:vasara-bpm/pkgs";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
      inputs.poetry2nix.follows = "poetry2nix";
    };
    poetry2nix.url = "github:nix-community/poetry2nix";

  };

  outputs = { self, nixpkgs, flake-utils, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ inputs.poetry2nix.overlays.default ];
        };
        call-name = "vasara-hasura-auth";
        overrides = pkgs.poetry2nix.overrides.withDefaults (self: super: { });
      in {

        # Default executable
        apps.default = {
          type = "app";
          program = self.packages.${system}.default + "/bin/${call-name}";
        };

        # Python package
        packages.default = pkgs.poetry2nix.mkPoetryApplication {
          python = pkgs.python39;
          projectDir = ./.;
          inherit overrides;
        };

        # Development environment
        packages.env = (pkgs.poetry2nix.mkPoetryEnv {
          python = pkgs.python39;
          projectDir = ./.;
          inherit overrides;
        });

        # Container image
        packages.image = pkgs.dockerTools.streamLayeredImage {
          name = call-name;
          tag = "latest";
          created = "now";
          contents = [
            (pkgs.buildEnv {
              name = "image-contents";
              paths = [
                pkgs.busybox
                pkgs.curl
                pkgs.dockerTools.fakeNss
                pkgs.dockerTools.usrBinEnv
                pkgs.tini
                self.packages.x86_64-linux.default
              ];
              pathsToLink = [ "/etc" "/sbin" "/bin" ];
            })
          ];
          extraCommands = ''
            mkdir -p usr/bin && ln -s /sbin/env usr/bin/env
            mkdir -p tmp && chmod a+rxwt tmp
          '';
          config = {
            Entrypoint = [
              "${pkgs.tini}/bin/tini"
              "--"
              "${self.packages.x86_64-linux.default}/bin/${call-name}"
            ];
            Env = [ "TMPDIR=/tmp" "HOME=/tmp" ];
            Labels = { };
            User = "nobody";
          };
        };

        # Nomad artifact
        packages.artifact = let
          build = self.packages.x86_64-linux.default;
          env = pkgs.buildEnv {
            name = "${call-name}-env";
            paths = [
              pkgs.bashInteractive
              pkgs.curl
              pkgs.coreutils
              pkgs.netcat
              pkgs.tini
              build
            ];
          };
          closure = (pkgs.writeReferencesToFile env);
        in pkgs.runCommand call-name { buildInputs = [ pkgs.makeWrapper ]; } ''
          # aliases
          mkdir -p usr/local/bin
          for filename in ${env}/bin/??*; do
            cat > usr/local/bin/$(basename $filename) << EOF
          #!/usr/local/bin/sh
          set -e
          exec $(basename $filename) "\$@"
          EOF
          done
          rm -f usr/local/bin/sh
          chmod a+x usr/local/bin/*

          # shell
          makeWrapper ${pkgs.bashInteractive}/bin/sh usr/local/bin/sh \
            --set SHELL /usr/local/bin/sh \
            --prefix PATH : ${pkgs.coreutils}/bin \
            --prefix PATH : ${pkgs.curl}/bin \
            --prefix PATH : ${pkgs.netcat}/bin \
            --prefix PATH : ${pkgs.tini}/bin \
            --prefix PATH : ${build}/bin

          # artifact
          tar cvzhP \
            --hard-dereference \
            --exclude="${env}*" \
            --exclude="*ncurses*/ncurses*/ncurses*" \
            --files-from=${closure} \
            usr > $out || true
        '';

        # Development shell
        devShells.default = pkgs.mkShell {
          buildInputs = [
            pkgs.cachix
            pkgs.jfrog-cli
            pkgs.entr
            pkgs.gnumake
            pkgs.jq
            pkgs.openssl
            pkgs.poetry
            pkgs.black
            self.packages.${system}.env
          ];
        };

        devShells.with-podman =
          inputs.vasara-bpm.devShells.${system}.podman.overrideAttrs (old: {
            buildInputs = old.buildInputs
              ++ self.devShells.${system}.default.buildInputs;
          });

        formatter = pkgs.nixfmt;
      });
}
