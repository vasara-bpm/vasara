.PHONY: image
image: $(SOURCES)
	$$(nix build $(NIX_OPTIONS) --json .#image|jq -r .[0].outputs.out)|podman load
