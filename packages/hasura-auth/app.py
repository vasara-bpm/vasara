#!/usr/bin/env python
from aiohttp import client
from aiohttp import web
from aiohttp import ServerTimeoutError
from aiohttp import ContentTypeError
from asyncio import TimeoutError
from asyncio import Lock
from os import environ as env

import aiocache
import asyncio
import hashlib
import logging
import time


CACHE_USER_TTL = int(env.get("CACHE_USER_TTL", "3600") or "3600")
CACHE_ALLOWED_IDS_TTL = int(env.get("CACHE_ALLOWED_IDS_TTL", "300") or "300")
CACHE_MANAGED_IDS_TTL = int(env.get("CACHE_MANAGED_IDS_TTL", "1") or "1")
NO_AUTHORIZATION_QUERY = (env.get("NO_AUTHORIZATION_QUERY") or "").lower() in [
    "on",
    "true",
    "1",
]
AIOHTTP_PORT = env.get("AIOHTTP_PORT", "8080")
CAMUNDA_GRAPHQL_ENDPOINT = env.get(
    "CAMUNDA_GRAPHQL_ENDPOINT", "http://localhost:8800/graphql"
)
CAMUNDA_GRAPHQL_AUTHORIZATION = env.get(
    "CAMUNDA_GRAPHQL_AUTHORIZATION", "Bearer vasara-graphql-secret"
)
ADMIN_GROUP = env.get("ADMIN_GROUP")
GUEST_GROUP = "camunda-transient"
USER_QUERY = """\
{
  camunda_AuthenticatedUser {
    id
    name
    email
    groups {
      id
    }
  }
}
"""
AUTHORIZATION_ALLOWED_IDS_QUERY = """\
query {
  camunda_AuthenticatedUser {
    id
    vasaraAllowedIds
  }
}
"""
AUTHORIZATION_MANAGED_IDS_QUERY = """\
query {
  camunda_AuthenticatedUser {
    id
    vasaraManagedIds
  }
}
"""


logger = logging.getLogger("hasura-auth-webhook")
logging.basicConfig(
    level=getattr(logging, (env.get("LOG_LEVEL") or "").upper() or "INFO")
)
logging.getLogger("aiohttp").setLevel(logging.INFO)
logging.getLogger("aiocache").setLevel(logging.INFO)
routes = web.RouteTableDef()


USER_ID_LOCKS = dict()
USER_AUTHORIZATION_LOCKS = dict()


def clear_lock(locks, key):
    """Remove lock."""
    loop = asyncio.get_event_loop()
    if key in locks:
        if not locks[key].locked():
            logger.debug(
                "LOCK DELETE : %s", hashlib.md5(key.encode("utf-8")).hexdigest()
            )
            del locks[key]
        else:
            loop.call_later(3600, clear_lock, locks, key)


@aiocache.cached(
    ttl=CACHE_USER_TTL,
    key_builder=lambda f, *args, **kwargs: f"userInfo:{args[0]}",
    serializer=aiocache.serializers.JsonSerializer(),
)
async def get_user(authorization):
    """Fetch fresh user info."""
    logger.debug(
        "CACHE MISS : user : %s", hashlib.md5(authorization.encode("utf-8")).hexdigest()
    )
    async with client.ClientSession(
        headers={
            "Authorization": authorization,
            "Content-Type": "application/json",
            "Accept": "application/json",
        }
    ) as session:
        for timeout in range(5):
            try:
                # TODO: ensure correct certs in container to avoid ssl=False
                async with session.post(
                    CAMUNDA_GRAPHQL_ENDPOINT,
                    json={"query": USER_QUERY, "variables": {}, "operationName": None},
                    timeout=timeout + 1,
                    ssl=False,
                ) as response:
                    return ((await response.json()).get("data") or {}).get(
                        "camunda_AuthenticatedUser"
                    ) or {}
            except (ServerTimeoutError, TimeoutError):
                pass
            except ContentTypeError:
                return {}
        return {}


async def __get_allowed_ids(authorization):
    """Fetch fresh vasaraAllowedIds."""
    logger.debug(
        "CACHE MISS : vasaraAllowedIds : %s",
        hashlib.md5(authorization.encode("utf-8")).hexdigest(),
    )
    async with client.ClientSession(
        headers={
            "Authorization": authorization,
            "Content-Type": "application/json",
            "Accept": "application/json",
        }
    ) as session:
        for timeout in range(5):
            try:
                # TODO: ensure correct certs in container to avoid ssl=False
                async with session.post(
                    CAMUNDA_GRAPHQL_ENDPOINT,
                    json={
                        "query": AUTHORIZATION_ALLOWED_IDS_QUERY,
                        "variables": {},
                        "operationName": None,
                    },
                    timeout=timeout + 1,
                    ssl=False,
                ) as response:
                    return (
                        ((await response.json()).get("data") or {}).get(
                            "camunda_AuthenticatedUser"
                        )
                        or {}
                    ).get("vasaraAllowedIds") or [], time.time()
            except (ServerTimeoutError, TimeoutError):
                pass
            except ContentTypeError:
                return {}, time.time() - CACHE_ALLOWED_IDS_TTL
        return {}, time.time() - CACHE_ALLOWED_IDS_TTL


@aiocache.cached(
    ttl=CACHE_ALLOWED_IDS_TTL,
    key_builder=lambda f, *args, **kwargs: f"vasaraAllowedIds:{args[0]}",
    serializer=aiocache.serializers.JsonSerializer(),
)
async def _get_allowed_ids(authorization):
    """Fetch fresh vasaraAllowedIds."""
    logger.debug(
        "CACHE MISS : vasaraAllowedIds : %s",
        hashlib.md5(authorization.encode("utf-8")).hexdigest(),
    )
    return await __get_allowed_ids(authorization)


async def get_allowed_ids(authorization, force=False):
    """Fetch vasaraAllowedIds."""
    loop = asyncio.get_event_loop()

    # Ensure lock by authorization
    if authorization not in USER_AUTHORIZATION_LOCKS:
        logger.debug(
            "LOCK CREATE : %s", hashlib.md5(authorization.encode("utf-8")).hexdigest()
        )
        USER_AUTHORIZATION_LOCKS[authorization] = Lock()
        loop.call_later(3600, clear_lock, USER_AUTHORIZATION_LOCKS, authorization)

    async with USER_AUTHORIZATION_LOCKS[authorization]:
        # Fetch possibly cached allowed ids
        if force:
            allowed_ids, timestamp = await __get_allowed_ids(authorization)
        else:
            allowed_ids, timestamp = await _get_allowed_ids(authorization)

        # if cache is old enough, trigger refresh
        if time.time() - timestamp > 60:
            loop.call_soon(
                lambda f, key, force: asyncio.create_task(f(key, force)),
                get_allowed_ids,
                authorization,
                True,
            )
        elif force:
            logger.debug(
                "CACHE UPDATE : vasaraAllowedIds : %s",
                hashlib.md5(authorization.encode("utf-8")).hexdigest(),
            )
            key = f"vasaraAllowedIds:{authorization}"
            aiocache.Cache().set(
                key, (allowed_ids, timestamp), ttl=CACHE_ALLOWED_IDS_TTL
            )

    return allowed_ids


@aiocache.cached(
    ttl=CACHE_MANAGED_IDS_TTL,
    key_builder=lambda f, *args, **kwargs: f"vasaraManagedIds:{args[0]}",
    serializer=aiocache.serializers.JsonSerializer(),
)
async def get_managed_ids(authorization):
    async with client.ClientSession(
        headers={
            "Authorization": authorization,
            "Content-Type": "application/json",
            "Accept": "application/json",
        }
    ) as session:
        for timeout in range(5):
            try:
                # TODO: ensure correct certs in container to avoid ssl=False
                async with session.post(
                    CAMUNDA_GRAPHQL_ENDPOINT,
                    json={
                        "query": AUTHORIZATION_MANAGED_IDS_QUERY,
                        "variables": {},
                        "operationName": None,
                    },
                    timeout=timeout + 1,
                    ssl=False,
                ) as response:
                    return (
                        ((await response.json()).get("data") or {}).get(
                            "camunda_AuthenticatedUser"
                        )
                        or {}
                    ).get("vasaraManagedIds") or []
            except (ServerTimeoutError, TimeoutError):
                pass
            except ContentTypeError:
                return {}
        return {}


@routes.get("/")
async def read_authorize_header(request):
    authorization = request.headers.get("Authorization") or ""
    is_bearer = authorization.startswith("Bearer ")
    is_transient = authorization.startswith("Transient ")
    loop = asyncio.get_event_loop()

    # No valid authorization header
    if not is_bearer and not is_transient:
        logger.debug("ANONYMOUS : %s", {"X-Hasura-Role": "anonymous"})
        return web.json_response({"X-Hasura-Role": "anonymous"})

    if authorization not in USER_AUTHORIZATION_LOCKS:
        logger.debug(
            "LOCK CREATE : %s", hashlib.md5(authorization.encode("utf-8")).hexdigest()
        )
        USER_AUTHORIZATION_LOCKS[authorization] = Lock()
        loop.call_later(3600, clear_lock, USER_AUTHORIZATION_LOCKS, authorization)
    async with USER_AUTHORIZATION_LOCKS[authorization]:
        user = await get_user(authorization)

    # Expired or invalid authorization header
    if not user.get("id"):
        logger.warning(f"INVALID USERINFO : {user}")
        logger.debug("ANONYMOUS : %s", {"X-Hasura-Role": "anonymous"})
        loop.call_later(
            60, lambda x: aiocache.Cache().delete(f"userInfo:{x}"), authorization
        )
        return web.json_response({"X-Hasura-Role": "anonymous"})

    schema = request.headers.get("X-Vasara-Remote-Schema") or None
    logger.debug(f"AUTHENTICATED : {user}")

    if NO_AUTHORIZATION_QUERY:
        # Completely disable authorization query, when Hasura can use direct
        # SQL access to Camunda database for authorizations
        allowed_ids = []
        managed_ids = []
    elif schema == "Camunda":
        # Request to Camunda don't need authorizations, because Camunda checks them
        allowed_ids = []
        managed_ids = []
        # Cache warmup
        loop.call_soon(
            lambda f, key: asyncio.create_task(f(key)), get_allowed_ids, authorization
        )
    elif schema == "Actions":
        # Also actions do their own authorization checks when necessary
        allowed_ids = []
        managed_ids = []
        # Cache warmup
        loop.call_soon(
            lambda f, key: asyncio.create_task(f(key)), get_allowed_ids, authorization
        )
    else:
        if user["id"] not in USER_ID_LOCKS:
            USER_ID_LOCKS[user["id"]] = Lock()
            loop.call_later(3600, clear_lock, USER_ID_LOCKS, authorization)
        async with USER_ID_LOCKS[user["id"]]:
            start = loop.time()
            allowed_ids, managed_ids = await asyncio.gather(
                get_allowed_ids(authorization),
                get_managed_ids(authorization),
            )
            allowed_ids += managed_ids
            end = loop.time()
            logger.debug(
                f"TIMED ALLOWED IDS : {end - start} : %s",
                hashlib.md5(authorization.encode("utf-8")).hexdigest(),
            )

    group_ids = [group["id"] for group in user["groups"]]
    groups = "{" + ",".join(group_ids) + "}"
    principal_ids = [f"{user['id']}"] + [f"{group_id}" for group_id in group_ids]
    principals = "{" + ",".join(principal_ids) + "}"
    role = (
        "guest"
        if is_transient or GUEST_GROUP in group_ids
        else "admin"
        if ADMIN_GROUP and ADMIN_GROUP in group_ids
        else "user"
    )

    allowed_ids_psql = "{" + ",".join(set(allowed_ids)) + "}"
    managed_ids_psql = "{" + ",".join(set(managed_ids)) + "}"

    logger.debug(
        "AUTHORIZED : %s",
        {
            "X-Hasura-Role": role,
            "X-Hasura-User-Id": user["id"],
            "X-Hasura-User-Name": user["name"],
            "X-Hasura-User-Email": user["email"],
            "X-Hasura-Groups": groups,
            "X-Hasura-Principals": principals,
            "X-Hasura-Allowed-Ids": allowed_ids_psql,
            "X-Hasura-Managed-Ids": managed_ids_psql,
            "X-Vasara-Remote-Schema": schema,
        },
    )
    return web.json_response(
        {
            "X-Hasura-Role": role,
            "X-Hasura-User-Id": user["id"],
            "X-Hasura-User-Name": user["name"],
            "X-Hasura-User-Email": user["email"],
            "X-Hasura-Groups": groups,
            "x-hasura-principals": principals,
            "x-hasura-allowed-ids": allowed_ids_psql,
            "x-hasura-managed-ids": managed_ids_psql,
        }
    )


def main():
    app = web.Application()
    app.add_routes(routes)
    web.run_app(app, port=int(AIOHTTP_PORT))


if __name__ == "__main__":
    main()
