{
  description = "Hasura Graphql Engine";

  nixConfig = {
    extra-trusted-public-keys =
      "vasara-bpm.cachix.org-1:T18iQZQvYDy/6VdGmttnkkq7rYi3JP0S1RjjdnXNu/4=";
    extra-substituters = "https://vasara-bpm.cachix.org";
  };

  inputs = {

    # Generic
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/release-22.11";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/master";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    # Vasara 
    vasara-bpm = {
      url = "gitlab:vasara-bpm/pkgs";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };

  };

  outputs = { self, nixpkgs, nixpkgs-unstable, flake-utils, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ ];
        };
        pkgs-unstable = import nixpkgs-unstable { inherit system; };
        call-name = "hasura-graphql-engine";

      in {

        # Default executable
        apps.default = {
          type = "app";
          program = "${self.packages.${system}.default}/bin/graphql-engine";
        };

        # Default package
        packages.default =
          inputs.vasara-bpm.packages.${system}.hasura-graphql-engine;

        # Container image
        packages.image = pkgs.dockerTools.streamLayeredImage {
          name = call-name;
          tag = "latest";
          created = "now";
          contents = [
            (pkgs.buildEnv {
              name = "image-contents";
              paths = [
                pkgs.busybox
                pkgs.curl
                pkgs.dockerTools.fakeNss
                pkgs.dockerTools.usrBinEnv
                pkgs.tini
                inputs.vasara-bpm.packages.x86_64-linux.hasura-graphql-engine
                inputs.vasara-bpm.packages.x86_64-linux.hasura-console-assets
              ];
              pathsToLink = [ "/etc" "/sbin" "/bin" ];
            })
          ];
          extraCommands = ''
            mkdir -p usr/bin && ln -s /sbin/env usr/bin/env
            mkdir -p tmp && chmod a+rxwt tmp
          '';
          config = {
            Entrypoint = [
              "${pkgs.tini}/bin/tini"
              "--"
              "${inputs.vasara-bpm.packages.x86_64-linux.hasura-graphql-engine}/bin/graphql-engine"
            ];
            Env = [ "TMPDIR=/tmp" "HOME=/tmp" ];
            Labels = { };
            User = "nobody";
          };
        };

        # Nomad artifact
        packages.artifact = let
          env = pkgs.buildEnv {
            name = "${call-name}-env";
            paths = [
              pkgs.bashInteractive
              pkgs.coreutils
              pkgs.curl
              inputs.vasara-bpm.packages.${system}.hasura-graphql-engine
              inputs.vasara-bpm.packages.${system}.hasura-console-assets
              pkgs.netcat
              pkgs.tini
            ];
          };
          closure = (pkgs.writeReferencesToFile env);
        in pkgs.runCommand call-name { buildInputs = [ pkgs.makeWrapper ]; } ''
          # aliases
          mkdir -p usr/local/bin
          for filename in ${env}/bin/??*; do
            cat > usr/local/bin/$(basename $filename) << EOF
          #!/usr/local/bin/sh
          set -e
          exec $(basename $filename) "\$@"
          EOF
          done
          rm -f usr/local/bin/sh
          chmod a+x usr/local/bin/*

          # shell
          makeWrapper ${pkgs.bashInteractive}/bin/sh usr/local/bin/sh \
            --set SHELL /usr/local/bin/sh \
            --prefix PATH : ${pkgs.coreutils}/bin \
            --prefix PATH : ${pkgs.curl}/bin \
            --prefix PATH : ${pkgs.netcat}/bin \
            --prefix PATH : ${
              inputs.vasara-bpm.packages.${system}.hasura-graphql-engine
            }/bin \
            --prefix PATH : ${pkgs.tini}/bin \
            --set HASURA_GRAPHQL_CONSOLE_ASSETS_DIR ${
              inputs.vasara-bpm.packages.${system}.hasura-console-assets
            }

          # artifact
          tar cvzhP \
            --hard-dereference \
            --exclude="${env}" \
            --exclude="*ncurses*/ncurses*/ncurses*" \
            --files-from=${closure} \
            usr > $out || true
        '';

        # Development shell
        devShells.default = pkgs.mkShell {
          buildInputs = [
            pkgs-unstable.cachix
            pkgs-unstable.jfrog-cli
            pkgs.gnumake
            pkgs.jq
            inputs.vasara-bpm.packages.${system}.hasura-cli-full
          ];
        };

        devShells.with-podman =
          inputs.vasara-bpm.devShells.${system}.podman.overrideAttrs (old: {
            buildInputs = old.buildInputs
              ++ self.devShells.${system}.default.buildInputs;
          });

        formatter = pkgs.nixfmt;
      });
}
