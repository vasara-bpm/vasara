package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import fi.jyu.vasara.graphql.types.TaskResults;
import org.springframework.stereotype.Component;

@Component
public class TaskResultsResolver implements GraphQLResolver<TaskResults> {
}
