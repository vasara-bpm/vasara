package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import fi.jyu.vasara.graphql.types.KeyValuePair;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class KeyValuePairResolver implements GraphQLResolver<KeyValuePair> {
}
