package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.management.JobDefinition;
import org.camunda.bpm.engine.repository.Deployment;
import org.camunda.bpm.engine.repository.DeploymentQuery;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.repository.ProcessDefinitionQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class JobDefinitionResolver implements GraphQLResolver<JobDefinition> {

    @Autowired
    RepositoryService repositoryService;

    public JobDefinitionResolver() {
    }

    public ProcessDefinition getProcessDefinition(JobDefinition jobDefinition) {
        String processDefinitionId = jobDefinition.getProcessDefinitionId();
        if (processDefinitionId == null) {
            return null;
        } else {
            ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
            return processDefinitionQuery.processDefinitionId(processDefinitionId).singleResult();
        }
    }

    public Boolean getSuspended(JobDefinition jobDefinition) {
        return jobDefinition.isSuspended();
    }

    public Deployment getDeployment(JobDefinition jobDefinition) {
        String deploymentId = jobDefinition.getProcessDefinitionId();
        if (deploymentId == null) {
            return null;
        } else {
            DeploymentQuery deploymentQuery = repositoryService.createDeploymentQuery();
            return deploymentQuery.deploymentId(deploymentId).singleResult();
        }
    }

}
