package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.ProcessEngineConfiguration;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.authorization.Authorization;
import org.camunda.bpm.engine.authorization.Permission;
import org.camunda.bpm.engine.history.HistoricProcessInstance;
import org.camunda.bpm.engine.history.HistoricProcessInstanceQuery;
import org.camunda.bpm.engine.identity.Group;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.impl.util.PermissionConverter;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.runtime.ProcessInstanceQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

@Component
public class AuthorizationResolver implements GraphQLResolver<Authorization> {

    private final static Logger LOGGER = Logger.getLogger(AuthorizationResolver.class.getName());

    @Autowired
    IdentityService identityService;

    @Autowired
    HistoryService historyService;

    @Autowired
    ProcessEngineConfiguration processEngineConfiguration;

    public AuthorizationResolver() {
    }

    public String getType(Authorization authorization) {
        int type = authorization.getAuthorizationType();
        switch (type) {
            case 1:
                return "GRANT";
            case 2:
                return "REVOKE";
            default:
                return "GLOBAL";
        }
    }

    // https://docs.camunda.org/manual/latest/user-guide/process-engine/authorization-service/#resources%22
    public String getResourceType(Authorization authorization) {
        int type = authorization.getResourceType();
        switch (type) {
            case 0:
                return "Application";
            case 4:
                return "Authorization";
            case 13:
                return "Batch";
            case 10:
                return "Decision Definition";
            case 14:
                return "Decision Requirements Definition";
            case 9:
                return "Deployment";
            case 5:
                return "Filter";
            case 2:
                return "Group";
            case 3:
                return "Group Membership";
            case 6:
                return "Process Definition";
            case 8:
                return "Process Instance";
            case 7:
                return "Task";
            case 19:
                return "Historic Task";
            case 20:
                return "Historic Process Instance";
            case 11:
                return "Tenant";
            case 12:
                return "Tenant Membership";
            case 1:
                return "User";
            case 15:
                return "Report";
            case 16:
                return "Dashboard";
            case 17:
                return "User Operation Log Category";
            default:
                return null;
        }
    }

    public List<String> getPermissions(Authorization authorization) {
        int givenResourceType = authorization.getResourceType();
        Permission[] permissionsByResourceType =
                ((ProcessEngineConfigurationImpl) processEngineConfiguration).getPermissionProvider().getPermissionsForResource(givenResourceType);
        Permission[] permissionsByAuthorization = authorization.getPermissions(permissionsByResourceType);
        return Arrays.asList(PermissionConverter.getNamesForPermissions(authorization, permissionsByAuthorization));
    }

    public User getUser(Authorization authorization) {
        String userId = authorization.getUserId();

        if (userId == null) {
            return null;
        } else {
            return identityService.createUserQuery().userId(userId).singleResult();
        }
    }

    public String getRemovalTime(Authorization authorization) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date removalTime = authorization.getRemovalTime();
        return removalTime != null ? dateFormat.format(authorization.getRemovalTime()) : null;
    }

    public Group getGroup(Authorization authorization) {
        String groupId = authorization.getGroupId();

        if (groupId == null) {
            return null;
        } else {
            return identityService.createGroupQuery().groupId(groupId).singleResult();
        }
    }

    public HistoricProcessInstance getRootProcessInstance(Authorization authorization) {
        String piId = authorization.getRootProcessInstanceId();
        if (piId == null) {
            return null;
        } else {
            return historyService.createHistoricProcessInstanceQuery().processInstanceId(piId).singleResult();
        }
    }

}
