package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import fi.jyu.vasara.graphql.Util;
import fi.jyu.vasara.graphql.types.KeyValuePair;
import fi.jyu.vasara.graphql.types.TransientUser;
import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.history.HistoricProcessInstance;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.identity.UserQuery;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.runtime.Execution;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Comment;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.value.StringValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Logger;

import static fi.jyu.vasara.graphql.Constants.GROUP_TRANSIENT;

@Component
public class TaskResolver implements GraphQLResolver<Task> {

    private final static Logger LOGGER = Logger.getLogger(TaskResolver.class.getName());

    @Autowired
    ProcessEngine processEngine;

    @Autowired
    TaskService taskService;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    ProcessEngineConfigurationImpl processEngineConfiguration;

    @Autowired
    IdentityService identityService;

    @Autowired
    HistoryService historyService;

    @Autowired
    FormService formService;

    public TaskResolver() {
    }

    public String getDescription(Task task) {
        String taskId = task.getId();

        StringValue name;
        String value;

        // Return local variable "name" as task "description"
        try {
            name = taskService.getVariableLocalTyped(taskId, "name");
            if (name != null) {
                value = name.getValue();
                if (value != null && value.length() > 0) {
                    return value;
                }
            }
        } catch (ClassCastException | ProcessEngineException e) {
            // do nothing
        }

        // Or return process variable "name" as task "description"
        try {
            name = taskService.getVariableTyped(taskId, "name");
            if (name != null) {
                value = name.getValue();
                if (value != null && value.length() > 0) {
                    return value;
                }
            }
        } catch (ClassCastException | ProcessEngineException e) {
            // do nothing
        }

        // Or return nothing
        return null;
    }

    public String getDocumentation(Task task) {
        // Return task description as its documentation as it's documented in Modeler
        return task.getDescription();
    }

    public User getAssignee(Task task) {
        String userId = task.getAssignee();
        if (userId == null) {
            return null;
        } else {
            UserQuery userQuery = identityService.createUserQuery();
            User assignee = userQuery.userId(userId).singleResult();
            return assignee != null ? assignee : new TransientUser(
                userId,
                Collections.singletonList(GROUP_TRANSIENT),
               null,
               null,
               null
            );
        }
    }

    public String getAssigneeId(Task task) {
        return task.getAssignee();
    }

    public User getOwner(Task task) {
        String userId = task.getOwner();
        if (userId == null) {
            return null;
        } else {
            UserQuery userQuery = identityService.createUserQuery();
            User owner = userQuery.userId(userId).singleResult();
            return owner != null ? owner : new TransientUser(
                userId,
                Collections.singletonList(GROUP_TRANSIENT),
                null,
                null,
                null
            );
        }
    }

    public String getOwnerId(Task task) {
        return task.getOwner();
    }

    public String getCreated(Task task) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(task.getCreateTime());
    }

    public String getDue(Task task) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(task.getDueDate());
    }

    public String getFollowUp(Task task) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(task.getFollowUpDate());
    }

    public String getDelegationState(Task task) {
        switch (task.getDelegationState()) {
            case PENDING:
                return "PENDING";
            case RESOLVED:
                return "RESOLVED";
        }
        return "PENDING";
    }

    public Execution getExecution(Task task) {
        String executionId = task.getExecutionId();
        if (executionId == null) {
            return null;
        } else {
            return runtimeService.createExecutionQuery().executionId(executionId).singleResult();
        }
    }

    public ProcessDefinition getProcessDefinition(Task task) {
        String processDefinitionId = task.getProcessDefinitionId();
        if (processDefinitionId == null) {
            return null;
        } else {
            return repositoryService.createProcessDefinitionQuery().processDefinitionId(processDefinitionId).singleResult();
        }
    }

    public ProcessInstance getProcessInstance(Task task) {
        String processInstanceId = task.getProcessInstanceId();
        if (processInstanceId == null) {
            return null;
        } else {
            return runtimeService.createProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
        }
    }

    public HistoricProcessInstance getHistoricProcessInstance(Task task) {
        String processInstanceId = task.getProcessInstanceId();
        if (processInstanceId == null) {
            return null;
        } else {
            return historyService.createHistoricProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
        }
    }

    public Boolean getSuspended(Task task) {
        return task.isSuspended();
    }

    public List<Comment> getComments(Task task) {
        return taskService.getTaskComments(task.getId());
    }

    public List<KeyValuePair> getVariables(Task task) {
        List<KeyValuePair> keyValuePairs;

        String processDefinitionId = task.getProcessDefinitionId();
        if (processDefinitionId == null)
            return null;

        VariableMap variableMap = taskService.getVariablesTyped(task.getId(), true);
        keyValuePairs = Util.getKeyValuePairs(variableMap);

        return keyValuePairs;
    }

    public List<KeyValuePair> getFormVariables(Task task) {
        List<KeyValuePair> keyValuePairs;

        try {
            VariableMap variableMap = formService.getTaskFormVariables(task.getId(), null, true);
            keyValuePairs = Util.getKeyValuePairs(variableMap);
        } catch (Exception e) {
            LOGGER.warning(e.toString());
            keyValuePairs = new ArrayList<>();
        }

        return keyValuePairs;
    }
}
