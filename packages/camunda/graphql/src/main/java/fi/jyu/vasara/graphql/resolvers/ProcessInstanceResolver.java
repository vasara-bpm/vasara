package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import fi.jyu.vasara.graphql.Util;
import fi.jyu.vasara.graphql.types.KeyValuePair;
import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.history.HistoricActivityInstance;
import org.camunda.bpm.engine.history.HistoricActivityInstanceQuery;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.repository.ProcessDefinitionQuery;
import org.camunda.bpm.engine.runtime.*;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.task.TaskQuery;
import org.camunda.bpm.engine.variable.VariableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProcessInstanceResolver implements GraphQLResolver<ProcessInstance> {

    @Autowired
    TaskService taskService;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    ManagementService managementService;

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    HistoryService historyService;

    @Autowired
    ProcessEngineConfigurationImpl processEngineConfiguration;

    public ProcessInstanceResolver() {
    }

    public String getId(ProcessInstance processInstance) {
        return processInstance.getId();
    }

    public ProcessDefinition getDefinition(ProcessInstance processInstance) {
        String processDefinitionId = processInstance.getProcessDefinitionId();
        if (processDefinitionId == null) {
            return null;
        } else {
            ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
            return processDefinitionQuery.processDefinitionId(processInstance.getProcessDefinitionId()).singleResult();
        }
    }

    public String getDefinitionId(ProcessInstance processInstance) {
        return processInstance.getProcessDefinitionId();
    }

    public Boolean getEnded(ProcessInstance processInstance) {
        return processInstance.isEnded();
    }

    public Boolean getSuspended(ProcessInstance processInstance) {
        return processInstance.isSuspended();
    }

    public ActivityInstance getActivityInstance(ProcessInstance processInstance) {
        return runtimeService.getActivityInstance(processInstance.getProcessInstanceId());
    }

    public List<HistoricActivityInstance> getHistoricActivityInstances(ProcessInstance processInstance) {
        HistoricActivityInstanceQuery historicActivityInstanceQuery = historyService.createHistoricActivityInstanceQuery();
        return historicActivityInstanceQuery.processInstanceId(processInstance.getId()).list();
    }

    public List<Execution> getExecutions(ProcessInstance processInstance) {
        ExecutionQuery executionQuery = runtimeService.createExecutionQuery();
        return executionQuery.processInstanceId(processInstance.getId()).list();
    }

    public List<Incident> getIncidents(ProcessInstance processInstance) {
        IncidentQuery incidentQuery = runtimeService.createIncidentQuery();
        return incidentQuery.processInstanceId(processInstance.getId()).list();
    }

    public List<Job> getJobs(ProcessInstance processInstance) {
        JobQuery jobQuery = managementService.createJobQuery();
        return jobQuery.processInstanceId(processInstance.getId()).list();
    }

    public List<Task> getTasks(ProcessInstance processInstance) {
        TaskQuery taskQuery = taskService.createTaskQuery();
        return taskQuery.processInstanceId(processInstance.getId()).list();
    }

    public List<Task> getTasksByBusinessKey(ProcessInstance processInstance) {
        String businessKey = processInstance.getBusinessKey();
        if (businessKey != null && businessKey.length() > 0) {
            TaskQuery taskQuery = taskService.createTaskQuery();
            return taskQuery.processInstanceBusinessKey(businessKey).list();
        }
        return new ArrayList<>();
    }

    public List<KeyValuePair> getVariables(ProcessInstance processInstance) {
        List<KeyValuePair> keyValuePairs;

        String pdid = processInstance.getProcessDefinitionId();
        if (pdid == null)
            return null;

        VariableMap variableMap = runtimeService.getVariablesTyped(processInstance.getId());
        keyValuePairs = Util.getKeyValuePairs(variableMap);

        return keyValuePairs;
    }

    public ProcessInstance getTopProcessInstance(ProcessInstance processInstance) {
        List<ProcessInstance> processInstances = getDomProcessInstances(processInstance);
        if (processInstances.size() == 0) {
            return processInstance;
        } else {
            return processInstances.get(processInstances.size() - 1);
        }
    }

    public ProcessInstance getRootProcessInstance(ProcessInstance processInstance) {
        String processInstanceId = processInstance.getRootProcessInstanceId();
        if (processInstanceId == null) {
            return null;
        } else {
            ProcessInstanceQuery processInstanceQuery = runtimeService.createProcessInstanceQuery();
            return processInstanceQuery.processInstanceId(processInstanceId).singleResult();
        }
    }

    public List<ProcessInstance> getDomProcessInstances(ProcessInstance processInstance) {
        List<ProcessInstance> processInstances = new ArrayList<>();
        int[] depth = {1, 2, 3, 4, 5};
        for (int ignored : depth) {
            String processInstanceId = processInstance.getId();
            processInstance = getRootProcessInstance(processInstance);
            if (processInstance == null || processInstanceId.equals(processInstance.getId())) {
                break;
            } else {
                processInstances.add(processInstance);
            }
        }
        return processInstances;
    }

    public List<ProcessInstance> getSubProcessInstances(ProcessInstance processInstance) {
        String processInstanceId = processInstance.getId();
        if (processInstanceId == null) {
            return new ArrayList<>();
        } else {
            ProcessInstanceQuery processInstanceQuery = runtimeService.createProcessInstanceQuery();
            return processInstanceQuery.superProcessInstanceId(processInstanceId).list();
        }
    }
}
