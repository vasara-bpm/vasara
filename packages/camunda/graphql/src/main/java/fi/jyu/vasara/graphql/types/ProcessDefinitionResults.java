package fi.jyu.vasara.graphql.types;

import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProcessDefinitionResults {

    private List<ProcessDefinition> data = null;
    private Long total = null;

    public ProcessDefinitionResults() {
    }

    public ProcessDefinitionResults(List<ProcessDefinition> data, Long total) {
        this.data = data;
        this.total = total;
    }

    public List<ProcessDefinition> getData () {
        return data;
    }

    public Long getTotal() {
        return total;
    }
}
