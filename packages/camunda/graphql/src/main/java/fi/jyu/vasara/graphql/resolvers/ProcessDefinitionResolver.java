package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.apache.commons.io.IOUtils;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.repository.DecisionDefinition;
import org.camunda.bpm.engine.repository.Deployment;
import org.camunda.bpm.engine.repository.DeploymentQuery;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.rest.util.ApplicationContextPathUtil;
import org.camunda.bpm.model.bpmn.BpmnModelInstance;
import org.camunda.bpm.model.bpmn.instance.StartEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

/**
 * Created by danielvogel on 20.06.17.
 */

@Component
public class ProcessDefinitionResolver implements GraphQLResolver<ProcessDefinition> {

    @Autowired
    RepositoryService repositoryService;

    public ProcessDefinitionResolver() {
    }

    public String getResource(ProcessDefinition processDefinition) {
        return processDefinition.getResourceName();
    }

    public Deployment getDeployment(ProcessDefinition decisionDefinition) {
        String deploymentId = decisionDefinition.getDeploymentId();
        if (deploymentId == null) {
            return null;
        } else {
            DeploymentQuery deploymentQuery = repositoryService.createDeploymentQuery();
            return deploymentQuery.deploymentId(deploymentId).singleResult();
        }
    }

    public String getDiagram(ProcessDefinition processDefinition) {
        InputStream inputStream = repositoryService.getProcessModel(processDefinition.getId());
        try {
            return IOUtils.toString(inputStream, "UTF-8");
        }
        catch (IOException ex) {
            return "";
        }
    }

    public Boolean getSuspended(ProcessDefinition processDefinition) {
        return processDefinition.isSuspended();
    }

    public Boolean getStartableInTasklist(ProcessDefinition processDefinition) {
        return processDefinition.isStartableInTasklist();
    }
}
