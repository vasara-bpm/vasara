package fi.jyu.vasara.graphql.types;

import org.camunda.bpm.engine.impl.identity.Authentication;

import java.util.List;

public class TransientAuthentication extends Authentication {
    protected String firstName;
    protected String lastName;
    protected String email;

    public TransientAuthentication(String authenticatedUserId, List<String> authenticatedGroupIds, List<String> authenticatedTenantIds, String authenticatedFirstName, String authenticatedLastName, String authenticatedEmail) {
        super(authenticatedUserId, authenticatedGroupIds, authenticatedTenantIds);
        firstName = authenticatedFirstName;
        lastName = authenticatedLastName;
        email = authenticatedEmail;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }
}
