package fi.jyu.vasara.graphql.types;

import org.camunda.bpm.engine.task.Task;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TaskResults {

    private List<Task> data = null;
    private Long total = null;

    public TaskResults() {
    }

    public TaskResults(List<Task> data, Long total) {
        this.data = data;
        this.total = total;
    }

    public List<Task> getData () {
        return data;
    }

    public Long getTotal() {
        return total;
    }
}
