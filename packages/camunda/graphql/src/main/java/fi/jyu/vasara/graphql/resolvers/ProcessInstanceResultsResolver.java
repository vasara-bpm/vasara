package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import fi.jyu.vasara.graphql.types.ProcessInstanceResults;
import org.springframework.stereotype.Component;

@Component
public class ProcessInstanceResultsResolver implements GraphQLResolver<ProcessInstanceResults> {
}
