package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.history.HistoricProcessInstance;
import org.camunda.bpm.engine.history.HistoricProcessInstanceQuery;
import org.camunda.bpm.engine.history.HistoricVariableInstance;
import org.camunda.bpm.engine.variable.value.TypedValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

@Component
public class HistoricVariableInstanceResolver implements GraphQLResolver<HistoricVariableInstance> {

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    HistoryService historyService;

    @Autowired
    RepositoryService repositoryService;

    public HistoricVariableInstanceResolver() {
    }

    public String getType(HistoricVariableInstance variableInstance) {
        return variableInstance.getTypeName();
    }

    public String getValue(HistoricVariableInstance variableInstance) {
        TypedValue value = variableInstance.getTypedValue();
        return value.toString();
    }

    public HistoricProcessInstance getProcessInstance(HistoricVariableInstance variableInstance) {
        String processInstanceId = variableInstance.getProcessInstanceId();
        if (processInstanceId == null) {
            return null;
        } else {
            HistoricProcessInstanceQuery processInstanceQuery = historyService.createHistoricProcessInstanceQuery();
            return processInstanceQuery.processInstanceId(processInstanceId).singleResult();
        }
    }

    public String getCreateTime(HistoricVariableInstance variableInstance) {
        Date followUpDate = variableInstance.getCreateTime();
        if (followUpDate == null) {
            return null;
        } else {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return dateFormat.format(followUpDate);
        }
    }

    public String getRemovalTime(HistoricVariableInstance variableInstance) {
        Date followUpDate = variableInstance.getRemovalTime();
        if (followUpDate == null) {
            return null;
        } else {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return dateFormat.format(followUpDate);
        }
    }

    public HistoricProcessInstance getRootProcessInstance(HistoricVariableInstance variableInstance) {
        String processInstanceId = variableInstance.getRootProcessInstanceId();
        if (processInstanceId == null) {
            return null;
        } else {
            HistoricProcessInstanceQuery processInstanceQuery = historyService.createHistoricProcessInstanceQuery();
            return processInstanceQuery.processInstanceId(processInstanceId).singleResult();
        }
    }
}
