package fi.jyu.vasara.graphql;

import fi.jyu.vasara.graphql.types.KeyValuePair;
import fi.jyu.vasara.graphql.types.ValueTypeEnum;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.impl.value.ObjectValueImpl;
import org.camunda.bpm.engine.variable.type.SerializableValueType;
import org.camunda.bpm.engine.variable.value.TypedValue;
import org.camunda.spin.plugin.variable.SpinValues;

import java.text.*;
import java.util.*;
import java.util.logging.Logger;

public class Util {

    private final static Logger LOGGER = Logger.getLogger(Util.class.getName());

    public static List<KeyValuePair> getKeyValuePairs(VariableMap variableMap) {

        ArrayList<KeyValuePair> keyValuePairs = new ArrayList<>();

        for (VariableMap.Entry<String, Object> i : variableMap.entrySet()) {

            Object objValue = i.getValue();
            String key = i.getKey();
            TypedValue typedValue = variableMap.getValueTyped(key);

            if (objValue != null) {
                String value;
                ValueTypeEnum valueType = ValueTypeEnum.from(variableMap.getValueTyped(key).getType());
                if (typedValue.getType() == SerializableValueType.OBJECT) {
                    ObjectValueImpl objectValueImpl = ((ObjectValueImpl) typedValue);
                    if (objectValueImpl.getSerializationDataFormat().equals("application/json")) {
                        value = objectValueImpl.getValueSerialized();
                        valueType = ValueTypeEnum.JSON;
                    } else {
                        value = typedValue.getValue().toString();
                    }
                } else if (typedValue.getType() == SerializableValueType.DATE) {
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
                    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                    value = dateFormat.format(objValue);
                } else {
                    value = objValue.toString();
                }
                KeyValuePair keyValuePair = new KeyValuePair(
                        key,
                        value,
                        valueType
                );
                keyValuePairs.add(keyValuePair);
            } else {
                LOGGER.fine("objValue is null: " + key);
            }
        }
        return keyValuePairs;
    }

    public static Map<String, Object> getVariablesMap(ArrayList<LinkedHashMap> variables) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        DecimalFormat numberFormat = new DecimalFormat("0.#");
        numberFormat.setDecimalFormatSymbols(symbols);

        Map<String, Object> map = new HashMap<>();
        for (LinkedHashMap i : variables) {
            switch (i.get("valueType").toString().toUpperCase()) {
                case "INT":
                    map.put(i.get("key").toString(), Integer.parseInt(i.get("value").toString()));
                    break;
                case "LONG":
                    // LONG is the only number value supported by Camunda Modeler Dynamic Forms,
                    // so... it could be anything.
                    try {
                        map.put(i.get("key").toString(), Long.parseLong(i.get("value").toString()));
                    } catch (NumberFormatException e) {
                        try {
                            map.put(i.get("key").toString(), numberFormat.parse(i.get("value").toString()).doubleValue());
                        } catch (ParseException e2) {
                            try {
                                map.put(i.get("key").toString(), Double.parseDouble(i.get("value").toString()));
                            } catch (NumberFormatException e3) {
                                try {
                                    map.put(i.get("key").toString(), numberFormat.parse(i.get("value").toString()).floatValue());
                                } catch (ParseException e4) {
                                    map.put(i.get("key").toString(), Float.parseFloat(i.get("value").toString()));
                                }
                            }
                        }
                    }
                    break;
                case "FLOAT":
                    try {
                        map.put(i.get("key").toString(), numberFormat.parse(i.get("value").toString()).floatValue());
                    } catch (ParseException e) {
                        map.put(i.get("key").toString(), Float.parseFloat(i.get("value").toString()));
                    }
                    break;
                case "DOUBLE":
                    try {
                        map.put(i.get("key").toString(), numberFormat.parse(i.get("value").toString()).doubleValue());
                    } catch (ParseException e) {
                        map.put(i.get("key").toString(), Double.parseDouble(i.get("value").toString()));
                    }
                    break;
                case "BOOLEAN":
                    map.put(i.get("key").toString(), Boolean.parseBoolean(i.get("value").toString()));
                    break;
                case "DATE":
                    try {
                        map.put(i.get("key").toString(), dateFormat.parse(i.get("value").toString()));
                    } catch (ParseException e) {
                        map.put(i.get("key").toString(), null);
                    }
                    break;
                case "JSON":
                    map.put(i.get("key").toString(), SpinValues.jsonValue(i.get("value").toString()));
                    break;
                case "ENUM":
                    if (i.get("value").toString().length() == 0) {
                        map.put(i.get("key").toString(), null);
                    } else {
                        map.put(i.get("key").toString(), i.get("value"));
                    }
                    break;
                default:
                    map.put(i.get("key").toString(), i.get("value"));
                    break;
            }
        }
        return map;
    }
}
