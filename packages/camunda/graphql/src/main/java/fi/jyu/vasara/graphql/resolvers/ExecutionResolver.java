package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.runtime.Execution;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.runtime.ProcessInstanceQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ExecutionResolver implements GraphQLResolver<Execution> {

    @Autowired
    RuntimeService runtimeService;

    public ExecutionResolver() {
    }

    public ProcessInstance getProcessInstance(Execution execution) {
        String processInstanceId = execution.getProcessInstanceId();
        if (processInstanceId == null) {
            return null;
        } else {
            ProcessInstanceQuery processInstanceQuery = runtimeService.createProcessInstanceQuery();
            return processInstanceQuery.processInstanceId(processInstanceId).singleResult();
        }
    }

    public Boolean getEnded(Execution execution) {
        return execution.isEnded();
    }
}
