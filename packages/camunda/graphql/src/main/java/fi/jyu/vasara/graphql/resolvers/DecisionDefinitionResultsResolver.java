package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import fi.jyu.vasara.graphql.types.DecisionDefinitionResults;
import org.springframework.stereotype.Component;

@Component
public class DecisionDefinitionResultsResolver implements GraphQLResolver<DecisionDefinitionResults> {
}
