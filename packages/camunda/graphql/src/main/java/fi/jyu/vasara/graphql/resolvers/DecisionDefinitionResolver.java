package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.apache.commons.io.IOUtils;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.management.JobDefinition;
import org.camunda.bpm.engine.repository.DecisionDefinition;
import org.camunda.bpm.engine.repository.Deployment;
import org.camunda.bpm.engine.repository.DeploymentQuery;
import org.camunda.bpm.engine.rest.util.ApplicationContextPathUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;

@Component
public class DecisionDefinitionResolver implements GraphQLResolver<DecisionDefinition> {

    @Autowired
    RepositoryService repositoryService;

    public DecisionDefinitionResolver() {
    }

    public String getResource(DecisionDefinition decisionDefinition) {
        return decisionDefinition.getResourceName();
    }

    public Deployment getDeployment(DecisionDefinition decisionDefinition) {
        String deploymentId = decisionDefinition.getDeploymentId();
        if (deploymentId == null) {
            return null;
        } else {
            DeploymentQuery deploymentQuery = repositoryService.createDeploymentQuery();
            return deploymentQuery.deploymentId(deploymentId).singleResult();
        }
    }

    public String getDiagram(DecisionDefinition decisionDefinition) {
        InputStream inputStream = repositoryService.getDecisionModel(decisionDefinition.getId());
        try {
            return IOUtils.toString(inputStream, "UTF-8");
        }
        catch (IOException ex) {
            return "";
        }
    }
}
