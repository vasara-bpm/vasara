package fi.jyu.vasara.graphql;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import fi.jyu.vasara.graphql.types.DeploymentResource;
import org.camunda.bpm.engine.*;
import org.camunda.bpm.engine.exception.NullValueException;
import org.camunda.bpm.engine.form.FormField;
import org.camunda.bpm.engine.form.FormType;
import org.camunda.bpm.engine.form.StartFormData;
import org.camunda.bpm.engine.impl.ServiceImpl;
import org.camunda.bpm.engine.impl.form.type.AbstractFormFieldType;
import org.camunda.bpm.engine.impl.identity.Authentication;
import org.camunda.bpm.engine.impl.interceptor.Command;
import org.camunda.bpm.engine.impl.interceptor.CommandContext;
import org.camunda.bpm.engine.migration.MigrationInstructionsBuilder;
import org.camunda.bpm.engine.migration.MigrationPlan;
import org.camunda.bpm.engine.migration.MigrationPlanExecutionBuilder;
import org.camunda.bpm.engine.repository.*;
import org.camunda.bpm.engine.runtime.MessageCorrelationBuilder;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.runtime.ProcessInstanceQuery;
import org.camunda.bpm.engine.task.Comment;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.task.TaskQuery;
import org.camunda.bpm.engine.variable.value.TypedValue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.logging.Logger;

import static org.camunda.bpm.engine.authorization.Permissions.UPDATE;
import static org.camunda.bpm.engine.authorization.Permissions.UPDATE_INSTANCE;
import static org.camunda.bpm.engine.authorization.Permissions.CREATE;
import static org.camunda.bpm.engine.authorization.Resources.PROCESS_INSTANCE;
import static org.camunda.bpm.engine.authorization.Resources.PROCESS_DEFINITION;

@Component
public class Mutation implements GraphQLMutationResolver {

    private static final Logger LOGGER = Logger.getLogger(Mutation.class.getName());

    @Autowired
    FormService formService;

    @Autowired
    TaskService taskService;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    IdentityService identityService;

    @Autowired
    AuthorizationService authorizationService;

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    ProcessEngine engine;

    public Mutation() {
    }

    public ProcessDefinition insert_camunda_ProcessDefinition(String name, List<DeploymentResource> resources, Boolean migrate) {
        // Deploy
        DeploymentBuilder deploymentBuilder = repositoryService.createDeployment();
        deploymentBuilder.name(name);
        for (DeploymentResource resource: resources) {
            deploymentBuilder.addInputStream(resource.getName(), new ByteArrayInputStream(resource.getPayload().getBytes(StandardCharsets.UTF_8)));
        }
        deploymentBuilder.enableDuplicateFiltering(false);  // true could let to broken deployment://references
        DeploymentWithDefinitions deployment = deploymentBuilder.deployWithResult();
        // Query
        ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
        List<ProcessDefinition> processDefinitions = processDefinitionQuery.deploymentId(deployment.getId()).list();
        // Migrate
        for (ProcessDefinition created: processDefinitions) {
            ProcessDefinition previous = null;
            if (migrate && created.getVersion() > 1) {
                ProcessDefinitionQuery previousDefinitionQuery = repositoryService.createProcessDefinitionQuery();
                previous = previousDefinitionQuery
                        .processDefinitionName(created.getName())
                        .processDefinitionVersion(created.getVersion() - 1)
                        .singleResult();
            }
            // Should migrate?
            if (migrate && previous != null) {
                ProcessInstanceQuery processInstanceQuery = runtimeService.createProcessInstanceQuery();
                List<ProcessInstance> processInstances = processInstanceQuery.processDefinitionId(previous.getId()).list();
                List<String> processInstanceIds = new ArrayList<>();
                for (ProcessInstance processInstance : processInstances) {
                    processInstanceIds.add(processInstance.getId());
                }
                if (processInstanceIds.size() > 0) {
                    MigrationInstructionsBuilder instructionsBuilder = runtimeService.createMigrationPlan(previous.getId(), created.getId()).mapEqualActivities();
                    instructionsBuilder = instructionsBuilder.updateEventTriggers();
                    MigrationPlan migrationPlan = instructionsBuilder.build();
                    MigrationPlanExecutionBuilder executionBuilder = runtimeService.newMigration(migrationPlan).processInstanceIds(processInstanceIds);
                    executionBuilder.execute();
                    for (ProcessInstance processInstance : processInstances) {
                        StartFormData startFormData = formService.getStartFormData(created.getId());
                        if (startFormData != null) {
                            List<FormField> formFieldList = startFormData.getFormFields();
                            Map<String, Object> variables = runtimeService.getVariables(processInstance.getId());
                            for (FormField formField : formFieldList) {
                                if (!variables.containsKey(formField.getId())) {
                                    FormType formType = formField.getType();
                                    TypedValue value = formField.getValue();
                                    if (formType instanceof AbstractFormFieldType) {
                                        value = ((AbstractFormFieldType) formType).convertToModelValue(value);
                                    }
                                    runtimeService.setVariable(processInstance.getId(), formField.getId(), value);
                                }
                            }
                        }
                    }
                }
            }
        }
        for (ProcessDefinition created: processDefinitions) {
            return created;
        }
        return null;
    }

    public DecisionDefinition insert_camunda_DecisionDefinition(String name, List<DeploymentResource> resources) {
        // Deploy
        DeploymentBuilder deploymentBuilder = repositoryService.createDeployment();
        deploymentBuilder.name(name);
        for (DeploymentResource resource: resources) {
            deploymentBuilder.addInputStream(resource.getName(), new ByteArrayInputStream(resource.getPayload().getBytes(StandardCharsets.UTF_8)));
        }
        deploymentBuilder.enableDuplicateFiltering(false);  // true could let to broken deployment://references
        DeploymentWithDefinitions deployment = deploymentBuilder.deployWithResult();
        // Query
        DecisionDefinitionQuery decisionDefinitionQuery = repositoryService.createDecisionDefinitionQuery();
        List<DecisionDefinition> decisionDefinitions = decisionDefinitionQuery.deploymentId(deployment.getId()).list();
        for (DecisionDefinition created: decisionDefinitions) {
            return created;
        }
        return null;
    }

    public ProcessInstance insert_camunda_ProcessInstance(String processDefinitionKey, String messageName, ArrayList<LinkedHashMap> variables) {
        String businessKey = UUID.randomUUID().toString();
        if (variables != null && variables.size() > 0) {
            if (messageName != null) {
                return runtimeService.startProcessInstanceByMessage(messageName, businessKey, Util.getVariablesMap(variables));
            } else {
                return runtimeService.startProcessInstanceByKey(processDefinitionKey, businessKey, Util.getVariablesMap(variables));
            }
        } else {
            ProcessInstance pi;
            if (messageName != null) {
                pi = runtimeService.startProcessInstanceByMessage(messageName, businessKey);
            } else {
                pi = runtimeService.startProcessInstanceByKey(processDefinitionKey, businessKey);
            }
            try {
                return runtimeService.createProcessInstanceQuery().processInstanceId(pi.getId()).singleResult();
            } catch (NullValueException e) {
                return null;
            } catch (NullPointerException e) {
                return null;
            }
        }
    }

    public ProcessInstance _update_camunda_ProcessInstance_correlate(String userId, List<String> groupIds, String messageName, String processInstanceId, String businessKey, ArrayList<LinkedHashMap> variables) {
        // Check for mandatory args
        if (userId == null || groupIds == null || (processInstanceId == null && businessKey == null)) {
            return null;
        }
        boolean allowed = false; // When allowed, we correlate as engine without authorization
        boolean authorized = false; // When authorized, we correlate as authorized user
        // Check for authorization on process instance
        if (processInstanceId != null) {
            // Check for direct authorization
            authorized = authorizationService.isUserAuthorized(userId, groupIds, UPDATE, PROCESS_INSTANCE, processInstanceId);
            if (!authorized) {
                // Check for authorization by process definition
                List<ProcessInstance> instances = runtimeService.createProcessInstanceQuery().processInstanceId(processInstanceId).list();
                for (ProcessInstance instance : instances) {
                    final String definitionId = instance.getProcessDefinitionId();
                    List<ProcessDefinition> definitions = repositoryService.createProcessDefinitionQuery().processDefinitionId(definitionId).list();
                    for (ProcessDefinition definition: definitions) {
                        authorized = authorizationService.isUserAuthorized(userId, groupIds, UPDATE_INSTANCE, PROCESS_DEFINITION, definition.getKey());
                    }
                }
            }
            // Check when should be allowed by active user task
            if (!authorized) {
                allowed = taskService.createTaskQuery().processInstanceId(processInstanceId).list().size() > 0;
            }
        }
        // Check for authorization on business key
        if (!authorized && businessKey != null) {
            List<ProcessInstance> instances = runtimeService.createProcessInstanceQuery().processInstanceBusinessKey(businessKey).list();
            for (ProcessInstance instance : instances) {
                // Check for direct authorization
                authorized = authorizationService.isUserAuthorized(userId, groupIds, UPDATE, PROCESS_INSTANCE, processInstanceId);
                if (!authorized) {
                    // Check for authorization by process definition
                    final String definitionId = instance.getProcessDefinitionId();
                    List<ProcessDefinition> definitions = repositoryService.createProcessDefinitionQuery().processDefinitionId(definitionId).list();
                    for (ProcessDefinition definition: definitions) {
                        authorized = authorizationService.isUserAuthorized(userId, groupIds, UPDATE_INSTANCE, PROCESS_DEFINITION, definition.getKey());
                    }
                }
                if (!authorized) {
                    // Check for authorization by process definition
                    final String definitionId = instance.getProcessDefinitionId();
                    List<ProcessDefinition> definitions = repositoryService.createProcessDefinitionQuery().processDefinitionId(definitionId).list();
                    for (ProcessDefinition definition: definitions) {
                        authorized = authorizationService.isUserAuthorized(userId, groupIds, UPDATE_INSTANCE, PROCESS_DEFINITION, definition.getKey());
                    }
                }
                if (authorized) {
                    break;
                }
                final String processInstanceId_ = instance.getProcessInstanceId();
                allowed = authorizationService.isUserAuthorized(userId, groupIds, CREATE, PROCESS_INSTANCE, processInstanceId_);
            }
        }
        // Correlate
        if (allowed && !authorized) {
            // User does not have permission for correlation, but we still want to allow them to do it,
            // and it is probably safer to let user correlate without authorization than try add user
            // the required authorizations and then to remote it properly in every scenario.
            ((ServiceImpl) identityService).getCommandExecutor().execute(new Command<Void>() {
                @Override
                public Void execute(CommandContext commandContext) {
                    commandContext.runWithoutAuthorization(new Callable<Void>() {
                        public Void call() {
                            MessageCorrelationBuilder correlation = runtimeService.createMessageCorrelation(messageName);
                            if (processInstanceId != null) {
                                correlation.processInstanceId(processInstanceId);
                            }
                            if (businessKey != null) {
                                correlation.processInstanceBusinessKey(businessKey);
                            }
                            if (variables != null) {
                                correlation.setVariables(Util.getVariablesMap(variables));
                            }
                            correlation.correlate();
                            return null;
                        }
                    });
                    return null;
                }
            });
        } else {
            // User had already enough permissions, so execute with users' permissions, even without tasks
            MessageCorrelationBuilder correlation = runtimeService.createMessageCorrelation(messageName);
            if (processInstanceId != null) {
                correlation.processInstanceId(processInstanceId);
            }
            if (businessKey != null) {
                correlation.processInstanceBusinessKey(businessKey);
            }
            if (variables != null) {
                correlation.setVariables(Util.getVariablesMap(variables));
            }
            correlation.correlate();
        }

        try {
            return runtimeService.createProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
        } catch (NullValueException | NullPointerException e) {
            return null;
        }
    }

    public ProcessInstance update_camunda_ProcessInstance_correlate(String messageName, String processInstanceId, ArrayList<LinkedHashMap> variables) {
        final Authentication authentication = identityService.getCurrentAuthentication();
        if (authentication != null) {
            final String userId = authentication.getUserId();
            final List<String> groupIds = authentication.getGroupIds();
            if (userId != null && groupIds != null) {
                return _update_camunda_ProcessInstance_correlate(userId, groupIds, messageName, processInstanceId, null, variables);
            } else if (userId != null) {
                return _update_camunda_ProcessInstance_correlate(userId, new ArrayList<>(), messageName, processInstanceId, null, variables);
            }
        }
        return null;
    }

    public ProcessInstance update_camunda_ProcessInstance_correlate_by_key(String messageName, String businessKey, ArrayList<LinkedHashMap> variables) {
        final Authentication authentication = identityService.getCurrentAuthentication();
        if (authentication != null) {
            final String userId = authentication.getUserId();
            final List<String> groupIds = authentication.getGroupIds();
            if (userId != null && groupIds != null) {
                return _update_camunda_ProcessInstance_correlate(userId, groupIds, messageName, null, businessKey, variables);
            } else if (userId != null) {
                return _update_camunda_ProcessInstance_correlate(userId, new ArrayList<>(), messageName, null, businessKey, variables);
            }
        }
        return null;
    }

    public Task update_camunda_Task_claim(String id, String userId) {
        taskService.claim(id, null);
        taskService.claim(id, userId);
        try {
            return taskService.createTaskQuery().initializeFormKeys().taskId(id).singleResult();
        } catch (NullValueException | NullPointerException e) {
            return null;
        }
    }

    public Task update_camunda_Task_assignee(String id, String userId) {
        Task task;
        try {
            task = taskService.createTaskQuery().taskId(id).singleResult();
        } catch (NullValueException | NullPointerException e) {
            return null;
        }
        task.setAssignee(userId);
        taskService.saveTask(task);
        return task;
    }

    //@todo issue: ArrayList<LinkedHashMap> should be ArrayList<KeyValuePair>
    public ProcessInstance update_camunda_Task_complete(String id, ArrayList<LinkedHashMap> variables) {
        // Ensure assignee
        if (identityService.getCurrentAuthentication() != null) {
            String userId = identityService.getCurrentAuthentication().getUserId();
            if (userId != null) {
                taskService.claim(id, null);
                taskService.claim(id, userId);
            }
        }

        // TODO: completing processes using form service would be great, but it does not support
        // all the field types and expects specific formats for some types (at least for date)
        FormService formService = engine.getFormService();
        //

        if (variables != null) {
            // formService.submitTaskForm(id, Util.getVariablesMap(variables));
            taskService.complete(id, Util.getVariablesMap(variables));
        } else {
            // Map<String, Object> map = new HashMap<>();
            // formService.submitTaskForm(id, map);
            taskService.complete(id);
        }

        // TODO: We used to return ProcessInstance, and would like to return to do so in the future,
        // but it caused unnecessary OptimisticLockingExceptions e.g. when processes were completed

        return null;
    }

    public Task update_camunda_Task_update(String id, ArrayList<LinkedHashMap> variables) {
        Task task = taskService.createTaskQuery().taskId(id).singleResult();
        if (task == null) {
            return null;
        } else {
            if (variables != null) {
                taskService.setVariables(id, Util.getVariablesMap(variables));
            }
            if (identityService.getCurrentAuthentication() != null) {
                String userId = identityService.getCurrentAuthentication().getUserId();
                if (userId != null) {
                    taskService.claim(id, null);
                    taskService.claim(id, userId);
                }
            }
            return task;
        }
    }

    public Comment insert_camunda_Comment(String id, String message) {
        TaskQuery taskQuery = taskService.createTaskQuery();
        taskQuery = taskQuery.taskId(id);
        taskQuery.initializeFormKeys();
        try {
            Task task = taskQuery.singleResult();
            return taskService.createComment(id, task.getProcessInstanceId(), message);
        } catch (NullValueException | NullPointerException e) {
            return null;
        }
    }
}

