package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import fi.jyu.vasara.graphql.Util;
import fi.jyu.vasara.graphql.types.KeyValuePair;
import org.camunda.bpm.engine.HistoryService;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.history.*;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.identity.UserQuery;
import org.camunda.bpm.engine.impl.persistence.entity.HistoricDetailVariableInstanceUpdateEntity;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.repository.ProcessDefinitionQuery;
import org.camunda.bpm.engine.runtime.Execution;
import org.camunda.bpm.engine.runtime.ExecutionQuery;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.impl.VariableMapImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

@Component
public class HistoricActivityInstanceResolver implements GraphQLResolver<HistoricActivityInstance> {

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    HistoryService historyService;

    @Autowired
    IdentityService identityService;

    public HistoricActivityInstanceResolver() {
    }

    public HistoricActivityInstance getParentActivityInstance(HistoricActivityInstance activityInstance) {
        String parentActivityInstanceId = activityInstance.getParentActivityInstanceId();
        if (parentActivityInstanceId == null) {
            return null;
        } else {
            HistoricActivityInstanceQuery historicActivityInstanceQuery = historyService.createHistoricActivityInstanceQuery();
            return historicActivityInstanceQuery.activityInstanceId(parentActivityInstanceId).singleResult();
        }
    }

    public ProcessDefinition getProcessDefinition(HistoricActivityInstance activityInstance) {
        String processDefinitionId = activityInstance.getProcessDefinitionId();
        if (processDefinitionId == null) {
            return null;
        } else {
            ProcessDefinitionQuery processDefinitionQuery = repositoryService.createProcessDefinitionQuery();
            return processDefinitionQuery.processDefinitionId(processDefinitionId).singleResult();
        }
    }

    public HistoricProcessInstance getProcessInstance(HistoricActivityInstance activityInstance) {
        String processInstanceId = activityInstance.getProcessInstanceId();
        if (processInstanceId == null) {
            return null;
        } else {
            HistoricProcessInstanceQuery processInstanceQuery = historyService.createHistoricProcessInstanceQuery();
            return processInstanceQuery.processInstanceId(processInstanceId).singleResult();
        }
    }

    public Execution getExecution(HistoricActivityInstance activityInstance) {
        String executionId = activityInstance.getExecutionId();
        if (executionId == null) {
            return null;
        } else {
            ExecutionQuery executionQuery = runtimeService.createExecutionQuery();
            return executionQuery.processInstanceId(executionId).singleResult();
        }
    }

    public HistoricTaskInstance getTask(HistoricActivityInstance activityInstance) {
        String taskId = activityInstance.getTaskId();
        if (taskId == null) {
            return null;
        } else {
            HistoricTaskInstanceQuery taskInstanceQuery = historyService.createHistoricTaskInstanceQuery();
            return taskInstanceQuery.taskId(taskId).singleResult();
        }
    }

    public User getAssignee(HistoricActivityInstance activityInstance) {
        String userId = activityInstance.getAssignee();
        if (userId == null) {
            return null;
        } else {
            UserQuery userQuery = identityService.createUserQuery();
            return userQuery.userId(userId).singleResult();
        }
    }

    public String getAssigneeId(HistoricActivityInstance activityInstance) {
        return activityInstance.getAssignee();
    }

    public HistoricProcessInstance getCalledProcessInstance(HistoricActivityInstance activityInstance) {
        String calledProcessInstanceId = activityInstance.getCalledProcessInstanceId();
        if (calledProcessInstanceId == null) {
            return null;
        } else {
            HistoricProcessInstanceQuery processInstanceQuery = historyService.createHistoricProcessInstanceQuery();
            return processInstanceQuery.processInstanceId(calledProcessInstanceId).singleResult();
        }
    }

    public String getStartTime(HistoricActivityInstance activityInstance) {
        Date startTime = activityInstance.getStartTime();
        if (startTime == null) {
            return null;
        } else {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return dateFormat.format(startTime);
        }
    }

    public String getEndTime(HistoricActivityInstance activityInstance) {
        Date endTime = activityInstance.getEndTime();
        if (endTime == null) {
            return null;
        } else {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return dateFormat.format(endTime);
        }
    }

    public Boolean getCanceled(HistoricActivityInstance activityInstance) {
        return activityInstance.isCanceled();
    }

    public Boolean getCompletedScope(HistoricActivityInstance activityInstance) {
        return activityInstance.isCompleteScope();
    }

    public String getRemovalTime(HistoricActivityInstance activityInstance) {
        Date date = activityInstance.getRemovalTime();
        if (date == null) {
            return null;
        } else {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
            dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            return dateFormat.format(date);
        }
    }

    public HistoricProcessInstance getRootProcessInstance(HistoricActivityInstance activityInstance) {
        String processInstanceId = activityInstance.getRootProcessInstanceId();
        if (processInstanceId == null) {
            return null;
        } else {
            HistoricProcessInstanceQuery processInstanceQuery = historyService.createHistoricProcessInstanceQuery();
            return processInstanceQuery.processInstanceId(processInstanceId).singleResult();
        }
    }

    public List<KeyValuePair> getVariables(HistoricActivityInstance activityInstance) {
        List<KeyValuePair> keyValuePairs;

    String activityInstanceId = activityInstance.getId();
    if (activityInstanceId == null) {
        return null;
    }

    HistoricDetailQuery historicDetailQuery = historyService.createHistoricDetailQuery();
    List<HistoricDetail> details = historicDetailQuery.activityInstanceId(activityInstanceId).orderByTime().asc().list();
    VariableMap variableMap = new VariableMapImpl();
    for (HistoricDetail detail: details) {
        if (detail instanceof HistoricDetailVariableInstanceUpdateEntity) {
            HistoricDetailVariableInstanceUpdateEntity variable = (HistoricDetailVariableInstanceUpdateEntity) detail;
            variableMap.putValueTyped(variable.getName(), variable.getTypedValue());
        }
    }
    keyValuePairs = Util.getKeyValuePairs(variableMap);

        return keyValuePairs;
    }
}
