package fi.jyu.vasara.graphql.types;

import org.camunda.bpm.engine.repository.DecisionDefinition;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class DecisionDefinitionResults {

    private List<DecisionDefinition> data = null;
    private Long total = null;

    public DecisionDefinitionResults() {
    }

    public DecisionDefinitionResults(List<DecisionDefinition> data, Long total) {
        this.data = data;
        this.total = total;
    }

    public List<DecisionDefinition> getData () {
        return data;
    }

    public Long getTotal() {
        return total;
    }
}
