package fi.jyu.vasara.graphql.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Comment;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.task.TaskQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
import java.util.logging.Logger;

@Component
public class CommentResolver implements GraphQLResolver<Comment> {

    private final static Logger LOGGER = Logger.getLogger(CommentResolver.class.getName());

    @Autowired
    TaskService taskService;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    IdentityService identityService;

    public CommentResolver() {
    }

    public User getUser(Comment comment) {
        String userId = comment.getUserId();
        if (userId == null) {
            return null;
        } else {
            return identityService.createUserQuery().userId(userId).singleResult();
        }
    }

    public Task getTask(Comment comment) {
        String taskId = comment.getTaskId();

        if (taskId == null)
            return null;

        TaskQuery taskQuery = taskService.createTaskQuery();
        taskQuery = taskQuery.taskId(taskId);
        taskQuery.initializeFormKeys();
        return taskQuery.singleResult();
    }

    public String getTime(Comment comment) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(comment.getTime());
    }

    public String getMessage(Comment comment) {
        return comment.getFullMessage();
    }

    public String getRemovalTime(Comment comment) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return dateFormat.format(comment.getRemovalTime());
    }

    public ProcessInstance getRootProcessInstance(Comment comment) {
        String piId = comment.getRootProcessInstanceId();
        if (piId == null) {
            return null;
        } else {
            return runtimeService.createProcessInstanceQuery().processInstanceId(piId).singleResult();
        }
    }
}
