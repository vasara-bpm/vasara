{
  description = "Vasura Camunda Application";

  # Cachix
  nixConfig = {
    extra-trusted-public-keys =
      "vasara-bpm.cachix.org-1:T18iQZQvYDy/6VdGmttnkkq7rYi3JP0S1RjjdnXNu/4=";
    extra-substituters = "https://vasara-bpm.cachix.org";
  };

  inputs = {

    # Core
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/release-23.11";
    nixpkgs-maven.url = "github:NixOS/nixpkgs/release-22.11";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/master";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    # Flakes
    vasara-bpm = {
      url = "gitlab:vasara-bpm/pkgs";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
    mvn2nix.url = "github:fzakaria/mvn2nix";
  };

  outputs = { self, nixpkgs, flake-utils, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ inputs.mvn2nix.overlay ];
        };
        pkgs-maven = import inputs.nixpkgs-maven { inherit system; };
        pkgs-unstable = import inputs.nixpkgs-unstable { inherit system; };
        jdk = pkgs.jdk17;
        jre = pkgs.temurin-jre-bin-17;
        maven = pkgs-maven.maven.override { inherit jdk; };
        call-name = "vasara-camunda";
      in {

        # Application
        apps.default = {
          type = "app";
          program = "${self.packages.${system}.default}/bin/${call-name}";
        };

        packages.default = pkgs.stdenv.mkDerivation {
          name = call-name;
          src = self.packages.${system}.jar;
          propagatedBuildInputs = [ pkgs.socat ];
          phases = [ "installPhase" "fixupPhase" ];
          installPhase = ''
            mkdir -p $out/bin $out/var/lib
            cp $src $out/var/lib/${call-name}.jar
            cat << EOF > $out/bin/${call-name}
            #!/usr/bin/env bash

            if [ ! -z "\$SOCAT_LISTEN_PORT" ]; then
              # Start socat in the background to support mapping keycloak to localhost in docker-compose
              ${pkgs.socat}/bin/socat TCP-LISTEN:\$SOCAT_LISTEN_PORT,fork,reuseaddr TCP:\$SOCAT_TARGET_HOST:\$SOCAT_TARGET_PORT &
            fi

            exec ${jre}/bin/java \$JAVA_OPTS \$@ -jar $out/var/lib/${call-name}.jar
            EOF
            chmod u+x $out/bin/${call-name}
          '';
        };

        # Jar
        packages.jar = let
          mavenRepository = pkgs.buildMavenRepositoryFromLockFile {
            file = ./mvn2nix-lock.json;
          };
        in pkgs.stdenv.mkDerivation rec {
          pname = "app";
          version = "0.1.0-SNAPSHOT";
          name = "${pname}-${version}.jar";
          src = ./.;

          buildInputs = [ jdk maven ];
          buildPhase = ''
            find . -print0|xargs -0 touch
            echo "mvn package --offline -Dmaven.repo.local=${mavenRepository}"
            mvn package --offline -Dmaven.repo.local=${mavenRepository}
          '';

          installPhase = ''
            mv app/target/${name} $out
            jar i $out
          '';
        };

        # Container image
        packages.image = pkgs.dockerTools.streamLayeredImage {
          name = call-name;
          tag = "latest";
          created = "now";
          contents = [
            (pkgs.buildEnv {
              name = "image-contents";
              paths = [
                pkgs.busybox
                pkgs.dockerTools.fakeNss
                pkgs.dockerTools.usrBinEnv
                pkgs.tini
                self.packages.${system}.default
              ];
              pathsToLink = [ "/etc" "/sbin" "/bin" ];
            })
          ];
          extraCommands = ''
            mkdir -p usr/bin && ln -s /sbin/env usr/bin/env
            mkdir -p tmp && chmod a+rxwt tmp
          '';
          config = {
            Entrypoint = [
              "${pkgs.tini}/bin/tini"
              "--"
              "${self.packages.${system}.default}/bin/${call-name}"
            ];
            Env =
              [ "TMPDIR=/tmp" "HOME=/tmp" "LOG4J_FORMAT_MSG_NO_LOOKUPS=true" ];
            Labels = { };
            User = "nobody";
          };
        };

        # Nomad artifact
        packages.artifact = let
          jar = self.packages.${system}.jar;
          env = pkgs.buildEnv {
            name = "env";
            paths = [
              pkgs.bashInteractive
              pkgs.coreutils
              pkgs.netcat
              pkgs.tini
              jre
              self.packages.${system}.default
            ];
          };
          closure = (pkgs.writeReferencesToFile env);
        in pkgs.runCommand call-name { buildInputs = [ pkgs.makeWrapper ]; } ''
          # aliases
          mkdir -p usr/local/bin
          for filename in ${env}/bin/??*; do
            cat > usr/local/bin/$(basename $filename) << EOF
          #!/usr/local/bin/sh
          set -e
          exec $(basename $filename) "\$@"
          EOF
          done
          rm -f usr/local/bin/sh
          chmod a+x usr/local/bin/*

          # shell
          makeWrapper ${pkgs.bashInteractive}/bin/sh usr/local/bin/sh \
            --set SHELL /usr/local/bin/sh \
            --prefix PATH : ${pkgs.coreutils}/bin \
            --prefix PATH : ${pkgs.netcat}/bin \
            --prefix PATH : ${pkgs.tini}/bin \
            --prefix PATH : ${self.packages.${system}.default}/bin \
            --set LOG4J_FORMAT_MSG_NO_LOOKUPS true \
            --set JAR ${jar}

          # artifact
          tar cvzhP \
            --hard-dereference \
            --exclude="${env}*" \
            --exclude="*ncurses*/ncurses*/ncurses*" \
            --exclude="*pixman-1*/pixman-1*/pixman-1*" \
            --files-from=${closure} ${jar} \
            usr > $out || true
        '';

        # Development shell
        devShells.default = pkgs.mkShell {
          buildInputs = [
            pkgs-unstable.cachix
            pkgs-unstable.jfrog-cli
            pkgs.entr
            pkgs.gnumake
            pkgs.jq
            pkgs.openssl
            pkgs.poetry
            jdk
            maven
            (inputs.mvn2nix.defaultPackage.${system}.override {
              inherit jdk maven;
            })
          ];
        };

        devShells.with-podman =
          inputs.vasara-bpm.devShells.${system}.podman.overrideAttrs (old: {
            buildInputs = old.buildInputs
              ++ self.devShells.${system}.default.buildInputs
              ++ [ pkgs.gnumake pkgs.jq ];
          });

        formatter = pkgs.nixfmt;
      });
}
