package fi.jyu.vasara.scim.endpoints;

import com.unboundid.scim2.common.exceptions.ScimException;
import com.unboundid.scim2.common.types.*;
import com.unboundid.scim2.server.annotations.ResourceType;
import com.unboundid.scim2.server.resources.AbstractServiceProviderConfigEndpoint;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.ws.rs.Path;
import java.net.URI;
import java.util.Collections;

@Component
@ResourceType(
        description = "SCIM 2.0 Service Provider Config",
        name = "ServiceProviderConfig",
        schema = ServiceProviderConfigResource.class,
        discoverable = false
)
@Path("/scim/ServiceProviderConfig")
public class ServiceProviderConfigEndpoint extends AbstractServiceProviderConfigEndpoint {

    @Value("${scim2.service-provider-config.documentationUri}")
    private String documentationUri;

    @Value("${scim2.service-provider-config.patch.supported}")
    private boolean patchSupported;

    @Value("${scim2.service-provider-config.bulk.supported}")
    private boolean bulkSupported;

    @Value("${scim2.service-provider-config.bulk.maxOperations}")
    private int bulkMaxOperations;

    @Value("${scim2.service-provider-config.bulk.maxPayloadSize}")
    private int bulkMaxPayload;

    @Value("${scim2.service-provider-config.filter.supported}")
    private boolean filterSupported;

    @Value("${scim2.service-provider-config.filter.maxResults}")
    private int filterMaxResults;

    @Value("${scim2.service-provider-config.changePassword.supported}")
    private boolean changePasswordSupported;

    @Value("${scim2.service-provider-config.sort.supported}")
    private boolean sortSupported;

    @Value("${scim2.service-provider-config.etag.supported}")
    private boolean eTagSupported;

    @Value("${scim2.service-provider-config.authenticationScheme.name}")
    private String authName;

    @Value("${scim2.service-provider-config.authenticationScheme.description}")
    private String authDesc;

    @Value("${scim2.service-provider-config.authenticationScheme.specUri}")
    private URI authSpecUri;

    @Value("${scim2.service-provider-config.authenticationScheme.documentationUri}")
    private URI authDocUri;

    @Value("${scim2.service-provider-config.authenticationScheme.type}")
    private String authType;

    @Value("${scim2.service-provider-config.authenticationScheme.primary}")
    private boolean authPrimary;

    @Override
    public ServiceProviderConfigResource getServiceProviderConfig() throws ScimException {
        return new ServiceProviderConfigResource(
                documentationUri,
                new PatchConfig(true),
                new BulkConfig(false, 0, 0),
                // We must rely on SimpleSearchResults to filter the result from the complete
                // available result set to not need to implement SCIM filter by ourselves.
                new FilterConfig(true, Integer.MAX_VALUE),
                new ChangePasswordConfig(false),
                new SortConfig(false),
                new ETagConfig(false),
                Collections.singletonList(authenticationScheme())
        );
    }

    private AuthenticationScheme authenticationScheme() {
        return new AuthenticationScheme(
                authName,
                authDesc,
                authSpecUri,
                authDocUri,
                authType,
                authPrimary
        );
    }
}
