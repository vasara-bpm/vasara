package fi.jyu.vasara.scim.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NameUtil {
    public static String guessCallName(String displayName, String firstNames) {
        String[] words1 = displayName.split("\\W+");
        String[] words2 = firstNames.split("\\W+");

        List<String> commonWords = new ArrayList<>();

        for (String word : words1) {
            if (Arrays.asList(words2).contains(word)) {
                commonWords.add(word);
            }
        }

        if (commonWords.isEmpty()) {
            commonWords.addAll(Arrays.asList(words2));
        }

        return String.join(" ", commonWords);
    }
}
