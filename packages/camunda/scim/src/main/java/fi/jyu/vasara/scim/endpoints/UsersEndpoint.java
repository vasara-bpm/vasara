package fi.jyu.vasara.scim.endpoints;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.unboundid.scim2.common.GenericScimResource;
import com.unboundid.scim2.common.exceptions.BadRequestException;
import com.unboundid.scim2.common.exceptions.ResourceNotFoundException;
import com.unboundid.scim2.common.exceptions.ScimException;
import com.unboundid.scim2.common.filters.EqualFilter;
import com.unboundid.scim2.common.filters.Filter;
import com.unboundid.scim2.common.filters.OrFilter;
import com.unboundid.scim2.common.messages.PatchRequest;
import com.unboundid.scim2.common.types.*;
import com.unboundid.scim2.common.utils.ApiConstants;
import com.unboundid.scim2.common.utils.JsonUtils;
import com.unboundid.scim2.common.utils.Parser;
import com.unboundid.scim2.server.annotations.ResourceType;
import com.unboundid.scim2.server.utils.ResourcePreparer;
import com.unboundid.scim2.server.utils.ResourceTypeDefinition;
import com.unboundid.scim2.server.utils.SimpleSearchResults;
import fi.jyu.vasara.scim.ScimSessionFactory;
import fi.jyu.vasara.scim.utils.NameUtil;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.ProcessEngineConfiguration;
import org.camunda.bpm.engine.exception.NullValueException;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.impl.interceptor.Command;
import org.camunda.bpm.engine.impl.interceptor.CommandContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import static com.unboundid.scim2.common.exceptions.BadRequestException.INVALID_VALUE;
import static com.unboundid.scim2.common.utils.ApiConstants.MEDIA_TYPE_SCIM;

@Component
@Controller
@ResourceType(
        description = "Access User Resources",
        name = "User",
        schema = UserResource.class,
        optionalSchemaExtensions = EnterpriseUserExtension.class
)
@Path("/scim/Users")
public class UsersEndpoint {

    private static final ResourceTypeDefinition RESOURCE_TYPE_DEFINITION = ResourceTypeDefinition.fromJaxRsResource(UsersEndpoint.class);

    @Inject
    private IdentityService identityService;

    @Inject
    private ProcessEngineConfiguration processEngineConfiguration;

    @Value("${scim2.baseUrl}")
    private String baseUrl;

    private final static Map<ProcessEngineConfiguration, ScimSessionFactory> dbSessionFactory = new HashMap<>();

    private static <T> T execute(Command<T> command, ProcessEngineConfiguration processEngineConfiguration) {
        if (!dbSessionFactory.containsKey(processEngineConfiguration)) {
            dbSessionFactory.put(processEngineConfiguration, new ScimSessionFactory());
            dbSessionFactory.get(processEngineConfiguration)
                    .initFromProcessEngineConfiguration(
                            ((ProcessEngineConfigurationImpl) processEngineConfiguration),
                            "mappings.xml"
                    );
        }
        return dbSessionFactory.get(processEngineConfiguration).getCommandExecutorTxRequired().execute(command);
    }

    @GET
    @Produces({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    public SimpleSearchResults<UserResource> search(
        @QueryParam(value = ApiConstants.QUERY_PARAMETER_FILTER) final String filterString,
        @Context final UriInfo uriInfo
    ) throws ScimException, URISyntaxException {
        final SimpleSearchResults<UserResource> results =
                new SimpleSearchResults<>(RESOURCE_TYPE_DEFINITION, uriInfo);

        final List<String> userName = new ArrayList<>();
        final List<String> externalId = new ArrayList<>();

        // Collect possible optimized query filters
        if (filterString != null) {
            Filter filter = Parser.parseFilter(filterString);
            if (filter instanceof OrFilter) {
                for (Filter filter_ : filter.getCombinedFilters()) {
                    if (filter_ instanceof EqualFilter && filter_.getAttributePath().toString().equals("userName")) {
                        userName.add(filter_.getComparisonValue().asText());
                    } else if (filter_ instanceof EqualFilter && filter_.getAttributePath().toString().equals("externalId")) {
                        externalId.add(filter_.getComparisonValue().asText());
                    }
                }
            } else if (filter instanceof EqualFilter && filter.getAttributePath().toString().equals("userName")) {
                userName.add(filter.getComparisonValue().asText());
            } else if (filter instanceof EqualFilter && filter.getAttributePath().toString().equals("externalId")) {
                externalId.add(filter.getComparisonValue().asText());
            }
        }

        // Optimized query for userName
        if (!userName.isEmpty() && userName.get(0) != null && !userName.get(0).isEmpty()) {
            UserResource user = execute(new Command<UserResource>() {
                @Override
                public UserResource execute(CommandContext commandContext) {
                    Map<String, String> parameters = new HashMap<String, String>();
                    parameters.put("baseUrl", baseUrl);
                    parameters.put("userName", userName.get(0));
                    try {
                        return (UserResource) commandContext.getDbEntityManager().selectOne("selectUserResourceByUserName", parameters);
                    } finally {
                        commandContext.getDbEntityManager().flush();
                    }
                }
            }, processEngineConfiguration);
            if (user != null) {
                results.add(user);
            }
        }

        // Optimized query for externalId
        if (!externalId.isEmpty() && externalId.get(0) != null && !externalId.get(0).isEmpty()) {
            UserResource user = execute(new Command<UserResource>() {
                @Override
                public UserResource execute(CommandContext commandContext) {
                    Map<String, String> parameters = new HashMap<String, String>();
                    parameters.put("baseUrl", baseUrl);
                    parameters.put("externalId", externalId.get(0));
                    try {
                        return (UserResource) commandContext.getDbEntityManager().selectOne("selectUserResourceByExternalId", parameters);
                    } finally {
                        commandContext.getDbEntityManager().flush();
                    }
                }
            }, processEngineConfiguration);
            if (user != null && !userName.contains(user.getUserName())) {
                results.add(user);
            }
        }

        // Fallback query for all
        if (userName.isEmpty() && externalId.isEmpty()) {
            results.addAll(execute(new Command<List<UserResource>>() {
                @SuppressWarnings({"unchecked"})
                @Override
                public List<UserResource> execute(CommandContext commandContext) {
                    try {
                        return (List<UserResource>) commandContext.getDbEntityManager().selectList("selectUserResources", baseUrl);
                    } finally {
                        commandContext.getDbEntityManager().flush();
                    }
                }
            }, processEngineConfiguration));
        }

        return results;
    }

    @GET
    @Path("/{id}")
    @Produces({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    public Response get(
        @PathParam("id") final String userId,
        @Context final UriInfo uriInfo
    ) throws ResourceNotFoundException, URISyntaxException, BadRequestException {
        UserResource user = getPersistedUserResource(userId);
        ResourcePreparer<UserResource> resourcePreparer = new ResourcePreparer<>(RESOURCE_TYPE_DEFINITION, uriInfo);
        return Response.ok(user.getMeta().getLocation()).entity(resourcePreparer.trimRetrievedResource(user)).build();
    }

    @POST
    @Consumes({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    @Produces({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    public Response post(
            final UserResource sourceUser,
            @Context final UriInfo uriInfo
    ) throws ScimException, URISyntaxException {
        // Ensure valid id
        String userId = sourceUser.getUserName();
        if (userId == null) {
            throw new BadRequestException("Required value 'userName' is missing", INVALID_VALUE);
        }
        if (identityService.createUserQuery().userId(userId).count() > 0) {
            throw new ScimException(409, "uniqueness", "User with userName '" + userId + "' already exists");
        }

        // Create user
        identityService.saveUser(identityService.newUser(userId));
        sourceUser.setId(userId);

        // Create mock to not rely on immediate db availability
        UserResource targetUser = new UserResource();
        targetUser.setId(userId);
        targetUser.setUserName(userId);
        Meta meta = new Meta();
        meta.setResourceType("User");
        meta.setLocation(new URI(baseUrl + "/Users/" + userId));
        meta.setVersion("2");  // updateUserAttributes below would increment 1 to 2
        targetUser.setMeta(meta);

        // Update user
        updateUserAttributes(sourceUser, targetUser);
        if (sourceUser.getGroups() != null) {
            updateUserGroups(sourceUser, targetUser);
        }

        // The server signals successful creation with an HTTP status code 201 (Created)
        // and returns a representation of the resource created.
        ResourcePreparer<UserResource> resourcePreparer = new ResourcePreparer<>(RESOURCE_TYPE_DEFINITION, uriInfo);
        return Response.created(targetUser.getMeta().getLocation()).entity(resourcePreparer.trimRetrievedResource(targetUser)).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    @Produces({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    public Response put(
            @PathParam("id") final String userId,
            final UserResource sourceUser,
            @Context final UriInfo uriInfo
    ) throws ScimException, URISyntaxException {
        // Ensure valid id
        sourceUser.setId(userId);
        sourceUser.setUserName(userId);

        // Update user
        UserResource targetUser = getPersistedUserResource(userId);
        updateUserAttributes(sourceUser, targetUser);
        if (sourceUser.getGroups() != null) {
            updateUserGroups(sourceUser, targetUser);
        }

        // Unless otherwise specified, a successful PUT operation returns a 200
        // OK response code and the entire resource within the response body
        ResourcePreparer<UserResource> resourcePreparer = new ResourcePreparer<>(RESOURCE_TYPE_DEFINITION, uriInfo);
        return Response.ok(targetUser.getMeta().getLocation()).entity(resourcePreparer.trimRetrievedResource(targetUser)).build();
    }

    @PATCH
    @Path("/{id}")
    @Consumes({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    @Produces({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    public synchronized Response patch(
            @PathParam("id") final String userId,
            @QueryParam(value = ApiConstants.QUERY_PARAMETER_ATTRIBUTES) final String attributes,
            final PatchRequest patchRequest,
            @Context final UriInfo uriInfo
    ) throws ScimException, URISyntaxException, JsonProcessingException {
        UserResource targetUser = getPersistedUserResource(userId);

        // Apply patch
        GenericScimResource resource = targetUser.asGenericScimResource();
        patchRequest.apply(resource);
        UserResource sourceUser = JsonUtils.nodeToValue(resource.getObjectNode(), UserResource.class);

        // Ensure valid id
        sourceUser.setId(userId);
        sourceUser.setUserName(userId);
        targetUser.setId(userId);
        targetUser.setUserName(userId);

        // Update user
        updateUserAttributes(sourceUser, targetUser);
        if (sourceUser.getGroups() != null) {
            updateUserGroups(sourceUser, targetUser);
        }

        // The server MUST return a 200 OK if the "attributes" parameter is specified in the request.
        ResourcePreparer<UserResource> resourcePreparer = new ResourcePreparer<>(RESOURCE_TYPE_DEFINITION, uriInfo);
        return Response.ok(targetUser.getMeta().getLocation()).entity(resourcePreparer.trimRetrievedResource(targetUser)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(
            @PathParam("id") final String userId
    ) throws ResourceNotFoundException, URISyntaxException {
        getPersistedUserResource(userId); // ResourceNotFoundException
        identityService.deleteUser(userId);
        return Response.noContent().build();
    }

    private UserResource getPersistedUserResource(final String userId) throws ResourceNotFoundException {
        UserResource user = execute(new Command<UserResource>() {
            @Override
            public UserResource execute(CommandContext commandContext) {
                Map<String, String> parameters = new HashMap<String, String>();
                parameters.put("baseUrl", baseUrl);
                parameters.put("userName", userId);
                try {
                    return (UserResource) commandContext.getDbEntityManager().selectOne("selectUserResourceByUserName", parameters);
                } finally {
                    commandContext.getDbEntityManager().flush();
                }
            }
        }, processEngineConfiguration);
        if (user == null) {
            throw new ResourceNotFoundException("No resource with ID '" + userId + "'");
        } else {
            return user;
        }
    }

    private void updateUserAttributes(final UserResource sourceUser, final UserResource targetUser) throws ResourceNotFoundException {
        // TODO: Skip write when there are no changes
        final String userId = sourceUser.getId();

        // Set externalId and active
        execute(new Command<UserResource>() {
            @Override
            public UserResource execute(CommandContext commandContext) {
                Map<String, Object> parameters = new HashMap<>();
                parameters.put("id", userId);
                parameters.put("active", sourceUser.getActive());
                parameters.put("externalId", sourceUser.getExternalId());
                try {
                    commandContext.getDbEntityManager().flush();
                    return (UserResource) commandContext.getDbEntityManager().selectOne("updateUserResourceExternalIdById", parameters);
                } finally {
                    commandContext.getDbEntityManager().flush();
                }
            }
        }, processEngineConfiguration);
        targetUser.setExternalId(sourceUser.getExternalId());

        // Set rest of the properties
        try {
            User camundaUser = identityService.createUserQuery().userId(userId).singleResult();

            // Set name
            String displayName = sourceUser.getDisplayName();
            String firstName = sourceUser.getName() == null ? null : sourceUser.getName().getGivenName();
            camundaUser.setFirstName(displayName != null && firstName != null
                                        ? NameUtil.guessCallName(displayName, firstName)
                                        : firstName);
            camundaUser.setLastName(sourceUser.getName() == null ? null : sourceUser.getName().getFamilyName());

            if (camundaUser.getFirstName() != null && camundaUser.getLastName() != null) {
                Name name = new Name();
                name.setGivenName(camundaUser.getFirstName());
                name.setFamilyName(camundaUser.getLastName());
                targetUser.setName(name);
            } else {
                targetUser.setName(null);
            }

            // Set email
            boolean workEmailFound = false;
            boolean primaryEmailFound = false;
            if (sourceUser.getEmails() != null) {
                for (Email email : sourceUser.getEmails()) {
                    if (email != null
                            && email.getType() != null
                            && email.getType().equals("work")
                            && email.getValue() != null
                            && !email.getValue().isEmpty()
                    ) {
                        // 1) work email first
                        camundaUser.setEmail(email.getValue().toLowerCase());
                        workEmailFound = true;
                    } else if (!workEmailFound
                            && email != null
                            && email.getPrimary() != null
                            && email.getPrimary()
                            && email.getValue() != null
                            && !email.getValue().isEmpty()
                    ) {
                        // 2) primary email second
                        camundaUser.setEmail(email.getValue().toLowerCase());
                        primaryEmailFound = true;
                    } else if (!workEmailFound
                            && !primaryEmailFound
                            && email != null
                            && email.getValue() != null
                            && !email.getValue().isEmpty()
                    ) {
                        // 3) any email last
                        camundaUser.setEmail(email.getValue().toLowerCase());
                    }
                }
            } else {
                camundaUser.setEmail(null);
            }

            List<Email> emails = new ArrayList<>();
            if (camundaUser.getEmail() != null) {
                Email email = new Email();
                email.setValue(camundaUser.getEmail());
                email.setType("work");
                email.setPrimary(true);
                emails.add(email);
            }
            targetUser.setEmails(emails);

            identityService.saveUser(camundaUser);
        } catch (NullValueException e) {
            throw new ResourceNotFoundException("No resource with ID '" + sourceUser.getId() +'"');
        }
        targetUser.setActive(sourceUser.getActive());
    }

    private void updateUserGroups(final UserResource sourceUser, final UserResource targetUser) throws URISyntaxException {
        String targetUserId = sourceUser.getId();

        List<Group> sourceUserGroups = sourceUser.getGroups() != null ? new ArrayList<>(sourceUser.getGroups()) : new ArrayList<>();
        List<Group> targetUserGroups = targetUser.getGroups() != null ? new ArrayList<>(targetUser.getGroups()) : new ArrayList<>();
        List<Group> targetUserGroupsRemovals = new ArrayList<>();

        LinkedHashMap<String, Group> sourceUserGroupsById = new LinkedHashMap<>();
        for (Group group: sourceUserGroups) {
            final String value = group.getValue();
            if (value != null && !value.isEmpty()) {
                sourceUserGroupsById.put(group.getValue(), group);
            }
        }

        for (Group group: targetUserGroups) {
            final String groupId = group.getValue();
            if (sourceUserGroupsById.containsKey(groupId)) {
                sourceUserGroupsById.remove(groupId);
            } else {
                try {
                    identityService.deleteMembership(targetUserId, groupId);
                    targetUserGroupsRemovals.add(group);
                } catch (NullValueException e) {
                    // "No group found with id '...'.: group is null"
                }
            }
        }

        targetUserGroups.removeAll(targetUserGroupsRemovals);

        for (Group group : sourceUserGroupsById.values()) {
            try {
                identityService.createMembership(targetUserId, group.getValue());
                group.setRef(new URI(baseUrl + "/Groups/" + group.getValue()));
                targetUserGroups.add(group);
            } catch (NullValueException e) {
                // "No group found with id '...'.: group is null"
            }
        }

        targetUser.setGroups(targetUserGroups);
    }
}
