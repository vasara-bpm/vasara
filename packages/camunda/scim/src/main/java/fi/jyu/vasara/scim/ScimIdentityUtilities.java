package fi.jyu.vasara.scim;

import com.unboundid.scim2.common.types.UserResource;
import org.camunda.bpm.engine.ProcessEngineConfiguration;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.impl.interceptor.Command;
import org.camunda.bpm.engine.impl.interceptor.CommandContext;

import java.util.HashMap;
import java.util.Map;

public class ScimIdentityUtilities {

    private static ScimSessionFactory dbSessionFactory;

    private static <T> T execute(Command<T> command, ProcessEngineConfiguration processEngineConfiguration) {
        if (dbSessionFactory == null) {
            dbSessionFactory = new ScimSessionFactory();
            dbSessionFactory.initFromProcessEngineConfiguration(
                ((ProcessEngineConfigurationImpl) processEngineConfiguration),
               "mappings.xml"
            );
        }
        return dbSessionFactory.getCommandExecutorTxRequired().execute(command);
    }

    public static Boolean hasActiveUser(String userId, ProcessEngineConfiguration processEngineConfiguration) {
        UserResource user = execute(new Command<UserResource>() {
            @Override
            public UserResource execute(CommandContext commandContext) {
                Map<String, String> parameters = new HashMap<String, String>();
                parameters.put("userName", userId);
                return (UserResource) commandContext.getDbEntityManager().selectOne("selectUserResourceByUserName", parameters);
            }
        }, processEngineConfiguration);

        if (user == null) {
            return false;
        } else if (user.getActive() == null) {
            return true;
        } else {
            return user.getActive();
        }
    }
}
