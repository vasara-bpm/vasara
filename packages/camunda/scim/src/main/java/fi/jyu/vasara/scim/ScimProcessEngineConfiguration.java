package fi.jyu.vasara.scim;

import org.camunda.bpm.engine.spring.SpringProcessEngineConfiguration;
import org.camunda.bpm.spring.boot.starter.configuration.CamundaProcessEngineConfiguration;
import org.camunda.bpm.spring.boot.starter.configuration.impl.AbstractCamundaConfiguration;
import org.springframework.stereotype.Component;


@Component
public class ScimProcessEngineConfiguration extends AbstractCamundaConfiguration implements CamundaProcessEngineConfiguration {

    @Override
    public void preInit(SpringProcessEngineConfiguration configuration) {
        configuration.setSchemaOperationsCommand(new ScimSchemaOperationCommand());
    }

}
