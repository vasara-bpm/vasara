package fi.jyu.vasara.scim.endpoints;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.unboundid.scim2.common.GenericScimResource;
import com.unboundid.scim2.common.filters.EqualFilter;
import com.unboundid.scim2.common.filters.Filter;
import com.unboundid.scim2.common.filters.OrFilter;
import com.unboundid.scim2.common.exceptions.BadRequestException;
import com.unboundid.scim2.common.exceptions.ResourceNotFoundException;
import com.unboundid.scim2.common.exceptions.ScimException;
import com.unboundid.scim2.common.messages.PatchRequest;
import com.unboundid.scim2.common.types.GroupResource;
import com.unboundid.scim2.common.types.Member;
import com.unboundid.scim2.common.types.Meta;
import com.unboundid.scim2.common.utils.ApiConstants;
import com.unboundid.scim2.common.utils.JsonUtils;
import com.unboundid.scim2.common.utils.Parser;
import com.unboundid.scim2.server.annotations.ResourceType;
import com.unboundid.scim2.server.utils.ResourcePreparer;
import com.unboundid.scim2.server.utils.ResourceTypeDefinition;
import com.unboundid.scim2.server.utils.SimpleSearchResults;
import fi.jyu.vasara.scim.ScimSessionFactory;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.ProcessEngineConfiguration;
import org.camunda.bpm.engine.exception.NullValueException;
import org.camunda.bpm.engine.identity.Group;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.impl.interceptor.Command;
import org.camunda.bpm.engine.impl.interceptor.CommandContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

import static com.unboundid.scim2.common.exceptions.BadRequestException.INVALID_VALUE;
import static com.unboundid.scim2.common.utils.ApiConstants.MEDIA_TYPE_SCIM;

@Component
@ResourceType(
        description = "Access Group Resources",
        name = "Group",
        schema = GroupResource.class
)
@Path("/scim/Groups")
public class GroupsEndpoint {

    private static final ResourceTypeDefinition RESOURCE_TYPE_DEFINITION = ResourceTypeDefinition.fromJaxRsResource(GroupsEndpoint.class);

    @Inject
    private IdentityService identityService;

    @Inject
    private ProcessEngineConfiguration processEngineConfiguration;

    @Value("${scim2.baseUrl}")
    private String baseUrl;

    private final static Map<ProcessEngineConfiguration, ScimSessionFactory> dbSessionFactory = new HashMap<>();

    private static <T> T execute(Command<T> command, ProcessEngineConfiguration processEngineConfiguration) {
        if (!dbSessionFactory.containsKey(processEngineConfiguration)) {
            dbSessionFactory.put(processEngineConfiguration, new ScimSessionFactory());
            dbSessionFactory.get(processEngineConfiguration)
                    .initFromProcessEngineConfiguration(
                            ((ProcessEngineConfigurationImpl) processEngineConfiguration),
                            "mappings.xml"
                    );
        }
        return dbSessionFactory.get(processEngineConfiguration).getCommandExecutorTxRequired().execute(command);
    }

    @GET
    @Produces({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    public SimpleSearchResults<GroupResource> search(
            @QueryParam(value = ApiConstants.QUERY_PARAMETER_FILTER) final String filterString,
            @QueryParam(value = ApiConstants.QUERY_PARAMETER_EXCLUDED_ATTRIBUTES) final String excludedAttributes,
            @Context final UriInfo uriInfo
    ) throws ScimException, URISyntaxException {
        final SimpleSearchResults<GroupResource> results =
                new SimpleSearchResults<>(RESOURCE_TYPE_DEFINITION, uriInfo);

        final boolean withMembers = excludedAttributes == null || !excludedAttributes.contains("members");

        final List<String> id = new ArrayList<>();
        final List<String> externalId = new ArrayList<>();

        // Collect possible optimized query filters
        if (filterString != null) {
            Filter filter = Parser.parseFilter(filterString);
            if (filter instanceof OrFilter) {
                for (Filter filter_ : filter.getCombinedFilters()) {
                    if (filter_ instanceof EqualFilter && filter_.getAttributePath().toString().equals("id")) {
                        id.add(filter_.getComparisonValue().asText());
                    } else if (filter_ instanceof EqualFilter && filter_.getAttributePath().toString().equals("externalId")) {
                        externalId.add(filter_.getComparisonValue().asText());
                    }
                }
            } else if (filter instanceof EqualFilter && filter.getAttributePath().toString().equals("id")) {
                id.add(filter.getComparisonValue().asText());
            } else if (filter instanceof EqualFilter && filter.getAttributePath().toString().equals("externalId")) {
                externalId.add(filter.getComparisonValue().asText());
            }
        }

        // Optimized query for id
        if (!id.isEmpty() && id.get(0) != null && !id.get(0).isEmpty()) {
            GroupResource group = execute(new Command<GroupResource>() {
                @Override
                public GroupResource execute(CommandContext commandContext) {
                    Map<String, String> parameters = new HashMap<String, String>();
                    parameters.put("baseUrl", baseUrl);
                    parameters.put("id", id.get(0));
                    try {
                        return withMembers
                            ? (GroupResource) commandContext.getDbEntityManager().selectOne("selectGroupResourceById", parameters)
                            : (GroupResource) commandContext.getDbEntityManager().selectOne("selectGroupResourceByIdWithoutMembers", parameters);
                    } finally {
                        commandContext.getDbEntityManager().flush();
                    }
                }
            }, processEngineConfiguration);
            if (group != null) {
                results.add(group);
            }
        }

        // Optimized query for externalId
        if (!externalId.isEmpty() && externalId.get(0) != null && !externalId.get(0).isEmpty()) {
            GroupResource group = execute(new Command<GroupResource>() {
                @Override
                public GroupResource execute(CommandContext commandContext) {
                    Map<String, String> parameters = new HashMap<String, String>();
                    parameters.put("baseUrl", baseUrl);
                    parameters.put("externalId", externalId.get(0));
                    try {
                        return withMembers
                                ? (GroupResource) commandContext.getDbEntityManager().selectOne("selectGroupResourceById", parameters)
                                : (GroupResource) commandContext.getDbEntityManager().selectOne("selectGroupResourceByIdWithoutMembers", parameters);
                    } finally {
                        commandContext.getDbEntityManager().flush();
                    }
                }
            }, processEngineConfiguration);
            if (group != null && !id.contains(group.getId())) {
                results.add(group);
            }
        }

        // Fallback query for all
        if (id.isEmpty() && externalId.isEmpty()) {
            results.addAll(execute(new Command<List<GroupResource>>() {
                @SuppressWarnings({"unchecked"})
                @Override
                public List<GroupResource> execute(CommandContext commandContext) {
                    try {
                        return withMembers
                                ? (List<GroupResource>) commandContext.getDbEntityManager().selectList("selectGroupResources", baseUrl)
                                : (List<GroupResource>) commandContext.getDbEntityManager().selectList("selectGroupResourcesWithoutMembers", baseUrl);
                    } finally {
                        commandContext.getDbEntityManager().flush();
                    }
                }
            }, processEngineConfiguration));
        }

        return results;
    }

    @GET
    @Path("/{id}")
    @Produces({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    public Response get(
            @PathParam("id") final String groupId,
            @QueryParam(value = ApiConstants.QUERY_PARAMETER_EXCLUDED_ATTRIBUTES) final String excludedAttributes,
            @Context final UriInfo uriInfo
    ) throws ResourceNotFoundException, URISyntaxException, JsonProcessingException, BadRequestException {
        final boolean withMembers = excludedAttributes == null || !excludedAttributes.contains("members");
        GroupResource group = getPersistedGroupResource(groupId, withMembers);
        ResourcePreparer<GroupResource> resourcePreparer = new ResourcePreparer<>(RESOURCE_TYPE_DEFINITION, uriInfo);
        return Response.ok(group.getMeta().getLocation()).entity(resourcePreparer.trimRetrievedResource(group)).build();
    }

    @POST
    @Consumes({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    @Produces({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    public Response post(
            GroupResource sourceGroup,
            @Context final UriInfo uriInfo
    ) throws ScimException, URISyntaxException {
        // Ensure valid id
        String groupId = sourceGroup.getId() != null && !sourceGroup.getId().isEmpty()
            ? sourceGroup.getId()
            : sourceGroup.getDisplayName();
        if (groupId == null) {
            throw new BadRequestException("Required value 'id' is missing", INVALID_VALUE);
        }
        if (identityService.createGroupQuery().groupId(groupId).count() > 0) {
            throw new ScimException(409, "uniqueness", "Group with id '" + groupId + "' already exists");
        }

        // Create group
        Group camundaGroup = identityService.newGroup(groupId);
        camundaGroup.setType("SCIM");
        identityService.saveGroup(camundaGroup);
        sourceGroup.setId(groupId);

        // Create mock to not rely on immediate db availability
        GroupResource targetGroup = new GroupResource();
        targetGroup.setId(groupId);
        targetGroup.setDisplayName(
            sourceGroup.getDisplayName() != null && !sourceGroup.getDisplayName().isEmpty()
                ? sourceGroup.getDisplayName()
                : sourceGroup.getId()
        );
        Meta meta = new Meta();
        meta.setResourceType("Group");
        meta.setLocation(new URI(baseUrl + "/Group/" + groupId));
        meta.setVersion("2");  // updateGroupAttributes below would increment 1 to 2
        targetGroup.setMeta(meta);

        // Update group
        updateGroupAttributes(sourceGroup, targetGroup);
        updateGroupMemberships(sourceGroup, targetGroup);

        // The server signals successful creation with an HTTP status code 201 (Created)
        // and returns a representation of the resource created.
        ResourcePreparer<GroupResource> resourcePreparer = new ResourcePreparer<>(RESOURCE_TYPE_DEFINITION, uriInfo);
        return Response.created(targetGroup.getMeta().getLocation()).entity(resourcePreparer.trimRetrievedResource(targetGroup)).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    @Produces({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    public Response put(
            @PathParam("id") final String groupId,
            final GroupResource sourceGroup,
            @Context final UriInfo uriInfo
    ) throws ResourceNotFoundException, URISyntaxException, BadRequestException {
        // Ensure valid id
        sourceGroup.setId(groupId);

        // Update group
        GroupResource targetGroup = getPersistedGroupResource(groupId);
        updateGroupAttributes(sourceGroup, targetGroup);
        updateGroupMemberships(sourceGroup, targetGroup);

        // Unless otherwise specified, a successful PUT operation returns a 200
        // OK response code and the entire resource within the response body
        ResourcePreparer<GroupResource> resourcePreparer = new ResourcePreparer<>(RESOURCE_TYPE_DEFINITION, uriInfo);
        return Response.ok(targetGroup.getMeta().getLocation()).entity(resourcePreparer.trimRetrievedResource(targetGroup)).build();
    }

    @PATCH
    @Path("/{id}")
    @Consumes({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    @Produces({MEDIA_TYPE_SCIM, MediaType.APPLICATION_JSON_VALUE})
    public synchronized Response patch(
            @PathParam("id") final String groupId,
            final PatchRequest patchRequest,
            @Context final UriInfo uriInfo
    ) throws ScimException, URISyntaxException, JsonProcessingException {
        GroupResource targetGroup = getPersistedGroupResource(groupId);

        // Apply patch
        GenericScimResource resource = targetGroup.asGenericScimResource();
        patchRequest.apply(resource);
        GroupResource sourceGroup = JsonUtils.nodeToValue(resource.getObjectNode(), GroupResource.class);

        // Ensure valid id
        sourceGroup.setId(groupId);
        targetGroup.setId(groupId);

        // Update group
        updateGroupAttributes(sourceGroup, targetGroup);
        updateGroupMemberships(sourceGroup, targetGroup);

        // The server MUST return a 200 OK if the "attributes" parameter is specified in the request.
        // ... but Azure AD expects to see the response even without attributes
        ResourcePreparer<GroupResource> resourcePreparer = new ResourcePreparer<>(RESOURCE_TYPE_DEFINITION, uriInfo);
        return Response.ok(targetGroup.getMeta().getLocation()).entity(resourcePreparer.trimRetrievedResource(targetGroup)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(
            @PathParam("id") final String groupId
    ) throws ResourceNotFoundException, URISyntaxException {
        getPersistedGroupResource(groupId); // ResourceNotFoundException
        identityService.deleteGroup(groupId);
        return Response.noContent().build();
    }

    private GroupResource getPersistedGroupResource(final String id, boolean withMembers) throws ResourceNotFoundException {
        GroupResource group = execute(new Command<GroupResource>() {
            @Override
            public GroupResource execute(CommandContext commandContext) {
                Map<String, String> parameters = new HashMap<String, String>();
                parameters.put("baseUrl", baseUrl);
                parameters.put("id", id);
                try {
                    commandContext.getDbEntityManager().flush();
                    return withMembers
                        ? (GroupResource) commandContext.getDbEntityManager().selectOne("selectGroupResourceById", parameters)
                        : (GroupResource) commandContext.getDbEntityManager().selectOne("selectGroupResourceByIdWithoutMembers", parameters);
                } finally {
                    commandContext.getDbEntityManager().flush();
                }
            }
        }, processEngineConfiguration);
        if (group == null) {
            throw new ResourceNotFoundException("No resource with ID '" + id + "'");
        } else {
            return group;
        }
    }

    private GroupResource getPersistedGroupResource(final String id) throws ResourceNotFoundException {
        return getPersistedGroupResource(id, true);
    }

    private void updateGroupAttributes(final GroupResource sourceGroup, final GroupResource targetGroup) throws ResourceNotFoundException {
        final String sourceGroupId = sourceGroup.getId();
        final String sourceExternalId = sourceGroup.getExternalId();
        final String targetExternalId = targetGroup.getExternalId();

        // Update externalId
        if ((sourceExternalId == null && targetExternalId != null) ||
            (sourceExternalId != null && !sourceExternalId.equals(targetExternalId))) {
            execute(new Command<GroupResource>() {
                @Override
                public GroupResource execute(CommandContext commandContext) {
                    Map<String, String> parameters = new HashMap<String, String>();
                    parameters.put("id", sourceGroupId);
                    parameters.put("externalId", sourceGroup.getExternalId());
                    try {
                        return (GroupResource) commandContext.getDbEntityManager().selectOne("updateGroupResourceExternalIdById", parameters);
                    } finally {
                        commandContext.getDbEntityManager().flush();
                    }
                }
            }, processEngineConfiguration);
            targetGroup.setExternalId(sourceExternalId);
        }

        // Update displayName
        final String sourceGroupDisplayName = sourceGroup.getDisplayName() != null && !sourceGroup.getDisplayName().isEmpty()
            ? sourceGroup.getDisplayName()
            : sourceGroupId;
        try {
            Group camundaGroup = identityService.createGroupQuery().groupId(sourceGroupId).singleResult();
            if (camundaGroup.getName() == null || !camundaGroup.getName().equals(sourceGroupDisplayName)) {
                camundaGroup.setName(sourceGroupDisplayName);
                identityService.saveGroup(camundaGroup);
            }
            targetGroup.setDisplayName(sourceGroupDisplayName);
        } catch (NullValueException e) {
            throw new ResourceNotFoundException("No resource with ID '" + sourceGroup.getId() +'"');
        }
    }

    private void updateGroupMemberships(final GroupResource sourceGroup, final GroupResource targetGroup) throws URISyntaxException {
        String sourceGroupId = sourceGroup.getId();

        List<Member> sourceGroupMembers = sourceGroup.getMembers() != null ? new ArrayList<>(sourceGroup.getMembers()) : new ArrayList<>();
        List<Member> targetGroupMembers = targetGroup.getMembers() != null ? new ArrayList<>(targetGroup.getMembers()) : new ArrayList<>();
        List<Member> targetGroupMembersRemovals = new ArrayList<>();

        LinkedHashMap<String, Member> sourceGroupMembersById = new LinkedHashMap<>();
        for (Member member: sourceGroupMembers) {
            final String value = member.getValue();
            if (value != null && !value.isEmpty()) {
                sourceGroupMembersById.put(member.getValue(), member);
            }
        }

        for (Member member: targetGroupMembers) {
            final String userId = member.getValue();
            if (sourceGroupMembersById.containsKey(userId)) {
                sourceGroupMembersById.remove(userId);
            } else {
                try {
                    identityService.deleteMembership(userId, sourceGroupId);
                    targetGroupMembersRemovals.add(member);
                } catch (NullValueException e) {
                    // "No user found with id '...'.: user is null"
                }
            }
        }

        targetGroupMembers.removeAll(targetGroupMembersRemovals);

        for (Member member : sourceGroupMembersById.values()) {
            try {
                identityService.createMembership(member.getValue(), sourceGroupId);
                member.setRef(new URI(baseUrl + "/Users/" + member.getValue()));
                targetGroupMembers.add(member);
            } catch (NullValueException e) {
                // "No user found with id '...'.: user is null"
            }
        }

        targetGroup.setMembers(targetGroupMembers);
    }
}
