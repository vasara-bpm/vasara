package fi.jyu.vasara.scim;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import fi.jyu.vasara.scim.utils.NameUtil;

public class NameUtilTest {
    @Test
    public void testFindNameUtil() {
        String displayName = "Foobar, John";
        String firstNames = "John Doe";
        String expected = "John";
        String result = NameUtil.guessCallName(displayName, firstNames);
        assertEquals(expected, result);
    }

    @Test
    public void testFindNameUtilReversed() {
        String displayName = "Foobar, John";
        String firstNames = "Doe John";
        String expected = "John";
        String result = NameUtil.guessCallName(displayName, firstNames);
        assertEquals(expected, result);
    }

    @Test
    public void testFindNameUtil_EmptyStrings() {
        String displayName = "";
        String firstNames = "";
        String expected = "";
        String result = NameUtil.guessCallName(displayName, firstNames);
        assertEquals(expected, result);
    }

    @Test
    public void testFindNameUtil_NoCommonPart() {
        String displayName = "Hello";
        String firstNames = "World";
        String expected = "World";
        String result = NameUtil.guessCallName(displayName, firstNames);
        assertEquals(expected, result);
    }

    @Test
    public void testFindNameUtil_OneEmptyString() {
        String displayName = "Hello";
        String firstNames = "";
        String expected = "";
        String result = NameUtil.guessCallName(displayName, firstNames);
        assertEquals(expected, result);
    }

    @Test
    public void testFindNameUtil_BothEmptyStrings() {
        String displayName = "";
        String firstNames = "";
        String expected = "";
        String result = NameUtil.guessCallName(displayName, firstNames);
        assertEquals(expected, result);
    }
}
