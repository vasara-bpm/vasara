package fi.jyu.vasara.scim;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {
        "fi.jyu.vasara.scim",
})
public class ScimServiceTestApplication {

    public static void main(String... args) {
        SpringApplication.run(ScimServiceTestApplication.class, args);
    }
}
