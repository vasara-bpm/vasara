package fi.jyu.vasara.scim;

import com.unboundid.scim2.common.types.Email;
import com.unboundid.scim2.common.types.Name;
import com.unboundid.scim2.common.types.UserResource;
import com.unboundid.scim2.common.utils.JsonUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ScimServiceTest {

    @LocalServerPort
    private int port;

    private String baseUrl;

    private RestTemplate restTemplate;

    static private MediaType APPLICATION_SCIM_JSON = new MediaType("application", "scim+json", StandardCharsets.UTF_8);

    @Autowired
    public void setRestTemplate(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.build();
        baseUrl = "http://localhost:" + port + "/engine-rest";
    }

    final private static ObjectMapper objectMapper = new ObjectMapper();

    @SuppressWarnings("unchecked")
    @Test
    void getEmptyUsersResponseOK() throws Exception {
        String response = restTemplate.getForObject(baseUrl + "/scim/Users", String.class);
        Map<String, Object> result = objectMapper.readValue(response, new TypeReference<>() {});
        assertTrue(result.containsKey("schemas"));
        assertTrue(result.containsKey("totalResults"));
        List<String> schemas = (List<String>) result.get("schemas");
        assertEquals(1, schemas.size());
        assertEquals("urn:ietf:params:scim:api:messages:2.0:ListResponse", schemas.get(0));
        assertEquals(0, result.get("totalResults"));
        assertEquals("{\"schemas\":[\"urn:ietf:params:scim:api:messages:2.0:ListResponse\"],\"totalResults\":0}", response);
    }

    @SuppressWarnings("unchecked")
    @Test
    void postNewUserResponseOK() throws Exception {
        UserResource user = new UserResource();
        user.setUserName("johndoe");
        Name name = new Name();
        name.setFamilyName("Doe");
        name.setGivenName("John");
        user.setName(name);
        List<Email> emails = new ArrayList<>();
        Email email = new Email();
        email.setValue(null);
        emails.add(email);
        email = new Email();
        email.setPrimary(true);
        email.setType("work");
        email.setPrimary(true);
        email.setValue("john.doe@example.com");
        emails.add(email);
        user.setEmails(emails);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(APPLICATION_SCIM_JSON);
        String jsonPayload = JsonUtils.valueToNode(user).toPrettyString();
        HttpEntity<String> requestEntity = new HttpEntity<>(jsonPayload, headers);

        ResponseEntity<String> createResponse = restTemplate.postForEntity(baseUrl + "/scim/Users", requestEntity, String.class);
        assertEquals(createResponse.getStatusCode(), HttpStatus.CREATED);
        assertTrue(createResponse.getHeaders().containsKey("Location"));

        if (createResponse.getHeaders().containsKey("Location")) {
            List<String> location = createResponse.getHeaders().get("Location");
            if (location != null) {
                ResponseEntity<String> getResponse = restTemplate.getForEntity(baseUrl + "/scim/Users/johndoe", String.class);
                assertEquals(getResponse.getStatusCode(), HttpStatus.OK);

                Map<String, Object> result = objectMapper.readValue(getResponse.getBody(), new TypeReference<>() {});
                assertTrue(result.containsKey("emails"));
                List<Map<String, Object>> resultEmails = (List<Map<String, Object>>) result.get("emails");
                assertEquals(1, resultEmails.size());

                restTemplate.delete(baseUrl + "/scim/Users/johndoe");
            }
        }
    }
}
