package fi.jyu.vasara;

import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.ProcessEngineConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class OidcAuthenticationUtilities {

    @Value("${vasara.graphql.secret}")
    public String vasaraGraphQLSecret;

    @Value("${vasara.scim.secret}")
    public String vasaraScimSecret;

    @Value("${vasara.rest.secret}")
    public String vasaraRestSecret;

    @Value("${vasara.tenants.oidc}")
    public String vasaraOidcTenant;

    @Value("${vasara.transient.publicKey}")
    public String vasaraTransientUserPublicKey;

    @Inject
    public IdentityService identityService;

    @Inject
    public ProcessEngineConfiguration processEngineConfiguration;

}
