package fi.jyu.vasara;

import org.camunda.bpm.engine.FormService;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.camunda.bpm.engine.form.FormField;
import org.camunda.bpm.engine.form.FormType;
import org.camunda.bpm.engine.form.StartFormData;
import org.camunda.bpm.engine.impl.form.type.AbstractFormFieldType;
import org.camunda.bpm.engine.variable.value.TypedValue;

import java.util.List;

public class CamundaStartEventExecutionListener implements ExecutionListener {

    public void notify(DelegateExecution execution) throws Exception {
        ProcessEngine processEngine = execution.getProcessEngine();
        FormService formService = processEngine.getFormService();
        StartFormData startFormData = formService.getStartFormData(execution.getProcessDefinitionId());
        if (startFormData != null) {
            List<FormField> formFieldList = startFormData.getFormFields();
            for (FormField formField : formFieldList) {
                if (!execution.hasVariable(formField.getId())) {
                    FormType formType = formField.getType();
                    TypedValue value = formField.getValue();
                    if (formType instanceof AbstractFormFieldType) {
                        value = ((AbstractFormFieldType) formType).convertToModelValue(value);
                    }
                    execution.setVariable(formField.getId(), value);
                }
            }
        }
    }
}
