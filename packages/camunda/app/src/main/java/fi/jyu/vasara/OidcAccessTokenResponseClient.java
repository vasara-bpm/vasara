package fi.jyu.vasara;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.oauth2.client.endpoint.DefaultAuthorizationCodeTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse;

import java.util.concurrent.*;


public class OidcAccessTokenResponseClient implements OAuth2AccessTokenResponseClient<OAuth2AuthorizationCodeGrantRequest> {

    private static final Logger LOG = LoggerFactory.getLogger(OidcAccessTokenResponseClient.class);
    private final OAuth2AccessTokenResponseClient<OAuth2AuthorizationCodeGrantRequest> client;

    public OidcAccessTokenResponseClient() {
        this.client = new DefaultAuthorizationCodeTokenResponseClient();
    }

    public OAuth2AccessTokenResponse getTokenResponse(OAuth2AuthorizationCodeGrantRequest authorizationCodeGrantRequest) {
        final ExecutorService executor = Executors.newCachedThreadPool();
        for (int timeout = 1; timeout < 6; timeout++) {
            Future<OAuth2AccessTokenResponse> future =
                    executor.submit(new GetTokenResponseCallable(this, authorizationCodeGrantRequest));
            try {
                return future.get(timeout, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
                LOG.debug("Timeout " + timeout + "s");
            } catch (InterruptedException e) {
                // Handle the interrupts
            } catch (ExecutionException e) {
                Throwable ee = e.getCause();
                if (timeout > 1) {
                    // We have seen the first call to our IdP to fail, requiring ultiple retries
                    if (ee instanceof OAuth2AuthenticationException) {
                        throw (OAuth2AuthenticationException) ee;
                    }
                }
            } finally {
                future.cancel(true);
            }
        }
        return null;
    }

    public OAuth2AccessTokenResponse getTokenResponseImpl(OAuth2AuthorizationCodeGrantRequest authorizationGrantRequest) {
        return client.getTokenResponse(authorizationGrantRequest);
    }

    static class GetTokenResponseCallable implements Callable<OAuth2AccessTokenResponse> {
        final private OidcAccessTokenResponseClient oidcAccessTokenResponseClient;
        final private OAuth2AuthorizationCodeGrantRequest oAuth2AuthorizationCodeGrantRequest;

        public GetTokenResponseCallable(OidcAccessTokenResponseClient oidcAccessTokenResponseClient, OAuth2AuthorizationCodeGrantRequest authorizationCodeGrantRequest) {
            this.oidcAccessTokenResponseClient = oidcAccessTokenResponseClient;
            this.oAuth2AuthorizationCodeGrantRequest = authorizationCodeGrantRequest;
        }

        public OAuth2AccessTokenResponse call() {
            return this.oidcAccessTokenResponseClient.getTokenResponseImpl(this.oAuth2AuthorizationCodeGrantRequest);
        }
    }
}
