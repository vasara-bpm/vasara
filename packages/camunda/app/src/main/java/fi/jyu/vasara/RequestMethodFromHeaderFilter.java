package fi.jyu.vasara;

import javax.ws.rs.HttpMethod;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;

@Provider
@PreMatching
public class RequestMethodFromHeaderFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext request) {
        String method = request.getHeaderString("X-Method");
        if (method != null && method.toUpperCase().equals("GET")) {
            request.setMethod(HttpMethod.GET);
        } else if (method != null && method.toUpperCase().equals("POST")) {
            request.setMethod(HttpMethod.POST);
        } else if (method != null && method.toUpperCase().equals("PUT")) {
            request.setMethod(HttpMethod.PUT);
        } else if (method != null && method.toUpperCase().equals("PATCH")) {
            request.setMethod(HttpMethod.PATCH);
        } else if (method != null && method.toUpperCase().equals("DELETE")) {
            request.setMethod(HttpMethod.DELETE);
        }
    }
}
