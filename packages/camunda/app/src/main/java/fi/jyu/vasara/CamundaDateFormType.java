package fi.jyu.vasara;

import org.camunda.bpm.engine.impl.form.type.DateFormType;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.engine.variable.value.StringValue;
import org.camunda.bpm.engine.variable.value.TypedValue;

public class CamundaDateFormType extends DateFormType {

    public CamundaDateFormType(String datePattern) {
        super(datePattern);
    }

    public TypedValue convertToFormValue(TypedValue modelValue) {
        if(modelValue.getValue() == null) {
            return Variables.stringValue("", modelValue.isTransient());
        } else if (modelValue instanceof StringValue && ((StringValue) modelValue).getValue().equals("")) {
            return Variables.stringValue("", modelValue.isTransient());
        } else {
            return super.convertToFormValue(modelValue);
        }
    }

    public String convertModelValueToFormValue(Object modelValue) {
        return super.convertModelValueToFormValue(modelValue);
    }

}
