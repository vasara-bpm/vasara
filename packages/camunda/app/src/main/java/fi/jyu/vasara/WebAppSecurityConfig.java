package fi.jyu.vasara;

import org.camunda.bpm.webapp.impl.security.auth.ContainerBasedAuthenticationFilter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.oauth2.client.OAuth2LoginConfigurer;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.web.context.request.RequestContextListener;

import javax.inject.Inject;
import java.util.Collections;


class OAuth2LoginCustomizer implements Customizer<OAuth2LoginConfigurer<HttpSecurity>> {
    public void customize(@NotNull OAuth2LoginConfigurer oAuth2LoginConfigurer) {
        OAuth2LoginConfigurer.UserInfoEndpointConfig userInfoEndpoint =
                oAuth2LoginConfigurer.userInfoEndpoint();
        userInfoEndpoint.oidcUserService(new OidcUserService());

        OAuth2LoginConfigurer.TokenEndpointConfig tokenEndpointConfig =
                oAuth2LoginConfigurer.tokenEndpoint();
        tokenEndpointConfig.accessTokenResponseClient(new OidcAccessTokenResponseClient());
    }
}

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class WebAppSecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${spring.security.oauth2.client.provider.oidc.issuer-uri}")
    private String issuerUri;

    @Value("${spring.security.oauth2.client.provider.oidc.logout-uri}")
    private String logoutUri;

    @Inject
    private ClientRegistrationRepository clientRegistrationRepository;

    @Override
    public void configure(WebSecurity web) {
        // Skip HttpSecurity for GraphQL and SCIM endpoints and authenticate them with filter to handle
        // use-cases for shared secret header authentication and encrypted token authentication
        AntPathRequestMatcher restAntPathRequestMatcher = new AntPathRequestMatcher("/engine-rest/**");
        AntPathRequestMatcher graphqlAntPathRequestMatcher = new AntPathRequestMatcher("/graphql");
        AntPathRequestMatcher metricsAntPathRequestMatcher = new AntPathRequestMatcher("/actuator/prometheus");
        web
                .ignoring()
                .requestMatchers(new OrRequestMatcher(restAntPathRequestMatcher, graphqlAntPathRequestMatcher, metricsAntPathRequestMatcher));
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.headers(headers -> headers.frameOptions(frameOptions -> frameOptions.sameOrigin()));
        http
                // For Camunda UI API calls
                .csrf().ignoringAntMatchers("/camunda/api/**")
                .and()

                // For OIDC https://www.baeldung.com/sso-spring-security-oauth2
                .antMatcher("/**").authorizeRequests()
                .antMatchers("/").permitAll()
                .anyRequest().authenticated()
                .and().logout().logoutSuccessUrl(this.logoutUri).permitAll()
                .and()

                // Customize OAuth2Login to retry IdP calls
                .oauth2Login(new OAuth2LoginCustomizer());
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Bean
    public FilterRegistrationBean<ContainerBasedAuthenticationFilter> containerBasedAuthenticationFilter() {
        FilterRegistrationBean<ContainerBasedAuthenticationFilter> filterRegistration = new FilterRegistrationBean<ContainerBasedAuthenticationFilter>();
        filterRegistration.setFilter(new ContainerBasedAuthenticationFilter());
        filterRegistration.setInitParameters(Collections.singletonMap("authentication-provider", "fi.jyu.vasara.OidcAuthenticationProvider"));
        filterRegistration.setOrder(101);  // make sure the filter is registered after the Spring Security Filter Chain
        filterRegistration.addUrlPatterns("/camunda/*");
        return filterRegistration;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    @Bean
    public FilterRegistrationBean<OidcAuthenticationFilter> tokenBasedAuthenticationFilter() {
        ClientRegistration clientRegistration = this.clientRegistrationRepository.findByRegistrationId("oidc");
        FilterRegistrationBean<OidcAuthenticationFilter> filterRegistration = new FilterRegistrationBean<OidcAuthenticationFilter>();
        filterRegistration.setFilter(new OidcAuthenticationFilter(clientRegistration));
        filterRegistration.setOrder(102); // make sure the filter is registered after the Spring Security Filter Chain
        filterRegistration.addUrlPatterns("/engine-rest/*", "/graphql");
        return filterRegistration;
    }

    @Bean
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }
}
