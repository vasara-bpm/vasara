package fi.jyu.vasara;

import org.camunda.bpm.engine.ProcessEngineConfiguration;
import org.camunda.bpm.engine.impl.bpmn.parser.BpmnParseListener;
import org.camunda.bpm.engine.impl.form.type.AbstractFormFieldType;
import org.camunda.bpm.engine.impl.form.type.DateFormType;
import org.camunda.bpm.engine.impl.form.type.LongFormType;
import org.camunda.bpm.engine.impl.form.validator.FormFieldValidator;
import org.camunda.bpm.engine.impl.form.validator.FormValidators;
import org.camunda.bpm.engine.spring.SpringProcessEngineConfiguration;
import org.camunda.bpm.spring.boot.starter.configuration.Ordering;
import org.camunda.bpm.spring.boot.starter.configuration.impl.AbstractCamundaConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@Order(Ordering.DEFAULT_ORDER + 1)
public class CamundaApplicationConfiguration extends AbstractCamundaConfiguration {

    @Value("${server.hostname}")
    public String hostname;

    @Override
    public void preInit(SpringProcessEngineConfiguration springProcessEngineConfiguration) {
        // These are default for Camunda Spring Boot
        springProcessEngineConfiguration.setJobExecutorDeploymentAware(false);
        springProcessEngineConfiguration.setDatabaseSchemaUpdate("true");
        springProcessEngineConfiguration.setAuthorizationEnabled(true);
        //

        // Register 'json' as valid custom modeler form type
        if (springProcessEngineConfiguration.getCustomFormTypes() == null) {
            springProcessEngineConfiguration.setCustomFormTypes(new ArrayList<>());
        }
        List<AbstractFormFieldType> formTypes = springProcessEngineConfiguration.getCustomFormTypes();
        for (Object formType : formTypes) {
            if (formType instanceof DateFormType) {
                formTypes.remove(formType);
            }
            if (formType instanceof LongFormType) {
                formTypes.remove(formType);
            }
        }

        Map<String, Class<? extends FormFieldValidator>> formValidators = springProcessEngineConfiguration.getCustomFormFieldValidators();
        formValidators.put("pattern", CamundaFormValidatorNoop.class);
        formValidators.put("type", CamundaFormValidatorNoop.class);

        formTypes.add(new CamundaJsonFormType());
        formTypes.add(new CamundaDateFormType("dd/MM/yyyy"));  // Camunda default format!
        formTypes.add(new CamundaNumberFormType());

        springProcessEngineConfiguration.setEnableExceptionsAfterUnhandledBpmnError(true);

        springProcessEngineConfiguration.setUserResourceWhitelistPattern("[a-zA-Z0-9-@\\.]+");
        springProcessEngineConfiguration.setGroupResourceWhitelistPattern("[a-zA-Z0-9-]+");
        springProcessEngineConfiguration.setEnableHistoricInstancePermissions(true);
        springProcessEngineConfiguration.setResourceAuthorizationProvider(new CamundaAuthorizationProvider());
        springProcessEngineConfiguration.setHistoryEventProducer(new CamundaHistoryEventProducer());
        springProcessEngineConfiguration.setHistoryTimeToLive("P730D");

        if (hostname != null && hostname.length() > 0 && !hostname.equals("localhost")) {
            springProcessEngineConfiguration.setHostname(hostname);
        }

        springProcessEngineConfiguration.setHistory(ProcessEngineConfiguration.HISTORY_FULL);

        springProcessEngineConfiguration.setHistoryCleanupEnabled(true);
        springProcessEngineConfiguration.setHistoryCleanupStrategy("removalTimeBased");
        springProcessEngineConfiguration.setHistoryRemovalTimeStrategy("end");
        springProcessEngineConfiguration.setHistoryCleanupBatchWindowStartTime("22:00+0200");
        springProcessEngineConfiguration.setHistoryCleanupBatchWindowEndTime("06:00+0200");
        springProcessEngineConfiguration.setHistoryCleanupBatchSize(100);
        springProcessEngineConfiguration.setHistoryCleanupDegreeOfParallelism(1);

        springProcessEngineConfiguration.setBatchOperationHistoryTimeToLive("7");
        springProcessEngineConfiguration.setHistoryCleanupJobLogTimeToLive("7");

        springProcessEngineConfiguration.setDefaultSerializationFormat("application/json");

        // The default is 1000, which allows significant memory usage. Unfortunately,
        // neither did this have much effect (even with lower values).
        // TODO: Custom cache implementation with automatic flushing.
        springProcessEngineConfiguration.setCacheCapacity(200);

        // Register implicit start event listener for all processes to ensure that all variables defined
        // in start event Form -definition are initialized.
        List<BpmnParseListener> postParseListeners = springProcessEngineConfiguration.getCustomPostBPMNParseListeners();
        postParseListeners.add(new CamundaBpmnParseListener());
        springProcessEngineConfiguration.setCustomPostBPMNParseListeners(postParseListeners);
    }
}
