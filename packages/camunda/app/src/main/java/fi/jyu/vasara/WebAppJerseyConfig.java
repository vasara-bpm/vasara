package fi.jyu.vasara;

import com.fasterxml.jackson.jaxrs.cfg.JaxRSFeature;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.unboundid.scim2.common.utils.JsonUtils;
import com.unboundid.scim2.server.providers.*;
import fi.jyu.vasara.scim.ScimAzureCompatibilityFilter;
import fi.jyu.vasara.scim.endpoints.*;
import org.camunda.bpm.spring.boot.starter.rest.CamundaJerseyResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class WebAppJerseyConfig extends CamundaJerseyResourceConfig {
    private static final Logger log = LoggerFactory.getLogger(WebAppJerseyConfig.class);

    protected void registerAdditionalResources() {
        super.registerAdditionalResources();
        log.info("Configuring SCIM rest api.");

        // Exception Mappers
        this.register(ScimExceptionMapper.class);
        this.register(RuntimeExceptionMapper.class);
        this.register(JsonProcessingExceptionMapper.class);

        // JSON provider
        final JacksonJsonProvider provider =
                new JacksonJsonProvider(JsonUtils.createObjectMapper());
        provider.configure(JaxRSFeature.ALLOW_EMPTY_INPUT, false);
        this.register(provider);

        // Filters
        this.register(RequestMethodFromHeaderFilter.class);
        this.register(ScimAzureCompatibilityFilter.class);
        this.register(DotSearchFilter.class);
        this.register(DefaultContentTypeFilter.class);

        // Metadata endpoints
        this.register(ResourceTypesEndpoint.class);
        this.register(SchemasEndpoint.class);
        this.register(ServiceProviderConfigEndpoint.class);

        // Resource type endpoints
        this.register(UsersEndpoint.class);
        this.register(GroupsEndpoint.class);

        log.info("Finished configuring SCIM rest api.");
    }
}
