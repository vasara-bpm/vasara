package fi.jyu.vasara;

import org.camunda.bpm.engine.impl.history.event.HistoricVariableUpdateEventEntity;
import org.camunda.bpm.engine.impl.history.event.HistoryEventType;
import org.camunda.bpm.engine.impl.history.producer.CacheAwareHistoryEventProducer;
import org.camunda.bpm.engine.impl.persistence.entity.VariableInstanceEntity;

public class CamundaHistoryEventProducer extends CacheAwareHistoryEventProducer {

    protected void initHistoricVariableUpdateEvt(HistoricVariableUpdateEventEntity evt, VariableInstanceEntity variableInstance, HistoryEventType eventType) {
        super.initHistoricVariableUpdateEvt(evt, variableInstance, eventType);
        // Attachments are considered transient. Do not store their contents into history.
        if (variableInstance.getSerializerName().equals("file") && variableInstance.getByteArrayValueId() != null) {
            evt.setByteValue(null);
        }
    }
}
