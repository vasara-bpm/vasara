package fi.jyu.vasara;

import org.camunda.bpm.engine.impl.form.validator.FormFieldValidator;
import org.camunda.bpm.engine.impl.form.validator.FormFieldValidatorContext;
import org.camunda.bpm.engine.variable.value.TypedValue;

public class CamundaFormValidatorNoop implements FormFieldValidator {
    public boolean validate(Object submittedValue, FormFieldValidatorContext validatorContext) {
        return true;
    }
}
