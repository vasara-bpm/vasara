package fi.jyu.vasara.tools;

import org.camunda.bpm.engine.MismatchingMessageCorrelationException;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.camunda.bpm.engine.runtime.MessageCorrelationBuilder;
import org.camunda.bpm.engine.runtime.MessageCorrelationResult;
import org.camunda.bpm.model.bpmn.BpmnModelException;
import org.camunda.bpm.model.bpmn.instance.MessageEventDefinition;
import org.camunda.bpm.model.bpmn.instance.ThrowEvent;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaInputOutput;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaInputParameter;
import org.slf4j.LoggerFactory;

import java.util.*;


public class SendMessageDelegateOne implements JavaDelegate {

    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(SendMessageDelegateOne.class);

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        LOGGER.debug("SendMessageDelegateOne started");

        // Read configuration and variables from input mapping on model
        boolean hasMessageName = false;
        boolean hasCorrelationKeys = false;
        final List<String> variableNames = new ArrayList<>();
        try {
            final CamundaInputOutput camundaInputOutput = execution
                    .getBpmnModelElementInstance()
                    .getExtensionElements()
                    .getElementsQuery()
                    .filterByType(CamundaInputOutput.class).singleResult();
            if (camundaInputOutput != null) {
                for (CamundaInputParameter camundaInputParameter : camundaInputOutput.getCamundaInputParameters()) {
                    String name = camundaInputParameter.getCamundaName();
                    switch (name) {
                        case "messageName":
                            hasMessageName = true;
                            break;
                        case "correlationKeys":
                            hasCorrelationKeys = true;
                            break;
                        default:
                            variableNames.add(name);
                    }
                }
            }
        } catch (BpmnModelException | NullPointerException e) {
          // ExtensionElements is null or does not contain CamundaInputOutput
        }

        // Get messageName from input mapped variable or from model
        String messageName = null;
        if (hasMessageName && execution.hasVariableLocal("messageName")) {
            messageName = (String) execution.getVariableLocal("messageName");
        } else {
            try {
                ThrowEvent messageEvent = (ThrowEvent) execution.getBpmnModelElementInstance();
                MessageEventDefinition messageEventDefinition = (MessageEventDefinition) messageEvent.getEventDefinitions().iterator().next();
                messageName = messageEventDefinition.getMessage().getName();
            } catch (ClassCastException e) {
                // Not called on a ThrowEvent
            }
        }
        if (messageName == null || messageName.length() == 0) {
            LOGGER.warn("No message name present");
            throw new Exception("Unsupported request: Must set messageName or use ThrowEvent") ;
        }

        LOGGER.debug("Will send Message named " + messageName);
        MessageCorrelationBuilder correlation = execution.getProcessEngineServices().getRuntimeService().createMessageCorrelation(messageName);

        // Are correlation keys present?
        if (hasCorrelationKeys && execution.hasVariableLocal("correlationKeys")) {
            // Use correlation keys
            LOGGER.debug("Using Correlation keys ");
            // First lets get the Correlation keys
            TreeMap<String, Object> correlationKeys = (TreeMap<String, Object>) execution.getVariableLocal("correlationKeys");
            Map<String, Object> correlationVariables = new HashMap<>();
            for (String name : correlationKeys.keySet()) {
                switch (name) {
                    case "businessKey":
                    case "processBusinessKey":
                    case "processInstanceBusinessKey":
                        correlation.processInstanceBusinessKey((String) correlationKeys.get(name));
                        break;
                    case "processInstanceId":
                        correlation.processInstanceId((String) correlationKeys.get(name));
                        break;
                    default:
                        correlationVariables.put(name, correlationKeys.get(name));
                        break;
                }
            }
            if (correlationVariables.size() > 0) {
                correlation.processInstanceVariablesEqual(correlationVariables);
            }
        } else {
            // Is Business key present?
            String bKey = execution.getBusinessKey();
            if (bKey != null && !bKey.isEmpty()) {
                // Send the message with variableNames
                LOGGER.debug("Using Business key " + bKey);
                correlation.processInstanceBusinessKey(bKey);
            } else {
                LOGGER.warn("No business key or correlation keys present");
                throw new Exception("Unsupported request: Must use business key or correlation keys");
            }
        }

        // Set local variableNames
        for (String name : variableNames) {
            if (execution.hasVariableLocal(name)) {
                correlation.setVariableLocal(name, execution.getVariableLocalTyped(name));
            }
        }

        correlation.correlate();

        LOGGER.debug("SendMessageDelegateOne ended");
    }
}
