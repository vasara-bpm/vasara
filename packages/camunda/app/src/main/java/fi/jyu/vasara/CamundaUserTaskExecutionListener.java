package fi.jyu.vasara;

import org.camunda.bpm.engine.FormService;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.camunda.bpm.engine.form.FormData;
import org.camunda.bpm.engine.form.FormField;
import org.camunda.bpm.engine.form.FormType;
import org.camunda.bpm.engine.impl.form.type.AbstractFormFieldType;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.task.TaskQuery;
import org.camunda.bpm.engine.variable.Variables;
import org.camunda.bpm.engine.variable.value.TypedValue;
import org.camunda.bpm.model.bpmn.BpmnModelException;
import org.camunda.bpm.model.bpmn.instance.camunda.CamundaInputOutput;

import java.util.List;

public class CamundaUserTaskExecutionListener implements ExecutionListener {

    public void notify(DelegateExecution execution) throws Exception {
        ProcessEngine processEngine = execution.getProcessEngine();
        TaskService taskService = processEngine.getTaskService();
        TaskQuery taskQuery = taskService.createTaskQuery();
        Task task = taskQuery.executionId(execution.getId()).singleResult();
        if (task != null) {
            FormService formService = processEngine.getFormService();
            FormData formData = formService.getTaskFormData(task.getId());
            if (formData != null) {
                List<FormField> formFieldList = formData.getFormFields();
                for (FormField formField : formFieldList) {
                    if (!execution.hasVariable(formField.getId())) {
                        FormType formType = formField.getType();
                        TypedValue value = formField.getValue();
                        if (formType instanceof AbstractFormFieldType) {
                            value = ((AbstractFormFieldType) formType).convertToModelValue(value);
                        }
                        execution.setVariable(formField.getId(), value);
                    }
                    // Ensure that empty string fields are empty strings, not nulls
                    if (formField.getTypeName().equals("string") && execution.getVariableTyped(formField.getId()).getValue() == null) {
                        execution.setVariable(formField.getId(), "");
                    }
                }
            }
            // Set convenient 'taskAssignee' local variable if task has a scope of its own
            try {
                final CamundaInputOutput camundaInputOutput = execution
                        .getBpmnModelElementInstance()
                        .getExtensionElements()
                        .getElementsQuery()
                        .filterByType(CamundaInputOutput.class).singleResult();
                if (camundaInputOutput != null
                        && (camundaInputOutput.getCamundaInputParameters().size() > 0
                        || camundaInputOutput.getCamundaOutputParameters().size() > 0)) {
                    // Task with input or output mapping is guaranteed to have a scope of its own
                    execution.setVariableLocal("taskAssignee", Variables.stringValue(task.getAssignee()));
                }
            } catch (BpmnModelException | NullPointerException e) {
                // ExtensionElements is null or does not contain CamundaInputOutput
            }
        }
    }
}
