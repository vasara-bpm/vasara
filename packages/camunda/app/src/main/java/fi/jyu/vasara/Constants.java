package fi.jyu.vasara;

public class Constants {
    public static final String TENANT_SYSTEM = "SYSTEM";
    public static final String TENANT_TRANSIENT = "TRANSIENT";
    public static final String GROUP_TRANSIENT = "camunda-transient";
    public static final String GROUP_AUTHENTICATED = "camunda-authenticated";
}
