package fi.jyu.vasara;

import org.camunda.bpm.engine.delegate.ExecutionListener;
import org.camunda.bpm.engine.impl.bpmn.parser.AbstractBpmnParseListener;
import org.camunda.bpm.engine.impl.pvm.process.ActivityImpl;
import org.camunda.bpm.engine.impl.pvm.process.ScopeImpl;
import org.camunda.bpm.engine.impl.util.xml.Element;


public class CamundaBpmnParseListener extends AbstractBpmnParseListener {

    protected CamundaStartEventExecutionListener camundaStartEventExecutionListener;
    protected CamundaUserTaskExecutionListener camundaUserTaskExecutionListener;

    public CamundaBpmnParseListener() {
        this.camundaStartEventExecutionListener = new CamundaStartEventExecutionListener();
        this.camundaUserTaskExecutionListener = new CamundaUserTaskExecutionListener();
    }

    @Override
    public void parseStartEvent(Element element, ScopeImpl scope, ActivityImpl activity) {
        activity.addListener(ExecutionListener.EVENTNAME_END, camundaStartEventExecutionListener);

    }

    @Override
    public void parseUserTask(Element element, ScopeImpl scope, ActivityImpl activity) {
        activity.addListener(ExecutionListener.EVENTNAME_END, camundaUserTaskExecutionListener);
    }
}

