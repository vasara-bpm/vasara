package fi.jyu.vasara;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.oauth2.client.oidc.userinfo.OidcUserRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;

import java.util.concurrent.*;


public class OidcUserService extends org.springframework.security.oauth2.client.oidc.userinfo.OidcUserService {
    private static final Logger LOG = LoggerFactory.getLogger(OidcUserService.class);

    @Override
    public OidcUser loadUser(OidcUserRequest oidcUserRequest) throws OAuth2AuthenticationException {
        // We have seen random timeouts from our IdP, requiring multiple retries...
        final ExecutorService executor = Executors.newCachedThreadPool();
        for (int timeout = 1; timeout < 6; timeout++) {
            Future<OidcUser> future = executor.submit(new LoadUserCallable(this, oidcUserRequest));
            try {
                return future.get(timeout, TimeUnit.SECONDS);
            } catch (TimeoutException ex) {
                LOG.debug("Timeout " + timeout + "s");
            } catch (InterruptedException e) {
                // handle the interrupts
            } catch (ExecutionException e) {
                Throwable ee = e.getCause();
                // We have seen the first call to our IdP to fail, requiring ultiple retries
                if (timeout > 1) {
                    if (ee instanceof OAuth2AuthenticationException) {
                        throw (OAuth2AuthenticationException) ee;
                    }
                }
            } finally {
                future.cancel(true);
            }
        }
        return null;
    }

    public OidcUser loadUserImpl(OidcUserRequest oidcUserRequest) throws OAuth2AuthenticationException {
        return super.loadUser(oidcUserRequest);
    }

    static class LoadUserCallable implements Callable<OidcUser> {
        final private OidcUserService oidcUserService;
        final private OidcUserRequest oidcUserRequest;

        public LoadUserCallable(OidcUserService oidcUserService, OidcUserRequest oidcUserRequest) {
            this.oidcUserService = oidcUserService;
            this.oidcUserRequest = oidcUserRequest;
        }

        public OidcUser call() {
            return this.oidcUserService.loadUserImpl(this.oidcUserRequest);
        }
    }
}
