.PHONY: image
image: $(SOURCES)
	$$(nix build $(NIX_OPTIONS) --json ../vasara-app#tasklist-image|jq -r .[0].outputs.out)|podman load
