# https://github.com/nmattia/niv
{ sources ? import ./sources.nix, nixpkgs ? sources."nixpkgs"
, system ? builtins.currentSystem }:

let

  overlay = final: pkgs: rec {
    gitignoreSource = pkgs.callPackage ./pkgs/gitignore.nix { };
  };

  pkgs = import nixpkgs {
    overlays = [ overlay ];
    inherit system;
  };

in pkgs
