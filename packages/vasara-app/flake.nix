{
  description = "Vasara App";

  # Cachix
  nixConfig = {
    extra-trusted-public-keys =
      "vasara-bpm.cachix.org-1:T18iQZQvYDy/6VdGmttnkkq7rYi3JP0S1RjjdnXNu/4=";
    extra-substituters = "https://vasara-bpm.cachix.org";
  };

  inputs = {

    # Core
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/release-23.11";
    nixpkgs-nodejs.url = "github:NixOS/nixpkgs/release-22.11";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    gitignore = {
      url = "github:hercules-ci/gitignore.nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    # Extras
    vasara-bpm = {
      url = "gitlab:vasara-bpm/pkgs";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };

    ###

    node2nix-src = {
      url =
        "github:svanderburg/node2nix/315e1b85a6761152f57a41ccea5e2570981ec670";
      flake = false;
    };
  };

  outputs = { self, ... }@inputs:
    inputs.flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import inputs.nixpkgs { inherit system; };
        nodejs = import inputs.nixpkgs-nodejs {
          inherit system;
          overlays = [
            (final: prev: {
              node2nix = (import (inputs.node2nix-src + "/default.nix") {
                pkgs = nodejs;
                inherit system;
              }).package;
            })
          ];
        };
        package-name =
          (builtins.fromJSON (builtins.readFile ./package.json)).name;
        packagesSource = builtins.filterSource (path: type:
          (baseNameOf path) == "package.json" || (baseNameOf path)
          == "package-lock.json") ./.;
        gitignoreSource = inputs.gitignore.lib.gitignoreSource;

        static = mode:
          pkgs.stdenv.mkDerivation {
            name = "vasara-app-static";
            src = builtins.filterSource
              (path: type: (baseNameOf path) != "flake.nix") ./.;
            buildInputs = [ nodejs.nodejs-14_x ];
            builder = pkgs.writeScript "builder.sh" ''
              source $stdenv/setup;
              cp -a $src src && chmod u+w -R src && cd src
              cp -a ${
                self.packages.${system}.node_modules
              } ./node_modules && chmod u+w -R node_modules
              home=$(pwd) npm run-script build
              cp -a build $out
            '';
            REACT_APP_AUTHORITY = "PROCESS_ENV_REACT_APP_AUTHORITY";
            REACT_APP_CLIENT_ID = "PROCESS_ENV_REACT_APP_CLIENT_ID";
            REACT_APP_DEFAULT_TENANT = "PROCESS_ENV_REACT_APP_DEFAULT_TENANT";
            REACT_APP_GRAPHQL_API_URL = "PROCESS_ENV_REACT_APP_GRAPHQL_API_URL";
            REACT_APP_LOGOUT_URI = "PROCESS_ENV_REACT_APP_LOGOUT_URI";
            REACT_APP_PUBLIC_URL = "PROCESS_ENV_REACT_APP_PUBLIC_URL";
            REACT_APP_REDIRECT_URI = "PROCESS_ENV_REACT_APP_REDIRECT_URI";
            REACT_APP_TRANSIENT_USER_PUBLIC_KEY =
              "PROCESS_ENV_REACT_APP_TRANSIENT_USER_PUBLIC_KEY";
            REACT_APP_ZIPKIN_COLLECT_URL =
              "PROCESS_ENV_REACT_APP_ZIPKIN_COLLECT_URL";
            REACT_APP_BUILD_TARGET = mode;
            PUBLIC_URL = "PROCESS_ENV_REACT_APP_PUBLIC_URL";
            IMAGE_INLINE_SIZE_LIMIT = "0";
            INLINE_RUNTIME_CHUNK = "false";
            NODE_OPTIONS="--max-old-space-size=4096";
          };

        app = call-name: static:
          pkgs.stdenv.mkDerivation {
            name = "${call-name}";
            src = gitignoreSource ./.;
            propagatedBuildInputs = [ pkgs.nginx ];
            builder = pkgs.writeScript "builder.sh" ''
              source $stdenv/setup;
              mkdir -p $out/bin/
              cat > $out/bin/${call-name} << EOF
              #!${pkgs.bash}/bin/sh

              # Exit on error
              set -e

              # Copy output to new location
              local=\$(pwd)
              if [[ -d \$local/public ]]; then chmod u+w -R \$local/public; fi
              if [[ -f \$local/nginx.conf ]]; then chmod u+w  \$local/nginx.conf; fi
              cp -R ${static} \$local/public
              chmod u+w -R \$local/public

              # List of environment variables to check
              variables=(
                "REACT_APP_ZIPKIN_COLLECT_URL"
                "REACT_APP_GRAPHQL_API_URL"
                "REACT_APP_CLIENT_ID"
                "REACT_APP_AUTHORITY"
                "REACT_APP_REDIRECT_URI"
                "REACT_APP_LOGOUT_URI"
                "REACT_APP_PUBLIC_URL"
                "REACT_APP_DEFAULT_TENANT"
              )

              for var in "\''${variables[@]}"; do
                # Check if the variable is set
                if [[ -n "\''${!var}" ]]; then
                  # Check if the value is a file path and the file exists
                  if [[ -f "\''${!var}" ]]; then
                    # Read the contents of the file and update the variable
                    value=\$(cat "\''${!var}")
                    eval "\$var=\$value"
                  fi
                fi
              done

              if [[ -f "\$REACT_APP_TRANSIENT_USER_PUBLIC_KEY" ]]; then
                REACT_APP_TRANSIENT_USER_PUBLIC_KEY=\$(cat "\$REACT_APP_TRANSIENT_USER_PUBLIC_KEY" | sed -z "s|\n|\\\\\\\\\\\\\\\\n|g")
              fi

              # Replace environment variables
              find \$local/public \\( -name "*.html" -or -name "*.js" -or -name "*.css" -or -name "*.map" \\) -print0|xargs -0 sed -i "\
              s|PROCESS_ENV_REACT_APP_ZIPKIN_COLLECT_URL|\$REACT_APP_ZIPKIN_COLLECT_URL|g; \
              s|PROCESS_ENV_REACT_APP_GRAPHQL_API_URL|\$REACT_APP_GRAPHQL_API_URL|g; \
              s|PROCESS_ENV_REACT_APP_CLIENT_ID|\$REACT_APP_CLIENT_ID|g; \
              s|PROCESS_ENV_REACT_APP_AUTHORITY|\$REACT_APP_AUTHORITY|g; \
              s|PROCESS_ENV_REACT_APP_REDIRECT_URI|\$REACT_APP_REDIRECT_URI|g; \
              s|PROCESS_ENV_REACT_APP_LOGOUT_URI|\$REACT_APP_LOGOUT_URI|g; \
              s|PROCESS_ENV_REACT_APP_PUBLIC_URL/|\$REACT_APP_PUBLIC_URL|g; \
              s|PROCESS_ENV_REACT_APP_DEFAULT_TENANT|\$REACT_APP_DEFAULT_TENANT|g; \
              s|PROCESS_ENV_REACT_APP_TRANSIENT_USER_PUBLIC_KEY|\$REACT_APP_TRANSIENT_USER_PUBLIC_KEY|g; \
              "

              if [[ -z "\$REACT_APP_CSP_DEFAULT_SRC" ]]; then
                REACT_APP_CSP_DEFAULT_SRC="'self'"
              fi

              mkdir -p \$local/nginx/logs
              cat > \$local/nginx.conf << END_OF_FILE
              daemon off;
              worker_processes 4;
              error_log /dev/stdout info;

              events {
                use epoll;
                worker_connections 128;
              }

              http {
                include ${pkgs.nginx}/conf/mime.types;
                default_type application/octet-stream;
                sendfile on;
                access_log /dev/stdout;
                server {
                  listen \$REACT_APP_PORT;
                  server_name \$REACT_APP_PORT;
                  gzip on;
                  gzip_types text/css application/javascript;
                  gzip_min_length 1000;
                  root \$local/public;
                  location / {
                    index index.html;
                    # kill cache
                    add_header Last-Modified \\\$date_gmt;
                    add_header Cache-Control 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0';
                    if_modified_since off;
                    expires off;
                    etag off;
                    # CSP
                    add_header X-Frame-Options "deny" always;
                    add_header X-XSS-Protection "mode=block" always;
                    add_header X-Content-Type-Options "nosniff" always;
                    add_header Referrer-Policy "same-origin" always;
                    add_header Content-Security-Policy "default-src \$REACT_APP_CSP_DEFAULT_SRC; img-src 'self' https://*.tile.openstreetmap.org; style-src 'self' 'unsafe-inline'; object-src 'none'" always;
                    add_header Feature-Policy "accelerometer 'none'; ambient-light-sensor 'none'; autoplay 'none'; battery 'none'; camera 'none'; display-capture 'none'; document-domain 'none'; encrypted-media 'none'; execution-while-not-rendered 'none'; execution-while-out-of-viewport 'none'; fullscreen 'none'; geolocation 'self'; gyroscope 'none'; layout-animations 'none'; legacy-image-formats 'none'; magnetometer 'none'; microphone 'none'; midi 'none'; navigation-override 'none'; oversized-images 'none'; payment 'none'; picture-in-picture 'none'; publickey-credentials 'none'; sync-xhr 'none'; usb 'none'; vr 'none'; wake-lock 'none'; xr-spatial-tracking 'none'" always;
                    #
                    try_files \\\$uri /index.html =440;
                  }
                }
              }
              END_OF_FILE

              exec ${pkgs.nginx}/bin/nginx -p \$local/nginx -c \$local/nginx.conf
              EOF
              chmod u+x $out/bin/${call-name}
            '';
          };

        image = call-name: package:
          pkgs.dockerTools.streamLayeredImage {
            name = "${call-name}";
            tag = "latest";
            created = "now";
            contents = [
              (pkgs.buildEnv {
                name = "image-contents";
                paths = [
                  pkgs.busybox
                  pkgs.dockerTools.fakeNss
                  pkgs.dockerTools.usrBinEnv
                  pkgs.tini
                  pkgs.nginx
                  package
                ];
                pathsToLink = [ "/etc" "/sbin" "/bin" ];
              })
            ];
            extraCommands = ''
              mkdir -p usr/bin && ln -s /sbin/env usr/bin/env
              mkdir -p tmp && chmod a+rxwt tmp
              mkdir -p var/log/nginx var/cache/nginx && chmod a+rxwt -R var
              mkdir -p app && chmod a+rxwt app
            '';
            config = {
              WorkingDir = "/app";
              Entrypoint =
                [ "${pkgs.tini}/bin/tini" "--" "${package}/bin/${call-name}" ];
              Env = [
                "TMPDIR=/tmp"
                "HOME=/tmp"
                "REACT_APP_PORT=8080"
                "REACT_APP_HOST=0.0.0.0"
                "REACT_APP_PUBLIC_URL=/"
              ];
              Labels = { };
              User = "nobody";
            };
          };

        artifact = call-name: package:
          let
            env = pkgs.buildEnv {
              name = "env";
              paths = [
                pkgs.bashInteractive
                pkgs.coreutils
                pkgs.netcat
                pkgs.tini
                pkgs.nginx
                package
              ];
            };
            closure = (pkgs.writeReferencesToFile env);
          in pkgs.runCommand call-name {
            buildInputs = [ pkgs.makeWrapper ];
          } ''
            # aliases
            mkdir -p usr/local/bin
            for filename in ${env}/bin/??*; do
              cat > usr/local/bin/$(basename $filename) << EOF
            #!/usr/local/bin/sh
            set -e
            exec $(basename $filename) "\$@"
            EOF
            done
            rm -f usr/local/bin/sh
            chmod a+x usr/local/bin/*

            # shell
            makeWrapper ${pkgs.bashInteractive}/bin/sh usr/local/bin/sh \
              --set SHELL /usr/local/bin/sh \
              --prefix PATH : ${pkgs.coreutils}/bin \
              --prefix PATH : ${pkgs.netcat}/bin \
              --prefix PATH : ${pkgs.tini}/bin \
              --prefix PATH : ${package}/bin

            # artifact
            tar cvzhP \
              --hard-dereference \
              --exclude="${env}/*" \
              --exclude="/nix/store/*-node_${package-name}-git*" \
              --exclude="*ncurses*/ncurses*/ncurses*" \
              --files-from=${closure} \
              usr > $out || true
          '';

      in {

        packages.node_modules =
          (import ./node-composition.nix { pkgs = nodejs; }).package.override {
            src = builtins.filterSource (path: type:
              (baseNameOf path) == "package.json" || (baseNameOf path)
              == "package-lock.json") ./.;
            buildInputs = [ pkgs.pkg-config ];
            preRebuild = ''
              # Disable opencollective postinstall
              substituteInPlace node_modules/inferno/package.json \
                --replace "opencollective postinstall" "" \
                --replace '"postinstall": "opencollective-postinstall"' ""
              # Fix eslint cache location
              substituteInPlace node_modules/react-scripts/config/webpack.config.js \
                --replace "cacheLocation:" "cacheLocation: true ? './' : "
            '';
            postInstall = ''
              rm -rf $out/lib/node_modules/*/node_modules/@types/react-native
              mv $out/lib/node_modules/*/node_modules /tmp/_; rm -rf $out; mv /tmp/_ $out
            '';
            DISABLE_OPENCOLLECTIVE = "true";
            CYPRESS_INSTALL_BINARY = "0";
          };

        packages.tasklist = app "vasara-tasklist" (static "tasklist");

        packages.modeler = app "vasara-modeler" (static "modeler");

        packages.tasklist-image =
          image "vasara-tasklist" self.packages.${system}.tasklist;

        packages.modeler-image =
          image "vasara-modeler" self.packages.${system}.modeler;

        packages.tasklist-artifact =
          image "vasara-tasklist" self.packages.${system}.tasklist;

        packages.modeler-artifact =
          image "vasara-modeler" self.packages.${system}.modeler;

        devShells.default = pkgs.mkShell {
          buildInputs = [
            pkgs.cachix
            pkgs.jfrog-cli
            pkgs.entr
            pkgs.jq
            nodejs.node2nix
            nodejs.nodejs-14_x
            pkgs.openssl
          ];
        };

        devShells.with-podman =
          inputs.vasara-bpm.devShells.${system}.podman.overrideAttrs (old: {
            buildInputs = old.buildInputs
              ++ self.devShells.${system}.default.buildInputs;
          });

        formatter = pkgs.nixfmt;
      });
}
