const webpack = require('webpack');
const path = require('path');

module.exports = {
  webpack: {
    plugins: [
      new webpack.NormalModuleReplacementPlugin(
        /useSidebarState/,
        path.resolve(__dirname, './src/Overrides/useSidebarState.ts')
      ),
      new webpack.NormalModuleReplacementPlugin(
        /FieldTitle/,
        path.resolve(__dirname, './src/Overrides/FieldTitle.tsx')
      ),
      new webpack.NormalModuleReplacementPlugin(
        /InputHelperText/,
        path.resolve(__dirname, './src/Overrides/InputHelperText.tsx')
      ),
      new webpack.NormalModuleReplacementPlugin(
        /RadioButtonGroupInputItem/,
        path.resolve(__dirname, './src/Overrides/RadioButtonGroupInputItem.tsx')
      ),
      new webpack.NormalModuleReplacementPlugin(
        /jsrsasign/,
        path.resolve(__dirname, './node_modules/oidc-client/jsrsasign/dist/jsrsasign.js')
      ),
    ],
  },
};
