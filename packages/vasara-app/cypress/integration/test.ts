/// <reference types="cypress" />
import { BPMN, getAllMessages, getAllUserTasks } from '../../src/DataProviders/Camunda/helpers';

describe('DataProviders:Camunda:helpers:BPMN', () => {
  before(function (done) {
    cy.fixture('VasaraTestMessage.bpmn', 'utf-8').then(async diagram => {
      this.diagram = diagram;
      done();
    });
  });
  it('can parse model', async function () {
    const bpmn = await BPMN(this.diagram);
    const registry = bpmn.get('elementRegistry');
    expect(typeof registry).to.be.equal('object');
    expect(typeof registry._elements).to.be.equal('object');
    expect(Object.keys(registry._elements).length).to.be.greaterThan(1);
  });
});

describe('DataProviders:Camunda:helpers:getAllUserTask', () => {
  before(function (done) {
    cy.fixture('VasaraTestMessage.bpmn', 'utf-8').then(async diagram => {
      this.diagram = diagram;
      this.bpmn = await BPMN(diagram);
      done();
    });
  });
  it('can parse user tasks', async function () {
    const tasks = await getAllUserTasks(this.bpmn);
    expect(tasks.length).to.be.greaterThan(0);
  });
});

describe('DataProviders:Camunda:helpers:getAllMessages', () => {
  before(function (done) {
    cy.fixture('VasaraTestMessage.bpmn', 'utf-8').then(async diagram => {
      this.diagram = diagram;
      this.bpmn = await BPMN(diagram);
      done();
    });
  });
  it('can parse messages', async function () {
    const messages = getAllMessages(this.bpmn);
    expect(messages.length).to.be.greaterThan(0);
  });
});
