import { IntrospectionSchema } from 'graphql';
import { CreateParams } from 'ra-core';
import { BuildQueryResult } from 'ra-data-graphql';

import { RaFetchType } from '../types';

export type ActionsQueryBuilder = (
  introspectionResults: IntrospectionSchema,
  raFetchType: RaFetchType,
  resource: string,
  params: CreateParams
) => BuildQueryResult;

export type vault_transit_encrypt_request = {
  businessKey: string;
  confidential: boolean;
  pii: boolean;
  plaintext: string;
  sources: string[];
};

export type vault_transit_encrypt_request_batch = {
  batch: vault_transit_encrypt_request[];
};

export type vault_transit_encrypt_response = {
  ciphertext: string;
};

export type vault_transit_decrypt_request = {
  ciphertext: string;
};

export type vault_transit_decrypt_request_batch = {
  batch: vault_transit_decrypt_request[];
};

export type vault_transit_decrypt_response = {
  plaintext?: string;
};

export type xlsx_import_request = {
  base64: string;
};

export type xlsx_import_response = {
  sheets: Record<string, any[][]>;
};

export type xlsx_export_request = {
  sheets: Record<string, any[][]>;
};

export type xlsx_export_response = {
  base64: string;
};
