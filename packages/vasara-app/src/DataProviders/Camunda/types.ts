import { IntrospectionSchema } from 'graphql';
import {
  CreateParams,
  DeleteManyParams,
  DeleteParams,
  GetListParams,
  GetManyParams,
  GetManyReferenceParams,
  UpdateManyParams,
  UpdateParams,
} from 'ra-core';
import { BuildQueryResult } from 'ra-data-graphql';

import { RaFetchType } from '../types';

export type CamundaQueryBuilder = (
  introspectionResults: IntrospectionSchema,
  raFetchType: RaFetchType,
  resource: string,
  params:
    | GetListParams
    | GetManyParams
    | GetManyReferenceParams
    | UpdateParams
    | UpdateManyParams
    | CreateParams
    | DeleteParams
    | DeleteManyParams
) => BuildQueryResult;

export type TaskUpdateAction = 'update' | 'complete' | 'claim';
