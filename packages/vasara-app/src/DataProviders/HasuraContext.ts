import { Context, createContext } from 'react';

import { HasuraContext as ContextInterface } from './types';

const HasuraContext: Context<ContextInterface> = createContext({} as ContextInterface);

export default HasuraContext;
