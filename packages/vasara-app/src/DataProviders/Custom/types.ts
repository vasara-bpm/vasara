import { IntrospectionSchema } from 'graphql';
import { GetListParams } from 'ra-core';
import { BuildQueryResult } from 'ra-data-graphql';

import { RaFetchType } from '../types';

export type QueryBuilder = (
  introspectionResults: IntrospectionSchema,
  raFetchType: RaFetchType,
  resource: string,
  params: GetListParams
) => BuildQueryResult;
