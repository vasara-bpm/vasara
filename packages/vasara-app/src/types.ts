import { ApolloQueryResult } from 'apollo-client';
import { IntrospectionQuery } from 'graphql/utilities/getIntrospectionQuery';
import { DataProvider } from 'react-admin';

import { HasuraSchema } from './DataProviders/types';

// Upper case format is defined at GraphQL plugin
export type CamundaVariableType = 'BOOLEAN' | 'DATE' | 'STRING' | 'LONG' | 'ENUM' | 'JSON' | 'NULL' | 'DOUBLE';

export interface CamundaVariable {
  key: string;
  value: string;
  valueType: CamundaVariableType;
}

export interface Settings {
  isLite: boolean;
  isEmbedded: boolean;
}

export interface IAppProps {}

export interface IAppState {
  isGuest?: boolean;
  introspectionQueryResponse?: ApolloQueryResult<IntrospectionQuery>;
  mainDataProvider?: DataProvider;
  camundaDataProvider?: DataProvider;
  hasuraDataProvider?: DataProvider;
  actionsDataProvider?: DataProvider;
  customDataProvider?: DataProvider;
  hasuraIntrospectionResults?: HasuraSchema;
}
