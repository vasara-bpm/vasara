import React from 'react';
import { Layout } from 'react-admin';

import GuestAppBar from './GuestAppBar';
import Menu from './GuestMenu';

const CustomLayout = (props: any) => <Layout {...props} menu={Menu} appBar={GuestAppBar} />;

export default CustomLayout;
