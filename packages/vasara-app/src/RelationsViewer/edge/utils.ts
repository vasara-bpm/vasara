import { ArrowHeadType, Node, Position, XYPosition } from 'react-flow-renderer';

// const shift = 10;

// this helper function returns the intersection point
// of the line between the center of the intersectionNode and the target node
function getNodeIntersection(intersectionNode: Node, targetNode: Node): XYPosition {
  // https://math.stackexchange.com/questions/1724792/an-algorithm-for-finding-the-intersection-point-between-a-center-of-vision-and-a
  const {
    width: intersectionNodeWidth,
    height: intersectionNodeHeight,
    position: intersectionNodePosition,
  } = intersectionNode.__rf;
  const targetPosition = targetNode.__rf.position;

  const w = intersectionNodeWidth / 2;
  const h = intersectionNodeHeight / 2;

  const x2 = intersectionNodePosition.x + w;
  const y2 = intersectionNodePosition.y + h;
  const x1 = intersectionNode.id === targetNode.id ? targetPosition.x : targetPosition.x + w;
  const y1 = targetPosition.y + h;

  const xx1 = (x1 - x2) / (2 * w) - (y1 - y2) / (2 * h);
  const yy1 = (x1 - x2) / (2 * w) + (y1 - y2) / (2 * h);
  const a = 1 / (Math.abs(xx1) + Math.abs(yy1));
  const xx3 = a * xx1;
  const yy3 = a * yy1;
  const x = w * (xx3 + yy3) + x2;
  const y = h * (-xx3 + yy3) + y2;

  return { x, y };
}

// returns the position (top,right,bottom or right) passed node compared to the intersection point
function getEdgePosition(node: Node, intersectionPoint: XYPosition) {
  const n = { ...node.__rf.position, ...node.__rf };
  const nx = Math.round(n.x);
  const ny = Math.round(n.y);
  const px = Math.round(intersectionPoint.x);
  const py = Math.round(intersectionPoint.y);

  if (px <= nx + 1) {
    return Position.Left;
  }
  if (px >= nx + n.width - 1 && py < n.y + n.height - 1) {
    return Position.Right;
  }
  if (py <= ny + 1) {
    return Position.Top;
  }
  if (py >= n.y + n.height - 1) {
    return Position.Bottom;
  }

  return Position.Top;
}

// returns the parameters (sx, sy, tx, ty, sourcePos, targetPos) you need to create an edge
export function getEdgeParams(source: Node, target: Node) {
  const sourceIntersectionPoint = getNodeIntersection(source, target);
  const targetIntersectionPoint = getNodeIntersection(target, source);

  const sourcePos = getEdgePosition(source, sourceIntersectionPoint);
  const targetPos = getEdgePosition(target, targetIntersectionPoint);

  let sx = sourceIntersectionPoint.x;
  let sy = sourceIntersectionPoint.y;
  let tx = targetIntersectionPoint.x;
  let ty = targetIntersectionPoint.y;

  // uncomment to have gap between two edges
  // switch (sourcePos) {
  //   case Position.Right: {
  //     if (sourceIntersectionPoint.y >= source.__rf.height + source.__rf.position.y - shift * 2) {
  //       sy = source.__rf.height + source.__rf.position.y - shift * 2;
  //     } else if (sourceIntersectionPoint.y - shift <= source.__rf.position.y) {
  //       sy = source.__rf.position.y + shift;
  //     } else {
  //       sy = sourceIntersectionPoint.y;
  //     }
  //     break;
  //   }
  //   case Position.Left: {
  //     if (sourceIntersectionPoint.y + shift * 2 >= source.__rf.height + source.__rf.position.y - shift) {
  //       sy = source.__rf.height + source.__rf.position.y - shift;
  //     } else {
  //       sy = sourceIntersectionPoint.y + shift * 2;
  //     }
  //     break;
  //   }
  //   case Position.Top: {
  //     if (sourceIntersectionPoint.x - shift * 2 < source.__rf.position.x + shift) {
  //       sx = source.__rf.position.x + shift;
  //     } else {
  //       sx = sourceIntersectionPoint.x - shift * 2;
  //     }
  //     break;
  //   }
  //   case Position.Bottom: {
  //     if (sourceIntersectionPoint.x + shift * 2 >= source.__rf.position.x + source.__rf.width - shift) {
  //       sx = source.__rf.width + source.__rf.position.x - shift;
  //     } else {
  //       sx = sourceIntersectionPoint.x + shift * 2;
  //     }
  //     break;
  //   }
  //   default: {
  //     sx = sourceIntersectionPoint.x;
  //     sy = sourceIntersectionPoint.y;
  //     break;
  //   }
  // }
  //
  // switch (targetPos) {
  //   case Position.Right: {
  //     if (targetIntersectionPoint.y + shift >= target.__rf.height + target.__rf.position.y - shift) {
  //       ty = target.__rf.height + target.__rf.position.y - shift;
  //     } else if (targetIntersectionPoint.y - shift <= target.__rf.position.y) {
  //       ty = target.__rf.position.y + shift * 2;
  //     } else {
  //       ty = targetIntersectionPoint.y + shift;
  //     }
  //     break;
  //   }
  //   case Position.Left: {
  //     if (targetIntersectionPoint.y + shift >= target.__rf.height + target.__rf.position.y - shift * 2) {
  //       ty = target.__rf.height + target.__rf.position.y - shift * 2;
  //     } else {
  //       ty = targetIntersectionPoint.y + shift;
  //     }
  //     break;
  //   }
  //   case Position.Top: {
  //     if (targetIntersectionPoint.x - shift <= target.__rf.position.x + shift * 2) {
  //       tx = target.__rf.position.x + shift * 2;
  //     } else {
  //       tx = targetIntersectionPoint.x - shift;
  //     }
  //     break;
  //   }
  //   case Position.Bottom: {
  //     if (targetIntersectionPoint.x + shift >= target.__rf.width + target.__rf.position.x - shift * 2) {
  //       tx = target.__rf.width + target.__rf.position.x - shift * 2;
  //     } else {
  //       tx = targetIntersectionPoint.x + shift;
  //     }
  //     break;
  //   }
  //   default: {
  //     tx = targetIntersectionPoint.x;
  //     ty = targetIntersectionPoint.y;
  //     break;
  //   }
  // }

  return {
    sx: sx,
    sy: sy,
    tx: tx,
    ty: ty,
    sourcePos,
    targetPos,
  };
}

export function createElements() {
  const elements = [];
  const center = { x: window.innerWidth / 2, y: window.innerHeight / 2 };

  elements.push({ id: 'target', data: { label: 'Target' }, position: center });

  for (let i = 0; i < 8; i++) {
    const degrees = i * (360 / 8);
    const radians = degrees * (Math.PI / 180);
    const x = 250 * Math.cos(radians) + center.x;
    const y = 250 * Math.sin(radians) + center.y;

    elements.push({ id: `${i}`, data: { label: 'Source' }, position: { x, y } });

    elements.push({
      id: `edge-${i}`,
      target: 'target',
      source: `${i}`,
      type: 'floating',
      arrowHeadType: ArrowHeadType.Arrow,
    });
  }

  return elements;
}

export interface GetCenterParams {
  sourceX: number;
  sourceY: number;
  targetX: number;
  targetY: number;
  sourcePosition?: Position;
  targetPosition?: Position;
}

const LeftOrRight = [Position.Left, Position.Right];

export const getCenter = ({
  sourceX,
  sourceY,
  targetX,
  targetY,
  sourcePosition = Position.Bottom,
  targetPosition = Position.Top,
}: GetCenterParams): [number, number, number, number] => {
  const sourceIsLeftOrRight = LeftOrRight.includes(sourcePosition);
  const targetIsLeftOrRight = LeftOrRight.includes(targetPosition);

  // we expect flows to be horizontal or vertical (all handles left or right respectively top or bottom)
  // a mixed edge is when one the source is on the left and the target is on the top for example.
  const mixedEdge = (sourceIsLeftOrRight && !targetIsLeftOrRight) || (targetIsLeftOrRight && !sourceIsLeftOrRight);

  if (mixedEdge) {
    const xOffset = sourceIsLeftOrRight ? Math.abs(targetX - sourceX) : 0;
    const centerX = sourceX > targetX ? sourceX - xOffset : sourceX + xOffset;

    const yOffset = sourceIsLeftOrRight ? 0 : Math.abs(targetY - sourceY);
    const centerY = sourceY < targetY ? sourceY + yOffset : sourceY - yOffset;

    return [centerX, centerY, xOffset, yOffset];
  }

  const xOffset = Math.abs(targetX - sourceX) / 2;
  const centerX = targetX < sourceX ? targetX + xOffset : targetX - xOffset;

  const yOffset = Math.abs(targetY - sourceY) / 2;
  const centerY = targetY < sourceY ? targetY + yOffset : targetY - yOffset;

  return [centerX, centerY, xOffset, yOffset];
};
