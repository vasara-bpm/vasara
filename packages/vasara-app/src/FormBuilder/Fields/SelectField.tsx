import { makeStyles } from '@mui/styles';
import { IntrospectionField, IntrospectionObjectType, IntrospectionOutputTypeRef } from 'graphql';
import get from 'lodash/get';
import React, { useContext, useEffect, useState } from 'react';
import {
  ArrayInput,
  AutocompleteInput,
  DataProvider,
  FormDataConsumer,
  Labeled,
  SelectField as RASelectField,
  RadioButtonGroupInput,
  ReferenceField,
  ReferenceInput,
  SimpleFormIterator,
  TextField,
  TextInput,
  required,
  useDataProvider,
  useLocaleState,
} from 'react-admin';
import { useFormContext } from 'react-hook-form';

import { withSecret } from '../../Components/VaultTextInput';
import VaultTextReadOnlyInputPlain, { withReadOnlySecret } from '../../Components/VaultTextReadOnlyInputPlain';
import { getUserTaskForm } from '../../DataProviders/Camunda/helpers';
import UserTaskEditContext from '../../DataProviders/Camunda/UserTaskEditContext';
import HasuraContext from '../../DataProviders/HasuraContext';
import { unary } from '../../util/feel';
import {
  getFieldTypeName,
  getReferenceResourceName,
  getRelationFromFkField,
  isEnumField,
  isFkField,
  labelFromField,
  labelFromSchema,
} from '../../util/helpers';
import { CommonFieldProps, FieldComponentProps } from '../fields';
import FieldsetField from '../Fieldsets/FieldsetField';
import { Choice } from '../types';
import { BpmnConstraint, useBpmnConstraints } from '../utils';
import * as Builder from './builderInputs';

// very large number for autocomplete-type selects so the list shows everything
const UNLIMITED_PER_PAGE = 10000;

const SelectField: React.FC<CommonFieldProps> = props => {
  const [locale] = useLocaleState();
  const combinedChoices = props.fieldChoices.concat(props.readonlySourceChoices);
  const [validateRequired, setValidateRequired] = useState<boolean>(false);

  const form = useFormContext();
  // only one of these three can be active at one time,
  // since each represents a variant taking its options from a different source
  const vocabulary = form.watch(`${props.inputName}.vocabulary`);
  const varSource = form.watch(`${props.inputName}.variableSource`);
  const options = form.watch(`${props.inputName}.options`) || [];

  const sources: string[] = form.watch(`${props.inputName}.sources`) || [];
  const constraints: BpmnConstraint[] = sources.flatMap(s => {
    let sourceChoice = combinedChoices.find(c => c.id === s);
    return sourceChoice?.constraints ?? [];
  });
  const hasReadonlyConstraint = constraints.some(c => c.name === 'readonly');
  const readonly: boolean = hasReadonlyConstraint || form.watch(`${props.inputName}.readonly`);
  const hasRequiredConstraint = constraints.some(c => c.name === 'required');
  const required: boolean = hasRequiredConstraint || form.watch(`${props.inputName}.required`);

  useEffect(() => {
    // Fixes: Cannot update a component () while rendering a different component ()
    if (props.expanded === props.inputName) {
      setValidateRequired(true);
    } else {
      setValidateRequired(false);
    }
  }, [props.expanded, props.inputName]);

  return (
    <FieldsetField {...props}>
      <Builder.LabelInput name={props.inputName} isRequired={validateRequired} />
      <Builder.HelperTextInput name={props.inputName} />

      {vocabulary || varSource ? null : (
        <ArrayInput source={`${props.inputName}.options`} label="vasara.form.options">
          <SimpleFormIterator className="sparse">
            <TextInput source={`id`} label="vasara.form.value" helperText={false} />
            <TextInput source={`name.${locale}`} label="vasara.form.label" helperText={false} />
          </SimpleFormIterator>
        </ArrayInput>
      )}

      {options.length > 0 || varSource ? null : (
        <AutocompleteInput
          id={`${props.inputName}-vocabulary`}
          label="vasara.form.vocabulary"
          source={`${props.inputName}.vocabulary`}
          choices={props.vocabularyChoices}
          fullWidth={true}
          helperText={false}
        />
      )}

      {options.length > 0 || vocabulary ? null : (
        <AutocompleteInput
          id={`${props.inputName}-variableSource`}
          label="vasara.form.variableSource"
          source={`${props.inputName}.variableSource`}
          choices={props.readonlySourceChoices
            // only list process variables
            .filter((src: Choice) => src.id.startsWith('context'))
            // remove the "context." prefix
            .map(
              (src: Choice): Choice => {
                return { id: src.id.substring(8), name: src.name };
              }
            )}
          fullWidth={true}
          helperText="vasara.form.helperText.variableSource"
        />
      )}

      <Builder.SourcesInput
        name={props.inputName}
        isRequired={validateRequired}
        choices={readonly ? props.readonlySourceChoices : props.sourceChoices}
      />
      <Builder.BpmnConstraintList constraints={constraints} />

      <Builder.ReadonlySwitch name={props.inputName} constraints={constraints} checked={readonly} />

      {readonly ? null : (
        <>
          <Builder.PIISwitch name={props.inputName} />
          {vocabulary ? null : <Builder.ConfidentialSwitch name={props.inputName} />}
          <Builder.RequiredSwitch name={props.inputName} constraints={constraints} checked={required} />
        </>
      )}

      <Builder.DependencyInput name={props.inputName} isRequired={validateRequired} choices={combinedChoices} />

      <Builder.TypeInput name={props.inputName} isRequired={validateRequired} />
    </FieldsetField>
  );
};

const useStyles = makeStyles({
  MuiTextField: {
    defaultProps: {
      variant: 'outlined',
    },
  },
  floatLeft: {
    float: 'left',
  },
  clearLeft: {
    clear: 'left',
  },
  fullWidth: {
    display: 'flex',
  },
});

const SecretSelectField = withReadOnlySecret(RASelectField);
const SecretRadioButtonGroupInput = withSecret(RadioButtonGroupInput);
const SecretAutocompleteInput = withSecret(AutocompleteInput);

const SimpleSelectInput: React.FC<FieldComponentProps> = ({ style, schemaField, schemaOverride }) => {
  const classes = useStyles();
  const [locale] = useLocaleState();
  const context = useContext(UserTaskEditContext);
  const taskForm = getUserTaskForm(context.userTask);
  const form = useFormContext();
  const schema = { ...form.getValues(schemaField), ...(schemaOverride || {}) };
  const label = schema.label?.[locale] ?? '';

  const [fixedOptionsIds, setFixedOptionsIds] = useState<any[]>([]);
  const [fixedOptions, setFixedOptions] = useState<any[]>([]);

  let options = fixedOptions;

  useEffect(() => {
    if (fixedOptions.length === 0) {
      // Resolve options from field configuratiojn
      let candidates = (schema.options || []).map((option: any) => {
        return {
          id: option.id,
          name: option.name[locale],
        };
      });

      // Resolve options from user task form
      if (!candidates || candidates.length === 0) {
        for (let field of taskForm) {
          const id = `context.${field.id}`;
          if (field.values && schema.sources.indexOf(id) > -1) {
            candidates = field.values;
            break;
          }
        }
      }

      setFixedOptions(candidates);
      setFixedOptionsIds(candidates.map((option: any) => option.id));
    }
  }, [fixedOptions.length, locale, schema.options, schema.sources, taskForm]);

  const bpmnConstraints = useBpmnConstraints(schema);
  const validators = [];
  if (schema.required || bpmnConstraints.some(c => c.name === 'required')) {
    validators.push(required());
    if (fixedOptions.length > 0) {
      validators.push(oneOfRequired(fixedOptionsIds));
    }
  }
  const isReadonly = schema.readonly || bpmnConstraints.some(c => c.name === 'readonly');

  // Resolve options from schema
  const { schemata, fields: fieldsByName, enums } = useContext(HasuraContext);

  if (!options || options.length === 0) {
    for (const source of schema?.sources || []) {
      const parts = source.split('.');
      if (fieldsByName.has(parts[0])) {
        const fields = fieldsByName.get(parts[0]) as Map<string, IntrospectionField>;
        const field = fields.get(parts[1]);
        if (field && isEnumField(field)) {
          const type_ = enums.get(getFieldTypeName((field.type as unknown) as IntrospectionOutputTypeRef));
          if (type_) {
            options = type_.enumValues.map(value => {
              return {
                id: value.name,
                name: value.description,
              };
            });
          }
        } else if (field && isFkField(field)) {
          const relation = getRelationFromFkField(field);

          if (fields.has(relation)) {
            const relationField = fields.get(relation) as IntrospectionField;
            const referencedResourceName = getReferenceResourceName(relationField);

            if (referencedResourceName === 'camunda_User') {
              return isReadonly ? (
                <FormDataConsumer subscription={{ values: true }}>
                  {({ formData }: any) => (
                    <Labeled label={label} className={classes.fullWidth} style={style}>
                      <ReferenceField
                        record={formData}
                        label={labelFromField(field)}
                        source={schema.id}
                        reference={referencedResourceName}
                        link={false}
                      >
                        <TextField source="name" />
                      </ReferenceField>
                    </Labeled>
                  )}
                </FormDataConsumer>
              ) : (
                <ReferenceInput
                  label={labelFromField(field)}
                  source={schema.id}
                  reference={referencedResourceName}
                  fullWidth={true}
                  clearAlwaysVisible={true}
                  resettable={true}
                  allowEmpty={true}
                  perPage={UNLIMITED_PER_PAGE}
                >
                  <AutocompleteInput
                    filterToQuery={(q: string) => {
                      if (!q) {
                        return {};
                      }
                      return { q };
                    }}
                    optionText={'name'}
                    style={style}
                    disabled={!!schema.disabled}
                  />
                </ReferenceInput>
              );
            }

            const referencedResourceSchema = schemata.get(referencedResourceName) as IntrospectionObjectType;

            if (referencedResourceSchema) {
              return isReadonly ? (
                <FormDataConsumer subscription={{ values: true }}>
                  {({ formData }: any) => {
                    return (
                      <Labeled label={label} className={classes.fullWidth} style={style}>
                        <ReferenceField
                          record={formData}
                          label={labelFromField(field, labelFromSchema(referencedResourceSchema))}
                          source={schema.id}
                          reference={referencedResourceName}
                          link="show"
                        >
                          <TextField source="name" />
                        </ReferenceField>
                      </Labeled>
                    );
                  }}
                </FormDataConsumer>
              ) : (
                <ReferenceInput
                  label={labelFromField(field, labelFromSchema(referencedResourceSchema))}
                  source={schema.id}
                  reference={referencedResourceName}
                  allowEmpty={true}
                  fullWidth={true}
                  perPage={UNLIMITED_PER_PAGE}
                  sort={{ field: 'name', order: 'ASC' }}
                >
                  <AutocompleteInput
                    filterToQuery={(q: string) => {
                      if (!q) {
                        return {};
                      }
                      return {
                        name: {
                          format: 'hasura-raw-query',
                          value: {
                            _ilike: `%${q}%`,
                          },
                        },
                      };
                    }}
                    optionText={'name'}
                    style={style}
                    disabled={!!schema.disabled}
                  />
                </ReferenceInput>
              );
            }
          }
        }
        if (options.length > 0) {
          break;
        }
      }
      if (options.length > 0) {
        break;
      }
    }
  }

  if (isReadonly) {
    if (options.length > 0) {
      return (
        <Labeled label={label} className={classes.fullWidth} style={style}>
          {schema.confidential ? (
            <SecretSelectField label={label} source={schema.id} choices={options} fullWidth={true} />
          ) : (
            <RASelectField label={label} source={schema.id} choices={options} fullWidth={true} />
          )}
        </Labeled>
      );
    }

    return (
      <Labeled label={label} className={classes.fullWidth} style={style}>
        {schema.confidential ? (
          <VaultTextReadOnlyInputPlain label={label} source={schema.id} fullWidth={true} />
        ) : (
          <TextField label={label} source={schema.id} fullWidth={true} />
        )}
      </Labeled>
    );
  }

  if (options.length < 6) {
    if (schema.confidential) {
      return (
        <SecretRadioButtonGroupInput
          label={label}
          helperText={(schema.helperText?.[locale] ?? '') || ''}
          source={schema.id}
          choices={options}
          row={false}
          validate={validators}
          style={style}
          disabled={!!schema.disabled}
        />
      );
    }
    return (
      <RadioButtonGroupInput
        label={label}
        helperText={(schema.helperText?.[locale] ?? '') || ''}
        source={schema.id}
        choices={options}
        row={false}
        validate={validators}
        style={style}
        disabled={!!schema.disabled}
      />
    );
  }

  if (schema.confidential) {
    return (
      <SecretAutocompleteInput
        label={label}
        helperText={(schema.helperText?.[locale] ?? '') || ''}
        source={schema.id}
        choices={options}
        validate={validators}
        fullWidth={true}
        clearAlwaysVisible={true}
        resettable={true}
        allowEmpty={true}
        style={style}
        disabled={!!schema.disabled}
      />
    );
  }

  return (
    <AutocompleteInput
      label={label}
      helperText={(schema.helperText?.[locale] ?? '') || ''}
      source={schema.id}
      choices={options}
      validate={validators}
      fullWidth={true}
      style={style}
      disabled={!!schema.disabled}
    />
  );
};

const vocabularyOneOfRequired = (dataProvider: DataProvider, vocabulary: string) => {
  let isFetching = false; // Flag to track if a fetch is already in progress

  return async (value: any) => {
    if (value !== null && value !== '' && typeof value !== 'undefined') {
      // Check if a fetch is already in progress
      if (isFetching) {
        return;
      }

      isFetching = true; // Set the flag to indicate that a fetch is in progress

      try {
        const data = await dataProvider.getOne(vocabulary, {
          id: value,
        });
        if (data === null) {
          return 'ra.validation.required';
        }
      } catch (e) {
        return 'ra.validation.required';
      } finally {
        isFetching = false; // Reset the flag after the fetch is completed
      }
    }
    return undefined;
  };
};

const VocabularySelectInput: React.FC<FieldComponentProps> = ({ style, schemaField, schemaOverride }) => {
  const classes = useStyles();
  const [locale] = useLocaleState();
  const form = useFormContext();
  const schema = { ...form.getValues(schemaField), ...(schemaOverride || {}) };
  const options: any[] = [];
  const fullWidth = schema?.fullWidth ?? true;
  const label = schema.label?.[locale] ?? '';
  const dataProvider = useDataProvider();

  const bpmnConstraints = useBpmnConstraints(schema);
  const validators = [];
  if (schema.required || bpmnConstraints.some(c => c.name === 'required')) {
    validators.push(required());
    validators.push(vocabularyOneOfRequired(dataProvider, schema.vocabulary));
  }
  const isReadonly = schema.readonly || bpmnConstraints.some(c => c.name === 'readonly');

  if (isReadonly) {
    const fieldValue = form.getValues(schema.id);
    if (fieldValue === undefined || fieldValue === null || fieldValue === '') {
      return null;
    }
    if (options.length > 0) {
      return (
        <Labeled label={label} className={fullWidth ? classes.fullWidth : undefined} style={style}>
          <RASelectField label={label} source={schema.id} choices={options} fullWidth={fullWidth} />
        </Labeled>
      );
    }
    return (
      <Labeled label={label} className={fullWidth ? classes.fullWidth : undefined} style={style}>
        <ReferenceField label={label} source={schema.id} reference={schema.vocabulary} link={false}>
          <TextField source="name" />
        </ReferenceField>
      </Labeled>
    );
  }

  return (
    <ReferenceInput
      label={label}
      source={`${schema.id}`}
      perPage={UNLIMITED_PER_PAGE}
      sort={{ field: 'name', order: 'ASC' }}
      reference={schema.vocabulary}
      fullWidth={fullWidth}
      clearAlwaysVisible={true}
      resettable={true}
      allowEmpty={true}
    >
      <AutocompleteInput
        validate={validators}
        filterToQuery={(q: string) => {
          if (!q) {
            return {};
          }
          if (schema.vocabulary.startsWith('camunda_')) {
            return { q };
          }
          return {
            name: {
              format: 'hasura-raw-query',
              value: {
                _ilike: `%${q}%`,
              },
            },
          };
        }}
        label={label}
        optionText={'name'}
        helperText={(schema.helperText?.[locale] ?? '') || ''}
        style={style}
        disabled={!!schema.disabled}
      />
    </ReferenceInput>
  );
};

const oneOfRequired = (values: any[]) => async (value: any) => {
  return values.includes(value) || values.includes(`${value}`) ? undefined : 'ra.validation.required';
};

/** A select input that takes its values from a process variable.
 * The variable's content must be a JSON list of objects with { id, name }.
 */
const VariableSourceSelectInput: React.FC<FieldComponentProps> = ({ style, schemaField, schemaOverride }) => {
  const classes = useStyles();
  const [locale] = useLocaleState();
  const form = useFormContext();
  const formData = form.getValues();
  const schema = { ...form.getValues(schemaField), ...(schemaOverride || {}) };
  const label = schema.label?.[locale] ?? '';
  const [options, setOptions] = useState<any[]>([]);
  const [optionsIds, setOptionsIds] = useState<any[]>([]);
  const [optionsError, setOptionsError] = useState<string>('');

  const bpmnConstraints = useBpmnConstraints(schema);
  const validators = [];
  if (schema.required || bpmnConstraints.some(c => c.name === 'required')) {
    validators.push(required());
    validators.push(oneOfRequired(optionsIds));
  }
  const isReadonly = schema.readonly || bpmnConstraints.some(c => c.name === 'readonly');

  useEffect(() => {
    if (options.length === 0) {
      const optionSource = formData.variables.find(
        (procVar: { key: string; valueType: string; value: any }) => procVar.key === schema.variableSource
      );
      if (!optionSource?.value || !optionSource?.valueType) {
        setOptionsError(`Error: process variable "${schema.variableSource}" not found`);
        return;
      }
      if (optionSource.valueType !== 'JSON') {
        setOptionsError(`Error: process variable "${schema.variableSource}" type incorrect`);
        return;
      }
      const value = JSON.parse(optionSource.value);
      if (!!value && Array.isArray(value)) {
        setOptionsIds(
          value
            .filter((option: any) => typeof option?.id !== 'undefined')
            .map((option: any) => option.id)
            .reduce((acc: any, cur: any) => acc.concat([cur, `${cur}`]), [])
        );
        setOptions(value);
      } else {
        setOptionsError(`Error: process variable "${schema.variableSource}" format incorrect`);
        return;
      }
    }
  }, [formData.variables, options.length, schema.variableSource]);

  if (!!optionsError) {
    return <>{optionsError}</>;
  }

  // input elements copied from SimpleSelectInput.
  // not all of them tested, but should work the same
  if (isReadonly) {
    if (options.length > 0) {
      return (
        <Labeled label={label} className={classes.fullWidth} style={style}>
          {schema.confidential ? (
            <SecretSelectField record={formData} label={label} source={schema.id} choices={options} fullWidth={true} />
          ) : (
            <RASelectField record={formData} label={label} source={schema.id} choices={options} fullWidth={true} />
          )}
        </Labeled>
      );
    }
    return (
      <Labeled label={label} className={classes.fullWidth} style={style}>
        {schema.confidential ? (
          <VaultTextReadOnlyInputPlain record={formData} label={label} source={schema.id} fullWidth={true} />
        ) : (
          <TextField record={formData} label={label} source={schema.id} fullWidth={true} />
        )}
      </Labeled>
    );
  }
  if (options.length < 6 && schemaOverride?.inputType !== 'select') {
    if (schema.confidential) {
      return (
        <SecretRadioButtonGroupInput
          label={label}
          helperText={(schema.helperText?.[locale] ?? '') || ''}
          source={schema.id}
          choices={options}
          row={false}
          validate={validators}
          style={style}
          disabled={!!schema.disabled}
        />
      );
    } else {
      return (
        <RadioButtonGroupInput
          label={label}
          helperText={(schema.helperText?.[locale] ?? '') || ''}
          source={schema.id}
          choices={options}
          row={false}
          validate={validators}
          style={style}
          disabled={!!schema.disabled}
        />
      );
    }
  }
  if (schema.confidential) {
    return (
      <SecretAutocompleteInput
        label={label}
        helperText={(schema.helperText?.[locale] ?? '') || ''}
        source={schema.id}
        choices={options}
        validate={validators}
        fullWidth={true}
        clearAlwaysVisible={true}
        resettable={true}
        allowEmpty={true}
        style={style}
        disabled={!!schema.disabled}
      />
    );
  } else {
    return (
      <AutocompleteInput
        label={label}
        helperText={(schema.helperText?.[locale] ?? '') || ''}
        source={schema.id}
        choices={options}
        validate={validators}
        fullWidth={true}
        style={style}
        disabled={!!schema.disabled}
      />
    );
  }
};

const SelectInputImpl: React.FC<FieldComponentProps> = props => {
  const form = useFormContext();
  const schema = { ...form.getValues(props.schemaField), ...(props.schemaOverride || {}) };
  const dependencyName = (schema.dependency || '').match('\\.')
    ? `${schema.id}:${schema.dependency}`
    : schema.dependency;
  const dependencyValue = dependencyName ? form.watch(dependencyName) : undefined;
  const condition = schema.condition;
  const variables = schema.variables || [];

  if (dependencyName) {
    return (
      <FormDataConsumer subscription={{ values: true }}>
        {({ formData }) => {
          const context: Record<string, any> = Object.fromEntries(
            variables.map((variable: any) => {
              return get(formData, variable.source) !== undefined
                ? [variable.id, get(formData, variable.source)]
                : [variable.id, get(formData, `${schema.id}:${variable.source}`)];
            })
          );

          if (
            dependencyValue === undefined ||
            (!condition && dependencyValue) ||
            (condition && unary(condition, dependencyValue, context))
          ) {
            if (schema?.vocabulary) {
              return <VocabularySelectInput {...props} />;
            }
            if (schema?.variableSource) {
              return <VariableSourceSelectInput {...props} />;
            }
            return <SimpleSelectInput {...props} />;
          }

          return null;
        }}
      </FormDataConsumer>
    );
  }
  if (schema?.vocabulary) {
    return <VocabularySelectInput {...props} />;
  }
  if (schema?.variableSource) {
    return <VariableSourceSelectInput {...props} />;
  }
  return <SimpleSelectInput {...props} />;
};

export const SelectInput = React.memo(SelectInputImpl);
export default React.memo(SelectField);
