import 'leaflet.locatecontrol';
import 'leaflet.locatecontrol/dist/L.Control.Locate.min.css';
import 'leaflet.fullscreen';
import 'leaflet.fullscreen/Control.FullScreen.css';

import { createControlComponent } from '@react-leaflet/core';
import { Point as GeoJsonPoint } from 'geojson';
import * as L from 'leaflet';
import markerIconPng from 'leaflet/dist/images/marker-icon.png';
import get from 'lodash/get';
import React from 'react';
import { Labeled, ValidationError, required, useInput, useLocaleState, useTranslate } from 'react-admin';
import { useFormContext } from 'react-hook-form';
import { GeoJSON as GeoJsonLayer, MapContainer, TileLayer, useMapEvents } from 'react-leaflet';

import { unary } from '../../util/feel';
import { FieldComponentProps } from '../fields';
import { useBpmnConstraints } from '../utils';

/** Map coordinate input
 * in a separate file from the editor field component to enable lazy loading.
 * Do not import directly; use the lazy-loading wrapper
 * exported from `MapCoordinateField.tsx`. */
export const MapCoordinateInput: React.FC<FieldComponentProps> = ({ schemaField, schemaOverride }) => {
  const [locale] = useLocaleState();
  const translate = useTranslate();
  const form = useFormContext();
  const formData = form.watch();
  const schema = { ...form.getValues(schemaField), ...(schemaOverride || {}) };
  const isFullWidth = schema?.fullWidth ?? true;
  const label = schema.label?.[locale] ?? '';

  const dependencyName = (schema.dependency || '').match('\\.')
    ? `${schema.id}:${schema.dependency}`
    : schema.dependency;
  const dependencyValue = dependencyName ? get(formData, dependencyName) : undefined;
  const condition = schema.condition;
  const variables = schema.variables || [];

  const bpmnConstraints = useBpmnConstraints(schema);
  const validators = [];
  if (schema.required || bpmnConstraints.some(c => c.name === 'required')) {
    validators.push(required());
  }
  const isReadonly = schema.readonly || bpmnConstraints.some(c => c.name === 'readonly');

  const { field, fieldState } = useInput({
    source: schema.id,
    validate: validators,
  });

  // coordinates are stored as GeoJSON points,
  // which need to be extracted from events
  const latLngToGeoJson = (latlng: L.LatLngLiteral): GeoJsonPoint => {
    return {
      type: 'Point',
      coordinates: L.GeoJSON.latLngToCoords(L.latLng(latlng)),
    };
  };

  const setFieldValue = (latlng: L.LatLngLiteral) => {
    field.onChange(latLngToGeoJson(latlng));
  };

  if (dependencyName) {
    const context: Record<string, any> = Object.fromEntries(
      variables.map((variable: any) => {
        return get(formData, variable.source) !== undefined
          ? [variable.id, get(formData, variable.source)]
          : [variable.id, get(formData, `${schema.id}:${variable.source}`)];
      })
    );

    const dependencyActive =
      dependencyValue === undefined ||
      (!condition && dependencyValue) ||
      (condition && unary(condition, dependencyValue, context));
    if (!dependencyActive) {
      return null;
    }
  }

  // default icon has wrong file path when using vendored Leaflet CSS;
  // override it with the correct import
  const markerIcon = new L.Icon({
    iconUrl: markerIconPng,
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [0, -38],
  });

  // GeoJSON layer configuration to override the default icon
  const pointToLayer = (_: any, latlng: L.LatLngExpression) => {
    return L.marker(latlng, { icon: markerIcon, alt: translate('vasara.form.selectedLocation') });
  };

  // centered on the JYU university library by default
  const DEFAULT_LOCATION = { lat: 62.2378, lng: 25.7362 };
  // if the field has a previously saved or default value in Vasara, center on that instead
  const initialLocation = field.value?.coordinates
    ? L.GeoJSON.coordsToLatLng(field.value.coordinates)
    : DEFAULT_LOCATION;

  // size of the map in pixels, applied to height always
  // and width if fullWidth is not set
  const MAP_SIZE = 600;
  const DEFAULT_ZOOM = 12;

  if (isReadonly) {
    if (field.value === undefined || field.value === null || field.value === '') {
      return null;
    }

    return (
      <Labeled fullWidth={isFullWidth} width={MAP_SIZE} label={label}>
        <MapContainer style={{ height: MAP_SIZE, width: '100%' }} center={initialLocation} zoom={DEFAULT_ZOOM}>
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          <FullscreenControl />
          <GeoJsonLayer data={field.value} pointToLayer={pointToLayer} />
        </MapContainer>
      </Labeled>
    );
  }

  // interaction handling in a subcomponent for access to map state hooks
  const DataLayer: React.FC<{}> = () => {
    const map = useMapEvents({
      click: evt => {
        setFieldValue(evt.latlng);
      },
      keydown: evt => {
        // keyboard controls:
        // press enter to select the center of the current view
        if (evt.originalEvent.key === 'Enter') {
          setFieldValue(map.getCenter());
        }
      },
      locationfound: evt => {
        map.setView(evt.latlng, map.getZoom());
        setFieldValue(evt.latlng);
        // don't continue to receive location events,
        // which would keep overwriting the field value.
        // this way we only set the value once
        // whenever the user activates geolocation
        map.stopLocate();
      },
    });

    if (!field.value) {
      return null;
    }

    return <GeoJsonLayer data={field.value} pointToLayer={pointToLayer} />;
  };

  return (
    <Labeled
      // custom styling for errors is needed because we're not using MUI inputs
      sx={fieldState.error ? { color: '#cc0000' } : {}}
      isRequired={schema.required}
      fullWidth={isFullWidth}
      width={MAP_SIZE}
      label={label}
    >
      <>
        <div
          style={{ borderWidth: 2, borderStyle: 'solid', borderColor: fieldState.error ? '#cc0000' : 'transparent' }}
        >
          <MapContainer style={{ height: MAP_SIZE, width: '100%' }} center={initialLocation} zoom={DEFAULT_ZOOM}>
            <TileLayer
              attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <LocateControl strings={{ title: translate('vasara.action.locate') }} />
            <FullscreenControl title={translate('vasara.action.fullscreen')} />
            <DataLayer />
          </MapContainer>
        </div>
        {fieldState.error?.message ? <ValidationError error={fieldState.error.message} /> : null}
      </>
    </Labeled>
  );
};

/** Button that centers the map on your current geolocation
 *  using the leaflet.locatecontrol plugin. */
const LocateControl = createControlComponent<L.Control.Locate, L.Control.LocateOptions & L.ControlOptions>(props => {
  return new L.Control.Locate(props);
});

const FullscreenControl = createControlComponent<L.Control.Fullscreen, L.Control.FullscreenOptions & L.ControlOptions>(
  props => {
    // @ts-ignore fullscreen control's TS typings don't match
    // (FullScreen is the actual name, typings say Fullscreen)
    return new L.Control.FullScreen(props);
  }
);

export default React.memo(MapCoordinateInput);
