import { BpmnConstraint } from './utils';

export type IndexableObject = {
  [index: string]: any;
};

export interface Choice {
  id: string;
  name: string;
  constraints?: BpmnConstraint[];
}
export interface FieldDependencyVariable {
  id: string;
  source: string;
}
