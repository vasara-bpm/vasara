ARTIFACT_NAME = keycloak-volatile
ARTIFACT_VERSION ?= 14.0.0
ARTIFACT_FULLNAME ?= $(ARTIFACT_NAME)-$(ARTIFACT_VERSION).tar.gz

REPOSITORY ?= nomad-local

.PHONY: artifact
artifact: ${ARTIFACT_FULLNAME}

${ARTIFACT_FULLNAME}: $(SOURCES)
	nix build $(NIX_OPTIONS) --json .#artifact|jq -r .[0].outputs.out|\
	xargs -Iresult cp -aL result $(ARTIFACT_FULLNAME)
	chmod u+w $(ARTIFACT_FULLNAME)

.PHONY: publish
publish: $(ARTIFACT_FULLNAME)
	jf rt u $(ARTIFACT_FULLNAME) $(REPOSITORY)/$(ARTIFACT_NAME)/
	jq '."package_$(subst -,_,$(ARTIFACT_NAME)_version)" = "$(ARTIFACT_VERSION)"' ../../versions.json  > versions.json
	mv versions.json ../../versions.json
