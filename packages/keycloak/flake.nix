{
  description = "Keycloak";

  nixConfig = {
    extra-trusted-public-keys =
      "vasara-bpm.cachix.org-1:T18iQZQvYDy/6VdGmttnkkq7rYi3JP0S1RjjdnXNu/4=";
    extra-substituters = "https://vasara-bpm.cachix.org";
  };

  inputs = {

    # Generic
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/release-22.11";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/master";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    # Vasara
    vasara-bpm = {
      url = "gitlab:vasara-bpm/pkgs";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
  };

  outputs = { self, nixpkgs, nixpkgs-unstable, flake-utils, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ ];
        };
        pkgs-unstable = import nixpkgs-unstable { inherit system; };
        call-name = "keycloak";
      in {

        # Default executable
        apps.default = {
          type = "app";
          program = "${self.packages.${system}.default}/bin/kc.sh";
        };

        # Default package
        packages.default = pkgs.keycloak;

        # Container image
        packages.image = pkgs.dockerTools.streamLayeredImage {
          name = call-name;
          tag = "latest";
          created = "now";
          contents = [
            (pkgs.buildEnv {
              name = "image-contents";
              paths = [
                pkgs.busybox
                pkgs.dockerTools.fakeNss
                pkgs.dockerTools.usrBinEnv
                pkgs.tini
                self.packages.${system}.default
              ];
              pathsToLink = [ "/etc" "/sbin" "/bin" ];
            })
          ];
          extraCommands = ''
            mkdir -p usr/bin && ln -s /sbin/env usr/bin/env
            mkdir -p tmp && chmod a+rxwt tmp
          '';
          config = {
            Entrypoint = [
              "${pkgs.tini}/bin/tini"
              "--"
              "${self.packages.${system}.default}/bin/kc.sh"
            ];
            Env = [
              "TMPDIR=/tmp"
              "HOME=/tmp"
              "LOG4J_FORMAT_MSG_NO_LOOKUPS=true"
              "KC_HOME_DIR=${pkgs.keycloak}"
              "QUARKUS_PACKAGE_OUTPUT_DIRECTORY=/tmp"
            ];
            Labels = { };
            User = "nobody";
          };
        };

        # Nomad artifact
        packages.artifact = let
          jar = self.packages.${system}.jar;
          env = pkgs.buildEnv {
            name = "env";
            paths = [
              pkgs.bashInteractive
              pkgs.coreutils
              pkgs.netcat
              pkgs.tini
              pkgs.keycloak
            ];
          };
          closure = (pkgs.writeReferencesToFile env);
        in pkgs.runCommand call-name { buildInputs = [ pkgs.makeWrapper ]; } ''
          # aliases
          mkdir -p usr/local/bin
          for filename in ${env}/bin/??*; do
            cat > usr/local/bin/$(basename $filename) << EOF
          #!/usr/local/bin/sh
          set -e
          exec $(basename $filename) "\$@"
          EOF
          done
          rm -f usr/local/bin/sh
          chmod a+x usr/local/bin/*

          # shell
          makeWrapper ${pkgs.bashInteractive}/bin/sh usr/local/bin/sh \
            --set SHELL /usr/local/bin/sh \
            --prefix PATH : ${pkgs.coreutils}/bin \
            --prefix PATH : ${pkgs.netcat}/bin \
            --prefix PATH : ${pkgs.tini}/bin \
            --prefix PATH : ${pkgs.keycloak}/bin \
            --set KEYCLOAK_APP_DIR ${pkgs.keycloak} \
            --set QUARKUS_PACKAGE_OUTPUT_DIRECTORY /tmp \
            --set LOG4J_FORMAT_MSG_NO_LOOKUPS true

          # artifact
          tar cvzhP \
            --hard-dereference \
            --exclude="${env}*" \
            --exclude="*ncurses*/ncurses*/ncurses*" \
            --exclude="*pixman-1*/pixman-1*/pixman-1*" \
            --files-from=${closure} \
            usr > $out || true
        '';

        # Development shell
        devShells.default = pkgs.mkShell {
          buildInputs = [
            pkgs-unstable.cachix
            pkgs-unstable.jfrog-cli
            pkgs.gnumake
            pkgs.jq
          ];
        };

        devShells.with-podman =
          inputs.vasara-bpm.devShells.${system}.podman.overrideAttrs (old: {
            buildInputs = old.buildInputs
              ++ self.devShells.${system}.default.buildInputs;
          });

        formatter = pkgs.nixfmt;
      });
}
