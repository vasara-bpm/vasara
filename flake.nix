{
  description = "Vasara Playground";

  # Cachix
  nixConfig = {
    extra-trusted-public-keys =
      "vasara-bpm.cachix.org-1:T18iQZQvYDy/6VdGmttnkkq7rYi3JP0S1RjjdnXNu/4=";
    extra-substituters = "https://vasara-bpm.cachix.org";
  };

  inputs = {

    # Core
    nixpkgs.url = "github:NixOS/nixpkgs/release-23.11";
    nixpkgs-python.url = "github:NixOS/nixpkgs/release-23.11";
    nixpkgs-nodejs.url = "github:NixOS/nixpkgs/release-22.11";
    nixpkgs-hasura.url =
      "github:NixOS/nixpkgs/5cf90a60e5ddf68fbfc04d643970136bf00ba315";
    flake-utils.url = "github:numtide/flake-utils";
    home-manager.url = "github:rycee/home-manager/release-23.11";

    # Extras
    vasara-bpm = {
      url = "gitlab:vasara-bpm/pkgs";
      inputs.nixpkgs.follows = "nixpkgs"; # to optimize disk usage
    };

    # Vasara
    vasara-carrot-rcc = {
      url = "gitlab:vasara-bpm/carrot-rcc";
      inputs.vasara-bpm.follows = "vasara-bpm"; # to optimize disk usage
    };
    vasara-pdf-tools = {
      url = "gitlab:vasara-bpm/pdf-tools-api";
      flake = false; # TODO: flake
    };
    vasara-camunda = {
      url = "./packages/camunda";
      inputs.nixpkgs.follows = "nixpkgs"; # to optimize disk usage
    };
    vasara-hasura-auth = {
      url = "./packages/hasura-auth";
      inputs.nixpkgs.follows = "nixpkgs-python"; # to optimize disk usage
    };
    vasara-middleware = {
      url = "./packages/middleware";
      inputs.nixpkgs.follows = "nixpkgs-python"; # to optimize disk usage
    };
    vasara-app = {
      url = "./packages/vasara-app";
      inputs.nixpkgs.follows = "nixpkgs"; # to optimize disk usage
    };

    # Misc

    vasara-mailhog = {
      url = "./packages/mailhog";
      inputs.nixpkgs.follows = "nixpkgs"; # to optimize disk usage
    };
    vasara-zipkin = {
      url = "./packages/zipkin";
      inputs.nixpkgs.follows = "nixpkgs"; # to optimize disk usage
    };

  };

  outputs =
    { self, nixpkgs, flake-utils, home-manager, nixpkgs-hasura, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          overlays = [
            (self: super: {
              gitignoreSource = self.lib.cleanSource;
              nodejs-14_x =
                nodejs.nodejs-14_x; # app is waiting for ReactJS 18 refactoring
            })
            inputs.vasara-bpm.overlays.${system}.default
          ];
          inherit system;
        };
        hasura = import nixpkgs-hasura { inherit system; };
        nodejs = import inputs.nixpkgs-nodejs { inherit system; };
      in {
        packages.vasara-camunda =
          inputs.vasara-camunda.packages.${system}.default;
        packages.vasara-hasura-auth =
          inputs.vasara-hasura-auth.packages.${system}.default;
        packages.vasara-middleware =
          inputs.vasara-middleware.packages.${system}.default;
        packages.vasara-tasklist = inputs.vasara-app.packages.${system}.default;
        packages.vasara-modeler = inputs.vasara-app.packages.${system}.default;

        packages.vasara-mailhog = inputs.vasara-mailhog.packages.${system}.default;
        packages.vasara-zipkin = inputs.vasara-zipkin.packages.${system}.default;

        packages.vasara-camunda-image =
          inputs.vasara-camunda.packages.${system}.image;
        packages.vasara-hasura-auth-image =
          inputs.vasara-hasura-auth.packages.${system}.image;
        packages.vasara-middleware-image =
          inputs.vasara-middleware.packages.${system}.image;
        packages.vasara-tasklist-image =
          inputs.vasara-app.packages.${system}.tasklist-image;
        packages.vasara-modeler-image =
          inputs.vasara-app.packages.${system}.modeler-image;

        packages.vasara-mailhog-image =
          inputs.vasara-mailhog.packages.${system}.image;
        packages.vasara-zipkin-image =
          inputs.vasara-zipkin.packages.${system}.image;

        packages.vasara-carrot-rcc =
          inputs.vasara-carrot-rcc.packages.${system}.default;

        overlays.default = final: super: {
          gitignoreSource = final.lib.cleanSource;
          vasara-camunda = self.packages.${system}.vasara-camunda;
          vasara-carrot-rcc = self.packages.${system}.vasara-carrot-rcc;
          vasara-middleware = self.packages.${system}.vasara-middleware;
          vasara-hasura-auth = self.packages.${system}.vasara-hasura-auth;
          ### 
          vasara-pdf-tools-jar =
            pkgs.callPackage inputs.vasara-pdf-tools { inherit pkgs; };
          vasara-pdf-tools-icc-profile = inputs.vasara-pdf-tools
            + "/api/src/main/resources/AdobeRGB1998.icc";
          vasara-pdf-tools-converter = inputs.vasara-pdf-tools
            + "/api/src/main/resources/converter";
          vasara-pdf-tools-client =
            pkgs.callPackage (inputs.vasara-pdf-tools + "/camunda/client") {
              inherit pkgs;
            };
          ### 
          pkgconfig = final.pkg-config;
          nodejs-14_x =
            nodejs.nodejs-14_x; # app is waiting for ReactJS 18 refactoring
        };

        packages.vagrantVirtualbox =
          (import (nixpkgs + "/nixos/lib/eval-config.nix") {
            modules = [
              (nixpkgs + "/nixos/modules/installer/cd-dvd/channel.nix")
              (nixpkgs
                + "/nixos/modules/virtualisation/vagrant-virtualbox-image.nix")
              (nixpkgs + "/nixos/modules/virtualisation/vagrant-guest.nix")
              ({ config, ... }: {
                nixpkgs.overlays = [
                  inputs.vasara-bpm.overlays.${system}.default
                  self.overlays.${system}.default
                ];
                virtualbox = {
                  vmDerivationName =
                    "nixos-ova-${config.system.nixos.label}-${pkgs.stdenv.hostPlatform.system}";
                  vmFileName =
                    "vasara-nixos-${config.system.nixos.label}-${pkgs.stdenv.hostPlatform.system}.ova";
                  vmName =
                    "Vasara (NixOS ${config.system.nixos.label} ${pkgs.stdenv.hostPlatform.system})";
                  memorySize = 8 * 1024;
                  params = { usbehci = "off"; };
                  baseImageSize = 50 * 1024;
                };
              })
              home-manager.nixosModules.home-manager
              ./vagrant/configuration-common.nix
              ./vagrant/configuration-virtualbox.nix
              ./vagrant/configuration-vasara.nix
            ];
            inherit system;
          }).config.system.build.vagrantVirtualbox;

        packages.vagrantLibvirt =
          (import (nixpkgs + "/nixos/lib/eval-config.nix") {
            modules = [
              (nixpkgs + "/nixos/modules/installer/cd-dvd/channel.nix")
              (nixpkgs + "/nixos/modules/profiles/qemu-guest.nix")
              (nixpkgs + "/nixos/modules/virtualisation/vagrant-guest.nix")
              ({ config, ... }: {
                nixpkgs.overlays = [
                  inputs.vasara-bpm.overlays.${system}.default
                  self.overlays.${system}.default
                  (self: super: {
                    makeDiskImage =
                      import (nixpkgs + "/nixos/lib/make-disk-image.nix");
                  })
                ];
                libvirt = {
                  vmDerivationName =
                    "nixos-qcow2-${config.system.nixos.label}-${pkgs.stdenv.hostPlatform.system}";
                  vmFileName =
                    "vasara-nixos-${config.system.nixos.label}-${pkgs.stdenv.hostPlatform.system}.qcow2";
                  vmName =
                    "Vasara (NixOS ${config.system.nixos.label} ${pkgs.stdenv.hostPlatform.system})";
                  baseImageSize = 50 * 1024;
                };
              })
              home-manager.nixosModules.home-manager
              ./vagrant/configuration-common.nix
              ./vagrant/configuration-libvirt.nix
              ./vagrant/configuration-vasara.nix
            ];
            inherit system;
          }).config.system.build.vagrantLibvirt;

        packages.nixosConfigurations = {

          libvirt = nixpkgs.lib.nixosSystem {
            modules = [
              (nixpkgs + "/nixos/modules/installer/cd-dvd/channel.nix")
              (nixpkgs + "/nixos/modules/profiles/qemu-guest.nix")
              (nixpkgs + "/nixos/modules/virtualisation/vagrant-guest.nix")
              ({ ... }: {
                nixpkgs.overlays = [
                  inputs.vasara-bpm.overlays.${system}.default
                  self.overlays.${system}.default
                  (self: super: {
                    makeDiskImage =
                      import (nixpkgs + "/nixos/lib/make-disk-image.nix");
                  })
                ];
              })
              home-manager.nixosModules.home-manager
              ./vagrant/configuration-common.nix
              ./vagrant/configuration-libvirt.nix
              ./vagrant/configuration-vasara.nix
            ];
            system = "x86_64-linux";
          };

          virtualbox = nixpkgs.lib.nixosSystem {
            modules = [
              (nixpkgs + "/nixos/modules/installer/cd-dvd/channel.nix")
              (nixpkgs
                + "/nixos/modules/virtualisation/vagrant-virtualbox-image.nix")
              (nixpkgs + "/nixos/modules/virtualisation/vagrant-guest.nix")
              ({ ... }: {
                nixpkgs.overlays = [
                  inputs.vasara-bpm.overlays.${system}.default
                  self.overlays.${system}.default
                ];
              })
              home-manager.nixosModules.home-manager
              ./vagrant/configuration-common.nix
              ./vagrant/configuration-virtualbox.nix
              ./vagrant/configuration-vasara.nix
            ];
            system = "x86_64-linux";
          };
        };

        formatter = pkgs.nixfmt;
      });
}
