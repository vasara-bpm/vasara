# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "jyukopla/vasara-bpm"
  config.vm.graceful_halt_timeout = 120

  config.vm.network "forwarded_port", guest: 8080, host: 8080
  config.vm.network "forwarded_port", guest: 5005, host: 5005

  # config.vm.network "forwarded_port", guest: 8000, host: 8000  # keycloak
  # config.vm.network "forwarded_port", guest: 8090, host: 8090  # hasura

  config.vm.provider :qemu do |provider, override|
    provider.arch = "x86_64"
    provider.machine = "q35,accel=kvm"
    provider.memory = "16G"
    provider.net_device = "virtio-net-pci"
    provider.qemu_dir = "/nix/store/wa1c6k3h6ca05a5l98ncwc0mh57mx26p-qemu-8.0.0/bin/"

    override.vm.provision "rebuild", type: "shell",
      inline: "sudo NIXOS_SWITCH_USE_DIRTY_ENV=1 nixos-rebuild --flake /vagrant#libvirt --no-write-lock-file --max-jobs 1 switch"
    override.vm.synced_folder "./", "/vagrant", type: "9p"
  end

  config.vm.provider :libvirt do |libvirt, override|
    libvirt.video_type = "qxl"
    libvirt.graphics_type = "spice"
    libvirt.graphics_port = 5634
    libvirt.memory = 16384
    libvirt.cpus = 4
    libvirt.keymap = "fi"

    override.vm.provision "rebuild", type: "shell",
      inline: "sudo NIXOS_SWITCH_USE_DIRTY_ENV=1 nixos-rebuild --flake /vagrant#libvirt --no-write-lock-file --max-jobs 1 switch"
    override.vm.synced_folder "./", "/vagrant", type: "9p"
  end

  config.vm.provider :virtualbox do |virtualbox, override|
    virtualbox.gui = true
    virtualbox.memory = 16384
    virtualbox.cpus = 4

    override.vm.provision "rebuild", type: "shell",
      inline: "sudo NIXOS_SWITCH_USE_DIRTY_ENV=1 nixos-rebuild --flake /vagrant#virtualbox --no-write-lock-file --max-jobs 1 switch"
    override.vm.provision "remount", type: "shell",
      inline: "sudo mount -t vboxsf vagrant /vagrant -o umask=0022,gid=1000,uid=1000"
  end
end
