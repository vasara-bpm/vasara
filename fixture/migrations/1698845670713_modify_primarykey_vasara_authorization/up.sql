BEGIN TRANSACTION;
ALTER TABLE "vasara"."authorization" DROP CONSTRAINT "authorization_pkey";

ALTER TABLE "vasara"."authorization"
    ADD CONSTRAINT "authorization_pkey" PRIMARY KEY ("principal", "resource", "specifier");
COMMIT TRANSACTION;
