alter table "vasara"."authorization" drop constraint "authorization_pkey";
alter table "vasara"."authorization"
    add constraint "authorization_pkey"
    primary key ("principal", "can_insert", "can_select", "can_update", "can_delete", "resource", "specifier");
