{ config, lib, pkgs, ... }:

{
  imports = [
     ./modules/mailpit.nix
  ];
  options = {
    options.vasara.caddy.port = lib.mkOption { default = 8080; };
    options.vasara.keycloak.port = lib.mkOption { default = 8000; };
    options.vasara.keycloak.realm_export =
      lib.mkOption { default = ../packages/keycloak/realm-export.json; };
    options.vasara.keycloak.admin_password =
      lib.mkOption { default = "admin"; };
    options.vasara.keycloak.db_password =
      lib.mkOption { default = "keycloak"; };
    options.vasara.redis.port = lib.mkOption { default = 6379; };
    options.vasara.redis.secret = lib.mkOption { default = "redis-secret"; };
    options.vasara.vault.secret = lib.mkOption { default = "vault-secret"; };
    options.vasara.camunda.port = lib.mkOption { default = 8081; };
    options.vasara.camunda.jdwp_port = lib.mkOption { default = 5005; };
    options.vasara.camunda.database = lib.mkOption { default = "camunda"; };
    options.vasara.camunda.db_username = lib.mkOption { default = "camunda"; };
    options.vasara.camunda.db_password = lib.mkOption { default = "camunda"; };
    options.vasara.camunda.oidc_issuer_uri = lib.mkOption {
      default = "http://localhost:${
          builtins.toString config.options.vasara.caddy.port
        }/auth/realms/vasara";
    };
    options.vasara.camunda.admin_user =
      lib.mkOption { default = "camunda-admin"; };
    options.vasara.camunda.transient_user_private_key = lib.mkOption {
      default = "/etc/vasara/transient-users-ec-prime256v1-priv-key.pem";
    };
    options.vasara.camunda.transient_user_public_key = lib.mkOption {
      default = "/etc/vasara/transient-users-ec-prime256v1-pub-key.pem";
    };
    options.vasara.camunda.scim_base_url = lib.mkOption {
      default = "http://localhost:${
          builtins.toString config.options.vasara.caddy.port
        }/engine-rest/scim";
    };
    options.vasara.camunda.oidc_client_id =
      lib.mkOption { default = "vasara-camunda"; };
    options.vasara.camunda.oidc_client_secret =
      lib.mkOption { default = "323054f7-bdd5-4b68-bd48-26f9eb25eb76"; };
    options.vasara.camunda.oidc_logout_uri = lib.mkOption {
      default = "http://localhost:${
          builtins.toString config.options.vasara.caddy.port
        }/auth/realms/vasara/protocol/openid-connect/logout";
    };
    options.vasara.camunda.scim_secret =
      lib.mkOption { default = "vasara-camunda-scim-secret"; };
    options.vasara.camunda.rest_secret =
      lib.mkOption { default = "vasara-camunda-rest-secret"; };
    options.vasara.camunda.graphql_secret =
      lib.mkOption { default = "vasara-camunda-graphql-secret"; };
    options.vasara.app.oidc_client_id =
      lib.mkOption { default = "vasara-app"; };
    options.vasara.hasura.port = lib.mkOption { default = 8090; };
    options.vasara.hasura.auth.port = lib.mkOption { default = 8091; };
    options.vasara.hasura.admin_secret =
      lib.mkOption { default = "hasura-admin-secret"; };
    options.vasara.hasura.database = lib.mkOption { default = "hasura"; };
    options.vasara.hasura.db_username = lib.mkOption { default = "hasura"; };
    options.vasara.hasura.db_password = lib.mkOption { default = "hasura"; };
    options.vasara.middleware.port = lib.mkOption { default = 8092; };
    options.vasara.tasklist.port = lib.mkOption { default = 3000; };
    options.vasara.tasklist.root =
      lib.mkOption { default = "/var/www/tasklist"; };
    options.vasara.modeler.port = lib.mkOption { default = 3001; };
    options.vasara.modeler.root =
      lib.mkOption { default = "/var/www/modeler"; };
    options.vasara.pdf-tools.port = lib.mkOption { default = 8093; };
    options.vasara.pdf-tools.client.port = lib.mkOption { default = 8094; };
  };

  config = let cfg = config.options.vasara;
  in {

    services.postgresql.package = pkgs.postgresql_14;

    networking.firewall.allowedTCPPorts = [
      cfg.caddy.port
      cfg.keycloak.port # for developing UI outside VM
      cfg.hasura.port # for developing UI outside VM
      cfg.camunda.jdwp_port # for debugging Camunda outside VM
      config.services.postgresql.port # for development purposes
      9695
      9693 # Hasura migration console ports
    ];

    services.postgresql.enableTCPIP = true;
    services.postgresql.authentication = ''
      host    hasura    hasura             10.0.0.0/16       md5
      host    hasura    hasura             192.168.0.0/16    md5
      host    camunda   camunda-readonly   ::1/128           md5
    '';

    environment.systemPackages = [ pkgs.vasara-carrot-rcc ];

    ###
    ### Frontend Proxy (to gather them all)
    ###

    services.caddy.enable = true;
    services.caddy.virtualHosts.":${
      builtins.toString cfg.caddy.port
    }".extraConfig = ''
      handle_path "/auth*" {
        rewrite * /auth{uri}
        reverse_proxy http://localhost:${builtins.toString cfg.keycloak.port}
      }
      handle_path "/guacamole*" {
        rewrite * /guacamole{uri}
        reverse_proxy http://localhost:${builtins.toString cfg.tomcat.port}
      }
      handle_path "/camunda*" {
        rewrite * /camunda{uri}
        reverse_proxy http://localhost:${builtins.toString cfg.camunda.port} {
            header_up X-Forwarded-Proto "http"
            header_up X-Forwarded-Port "${builtins.toString cfg.caddy.port}"
        }
      }
      handle_path "/login/oauth2*" {
        rewrite * /login/oauth2{uri}
        reverse_proxy http://localhost:${builtins.toString cfg.camunda.port} {
            header_up X-Forwarded-Proto "http"
            header_up X-Forwarded-Port "${builtins.toString cfg.caddy.port}"
        }
      }
      handle_path "/oauth2*" {
        rewrite * /oauth2{uri}
        reverse_proxy http://localhost:${builtins.toString cfg.camunda.port} {
            header_up X-Forwarded-Proto "http"
            header_up X-Forwarded-Port "${builtins.toString cfg.caddy.port}"
        }
      }
      handle_path "/engine-rest*" {
        rewrite * /engine-rest{uri}
        reverse_proxy http://localhost:${builtins.toString cfg.camunda.port} {
            header_up X-Forwarded-Proto "http"
            header_up X-Forwarded-Port "${builtins.toString cfg.caddy.port}"
        }
      }
      handle_path "/v1/graphql*" {
        rewrite * /v1/graphql{uri}
        reverse_proxy http://localhost:${builtins.toString cfg.hasura.port}
      }
      handle_path "/v1/query*" {
        rewrite * /v1/query{uri}
        reverse_proxy http://localhost:${builtins.toString cfg.hasura.port}
      }
      handle_path "/console*" {
        rewrite * /console{uri}
        reverse_proxy http://localhost:${builtins.toString cfg.hasura.port}
      }
      handle_path "/v1/metadata*" {
        rewrite * /v1/metadata{uri}
        reverse_proxy http://localhost:${builtins.toString cfg.hasura.port}
      }
      handle_path "/v1/version*" {
        rewrite * /v1/version{uri}
        reverse_proxy http://localhost:${builtins.toString cfg.hasura.port}
      }
      handle_path "/v2*" {
        rewrite * /v2{uri}
        reverse_proxy http://localhost:${builtins.toString cfg.hasura.port}
      }
      handle_path "/transit*" {
        rewrite * /transit{uri}
        reverse_proxy http://${config.services.vault.address}
      }
      handle_path "/login*" {
        rewrite * /login{uri}
        reverse_proxy http://localhost:${builtins.toString cfg.middleware.port}
      }
      handle_path "/modeler*" {
        reverse_proxy http://localhost:${builtins.toString cfg.modeler.port}
      }
      handle_path "/mailpit*" {
        rewrite * /mailpit{uri}
        reverse_proxy http://localhost:${
          builtins.elemAt (builtins.split ":" config.services.mailpit.instances.default.listen) 2
        }
      }
      handle_path "/*" {
        reverse_proxy http://localhost:${builtins.toString cfg.tasklist.port}
      }
    '';

    ###
    ### Redis (for Camunda sessions)
    ###

    services.redis.servers.default.enable = true;
    services.redis.servers.default.port = cfg.redis.port;
    services.redis.servers.default.requirePass = cfg.redis.secret;

    ###
    ### MailPit (for process emails)
    ###

    services.mailpit.instances.default.listen = "0.0.0.0:8025";

    ###
    ### Keycloak (as IdP)
    ###

    services.keycloak.enable = true;
    services.keycloak.database = {
      passwordFile = builtins.toFile "db_password" cfg.keycloak.db_password;
    };
    services.keycloak.settings.hostname = "localhost";
    services.keycloak.settings.http-port = cfg.keycloak.port;
    services.keycloak.settings.http-relative-path = "/auth";
    services.keycloak.initialAdminPassword = cfg.keycloak.admin_password;
    # Apply to import for default configuration
    systemd.services.keycloak.environment.JAVA_OPTS_APPEND =
      "-Dkeycloak.migration.action=import -Dkeycloak.migration.provider=singleFile -Dkeycloak.migration.file=${cfg.keycloak.realm_export}";
    systemd.services.keycloak.environment.CONFIG_ARGS = "-start-dev #";

    ###
    ### Hashicorp Vault (for encrypting process variables)
    ###

    services.vault.enable = true;
    services.vault.package = pkgs.vault-bin;
    # Fix to run vault with -dev, because this is just a development VM
    systemd.services.vault.serviceConfig.ExecStart =
      pkgs.lib.mkForce "${config.services.vault.package}/bin/vault server -dev";
    # Set test secret
    systemd.services.vault.environment = {
      HOME = "/tmp";
      VAULT_DEV_ROOT_TOKEN_ID = cfg.vault.secret;
    };
    # Configure vault due to transient -dev mode
    systemd.services.vault.path = with pkgs; [ vault netcat ];
    systemd.services.vault.postStart = ''
      export VAULT_ADDR="http://${config.services.vault.address}";
      export VAULT_TOKEN="${cfg.vault.secret}";
      while ! nc -z ${
        builtins.replaceStrings [ ":" ] [ " " ]
        "${config.services.vault.address}"
      }; do
        echo "Waiting for Vault at ${config.services.vault.address}"; sleep 5
      done
      vault secrets enable -path=transit/default transit
      vault write -f transit/default/keys/test1
    '';

    ###
    ### Camunda
    ###

    systemd.services.vasara-camunda-init = {
      after = [ "postgresql.service" ];
      before = [ "vasara-camunda.service" ];
      bindsTo = [ "postgresql.service" ];
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = true;
        User = "postgres";
        Group = "postgres";
      };
      path = [ config.services.postgresql.package ];
      script = ''
        set -o errexit -o pipefail -o nounset -o errtrace
        shopt -s inherit_errexit
        create_role="$(mktemp)"
        trap 'rm -f "$create_role"' ERR EXIT
        echo "CREATE ROLE \"${cfg.camunda.db_username}\" WITH LOGIN PASSWORD '${cfg.camunda.db_password}' CREATEDB" > "$create_role"
        psql -tAc "SELECT 1 FROM pg_roles WHERE rolname='${cfg.camunda.db_username}'" | grep -q 1 || psql -tA --file="$create_role"
        psql -tAc "SELECT 1 FROM pg_database WHERE datname='${cfg.camunda.database}'" | grep -q 1 || psql -tAc 'CREATE DATABASE "${cfg.camunda.database}" OWNER "${cfg.camunda.db_username}"'
        psql -c "CREATE EXTENSION IF NOT EXISTS pgcrypto SCHEMA public" -d ${cfg.camunda.database}
        echo "CREATE ROLE \"${cfg.camunda.db_username}-readonly\" WITH LOGIN PASSWORD '${cfg.camunda.db_password}'" > "$create_role"
        psql -tAc "SELECT 1 FROM pg_roles WHERE rolname='${cfg.camunda.db_username}-readonly'" | grep -q 1 || psql -tA --file="$create_role"
        psql -c "GRANT SELECT ON ALL TABLES IN SCHEMA public TO \"${cfg.camunda.db_username}-readonly\"" -d ${cfg.camunda.database}
        psql -c "GRANT CONNECT ON DATABASE \"${cfg.camunda.database}\" TO \"${cfg.camunda.db_username}-readonly\"" -d ${cfg.camunda.database}
      '';
    };

    systemd.services.vasara-camunda = let
      mail_config = builtins.toFile "mail.config" ''
        mail.transport.protocol=smtp
        mail.smtp.host=localhost
        mail.smtp.port=${builtins.elemAt (builtins.split ":" config.services.mailpit.instances.default.smtp) 2}
        mail.smtp.auth=false
        mail.ssl.enable=false
        mail.sender=noreply@jyu.fi
      '';
    in {
      after = [
        "vasara-transient-users-public-key.service"
        "vasara-camunda-init.service"
        "redis-default.service"
        "keycloak.service"
        "postgresql.service"
      ];
      bindsTo = [
        "vasara-camunda-init.service"
        "redis-default.service"
        "keycloak.service"
        "postgresql.service"
      ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        User = "camunda";
        Group = "camunda";
        DynamicUser = true;
        Restart = "on-failure";
        RestartSec = 10;
      };
      unitConfig = { StartLimitIntervalSec = 0; };
      environment = {
        HTTP_PORT = "${toString cfg.camunda.port}";
        DB_TYPE = "postgres";
        DB_DRIVER = "org.postgresql.Driver";
        DB_URL = "jdbc:postgresql://localhost/${cfg.camunda.database}";
        DB_USERNAME = cfg.camunda.db_username;
        DB_PASSWORD = cfg.camunda.db_password;
        OIC_ISSUER_URI = cfg.camunda.oidc_issuer_uri;
        CAMUNDA_ADMIN_USER = cfg.camunda.admin_user;
        VASARA_TENANTS_OIDC =
          ""; # bad naming; default host in principal_username ("@jyu.fi") when missing
        SCIM_BASE_URL = cfg.camunda.scim_base_url;
        LOG_LEVEL_VASARA = "DEBUG";
        LOG_LEVEL_WEB = "DEBUG";
        OIC_CLIENT_ID = cfg.camunda.oidc_client_id;
        OIC_CLIENT_SECRET = cfg.camunda.oidc_client_secret;
        OIC_LOGOUT_URI = cfg.camunda.oidc_logout_uri;
        VASARA_GRAPHQL_SECRET = cfg.camunda.graphql_secret;
        VASARA_SCIM_SECRET = cfg.camunda.scim_secret;
        VASARA_REST_SECRET = cfg.camunda.rest_secret;
        REDIS_PASSWORD = cfg.redis.secret;
        REDIS_HOST = "localhost";
        REDIS_PORT = "${toString config.services.redis.servers.default.port}";
        COOKIE_SECURE = "false";
        MAX_HTTP_HEADER_SIZE =
          "10240KB"; # hasura webhook auth payload may be huge until refactored...
        MAIL_HOST = "localhost";
        MAIL_PORT = (builtins.elemAt (builtins.split ":" config.services.mailpit.instances.default.smtp) 2);
        MAIL_AUTH = "false";
        MAIL_SSL = "false";
        MAIL_SENDER = "noreply@jyu.fi";
        MAIL_CONFIG = mail_config;
      };
      path = [ pkgs.vasara-camunda pkgs.curl ];
      preStart = with pkgs; ''
        while true; do
          response=$(curl -s -o /dev/null -w "%{http_code}" "${cfg.camunda.oidc_issuer_uri}/.well-known/openid-configuration")
          if [ "$response" == "200" ]; then
              echo "IDP is responding with status code 200. Exiting loop."
              break
          else
              echo "Endpoint is not yet responding (Status code: $response). Retrying in 5 seconds..."
              sleep 5
          fi
        done
      '';
      script = ''
        # Read transient user public key
        transient_user_public_key=$(cat ${cfg.camunda.transient_user_public_key})
        export VASARA_TRANSIENT_USER_PUBLIC_KEY=$transient_user_public_key

        exec vasara-camunda -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:${
          toString cfg.camunda.jdwp_port
        };
      '';
    };

    ###
    ### Hasura
    ###

    systemd.services.vasara-hasura-auth = {
      after = [ "network-online.target" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        User = "hasura";
        Group = "hasura";
        DynamicUser = true;
        Restart = "on-failure";
        RestartSec = 30;
      };
      unitConfig = { StartLimitIntervalSec = 0; };
      environment = {
        AIOHTTP_PORT = "${toString cfg.hasura.auth.port}";
        CAMUNDA_GRAPHQL_ENDPOINT =
          "http://localhost:${builtins.toString cfg.camunda.port}/graphql";
        CAMUNDA_GRAPHQL_AUTHORIZATION = "Bearer ${cfg.camunda.graphql_secret}";
        ADMIN_GROUP = "${cfg.camunda.admin_user}";
        LOG_LEVEL = "DEBUG";
      };
      path = [ pkgs.vasara-hasura-auth ];
      script = ''
        exec vasara-hasura-auth
      '';
    };

    systemd.services.vasara-hasura-init = {
      after = [ "postgresql.service" ];
      before = [ "vasara-hasura.service" ];
      bindsTo = [ "postgresql.service" ];
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = true;
        User = "postgres";
        Group = "postgres";
      };
      unitConfig = { StartLimitIntervalSec = 0; };
      path = [ config.services.postgresql.package ];
      script = ''
        set -o errexit -o pipefail -o nounset -o errtrace
        shopt -s inherit_errexit
        create_role="$(mktemp)"
        trap 'rm -f "$create_role"' ERR EXIT
        echo "CREATE ROLE \"${cfg.hasura.db_username}\" WITH LOGIN PASSWORD '${cfg.hasura.db_password}' CREATEDB" > "$create_role"
        psql -tAc "SELECT 1 FROM pg_roles WHERE rolname='${cfg.hasura.db_username}'" | grep -q 1 || psql -tA --file="$create_role"
        psql -tAc "SELECT 1 FROM pg_database WHERE datname='${cfg.hasura.database}'" | grep -q 1 || psql -tAc 'CREATE DATABASE "${cfg.hasura.database}" OWNER "${cfg.hasura.db_username}"'
        psql -c "CREATE EXTENSION IF NOT EXISTS pgcrypto SCHEMA public" -d ${cfg.hasura.database}
        psql -c "CREATE EXTENSION IF NOT EXISTS postgres_fdw SCHEMA public" -d ${cfg.hasura.database}
        psql -c "CREATE SERVER IF NOT EXISTS camunda FOREIGN DATA WRAPPER postgres_fdw OPTIONS (host 'localhost', port '${
          builtins.toString config.services.postgresql.settings.port
        }', dbname '${cfg.camunda.database}')" -d ${cfg.hasura.database}
        psql -c "CREATE USER MAPPING IF NOT EXISTS FOR \"${cfg.hasura.database}\" SERVER camunda OPTIONS (user '${cfg.camunda.db_username}-readonly', password '${cfg.camunda.db_password}')" -d ${cfg.hasura.database}
        psql -c "ALTER USER MAPPING FOR \"${cfg.hasura.db_username}\" SERVER \"${cfg.camunda.database}\" OPTIONS (SET user '${cfg.camunda.db_username}-readonly', SET password '${cfg.camunda.db_password}')" -d ${cfg.hasura.database}
        psql -c "GRANT USAGE ON FOREIGN SERVER camunda TO \"${cfg.hasura.db_username}\"" -d ${cfg.hasura.database}
      '';
    };

    systemd.services.vasara-hasura = let
      migrations = ../fixture/migrations;
      metadata = ../fixture/metadata;
      hasura-cli_config_yaml = builtins.toFile "config.yaml" ''
        version: 2
        endpoint: http://localhost:${builtins.toString cfg.hasura.port}
        metadata_directory: metadata
        migrations_directory: migrations
      '';
      hasura-cli_home = pkgs.stdenv.mkDerivation {
        name = "hasura-cli-home";
        phases = [ "installPhase" ];
        installPhase = ''
          mkdir -p $out/seeds
          ln -s ${metadata} $out/metadata
          ln -s ${migrations} $out/migrations
          ln -s ${pkgs.hasura-cli-dot-hasura} $out/.hasura
          cp -a ${hasura-cli_config_yaml} $out/config.yaml
        '';
      };
    in {
      after = [
        "vasara-hasura-init.service"
        "vasara-hasura-auth.service"
        "postgresql.service"
      ];
      bindsTo = [
        "vasara-hasura-init.service"
        "vasara-hasura-auth.service"
        "postgresql.service"
      ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        DynamicUser = true;
        User = "hasura";
        Group = "hasura";
        StateDirectory = "hasura";
        Restart = "on-failure";
        RestartSec = 15;
      };
      unitConfig = { StartLimitIntervalSec = 0; };
      environment = {
        HASURA_GRAPHQL_DATABASE_URL =
          "postgres://${cfg.hasura.db_username}:${cfg.hasura.db_password}@localhost:${
            builtins.toString config.services.postgresql.settings.port
          }/${cfg.hasura.database}";
        HASURA_GRAPHQL_ADMIN_SECRET = "${cfg.hasura.admin_secret}";
        HASURA_GRAPHQL_SERVER_PORT = "${builtins.toString cfg.hasura.port}";
        HASURA_GRAPHQL_AUTH_HOOK =
          "http://localhost:${builtins.toString cfg.hasura.auth.port}";
        HASURA_GRAPHQL_ENABLE_CONSOLE = "true";
        HASURA_GRAPHQL_CONSOLE_ASSETS_DIR = "${pkgs.hasura-console-assets}";
        HASURA_GRAPHQL_CORS_DOMAIN = "*";
        HASURA_GRAPHQL_DEV_MODE = "true";
        HASURA_GRAPHQL_ENABLE_TELEMETRY = "false";
        CAMUNDA_GRAPHQL_ENDPOINT =
          "http://localhost:${builtins.toString cfg.camunda.port}/graphql";
        CAMUNDA_REST_ENDPOINT =
          "http://localhost:${builtins.toString cfg.camunda.port}/engine-rest";
        VASARA_GRAPHQL_SECRET =
          "${builtins.toString cfg.camunda.graphql_secret}";
        VASARA_MIDDLEWARE_ADDR =
          "http://localhost:${builtins.toString cfg.middleware.port}";
      };
      path = [
        pkgs.netcat
        pkgs.curl
        pkgs.hasura-cli-full
        pkgs.hasura-graphql-engine
        config.services.postgresql.package
      ];
      script = ''
        exec graphql-engine serve
      '';
      preStart = with pkgs; ''
        while true; do
            response=$(curl -sS -X POST -H "Authorization: Bearer $VASARA_GRAPHQL_SECRET" -H "Content-Type: application/json" -d '{"query":"{__schema{queryType{fields{name}}}}"}' "$CAMUNDA_GRAPHQL_ENDPOINT")

            if [ $? -eq 0 ]; then
                echo "curl command returned successfully. Exiting loop."
                break
            else
                echo "curl command failed. Retrying in 5 seconds..."
                sleep 5
            fi
        done
      '';
      postStart = with pkgs; ''
        while ! nc -z localhost ${builtins.toString cfg.hasura.port}; do
          echo "Waiting for Hasura at localhost:${
            builtins.toString cfg.hasura.port
          }"; sleep 5
        done
        if [ ! -f $STATE_DIRECTORY/.hasura/config.json ]; then
          mkdir -p $STATE_DIRECTORY/.hasura
          cp -a ${hasura-cli_home}/.hasura/config.json $STATE_DIRECTORY/.hasura
        fi
        cd ${hasura-cli_home}
        if [ ! -f $STATE_DIRECTORY/vasara-hasura-initialized ]; then
          export HOME=$STATE_DIRECTORY
          hasura migrate apply --skip-update-check
          hasura metadata apply --skip-update-check
          touch $STATE_DIRECTORY/vasara-hasura-initialized
        fi
      '';
    };

    ###
    ### Middleware
    ###

    systemd.services.vasara-transient-users-keys = {
      after = [ "network-online.target" ];
      wantedBy = [ "multi-user.target" ];
      path = [ pkgs.openssl ];
      serviceConfig = { Type = "oneshot"; };
      script = ''
        if [ ! -f ${cfg.camunda.transient_user_public_key} ]; then
          mkdir -p $(dirname ${cfg.camunda.transient_user_private_key}) $(dirname ${cfg.camunda.transient_user_public_key})
          openssl ecparam -name prime256v1 -genkey -noout -out ${cfg.camunda.transient_user_private_key}
          openssl ec -in ${cfg.camunda.transient_user_private_key} -pubout > ${cfg.camunda.transient_user_public_key}
        fi
        chmod 444 ${cfg.camunda.transient_user_private_key}  # TODO
        chmod 444 ${cfg.camunda.transient_user_public_key}
      '';
    };

    systemd.services.vasara-middleware = {
      after = [ "vasara-transient-users-keys.service" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        DynamicUser = true;
        User = "vasara";
        Group = "vasara";
        StateDirectory = "vasara";
        Restart = "on-failure";
        RestartSec = 15;
      };
      unitConfig = { StartLimitIntervalSec = 0; };
      environment = {
        UVICORN_HOST = "localhost";
        UVICORN_PORT = "${builtins.toString cfg.middleware.port}";
        VASARA_GRAPHQL_ENDPOINT =
          "http://localhost:${builtins.toString cfg.hasura.port}/v1/graphql";
        VASARA_GRAPHQL_ADMIN_SECRET = "${cfg.hasura.admin_secret}";
        CAMUNDA_GRAPHQL_ENDPOINT =
          "http://localhost:${builtins.toString cfg.camunda.port}/graphql";
        CAMUNDA_GRAPHQL_AUTHORIZATION = "Bearer ${cfg.camunda.graphql_secret}";
        CAMUNDA_REST_AUTHORIZATION = "Bearer ${cfg.camunda.rest_secret}";
        CAMUNDA_REST_ENDPOINT =
          "http://localhost:${builtins.toString cfg.camunda.port}/engine-rest";
        VAULT_ADDR = "http://${config.services.vault.address}";
        VAULT_TOKEN = "${cfg.vault.secret}";
        VAULT_TRANSIT_DEFAULT_PATH = "transit/default";
        VAULT_TRANSIT_DEFAULT_KEY = "test1";
        VAULT_TRANSIT_PII_SENSITIVE_PATH = "transit/default";
        VAULT_TRANSIT_PII_SENSITIVE_KEY = "test1";
        VAULT_TRANSIT_PII_RESTRICTED_PATH = "transit/default";
        VAULT_TRANSIT_PII_RESTRICTED_KEY = "test1";
        VAULT_TRANSIT_DATA_RESTRICTED_PATH = "transit/default";
        VAULT_TRANSIT_DATA_RESTRICTED_KEY = "test1";
        VASARA_TRANSIENT_USER_PRIVATE_KEY =
          cfg.camunda.transient_user_private_key;
        SMTP_HOST = "localhost";
        SMTP_PORT = (builtins.elemAt (builtins.split ":" config.services.mailpit.instances.default.smtp) 2);
        SMTP_KEEP_BCC = "True";
        VASARA_PURGE_RUNTIME_ENABLED = "True";
        # NOTE: Vagrant is missing MinIO!
        S3_BUCKET_NAME = "vasara";
        S3_ENDPOINT_URL = "http://localhost:9000";
        S3_ACCESS_KEY = "s3key";
        S3_SECRET_KEY = "s3secret";
        # NOTE: Vasara SSO is in development...
        OIDC_PROVIDER_URI = cfg.camunda.oidc_issuer_uri;
        OIDC_CLIENT_ID = cfg.app.oidc_client_id;
        OIDC_CLIENT_SECRET = cfg.camunda.oidc_client_secret;
        OIDC_REDIRECT_URI = "http://localhost:${
            builtins.toString cfg.caddy.port
          }/login/oidc_redirect_uri";
        OIDC_USERNAME_CLAIM = "preferred_username";
      };
      path = [ pkgs.vasara-middleware pkgs.netcat pkgs.openssl ];
      script = ''
        exec vasara-middleware
      '';
    };

    systemd.services.vasara-transient-users-keys-lock = {
      after = [ "vasara-middleware.service" ];
      wantedBy = [ "multi-user.target" ];
      path = [ pkgs.openssl ];
      serviceConfig = { Type = "oneshot"; };
      script = ''
        if [ -f ${cfg.camunda.transient_user_private_key} ]; then
          chown vasara:vasara ${cfg.camunda.transient_user_private_key}
          chmod 400 ${cfg.camunda.transient_user_private_key}
        fi
      '';
    };

    ###
    ### App
    ###

    systemd.services.nginx-vasara = let
      tasklist = import ../packages/vasara-app {
        inherit pkgs;
        REACT_APP_BUILD_TARGET = "tasklist";
      };
      modeler = import ../packages/vasara-app {
        inherit pkgs;
        REACT_APP_BUILD_TARGET = "modeler";
      };
    in {
      wantedBy = [ "multi-user.target" ];
      after = [ "vasara-transient-users-keys.service" ];
      before = [ "nginx.service" ];
      serviceConfig = { Type = "oneshot"; };
      environment = {
        REACT_APP_GRAPHQL_API_URL = "/v1/graphql";
        REACT_APP_CLIENT_ID = "vasara-app";
        REACT_APP_AUTHORITY = "/auth/realms/vasara";
        REACT_APP_DEFAULT_TENANT = "";
        REACT_APP_LOGOUT_URI =
          "/auth/realms/vasara/protocol/openid-connect/logout";
      };
      script = ''
        # Read transient user public key in JavaScript compatible string format
        transient_user_public_key=$(cat ${cfg.camunda.transient_user_public_key} | sed -z "s|\n|\\\\\\\\n|g")

        rm -rf ${cfg.tasklist.root}
        mkdir -p ${cfg.tasklist.root}
        cd ${cfg.tasklist.root}
        REACT_APP_TRANSIENT_USER_PUBLIC_KEY=$transient_user_public_key \
        REACT_APP_REDIRECT_URI="/" \
        REACT_APP_PUBLIC_URL="/" \
        ${tasklist}/bin/init

        rm -rf ${cfg.modeler.root}
        mkdir -p ${cfg.modeler.root}
        cd ${cfg.modeler.root}
        REACT_APP_TRANSIENT_USER_PUBLIC_KEY=$transient_user_public_key \
        REACT_APP_REDIRECT_URI="/modeler/" \
        REACT_APP_PUBLIC_URL="/modeler/" \
        ${modeler}/bin/init
      '';
    };

    services.nginx = {
      enable = true;
      config = ''
        worker_processes 4;

        events {
          use epoll;
          worker_connections 128;
        }

        http {
          include ${pkgs.nginx}/conf/mime.types;
          default_type application/octet-stream;
          sendfile on;
          server {
            listen ${builtins.toString cfg.tasklist.port};
            server_name localhost;
            gzip on;
            gzip_types text/css application/javascript;
            gzip_min_length 1000;
            root /var/www/tasklist/var/www;
            location / {
              index index.html;
              # kill cache
              add_header Last-Modified $date_gmt;
              add_header Cache-Control 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0';
              if_modified_since off;
              expires off;
              etag off;
              # CSP
              add_header X-Frame-Options "deny" always;
              add_header X-XSS-Protection "mode=block" always;
              add_header X-Content-Type-Options "nosniff" always;
              add_header Referrer-Policy "same-origin" always;
              add_header Content-Security-Policy "default-src 'self'; img-src 'self' https://*.tile.openstreetmap.org; style-src 'self' 'unsafe-inline'; object-src 'none'" always;
              add_header Feature-Policy "accelerometer 'none'; ambient-light-sensor 'none'; autoplay 'none'; battery 'none'; camera 'none'; display-capture 'none'; document-domain 'none'; encrypted-media 'none'; execution-while-not-rendered 'none'; execution-while-out-of-viewport 'none'; fullscreen 'none'; geolocation 'self'; gyroscope 'none'; layout-animations 'none'; legacy-image-formats 'none'; magnetometer 'none'; microphone 'none'; midi 'none'; navigation-override 'none'; oversized-images 'none'; payment 'none'; picture-in-picture 'none'; publickey-credentials 'none'; sync-xhr 'none'; usb 'none'; vr 'none'; wake-lock 'none'; xr-spatial-tracking 'none'" always;
              #
              try_files $uri /index.html =440;
            }
          }
          server {
            listen ${builtins.toString cfg.modeler.port};
            server_name localhost;
            gzip on;
            gzip_types text/css application/javascript;
            gzip_min_length 1000;
            root /var/www/modeler/var/www;
            location /modeler/ {
              index index.html;
              # kill cache
              add_header Last-Modified $date_gmt;
              add_header Cache-Control 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0';
              if_modified_since off;
              expires off;
              etag off;
              # CSP
              add_header X-Frame-Options "deny" always;
              add_header X-XSS-Protection "mode=block" always;
              add_header X-Content-Type-Options "nosniff" always;
              add_header Referrer-Policy "same-origin" always;
              add_header Content-Security-Policy "default-src 'self'; img-src 'self' https://*.tile.openstreetmap.org; style-src 'self' 'unsafe-inline'; object-src 'none'" always;
              add_header Feature-Policy "accelerometer 'none'; ambient-light-sensor 'none'; autoplay 'none'; battery 'none'; camera 'none'; display-capture 'none'; document-domain 'none'; encrypted-media 'none'; execution-while-not-rendered 'none'; execution-while-out-of-viewport 'none'; fullscreen 'none'; geolocation 'self'; gyroscope 'none'; layout-animations 'none'; legacy-image-formats 'none'; magnetometer 'none'; microphone 'none'; midi 'none'; navigation-override 'none'; oversized-images 'none'; payment 'none'; picture-in-picture 'none'; publickey-credentials 'none'; sync-xhr 'none'; usb 'none'; vr 'none'; wake-lock 'none'; xr-spatial-tracking 'none'" always;
              #
              try_files $uri /index.html =440;
            }
          }
        }
      '';
    };

    ###
    ### RCC
    ###

    systemd.services.rcc-init = {
      after = [ "network-online.target" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = { Type = "oneshot"; };
      script = ''
        mkdir -p /var/lib/rcc /opt/robocorp
        chown -R ${cfg.username}:users /var/lib/rcc /opt/robocorp
      '';
    };

    ###
    ### PDF
    ###

    systemd.services.pdf-init = {
      after = [ "network-online.target" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = { Type = "oneshot"; };
      script = ''
        mkdir -p /var/lib/pdf
        chown -R ${cfg.username}:users /var/lib/pdf
      '';
    };

    systemd.services.vasara-pdf-tools-api = {
      after = [ "network-online.target" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        DynamicUser = true;
        User = "pdf-tools";
        Group = "pdf-tools";
        Restart = "on-failure";
        RestartSec = 30;
      };
      unitConfig = { StartLimitIntervalSec = 0; };
      environment = {
        SHELL = "/bin/sh";
        CONVERTER = pkgs.vasara-pdf-tools-converter;
        ICC_PROFILE = pkgs.vasara-pdf-tools-icc-profile;
        FORM_FONT = "${pkgs.dejavu_fonts}/share/fonts/truetype/DejaVuSans.ttf";
        HTTP_PORT = "${builtins.toString cfg.pdf-tools.port}";
        FONTMAP_GS_PATH =
          "${pkgs.ghostscript}/share/ghostscript/${pkgs.ghostscript.version}/Resource/Init/Fontmap.GS";
        GS_FONTPATH =
          "${pkgs.carlito}:${pkgs.dejavu_fonts}:${pkgs.freefont_ttf}:${pkgs.liberation_ttf_v2}:${pkgs.noto-fonts}";
        JAR = pkgs.vasara-pdf-tools-jar;
      };
      path = [
        pkgs.temurin-bin-17
        pkgs.gawk
        pkgs.ghostscript
        pkgs.glibc.bin
        pkgs.gnugrep
        pkgs.gnused
      ];
      script = ''
        exec java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5006 -jar $JAR
      '';
    };

    fonts.packages = [
      pkgs.carlito
      pkgs.dejavu_fonts
      pkgs.freefont_ttf
      pkgs.liberation_ttf_v2
      pkgs.noto-fonts
    ];

    systemd.services.vasara-pdf-tools-client = {
      after = [ "network-online.target" ];
      wantedBy = [ "multi-user.target" ];
      serviceConfig = {
        User = "vagrant";
        Group = "users";
        StateDirectory = "pdf";
        Restart = "on-failure";
        RestartSec = 30;
      };
      unitConfig = { StartLimitIntervalSec = 0; };
      environment = {
        MONITOR_PORT = "${builtins.toString cfg.pdf-tools.client.port}";
        PDF_TOOLS_API_URL =
          "http://localhost:${builtins.toString cfg.pdf-tools.port}/api/v1";
        BPM_ENGINE_URL =
          "http://localhost:${builtins.toString cfg.camunda.port}/engine-rest";
        AUTH_TOKEN = cfg.camunda.rest_secret;
      };
      path = [ pkgs.vasara-pdf-tools-client ];
      script = ''
        rm -f $STATE_DIRECTORY/pdf
        cp ${./files/Blank.pdf} $STATE_DIRECTORY/Blank.pdf
        chmod u+w $STATE_DIRECTORY/Blank.pdf
        TEMPLATE_DIR=$STATE_DIRECTORY app
      '';
    };
  };
}
