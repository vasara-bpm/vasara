{ config, pkgs, lib, ... }:

let

  inherit (config.boot.kernelPackages) virtualboxGuestAdditions;

  path = lib.strings.makeBinPath [
    pkgs.gnugrep
    pkgs.xorg.xorgserver.out
    pkgs.which
  ];

in {
  services.xserver.videoDrivers =
    [ "virtualbox" "vmware" "cirrus" "vesa" "modesetting" ];

  users.extraUsers.vagrant.extraGroups = [ "vboxsf" ];

  virtualisation.virtualbox.guest.enable = true;
  virtualisation.virtualbox.guest.x11 = true;

  services.xserver.displayManager.sessionCommands = ''
    PATH=${path}:$PATH  ${
      lib.meta.getExe' virtualboxGuestAdditions "VBoxClient"
    } --vmsvga
  '';
}
