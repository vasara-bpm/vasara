{ config, lib, pkgs, ... }:

{
  options = {
    options.vasara.tomcat.port = lib.mkOption { default = 9080; };
    options.vasara.username = lib.mkOption { default = "vagrant"; };
    options.vasara.allowUnfree = lib.mkOption { default = true; };
  };

  config = let cfg = config.options.vasara;
  in {
    system.stateVersion = "23.11";

    console.font = "Lat2-Terminus16";
    console.keyMap = "fi";

    i18n.defaultLocale = "fi_FI.UTF-8";
    time.timeZone = "Europe/Helsinki";

    services.xrdp.enable = true;
    services.xrdp.defaultWindowManager = ''
      pkill xfce4-session
      source \$HOME/.config/xfce4/xinitrc
      xfce4-session
    '';
    services.guacamole-server.enable = true;
    # https://guacamole.apache.org/doc/0.9.3/gug/configuring-guacamole.html
    services.guacamole-server.userMappingXml =
      builtins.toFile "user-mapping.xml" ''
        <?xml version="1.0" encoding="UTF-8"?>
        <user-mapping>
             <authorize
                    username="${cfg.username}"
                    password="${builtins.hashString "md5" cfg.username}"
                    encoding="md5">
                <connection name="localhost">
                    <protocol>rdp</protocol>
                    <param name="hostname">localhost</param>
                    <param name="port">${
                      builtins.toString config.services.xrdp.port
                    }</param>
                    <param name="username">${cfg.username}</param>
                    <param name="password">${cfg.username}</param>
                    <param name="ignore-cert">true</param>
                    <param name="server-layout">sv-se-qwerty</param>
                </connection>
            </authorize>
        </user-mapping>
      '';

    services.guacamole-client.enable = true;
    services.tomcat.package = pkgs.tomcat9.overrideDerivation (old: {
      postBuild = ''
        substituteInPlace conf/server.xml --replace "8080" "${
          builtins.toString cfg.tomcat.port
        }"
      '';
    });

    networking.firewall.allowedTCPPorts = [ 8080 ];

    services.xserver.enable = true;
    services.xserver.layout = "fi";
    services.xserver.xkbOptions = "eurosign:e";
    services.xserver.libinput.enable = true;
    services.xserver.desktopManager.xterm.enable = false;
    services.xserver.desktopManager.xfce.enable = true;
    services.xserver.desktopManager.xfce.enableScreensaver = true;
    services.xserver.displayManager.defaultSession = "xfce";
    services.xserver.displayManager.autoLogin.enable = true;
    services.xserver.displayManager.autoLogin.user = cfg.username;

    programs.thunar.plugins = [ pkgs.xfce.thunar-archive-plugin ];

    environment.systemPackages = [
      pkgs.camunda-modeler
      pkgs.chromium
      pkgs.git
      pkgs.gnumake
      pkgs.htop
      pkgs.jq
      pkgs.mockoon
      pkgs.mockoon-cli
      pkgs.rcc-fhs
      pkgs.robocorp-code-fhs
      pkgs.unzip
      pkgs.vim
      pkgs.xfce.xfdesktop
    ];

    nixpkgs.config.allowUnfree = cfg.allowUnfree;

    nix.extraOptions = ''
      auto-optimise-store = false
      builders-use-substitutes = true
      keep-outputs = true
      keep-derivations = true
      min-free = ${toString (100 * 1024 * 1024)}
      max-free = ${toString (1024 * 1024 * 1024)}
      experimental-features = nix-command flakes
      accept-flake-config = true
    '';

    nix.settings.substituters =
      [ "https://vasara-bpm.cachix.org" "https://datakurre.cachix.org" ];
    nix.settings.trusted-public-keys = [
      "vasara-bpm.cachix.org-1:T18iQZQvYDy/6VdGmttnkkq7rYi3JP0S1RjjdnXNu/4="
      "datakurre.cachix.org-1:ayZJTy5BDd8K4PW9uc9LHV+WCsdi/fu1ETIYZMooK78="
    ];
    nix.settings.trusted-users = [ cfg.username ];

    users.extraUsers.vagrant.extraGroups = [ cfg.username ];
    users.groups.vagrant.gid = 1000;
    users.groups.vagrant.members = [ cfg.username ];

    home-manager.users."${cfg.username}" = { lib, ... }: {
      home.stateVersion = "23.11";
      home.file.".config/playground/camunda-modeler.png".source =
        ./files/camunda-modeler.png;
      home.file.".config/playground/camunda-modeler.desktop".source =
        pkgs.writeText "camunda-modeler.desktop" ''
          [Desktop Entry]
          StartupWMClass=camunda-modeler
          Version=1.0
          Name=Modeler
          Exec=camunda-modeler
          StartupNotify=true
          Terminal=false
          Icon=/home/${cfg.username}/.config/playground/camunda-modeler.png
          Type=Application
          MimeType=application/bpmn+xml;application/dmn+xml
        '';
      home.file.".config/playground/chromium-browser.desktop".source =
        pkgs.writeText "chromium-browser.desktop" ''
          [Desktop Entry]
          StartupWMClass=chromium-browser
          Version=1.0
          Name=Chromium
          Exec=chromium %U
          StartupNotify=true
          Terminal=false
          Icon=chromium
          Type=Application
          Categories=Network;WebBrowser;
          MimeType=application/pdf;application/rdf+xml;application/rss+xml;application/xhtml+xml;application/xhtml_xml;application/xml;image/gif;image/jpeg;image/png;image/webp;text/plain;text/html;text/xml;x-scheme-handler/ftp;x-scheme-handler/http;x-scheme-handler/https;x-scheme-handler/webcal;x-scheme-handler/mailto;x-scheme-handler/about;x-scheme-handler/unknown
        '';
      home.file.".config/playground/rcc.desktop".source =
        pkgs.writeText "rcc.desktop" ''
          [Desktop Entry]
          Version=1.0
          Type=Application
          Name=RCC
          Comment=
          Exec=env CAMUNDA_API_BASE_URL=http://localhost:${
            builtins.toString config.options.vasara.camunda.port
          }/engine-rest CAMUNDA_API_AUTHORIZATION="Bearer ${config.options.vasara.camunda.rest_secret}" SMTP_HOST=localhost SMTP_PORT=${
            builtins.elemAt (builtins.split ":" config.services.mailpit.instances.default.smtp) 2
          } VAULT_ADDR=http://${config.services.vault.address} VAULT_TOKEN=secret bash -c "carrot-rcc $(find . -name '*.zip') --rcc-fixed-spaces --log-level=debug"
          Icon=utilities-terminal
          Path=/var/lib/rcc
          Terminal=true
          StartupNotify=false
        '';
      home.file.".config/playground/keyboard.desktop".source =
        pkgs.writeText "keyboard.desktop" ''
          [Desktop Entry]
          StartupWMClass=xfce4-keyboard-settings
          Version=1.0
          Name=Keyboard
          Exec=/run/current-system/sw/bin/xfce4-keyboard-settings
          StartupNotify=false
          Terminal=false
          Icon=input-keyboard
          Type=Application
        '';
      home.file.".config/playground/mailhog.png".source = ./files/mailhog.png;
      home.file.".config/playground/mailhog.desktop".source =
        pkgs.writeText "mailhog.desktop" ''
          [Desktop Entry]
          Version=1.0
          Type=Link
          Name=MailPit
          Icon=/home/${cfg.username}/.config/playground/mailhog.png
          URL=http://localhost:8025/mailpit/
        '';
      home.file.".config/playground/vasara.png".source = ./files/vasara.png;
      home.file.".config/playground/tasklist.desktop".source =
        pkgs.writeText "tasklist.desktop" ''
          [Desktop Entry]
          Version=1.0
          Type=Link
          Name=Vasara Tasklist
          Icon=/home/${cfg.username}/.config/playground/vasara.png
          URL=http://localhost:8080/
        '';
      home.file.".config/playground/modeler.desktop".source =
        pkgs.writeText "modeler.desktop" ''
          [Desktop Entry]
          Version=1.0
          Type=Link
          Name=Vasara Modeler
          Icon=/home/${cfg.username}/.config/playground/vasara.png
          URL=http://localhost:8080/modeler/
        '';
      home.file.".config/playground/mockoon.png".source = ./files/mockoon.png;
      home.file.".config/playground/mockoon.desktop".source =
        pkgs.writeText "mockoon.desktop" ''
          [Desktop Entry]
          StartupWMClass=mockoon
          Version=1.0
          Name=Mockoon
          Exec=mockoon-3.0.0
          StartupNotify=true
          Terminal=false
          Icon=/home/${cfg.username}/.config/playground/mockoon.png
          Type=Application
        '';
      home.file.".config/playground/robocorp-code.png".source =
        ./files/robocorp-code.png;
      home.file.".config/playground/robocorp-code.desktop".source =
        pkgs.writeText "robocorp-code.desktop" ''
          [Desktop Entry]
          Categories=Utility;TextEditor;Development;IDE;
          Comment=Code Editing. Redefined.
          Exec=codium /var/lib/rcc
          GenericName=Text Editor
          Icon=/home/${cfg.username}/.config/playground/robocorp-code.png
          MimeType=text/plain;inode/directory;
          Name=Code
          StartupNotify=true
          Terminal=false
          Type=Application
          StartupWMClass=Code
          Actions=new-empty-window;
          Keywords=codium
        '';
      home.file.".config/playground/camunda.png".source = ./files/camunda.png;
      home.file.".config/playground/camunda.desktop".source =
        pkgs.writeText "camunda.desktop" ''
          [Desktop Entry]
          Version=1.0
          Type=Link
          Name=Camunda
          Icon=/home/${cfg.username}/.config/playground/camunda.png
          URL=http://localhost:${
            builtins.toString config.options.vasara.caddy.port
          }/camunda/app/cockpit/default/
        '';
      home.file.".config/playground/hasura.png".source = ./files/hasura.png;
      home.file.".config/playground/hasura.desktop".source =
        pkgs.writeText "hasura.desktop" ''
          [Desktop Entry]
          Version=1.0
          Type=Link
          Name=Hasura
          Icon=/home/${cfg.username}/.config/playground/hasura.png
          URL=http://localhost:${
            builtins.toString config.options.vasara.hasura.port
          }/
        '';
      home.file.".config/playground/vault.png".source = ./files/vault.png;
      home.file.".config/playground/vault.desktop".source =
        pkgs.writeText "vault.desktop" ''
          [Desktop Entry]
          Version=1.0
          Type=Link
          Name=Vault
          Icon=/home/${cfg.username}/.config/playground/vault.png
          URL=http://${config.services.vault.address}/
        '';
      home.file.".config/playground/dmn-simulator.desktop".source =
        pkgs.writeText "dmn-simulator.desktop" ''
          [Desktop Entry]
          Version=1.0
          Type=Link
          Name=DMN
          Comment=
          Icon=web-browser
          URL=https://consulting.camunda.com/dmn-simulator/
        '';
      home.file.".config/playground/xfce4-session-logout.desktop".source =
        pkgs.writeText "xfce4-session-logout.desktop" ''
          [Desktop Entry]
          Version=1.0
          Type=Application
          Exec=xfce4-session-logout
          Icon=system-log-out
          StartupNotify=false
          Terminal=false
          Categories=System;X-XFCE;X-Xfce-Toplevel;
          OnlyShowIn=XFCE;
          Name=Log Out
          Name[fi]=Kirjaudu ulos
        '';
      home.file.".config/playground/alasin.png".source = ./files/alasin.png;
      home.file.".config/xfce4/xinitrc".source = pkgs.writeText "xinitrc" ''
        # resolve directory
        if [[ -f ~/.config/user-dirs.dirs ]]; then
          source ~/.config/user-dirs.dirs
        fi
        if [[ -z "$XDG_DESKTOP_DIR" ]]; then
          XDG_DESKTOP_DIR="Työpöytä"
        fi

        # configure desktop icons
        mkdir -p $XDG_DESKTOP_DIR
        cp -L $HOME/.config/playground/*.desktop $XDG_DESKTOP_DIR
        chmod 0755 $XDG_DESKTOP_DIR/*.desktop
        for desktop_file in $XDG_DESKTOP_DIR/*.desktop; do
          if [ -f "$desktop_file" ]; then
            full_path=$(realpath "$desktop_file")
            gio set -t string "$full_path" metadata::xfce-exe-checksum "$(sha256sum "$desktop_file" | awk '{print $1}')"
          fi
        done
        if [[ -d $HOME/Desktop ]]; then
          cp -L $HOME/.config/playground/*.desktop $HOME/Desktop
          chmod 0755 $HOME/Desktop/*.desktop
          for desktop_file in $HOME/Desktop/*.desktop; do
            if [ -f "$desktop_file" ]; then
              full_path=$(realpath "$desktop_file")
              gio set -t string "$full_path" metadata::xfce-exe-checksum "$(sha256sum "$desktop_file" | awk '{print $1}')"
            fi
          done
        fi

        if [[ -d /vagrant ]]; then
          rm -f $XDG_DESKTOP_DIR/Shared
          ln -s /vagrant $XDG_DESKTOP_DIR/Shared
        fi
        rm -f $XDG_DESKTOP_DIR/Robots
        ln -s /var/lib/rcc $XDG_DESKTOP_DIR/Robots
        rm -f $XDG_DESKTOP_DIR/PDFs
        ln -s /var/lib/pdf $XDG_DESKTOP_DIR/PDFs
        if [[ -d $HOME/Desktop ]]; then
          rm -f $HOME/Desktop/Robots
          ln -s /var/lib/rcc $HOME/Desktop/Robots
          rm -f $HOME/Desktop/PDFs
          ln -s /var/lib/pdf $HOME/Desktop/PDFs
          if [ -d /vagrant ]; then
            rm -f $HOME/Desktop/Shared
            ln -s /vagrant $HOME/Desktop/Shared
          fi
        fi

        # configure desktop background
        xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorVirtual1/workspace0/last-image -t string -s $HOME/.config/playground/alasin.png
        if [[ $? -ne 0 ]]; then
          xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorVirtual1/workspace0/last-image -t string -s $HOME/.config/playground/alasin.png --create
        fi
        xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorVirtual1/workspace0/color-style -t int -s 0
        if [[ $? -ne 0 ]]; then
          xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorVirtual1/workspace0/color-style -t int -s 0 --create
        fi
        xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorVirtual1/workspace0/image-style -t int -s 1
        if [[ $? -ne 0 ]]; then
          xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorVirtual1/workspace0/image-style -t int -s 1 --create
        fi
        xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorVirtual1/workspace0/rgba1 -t double -t double -t double -t double -s 0.368627 -s 0.360784 -s 0.392157 -s 1.0
        if [[ $? -ne 0 ]]; then
          xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorVirtual1/workspace0/rgba1 -t double -t double -t double -t double -s 0.368627 -s 0.360784 -s 0.392157 -s 1.0 --create
        fi
        xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorscreen/workspace0/last-image -t string -s $HOME/.config/playground/alasin.png
        if [[ $? -ne 0 ]]; then
          xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorscreen/workspace0/last-image -t string -s $HOME/.config/playground/alasin.png --create
        fi
        xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorscreen/workspace0/color-style -t int -s 0
        if [[ $? -ne 0 ]]; then
          xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorscreen/workspace0/color-style -t int -s 0 --create
        fi
        xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorscreen/workspace0/image-style -t int -s 1
        if [[ $? -ne 0 ]]; then
          xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorscreen/workspace0/image-style -t int -s 1 --create
        fi
        xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorscreen/workspace0/rgba1 -t double -t double -t double -t double -s 0.368627 -s 0.360784 -s 0.392157 -s 1.0
        if [[ $? -ne 0 ]]; then
          xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorscreen/workspace0/rgba1 -t double -t double -t double -t double -s 0.368627 -s 0.360784 -s 0.392157 -s 1.0 --create
        fi
        xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorrdp0/workspace0/last-image -t string -s $HOME/.config/playground/alasin.png
        if [[ $? -ne 0 ]]; then
          xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorrdp0/workspace0/last-image -t string -s $HOME/.config/playground/alasin.png --create
        fi
        xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorrdp0/workspace0/color-style -t int -s 0
        if [[ $? -ne 0 ]]; then
          xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorrdp0/workspace0/color-style -t int -s 0 --create
        fi
        xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorrdp0/workspace0/image-style -t int -s 1
        if [[ $? -ne 0 ]]; then
          xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorrdp0/workspace0/image-style -t int -s 1 --create
        fi
        xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorrdp0/workspace0/rgba1 -t double -t double -t double -t double -s 0.368627 -s 0.360784 -s 0.392157 -s 1.0
        if [[ $? -ne 0 ]]; then
          xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitorrdp0/workspace0/rgba1 -t double -t double -t double -t double -s 0.368627 -s 0.360784 -s 0.392157 -s 1.0 --create
        fi

        # configure desktop user experience
        xfconf-query -c xsettings -p /Net/IconThemeName -t string -s gnome --create             
        xfconf-query -c xfwm4 -p /general/workspace_count -t int -s 1 --create
        setxkbmap -layout fi

        # Check if xfce4-panel is already running
        if ps aux | grep -q "xfce4-panel"; then
            echo "xfce4-panel is already running."
        else
            # Start xfce4-panel
            echo "xfce4-panel started."
        fi
      '';
      xsession = {
        enable = true;
        windowManager.command = ''test -n "$1" && eval "$@"'';
        initExtra = ''
          if [ -f $HOME/.config/xfce4/xinitrc ]; then
            source $HOME/.config/xfce4/xinitrc
          fi
        '';
      };
      home.activation = {
        xfce4Configuration = lib.hm.dag.entryAfter [ "writeBoundary" ] ''
          # Disable RCC telemetry by default
          if [ ! -d "$HOME/.robocorp" ]; then
            ${pkgs.rcc-fhs}/bin/rcc configuration identity -t
          fi

          # Default applications
          if [ ! -f "$HOME/.config/xfce4/helpers.rc" ]; then
            mkdir -p $HOME/.config/xfce4
            cat << EOF > $HOME/.config/xfce4/helpers.rc
          WebBrowser=chromium
          EOF
          fi

          # Initial icons configuration
          # mkdir -p $HOME/.config/xfce4/desktop
          # if [ ! -e "$HOME/.config/xfce4/desktop/icons.screen.latest.rc" ]; then
          #   cat $\{./files/icons.rc} > $HOME/.config/xfce4/desktop/icons.screen0-1902x1035.rc      
          #   cat $\{./files/icons-gce.rc} > $HOME/.config/xfce4/desktop/icons.screen0-1904x1037.rc  
          #   cat $\{./files/icons.rc} > $HOME/.config/xfce4/desktop/icons.screen0-1920x1080.rc      
          #   ln -s $HOME/.config/xfce4/desktop/icons.screen0-1920x1080.rc $HOME/.config/xfce4/desktop/icons.screen.latest.rc
          # fi

          # VSCode defaults
          if [ ! -f "$HOME/.config/VSCodium/User/settings.json" ]; then
            mkdir -p $HOME/.config/VSCodium/User
            cat << EOF > $HOME/.config/VSCodium/User/settings.json
          {
            "editor.minimap.enabled": false,
            "python.experiments.enabled": false,
            "robocorp.verifyLSP": true,
            "robot.codeLens.enabled": true,
            "terminal.integrated.inheritEnv": false
          }
          EOF
          fi
          if [ ! -f "$HOME/.config/Code/User/settings.json" ]; then
            mkdir -p $HOME/.config/Code/User
            cat << EOF > $HOME/.config/Code/User/settings.json
          {
            "editor.minimap.enabled": false,
            "python.experiments.enabled": false,
            "robocorp.verifyLSP": true,
            "robot.codeLens.enabled": true,
            "terminal.integrated.inheritEnv": false
          }
          EOF
          fi

          # ~/.bashrc
          if [ ! -f "$HOME/.bashrc" ]; then
            cat << EOF > $HOME/.bashrc
          alias wrap="echo 'rcc robot wrap'; rcc robot wrap; touch ."
          EOF
          fi

          # Default panel
          if [ ! -f "$HOME/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml" ]; then
            mkdir -p $HOME/.config/xfce4/xfconf/xfce-perchannel-xml
            cat << EOF > $HOME/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml
          <?xml version="1.0" encoding="UTF-8"?>
          <channel name="xfce4-panel" version="1.0">
            <property name="configver" type="int" value="2"/>
            <property name="panels" type="array">
              <value type="int" value="1"/>
              <property name="dark-mode" type="bool" value="true"/>
              <property name="panel-1" type="empty">
                <property name="position" type="string" value="p=8;x=959;y=1064"/>
                <property name="length" type="uint" value="100"/>
                <property name="position-locked" type="bool" value="true"/>
                <property name="icon-size" type="uint" value="16"/>
                <property name="size" type="uint" value="26"/>
                <property name="plugin-ids" type="array">
                  <value type="int" value="1"/>
                  <value type="int" value="2"/>
                </property>
                <property name="disable-struts" type="bool" value="false"/>
                <property name="mode" type="uint" value="0"/>
                <property name="autohide-behavior" type="uint" value="0"/>
              </property>
            </property>
            <property name="plugins" type="empty">
              <property name="plugin-2" type="string" value="tasklist">
                <property name="grouping" type="uint" value="0"/>
                <property name="show-labels" type="bool" value="true"/>
                <property name="flat-buttons" type="bool" value="true"/>
                <property name="show-handle" type="bool" value="false"/>
                <property name="include-all-workspaces" type="bool" value="true"/>
                <property name="include-all-monitors" type="bool" value="true"/>
                <property name="show-only-minimized" type="bool" value="false"/>
              </property>
              <property name="plugin-1" type="string" value="showdesktop"/>
            </property>
          </channel>
          EOF
          fi
        '';
      };
    };
  };
}
